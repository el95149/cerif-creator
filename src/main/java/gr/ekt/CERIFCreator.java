package gr.ekt;

import java.io.File;
import java.io.FileInputStream;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CERIFCreator {

	private static Log log = LogFactory.getLog(CERIFCreator.class);
	
	@Autowired
	private RuntimeService runtimeService;
	
	@Autowired
	private RepositoryService repositoryService;

	public CERIFCreator() {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				"**/applicationContext.xml");
		AutowireCapableBeanFactory autowireCapableBeanFactory = applicationContext
				.getAutowireCapableBeanFactory();
		autowireCapableBeanFactory.autowireBean(this);
	}

	public void executeProcess(String processKey) throws Exception {

		  // Start a process instance
		  runtimeService.startProcessInstanceByKey(processKey);
	}

	public void deployProcessDefinition(String filename) throws Exception {
		File file = new File(filename);
		Deployment deployment = repositoryService.createDeployment().addInputStream(file.getName(), new FileInputStream(filename)).deploy();
		log.debug(deployment);
	}


	public static void main(String[] args) throws Exception {

		if(args.length==0) {
			System.out.println("Invalid number of arguments");
			System.exit(1);
		}
		
		String command = args[0];
		if(command.equals("execute")) {
			if(args.length<2) {
				System.out.println("No target process specified");
				System.exit(1);
			}
			CERIFCreator creator = new CERIFCreator();
			creator.executeProcess(args[1]);
			
		}else if(command.equals("deploy")) {
			if(args.length<2) {
				System.out.println("No target process definition file specified");
				System.exit(1);
			}
			CERIFCreator creator = new CERIFCreator();
			creator.deployProcessDefinition(args[1]);
			System.out.println("Process deployed successfully");
		}else {
			System.out.println("Invalid command, expected one of execute/deploy");
			System.exit(1);
		}

	}

}

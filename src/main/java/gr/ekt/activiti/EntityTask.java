/**
 * 
 */
package gr.ekt.activiti;

import java.io.File;
import java.util.List;

import gr.ekt.cerif.creator.service.ICERIFPopulator;
import gr.ekt.cerif.creator.service.TermResult;
import gr.ekt.cerif.schema.CERIF;
import gr.ekt.cerif.schema.CERIFItem;
import gr.ekt.cerif.schema.CfClass__TypeCfFedId;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author aanagnostopoulos
 *
 */
@Component
public class EntityTask implements JavaDelegate {

	private static Log log = LogFactory.getLog(EntityTask.class);

	@Autowired
	private ICERIFPopulator populator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.activiti.engine.delegate.JavaDelegate#execute(org.activiti.engine
	 * .delegate.DelegateExecution)
	 */
	@Override
	public void execute(DelegateExecution execution) throws Exception {

		String currentActivityName = execution.getCurrentActivityName();
		log.debug("Populating entity data:" + currentActivityName);

		// retrieve CERIF graph
		CERIF cerif = (CERIF) execution
				.getVariable(IActivitiConstants.VARIABLE_CERIF);
		Boolean enableChecks = (Boolean) execution
				.getVariable(IActivitiConstants.VARIABLE_ENABLE_CHECKS);

		// populate graph with CSV entity data
		List<CERIFItem> cerifItems = populator
				.createCerifItemsFromCSVFile(new File(
						(String) execution
								.getVariable(IActivitiConstants.VARIABLE_WORK_DIRECTORY),
						currentActivityName
								+ IActivitiConstants.FILE_EXTENSION_CSV));
		log.info(currentActivityName+",,Rows converted:"+cerifItems.size());
		for (CERIFItem cerifItem : cerifItems) {
			cerif.addCERIFItem(cerifItem);
			if (enableChecks) {
				List<CfClass__TypeCfFedId> existingFederatedIdentifiers = populator
						.getExistingFederatedIdentifiers(cerifItem,
								currentActivityName);
				for (CfClass__TypeCfFedId cfClass__TypeCfFedId : existingFederatedIdentifiers) {
					log.warn(currentActivityName + ",Row:"
							+ cerifItems.indexOf(cerifItem)
							+ ",Existing cfFedId/cfFed:"
							+ cfClass__TypeCfFedId.getCfFedId());
				}
				List<TermResult> ambiguousTerms = populator
						.replaceEntityClassTermsWithUUIDs(cerifItem,
								currentActivityName);
				for (TermResult ambiguousTerm : ambiguousTerms) {
					log.warn(currentActivityName + ",Row:"
							+ cerifItems.indexOf(cerifItem)
							+ ",Ambiguous class:"
							+ ambiguousTerm.getTerm() + ",Scheme:"
							+ ambiguousTerm.getClassSchemeUUID() + ",Results:"
							+ ambiguousTerm.getCount());
				}
			}
		}

		// store updated CERIF graph
		execution.setVariable(IActivitiConstants.VARIABLE_CERIF, cerif);

	}

}

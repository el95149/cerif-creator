/**
 * 
 */
package gr.ekt.activiti;

import java.io.File;

import gr.ekt.cerif.creator.service.ICERIFExporter;
import gr.ekt.cerif.schema.CERIF;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author aanagnostopoulos
 *
 */
@Component
public class ExportXMLTask implements JavaDelegate {

	private static Log log = LogFactory.getLog(ExportXMLTask.class);
	
	@Autowired
	private ICERIFExporter exporter;

	/* (non-Javadoc)
	 * @see org.activiti.engine.delegate.JavaDelegate#execute(org.activiti.engine.delegate.DelegateExecution)
	 */
	@Override
	public void execute(DelegateExecution execution) throws Exception {
		
		CERIF cerif = (CERIF) execution.getVariable(IActivitiConstants.VARIABLE_CERIF);
		Boolean xmlValidation = (Boolean) execution.getVariable(IActivitiConstants.VARIABLE_XML_VALIDATION);
		Boolean exportToFile = (Boolean) execution.getVariable(IActivitiConstants.VARIABLE_EXPORT_TO_FILE);
		Boolean exportToConsole = (Boolean) execution.getVariable(IActivitiConstants.VARIABLE_EXPORT_TO_CONSOLE);
		String workDirectory = (String) execution
				.getVariable(IActivitiConstants.VARIABLE_WORK_DIRECTORY);

		
		
		if(exportToFile) {
			exporter.exportToXMLFile(cerif, new File(workDirectory, IActivitiConstants.FILENAME_CERIF_XML), xmlValidation);
		}
		
		if(exportToConsole) {
			log.info("\n"+exporter.exportToXMLString(cerif, xmlValidation));
		}
	}

}

/**
 * 
 */
package gr.ekt.activiti;

/**
 * @author aanagnostopoulos
 *
 */
public interface IActivitiConstants {

	public static final String VARIABLE_CERIF = "CERIF";

	public static final String VARIABLE_WORK_DIRECTORY = "workDirectory";

	public static final String VARIABLE_SOURCE_DATABASE = "sourceDatabase";

	public static final String MAPPING_FILE = "mapping.xml";

	public static final String FILE_EXTENSION_CSV = ".csv";

	public static final String VARIABLE_XML_VALIDATION = "xmlValidation";

	public static final String VARIABLE_EXPORT_TO_FILE = "exportToFile";
	
	public static final String VARIABLE_EXPORT_TO_CONSOLE = "exportToConsole";
	
	public static final String VARIABLE_ENABLE_CHECKS= "enableChecks";

	public static final String FILENAME_CERIF_XML = "cerif.xml";

}

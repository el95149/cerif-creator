/**
 * 
 */
package gr.ekt.activiti;

import java.io.File;
import java.io.FileInputStream;

import gr.ekt.cerif.schema.CERIF;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dozer.DozerBeanMapper;
import org.exolab.castor.types.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author aanagnostopoulos
 *
 */
@Component
public class InitTask implements JavaDelegate {

	private static Log log = LogFactory.getLog(InitTask.class);

	@Autowired
	private DozerBeanMapper dozerMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.activiti.engine.delegate.JavaDelegate#execute(org.activiti.engine
	 * .delegate.DelegateExecution)
	 */
	@Override
	public void execute(DelegateExecution execution) throws Exception {

		//retrieve global process variables
		String sourceDatabase = (String) execution
				.getVariable(IActivitiConstants.VARIABLE_SOURCE_DATABASE);
		String workDirectory = (String) execution
				.getVariable(IActivitiConstants.VARIABLE_WORK_DIRECTORY);
		log.info(",,Source database:" + sourceDatabase);
		log.info(",,Work directory:" + workDirectory);

		//initialize dozer mappings
		log.info(",,Initializing Dozer Mapping");
		dozerMapper.addMapping(new FileInputStream(new File(workDirectory,
				IActivitiConstants.MAPPING_FILE)));

		//initialize & store CERIF graph
		log.info(",,Initializing CERIF");
		CERIF cerif = new CERIF();
		cerif.setDate(new Date(new java.util.Date()));
		cerif.setSourceDatabase(sourceDatabase);
		execution.setVariable(IActivitiConstants.VARIABLE_CERIF, cerif);
	}

}

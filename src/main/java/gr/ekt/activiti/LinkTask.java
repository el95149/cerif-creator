/**
 * 
 */
package gr.ekt.activiti;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import gr.ekt.cerif.creator.service.ICERIFPopulator;
import gr.ekt.cerif.creator.service.RowResult;
import gr.ekt.cerif.creator.service.TermResult;
import gr.ekt.cerif.schema.CERIF;
import gr.ekt.cerif.schema.CERIFItem;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author aanagnostopoulos
 *
 */
@Component
public class LinkTask implements JavaDelegate {

	private static Log log = LogFactory.getLog(LinkTask.class);

	@Autowired
	private ICERIFPopulator populator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.activiti.engine.delegate.JavaDelegate#execute(org.activiti.engine
	 * .delegate.DelegateExecution)
	 */
	@Override
	public void execute(DelegateExecution execution) throws Exception {

		String currentActivityName = execution.getCurrentActivityName();
		log.debug("Populating link data:"+currentActivityName);
		
		//retrieve CERIF graph
		CERIF cerif = (CERIF) execution
				.getVariable(IActivitiConstants.VARIABLE_CERIF);
		Boolean enableChecks = (Boolean) execution
				.getVariable(IActivitiConstants.VARIABLE_ENABLE_CHECKS);

		//populate graph with CSV entity data 
		List<RowResult> results = new ArrayList<RowResult>();
		cerif = populator
				.populateNestedLinkEntities(
						new File(
								(String) execution
										.getVariable(IActivitiConstants.VARIABLE_WORK_DIRECTORY),
								currentActivityName + IActivitiConstants.FILE_EXTENSION_CSV),
						cerif, results);

		for (RowResult rowResult : results) {
			switch (rowResult.getLevel()) {
			case INFO:
				log.info(currentActivityName+",Row:"+rowResult.getRow()+","+rowResult.getResult());
				break;
			case WARN:
				log.warn(currentActivityName+",Row:"+rowResult.getRow()+","+rowResult.getResult());
				break;
			case ERROR:
				log.error(currentActivityName+",Row:"+rowResult.getRow()+","+rowResult.getResult());
				break;
			default:
				break;
			}
		}
		
		if(enableChecks) {
			CERIFItem[] cerifItems = cerif.getCERIFItem();
			for (CERIFItem cerifItem : cerifItems) {
				List<TermResult> ambiguousTerms = populator.replaceLinkEntityClassTermsWithUUIDs(cerifItem, currentActivityName);
				for (TermResult ambiguousTerm : ambiguousTerms) {
					log.info(currentActivityName + ",Row:"
							+ "n/a,Ambiguous class:"
							+ ambiguousTerm.getTerm() + ",Scheme:"
							+ ambiguousTerm.getClassSchemeUUID() + ",Results:"
							+ ambiguousTerm.getCount());
				}
			}
		}
		
		//store updated CERIF graph
		execution.setVariable(IActivitiConstants.VARIABLE_CERIF, cerif);

	}

}

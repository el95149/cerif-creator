/**
 * 
 */
package gr.ekt.cerif.creator.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.ValidationException;
import org.springframework.stereotype.Service;

import gr.ekt.cerif.schema.CERIF;

/**
 * @author aanagnostopoulos
 *
 */
@Service
public class CERIFExporterImpl implements ICERIFExporter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gr.ekt.cerif.creator.service.ICERIFExporter#exportToXMLFile(gr.ekt.cerif
	 * .schema.CERIF, java.lang.String)
	 */
	public void exportToXMLFile(CERIF cerif, File file, boolean xmlValidation)
			throws IOException, MarshalException, ValidationException {
		Writer out = new FileWriter(file);
		Marshaller marshaller = new Marshaller();
		marshaller
				.setSchemaLocation("urn:xmlns:org:eurocris:cerif-1.6-2 http://www.eurocris.org/Uploads/Web%20pages/CERIF-1.6/CERIF_1.6_2.xsd");
		marshaller.setSuppressNamespaces(true);
		marshaller.setRootElement("CERIF");
		marshaller.setProperty("org.exolab.castor.indent", "true");
		marshaller
				.setNamespaceMapping("", "urn:xmlns:org:eurocris:cerif-1.6-2");
		marshaller.setValidation(xmlValidation);
		marshaller.setWriter(out); //must be set AFTER the properties are set!
		marshaller.marshal(cerif);
		out.close();
	}

	public String exportToXMLString(CERIF cerif, boolean xmlValidation)
			throws IOException, MarshalException, ValidationException {
		Writer out = new StringWriter();
		Marshaller marshaller = new Marshaller();
		marshaller
				.setSchemaLocation("urn:xmlns:org:eurocris:cerif-1.6-2 http://www.eurocris.org/Uploads/Web%20pages/CERIF-1.6/CERIF_1.6_2.xsd");
		marshaller.setSuppressNamespaces(true);
		marshaller.setRootElement("CERIF");
		marshaller.setProperty("org.exolab.castor.indent", "true");
		marshaller
				.setNamespaceMapping("", "urn:xmlns:org:eurocris:cerif-1.6-2");
		marshaller.setValidation(xmlValidation);
		marshaller.setWriter(out); //must be set AFTER the properties are set!
		marshaller.marshal(cerif);
		out.close();
		return out.toString();
	}

}

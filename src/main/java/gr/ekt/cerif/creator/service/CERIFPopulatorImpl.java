/**
 * 
 */
package gr.ekt.cerif.creator.service;

import gr.ekt.cerif.creator.dozer.CsvDozerContextBeanReader;
import gr.ekt.cerif.creator.service.RowResult.ErrorLevel;
import gr.ekt.cerif.schema.CERIF;
import gr.ekt.cerif.schema.CERIFItem;
import gr.ekt.cerif.schema.CfClass__TypeCfFedId;
import gr.ekt.cerif.schema.CfCoreClassWithFraction__Group;
import gr.ekt.cerif.schema.CfCoreClass__Group;
import gr.ekt.cerif.schema.CfFedId__EmbTypeCfFedId_Class;
import gr.ekt.cerif.schema.CfFedId__EmbTypeChoice;
import gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem;
import gr.ekt.cerif.schema.CfProj;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.io.dozer.CsvDozerBeanReader;
import org.supercsv.io.dozer.ICsvDozerBeanReader;
import org.supercsv.prefs.CsvPreference;

/**
 * @author aanagnostopoulos
 *
 */
@Service
public class CERIFPopulatorImpl implements ICERIFPopulator {

	private static final String CERIF_SCHEMA_PACKAGE = "gr.ekt.cerif.schema.";

	private static Log log = LogFactory.getLog(CERIFPopulatorImpl.class);

	@Autowired
	private DozerBeanMapper dozerMapper;

	@Autowired
	private IGenericQueryService genericQueryService;

	// private Mapper dozerMapper;

	private List<CERIFItem> populate(Reader reader, String mapId) {

		List<CERIFItem> cerifItems = new ArrayList<CERIFItem>();

		CsvDozerContextBeanReader beanReader = new CsvDozerContextBeanReader(
				reader, CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE,
				(DozerBeanMapper) dozerMapper);

		try {
			beanReader.getHeader(true); // ingore the header
			CERIFItem cerifItem = null;
			while ((cerifItem = (mapId != null ? beanReader.read(
					CERIFItem.class, mapId) : beanReader.read(CERIFItem.class))) != null) {
				cerifItems.add(cerifItem);
			}

		} catch (IOException e) {
			log.error(e);
		} finally {
			if (beanReader != null) {
				try {
					beanReader.close();
				} catch (IOException e) {
					log.error(e);
				}
			}
		}
		return cerifItems;
	}

	private CERIF populate(Reader reader, CERIF cerif, String mapId) {

		List<CERIFItem> cerifItems = populate(reader, mapId);
		for (CERIFItem cerifItem : cerifItems) {
			cerif.addCERIFItem(cerifItem);
		}
		return cerif;
	}

	private void createLink(String[] header, String entityName,
			Map<String, String> row, Object entity) {
		try {

			String firstEntityTypeChoiceName = entityName + "__TypeChoice";
			// log.debug(firstEntityTypeChoiceName);
			Object firstEntityTypeChoice = PropertyUtils.getProperty(entity,
					firstEntityTypeChoiceName);
			if (firstEntityTypeChoice == null) {
				firstEntityTypeChoice = Class
						.forName(
								CERIF_SCHEMA_PACKAGE
										+ (firstEntityTypeChoiceName.substring(
												0, 1).toUpperCase() + firstEntityTypeChoiceName
												.substring(1,
														firstEntityTypeChoiceName
																.length())))
						.newInstance();
				PropertyUtils.setProperty(entity, firstEntityTypeChoiceName,
						firstEntityTypeChoice);
			}
			java.lang.reflect.Method method = null;
			String firstEntityTypeChoiceItemName = firstEntityTypeChoiceName
					.substring(0, 1).toUpperCase()
					+ firstEntityTypeChoiceName.substring(1,
							firstEntityTypeChoiceName.length()) + "Item";

			String methodName = "add" + firstEntityTypeChoiceItemName;
			// log.debug(methodName);
			method = firstEntityTypeChoice.getClass().getMethod(
					methodName,
					Class.forName(CERIF_SCHEMA_PACKAGE
							+ firstEntityTypeChoiceItemName));

			// create a nested link to the "opposing" entity for both
			// matching
			// items
			Object firstEntityTypeChoiceItem = dozerMapper.map(
					row,
					Class.forName(CERIF_SCHEMA_PACKAGE
							+ firstEntityTypeChoiceItemName));

			method.invoke(firstEntityTypeChoice, firstEntityTypeChoiceItem);

		} catch (Exception e) {
			log.error(e);
			return;
		}
	}

	private TermResult replaceClassTermsWithUUID(Object parent)
			throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		TermResult result = null;
		CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group = (CfCoreClassWithFraction__Group) PropertyUtils
				.getProperty(parent, "cfCoreClassWithFraction__Group");
		if (cfCoreClassWithFraction__Group != null) {
			CfCoreClass__Group cfCoreClass__Group = (CfCoreClass__Group) PropertyUtils
					.getProperty(cfCoreClassWithFraction__Group,
							"cfCoreClass__Group");
			if (cfCoreClass__Group != null) {
				String cfClassId = cfCoreClass__Group.getCfClassId();
				String cfClassSchemeId = cfCoreClass__Group
						.getCfClassSchemeId();
				if (cfClassId != null && cfClassSchemeId != null) {
					// check if value is already a valid UUID
					UUID uuid = null;
					try {
						uuid = UUID.fromString(cfClassId);
					} catch (IllegalArgumentException e) {
						// swallow
					}
					if (uuid == null) {
						List<String> classUUIDs = genericQueryService
								.findClassUUIDsByTerm(cfClassId,
										cfClassSchemeId);
						if (classUUIDs.size() != 1) {
							// ambiguous number of UUIDs found, report
							result = new TermResult(cfClassId, cfClassSchemeId,
									classUUIDs.size());
						} else {
							cfCoreClass__Group.setCfClassId(classUUIDs.get(0));
						}
					}
				}
			}
		}
		return result;
	}

	public CERIF populateWithCSVFile(File file, CERIF cerif)
			throws FileNotFoundException {
		FileReader reader = new FileReader(file);
		return populate(reader, cerif,
				file.getName().substring(0, file.getName().indexOf(".")));
	}

	public CERIF populateWithCSVFile(File file) throws FileNotFoundException {

		CERIF cerif = new CERIF();
		return populateWithCSVFile(file, cerif);
	}

	public List<CERIFItem> createCerifItemsFromCSVFile(File file)
			throws FileNotFoundException {
		FileReader reader = new FileReader(file);
		return populate(reader,
				file.getName().substring(0, file.getName().indexOf(".")));
	}

	public CERIF populateNestedLinkEntities(File file, CERIF cerif,
			List<RowResult> results) throws FileNotFoundException, IOException {

		// read CSV
		ICsvMapReader mapReader = null;
		String[] header = null;
		List<Map<String, String>> csv = new ArrayList<>();
		try {
			mapReader = new CsvMapReader(new FileReader(file),
					CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE);

			// the header columns are used as the keys to the Map
			header = mapReader.getHeader(true);

			Map<String, String> csvMap = null;
			while ((csvMap = mapReader.read(header)) != null) {
				csv.add(csvMap);
			}
		} finally {
			if (mapReader != null) {
				mapReader.close();
			}
		}
		if (csv.isEmpty()) {
			log.error("Empty CSV, ignoring");
			return cerif;
		}

		String firstEntityName = header[0]
				.substring(0, header[0].indexOf("Id"));
		String secondEntityName = header[1].substring(0,
				header[1].indexOf("Id"));
		// for each CSV Row
		CERIFItem[] cerifItems = cerif.getCERIFItem();

		for (Map<String, String> row : csv) {
			// // locate the matching CERIF items (matching the first and second
			// // column
			// // IDs) graph
			Object firstEntity = null;
			Object secondEntity = null;
			for (CERIFItem cerifItem : cerifItems) {
				try {
					if (firstEntity == null) {
						Object firstEntityCandidate = PropertyUtils
								.getProperty(
										cerifItem,
										header[0].substring(0,
												header[0].indexOf("Id")));
						if (firstEntityCandidate != null) {
							Object id = PropertyUtils.getProperty(
									firstEntityCandidate, header[0]);
							if (id != null) {
								if (id.equals(row.get(header[0]))) {
									firstEntity = firstEntityCandidate;
								}
							}
						}
					}
					if (secondEntity == null) {
						Object secondEntityCandidate = PropertyUtils
								.getProperty(
										cerifItem,
										header[1].substring(0,
												header[1].indexOf("Id")));
						if (secondEntityCandidate != null) {
							Object id = PropertyUtils.getProperty(
									secondEntityCandidate, header[1]);
							if (id != null) {
								if (id.equals(row.get(header[1]))) {
									secondEntity = secondEntityCandidate;
								}
							}
						}
					}
				} catch (Exception e) {
					log.error(e);
					log.error("Error getting entities, pair:" + header[0] + "/"
							+ header[1] + ":" + row.get(header[0]) + "/"
							+ row.get(header[1]) + ", Row:" + csv.indexOf(row));
					results.add(new RowResult(csv.indexOf(row),
							ErrorLevel.ERROR, "Error getting entities, pair:"
									+ header[0] + "/" + header[1] + ":"
									+ row.get(header[0]) + "/"
									+ row.get(header[1])));
					continue;
				}
				if (firstEntity != null && secondEntity != null) {
					break;
				}
			}

			if (firstEntity == null || secondEntity == null) {
				log.error("Could not locate entity pair:" + header[0] + "/"
						+ header[1] + ":" + row.get(header[0]) + "/"
						+ row.get(header[1]) + ", Row:" + csv.indexOf(row));
				results.add(new RowResult(csv.indexOf(row), ErrorLevel.ERROR,
						"Could not locate entity pair:" + header[0] + "/"
								+ header[1] + ":" + row.get(header[0]) + "/"
								+ row.get(header[1])));
				continue;
			}

			if (firstEntity.equals(secondEntity)) {
				log.error("Both entities belong to the same CERIF element, entity pair:"
						+ header[0]
						+ "/"
						+ header[1]
						+ ":"
						+ row.get(header[0])
						+ "/"
						+ row.get(header[1])
						+ ", Row:" + csv.indexOf(row));
				results.add(new RowResult(csv.indexOf(row), ErrorLevel.ERROR,
						"Both entities belong to the same CERIF element, entity pair:"
								+ header[0] + "/" + header[1] + ":"
								+ row.get(header[0]) + "/" + row.get(header[1])));
				continue;
			}

			createLink(header, firstEntityName, row, firstEntity);
			createLink(header, secondEntityName, row, secondEntity);

		}

		return cerif;
	}

	public List<CfClass__TypeCfFedId> getExistingFederatedIdentifiers(
			CERIFItem cerifItem, String entityType) {

		List<CfClass__TypeCfFedId> existingFederatedIdentifiers = new ArrayList<>();

		try {
			Object entity = PropertyUtils.getProperty(cerifItem, entityType);
			Object entityTypeChoice = PropertyUtils.getProperty(entity,
					entityType + "__TypeChoice");
			if (entityTypeChoice != null) {
				Object[] items = (Object[]) PropertyUtils.getProperty(
						entityTypeChoice, entityType + "__TypeChoiceItem");
				for (Object item : items) {
					CfClass__TypeCfFedId fedId = (CfClass__TypeCfFedId) PropertyUtils
							.getProperty(item, "cfFedId");
					if (fedId == null) {
						continue;
					}

					CfFedId__EmbTypeChoice cfFedId__EmbTypeChoice = fedId
							.getCfFedId__EmbTypeChoice();
					if (cfFedId__EmbTypeChoice == null) {
						throw new RuntimeException(
								"Missing Federated identifier classification, entity type/identifier:"
										+ entityType + "/" + fedId.getCfFedId());
					}
					CfFedId__EmbTypeChoiceItem[] cfFedId__EmbTypeChoiceItem = cfFedId__EmbTypeChoice
							.getCfFedId__EmbTypeChoiceItem();
					for (CfFedId__EmbTypeChoiceItem cfFedId__EmbTypeChoiceItem2 : cfFedId__EmbTypeChoiceItem) {
						CfFedId__EmbTypeCfFedId_Class cfFedId_Class = cfFedId__EmbTypeChoiceItem2
								.getCfFedId_Class();
						if (cfFedId_Class == null) {
							continue;
						}
						String cfClassUUID = cfFedId_Class
								.getCfCoreClassWithFraction__Group()
								.getCfCoreClass__Group().getCfClassId();
						Boolean fedIdExists = genericQueryService
								.federatedIdentifierExistsForEntityType(
										fedId.getCfFedId(), entityType,
										cfClassUUID);
						if (fedIdExists) {
							existingFederatedIdentifiers.add(fedId);
						}
					}

				}
			}

		} catch (IllegalAccessException | InvocationTargetException
				| NoSuchMethodException e) {
			log.error(e);
			throw new RuntimeException(e);
		}
		return existingFederatedIdentifiers;
	}

	public List<TermResult> replaceEntityClassTermsWithUUIDs(
			CERIFItem cerifItem, String entityType) {

		List<TermResult> ambiguousTerms = new ArrayList<TermResult>();
		try {
			Object entity = PropertyUtils.getProperty(cerifItem, entityType);
			Object entityTypeChoice = PropertyUtils.getProperty(entity,
					entityType + "__TypeChoice");
			if (entityTypeChoice != null) {
				Object[] items = (Object[]) PropertyUtils.getProperty(
						entityTypeChoice, entityType + "__TypeChoiceItem");
				for (Object item : items) {
					Object entity_Class = PropertyUtils.getProperty(item,
							entityType + "_Class");
					if (entity_Class == null) {
						continue;
					}
					TermResult termResult = replaceClassTermsWithUUID(entity_Class);
					if (termResult != null) {
						ambiguousTerms.add(termResult);
					}
				}
			}
		} catch (IllegalAccessException | InvocationTargetException
				| NoSuchMethodException e) {
			log.error(e);
			throw new RuntimeException(e);
		}
		return ambiguousTerms;
	}

	public List<TermResult> replaceLinkEntityClassTermsWithUUIDs(
			CERIFItem cerifItem, String linkEntityType) {

		String firstEntityName = linkEntityType.substring(0,
				linkEntityType.indexOf("_"));
		String secondEntityName = "cf"
				+ linkEntityType.substring(linkEntityType.indexOf("_") + 1);
		List<TermResult> ambiguousTerms = new ArrayList<TermResult>();
		TermResult termResult = null;
		try {
			Object firstEntity = PropertyUtils.getProperty(cerifItem,
					firstEntityName);
			Object secondEntity = PropertyUtils.getProperty(cerifItem,
					secondEntityName);
			if ((firstEntity != null && secondEntity != null)
					|| (firstEntity == null && secondEntity == null)) {
				// precisely one cerif item type must be set at any given time,
				// skip
				// further checking (XML validator should be responsible for
				// such checks)
				return null;
			}
			Object entity = firstEntity != null ? firstEntity : secondEntity;
			String entityType = firstEntity != null ? firstEntityName
					: secondEntityName;
			Object entityTypeChoice = PropertyUtils.getProperty(entity,
					entityType + "__TypeChoice");
			if (entityTypeChoice != null) {
				Object[] items = (Object[]) PropertyUtils.getProperty(
						entityTypeChoice, entityType + "__TypeChoiceItem");
				for (Object item : items) {
					Object nestedLinkEntity = PropertyUtils.getProperty(item,
							linkEntityType);
					if (nestedLinkEntity == null) {
						continue;
					}
					termResult = replaceClassTermsWithUUID(nestedLinkEntity);
					if (termResult != null) {
						ambiguousTerms.add(termResult);
					}
				}
			}

		} catch (IllegalAccessException | InvocationTargetException
				| NoSuchMethodException e) {
			log.error(e);
			throw new RuntimeException(e);
		}
		return ambiguousTerms;
	}

}

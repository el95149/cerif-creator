/**
 * 
 */
package gr.ekt.cerif.creator.service;

import java.io.StringWriter;

import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.springframework.stereotype.Service;

/**
 * @author aanagnostopoulos
 *
 */
@Service
public class CSVConverterImpl implements ICSVConverter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gr.ekt.cerif.creator.service.ICSVConverter#convertToCSV(org.springframework
	 * .jdbc.support.rowset.SqlRowSet)
	 */
	public String convertToCSV(SqlRowSet sqlRowSet) {
		StringWriter writer = new StringWriter();

		SqlRowSetMetaData metaData = sqlRowSet.getMetaData();
		int columnCount = metaData.getColumnCount();

		while (sqlRowSet.next()) {
			for (int i = 1; i < columnCount; i++) {
				writer.append("\"" + sqlRowSet.getString(i) + "\";");
			}
			writer.append("\"" + sqlRowSet.getString(columnCount) + "\"");
			if (!sqlRowSet.isLast()) {
				writer.append("\n");
			}
		}
		return writer.toString();
	}

}

/**
 * 
 */
package gr.ekt.cerif.creator.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author aanagnostopoulos
 *
 */
@Service
public class GenericQueryServiceImpl implements IGenericQueryService {

	private static final Map<String, String> entityClassMap = new HashMap<String, String>();

	private static Log log = LogFactory.getLog(GenericQueryServiceImpl.class);

	private static final String CERIF_ENTITIES_SCHEME_UUID = "6e0d9af0-1cd6-11e1-8bc2-0800200c9a66";

	@Autowired
	private DataSource dataSource;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	static {
		entityClassMap.put("cfPers", "cf7799e0-3477-11e1-b86c-0800200c9a66");
		entityClassMap.put("cfProj", "cf7799e1-3477-11e1-b86c-0800200c9a66");
		entityClassMap.put("cfOrgUnit", "cf7799e2-3477-11e1-b86c-0800200c9a66");
		entityClassMap.put("cfResPat", "cf7799e3-3477-11e1-b86c-0800200c9a66");
		entityClassMap.put("cfResProd", "cf7799e4-3477-11e1-b86c-0800200c9a66");
		entityClassMap.put("cfResPubl", "cf7799e5-3477-11e1-b86c-0800200c9a66");
		entityClassMap.put("cfFedId", "a1e51365-b7c4-4bdb-bbd1-0530840148be");
		entityClassMap.put("cfMeas", "1cc0fbd8-a160-41df-9af8-d436e6a5296f");
		entityClassMap.put("cfIndic", "ed96ef79-f77d-4aaf-8c19-f727bbb49937");
		entityClassMap.put("cfFund", "cf7799e6-3477-11e1-b86c-0800200c9a66");
		entityClassMap.put("cfFacil", "cf7799e7-3477-11e1-b86c-0800200c9a66");
		entityClassMap.put("cfClass", "613b117e-980e-4051-8876-8524cd498caf");
		entityClassMap.put("cfClassScheme",
				"4c93b3b2-d5ff-442c-9b28-4a028305bcf1");
		entityClassMap.put("cfEquip", "cf7799e8-3477-11e1-b86c-0800200c9a66");
		entityClassMap.put("cfSrv", "cf7799e9-3477-11e1-b86c-0800200c9a66");
		entityClassMap.put("cfEvent", "68aa07f4-34c9-11e1-b86c-0800200c9a66");
	}

	@Transactional(value = "transactionManager")
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gr.ekt.cerif.creator.service.IGenericQueryService#executeQuery(java.lang
	 * .String)
	 */
	public SqlRowSet executeQuery(String query) {

		SqlRowSet srs = jdbcTemplate.queryForRowSet(query);
		return srs;
		// int rowCount = 0;
		// while (srs.next()) {
		// System.out.println(srs.getString(1) + " - " + srs.getString(2));
		// System.out.println(srs.getString("poi_id") + " - " +
		// srs.getString("poi_name"));
		// rowCount++;
		// }
		// System.out.println(rowCount); // TODO Auto-generated method stub
	}

	@Transactional(value = "transactionManager", readOnly = true)
	public Boolean federatedIdentifierExistsForEntityType(String fedId,
			String entityType, String identifierTypeUUID) {

		if (!entityClassMap.containsKey(entityType)) {
			throw new RuntimeException(
					"Entity type not found in Class-UUID map:" + entityType);
		}

		String entityTypeClassUUID = entityClassMap.get(entityType);

		Long fedIdCount = jdbcTemplate.queryForObject(
				"select count(f.cfFedId) from cfFedId f, cfClass c, cfClassScheme cs, cfFedId_Class fc"
						+ " where f.cfClassId = c.cfClassId"
						+ " and c.cfClassSchemeId = cs.cfClassSchemeId"
						+ " and cs.cfUUID=?" + " and c.cfUUID=?"
						+ " and f.cfFedId=? and f.cfFedIdId in ("
						+ "select fc.cfFedIdId from cfFedId_Class, cfClass c2"
						+ " where fc.cfClassId=c2.cfClassId"
						+ " and c2.cfUUID=?"
						+ ")", Long.class,
				new Object[] { CERIF_ENTITIES_SCHEME_UUID, entityTypeClassUUID,
						fedId, identifierTypeUUID });
		log.debug("Identifiers for entity type:" + entityType + " and fedId/identifierTypeUUID '"
				+ fedId + "/"+identifierTypeUUID+"': " + fedIdCount);
		return fedIdCount != 0;

	}

	@Transactional(value = "transactionManager", readOnly = true)
	public List<String> findClassUUIDsByTerm(String term, String classSchemeUUID) {
		String query = "select c.cfUUID from cfClass c, cfClassTerm ct, cfClassScheme cs"
				+ " where c.cfClassId=ct.cfClassId"
				+ " and c.cfClassSchemeId = cs.cfClassSchemeId"
				+ " and cs.cfUUID=?" + " and ct.cfTerm = ?";
		List<String> classUUIDs = jdbcTemplate.queryForList(query,
				new Object[] { classSchemeUUID, term }, String.class);
		return classUUIDs;

	}
}

/**
 * 
 */
package gr.ekt.cerif.creator.service;

import java.io.File;
import java.io.IOException;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import gr.ekt.cerif.schema.CERIF;

/**
 * @author aanagnostopoulos
 *
 */
public interface ICERIFExporter {

	void exportToXMLFile(CERIF cerif, File file, boolean xmlValidation) throws IOException, MarshalException, ValidationException;

	String exportToXMLString(CERIF cerif, boolean xmlValidation) throws IOException, MarshalException,
			ValidationException;
	
}

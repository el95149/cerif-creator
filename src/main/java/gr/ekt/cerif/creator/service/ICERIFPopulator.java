/**
 * 
 */
package gr.ekt.cerif.creator.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import gr.ekt.cerif.schema.CERIF;
import gr.ekt.cerif.schema.CERIFItem;
import gr.ekt.cerif.schema.CfClass__TypeCfFedId;

/**
 * @author aanagnostopoulos
 *
 */
public interface ICERIFPopulator {

	CERIF populateWithCSVFile(File file, CERIF cerif) throws FileNotFoundException;
	
	CERIF populateWithCSVFile(File file) throws FileNotFoundException;

	CERIF populateNestedLinkEntities(File file, CERIF cerif, List<RowResult> results)
			throws FileNotFoundException, IOException;

	List<CERIFItem> createCerifItemsFromCSVFile(File file)
			throws FileNotFoundException;

	List<CfClass__TypeCfFedId> getExistingFederatedIdentifiers(
			CERIFItem cerifItem, String entityType);

	List<TermResult> replaceEntityClassTermsWithUUIDs(CERIFItem cerifItem,
			String entityType);

	List<TermResult> replaceLinkEntityClassTermsWithUUIDs(CERIFItem cerifItem,
			String linkEntityType);
	
}

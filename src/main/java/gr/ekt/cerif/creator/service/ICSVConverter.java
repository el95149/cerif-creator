/**
 * 
 */
package gr.ekt.cerif.creator.service;

import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * @author aanagnostopoulos
 *
 */
public interface ICSVConverter {

	
	String convertToCSV(SqlRowSet sqlRowSet);
	
}

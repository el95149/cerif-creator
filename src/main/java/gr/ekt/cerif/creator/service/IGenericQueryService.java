/**
 * 
 */
package gr.ekt.cerif.creator.service;

import java.util.List;

import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * @author aanagnostopoulos
 *
 */
public interface IGenericQueryService {

	SqlRowSet executeQuery(String query);

	Boolean federatedIdentifierExistsForEntityType(String fedId,
			String entityType, String identifierTypeUUID);

	List<String> findClassUUIDsByTerm(String term, String classSchemeUUID);
	
}

/**
 * 
 */
package gr.ekt.cerif.creator.service;

/**
 * @author aanagnostopoulos
 *
 */
public class RowResult {

	private Integer row = null;

	private ErrorLevel level = null;

	private String result = null;

	public RowResult(Integer row, ErrorLevel level, String result) {
		super();
		this.row = row;
		this.level = level;
		this.result = result;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public ErrorLevel getLevel() {
		return level;
	}

	public void setLevel(ErrorLevel level) {
		this.level = level;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public enum ErrorLevel {
		INFO, WARN, ERROR;
	}
}

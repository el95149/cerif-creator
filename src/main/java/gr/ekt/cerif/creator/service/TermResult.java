/**
 * 
 */
package gr.ekt.cerif.creator.service;

/**
 * @author aanagnostopoulos
 *
 */
public class TermResult {

	private String term = null;
	
	private String classSchemeUUID = null;
	
	private Integer count = null;

	public TermResult(String term, String classSchemeUUID, Integer count) {
		super();
		this.term = term;
		this.classSchemeUUID = classSchemeUUID;
		this.count = count;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getClassSchemeUUID() {
		return classSchemeUUID;
	}

	public void setClassSchemeUUID(String classSchemeUUID) {
		this.classSchemeUUID = classSchemeUUID;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	
	
}

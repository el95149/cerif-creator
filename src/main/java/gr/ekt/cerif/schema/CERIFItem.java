/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CERIFItem implements java.io.Serializable {

    /**
     * Internal choice value storage
     */
    private java.lang.Object _choiceValue;

    private gr.ekt.cerif.schema.CfClass cfClass;

    private gr.ekt.cerif.schema.CfClassScheme cfClassScheme;

    private gr.ekt.cerif.schema.CfClassSchemeDescr cfClassSchemeDescr;

    private gr.ekt.cerif.schema.CfLang cfLang;

    private gr.ekt.cerif.schema.CfLangName cfLangName;

    private gr.ekt.cerif.schema.CfClassDescr cfClassDescr;

    private gr.ekt.cerif.schema.CfClass_Class cfClass_Class;

    private gr.ekt.cerif.schema.CfClassTerm cfClassTerm;

    private gr.ekt.cerif.schema.CfClassScheme_ClassScheme cfClassScheme_ClassScheme;

    private gr.ekt.cerif.schema.CfFacil_Class cfFacil_Class;

    private gr.ekt.cerif.schema.CfCountry cfCountry;

    private gr.ekt.cerif.schema.CfCV cfCV;

    private gr.ekt.cerif.schema.CfEquip cfEquip;

    private gr.ekt.cerif.schema.CfEquip_Class cfEquip_Class;

    private gr.ekt.cerif.schema.CfEquip_Fund cfEquip_Fund;

    private gr.ekt.cerif.schema.CfEquipDescr cfEquipDescr;

    private gr.ekt.cerif.schema.CfEquipKeyw cfEquipKeyw;

    private gr.ekt.cerif.schema.CfEquipName cfEquipName;

    private gr.ekt.cerif.schema.CfEvent cfEvent;

    private gr.ekt.cerif.schema.CfEventName cfEventName;

    private gr.ekt.cerif.schema.CfEventDescr cfEventDescr;

    private gr.ekt.cerif.schema.CfEventKeyw cfEventKeyw;

    private gr.ekt.cerif.schema.CfEvent_Class cfEvent_Class;

    private gr.ekt.cerif.schema.CfEvent_Fund cfEvent_Fund;

    private gr.ekt.cerif.schema.CfResPubl_Event cfResPubl_Event;

    private gr.ekt.cerif.schema.CfFacil cfFacil;

    private gr.ekt.cerif.schema.CfFacilName cfFacilName;

    private gr.ekt.cerif.schema.CfFacilDescr cfFacilDescr;

    private gr.ekt.cerif.schema.CfFacilKeyw cfFacilKeyw;

    private gr.ekt.cerif.schema.CfFacil_Fund cfFacil_Fund;

    private gr.ekt.cerif.schema.CfFund cfFund;

    private gr.ekt.cerif.schema.CfFundName cfFundName;

    private gr.ekt.cerif.schema.CfFundDescr cfFundDescr;

    private gr.ekt.cerif.schema.CfFund_Class cfFund_Class;

    private gr.ekt.cerif.schema.CfFundKeyw cfFundKeyw;

    private gr.ekt.cerif.schema.CfFund_Fund cfFund_Fund;

    private gr.ekt.cerif.schema.CfOrgUnit cfOrgUnit;

    private gr.ekt.cerif.schema.CfOrgUnitName cfOrgUnitName;

    private gr.ekt.cerif.schema.CfOrgUnitResAct cfOrgUnitResAct;

    private gr.ekt.cerif.schema.CfOrgUnitKeyw cfOrgUnitKeyw;

    private gr.ekt.cerif.schema.CfOrgUnit_Class cfOrgUnit_Class;

    private gr.ekt.cerif.schema.CfOrgUnit_Equip cfOrgUnit_Equip;

    private gr.ekt.cerif.schema.CfOrgUnit_EAddr cfOrgUnit_EAddr;

    private gr.ekt.cerif.schema.CfOrgUnit_Event cfOrgUnit_Event;

    private gr.ekt.cerif.schema.CfOrgUnit_ExpSkills cfOrgUnit_ExpSkills;

    private gr.ekt.cerif.schema.CfOrgUnit_Facil cfOrgUnit_Facil;

    private gr.ekt.cerif.schema.CfOrgUnit_Fund cfOrgUnit_Fund;

    private gr.ekt.cerif.schema.CfOrgUnit_OrgUnit cfOrgUnit_OrgUnit;

    private gr.ekt.cerif.schema.CfOrgUnit_Prize cfOrgUnit_Prize;

    private gr.ekt.cerif.schema.CfOrgUnit_ResPat cfOrgUnit_ResPat;

    private gr.ekt.cerif.schema.CfOrgUnit_ResProd cfOrgUnit_ResProd;

    private gr.ekt.cerif.schema.CfOrgUnit_ResPubl cfOrgUnit_ResPubl;

    private gr.ekt.cerif.schema.CfOrgUnit_Srv cfOrgUnit_Srv;

    private gr.ekt.cerif.schema.CfPers cfPers;

    private gr.ekt.cerif.schema.CfPersResInt cfPersResInt;

    private gr.ekt.cerif.schema.CfPersKeyw cfPersKeyw;

    private gr.ekt.cerif.schema.CfPers_Pers cfPers_Pers;

    private gr.ekt.cerif.schema.CfPers_EAddr cfPers_EAddr;

    private gr.ekt.cerif.schema.CfPers_Class cfPers_Class;

    private gr.ekt.cerif.schema.CfPers_CV cfPers_CV;

    private gr.ekt.cerif.schema.CfPers_Equip cfPers_Equip;

    private gr.ekt.cerif.schema.CfPers_Event cfPers_Event;

    private gr.ekt.cerif.schema.CfPers_ExpSkills cfPers_ExpSkills;

    private gr.ekt.cerif.schema.CfPers_Facil cfPers_Facil;

    private gr.ekt.cerif.schema.CfPers_Fund cfPers_Fund;

    private gr.ekt.cerif.schema.CfPers_Lang cfPers_Lang;

    private gr.ekt.cerif.schema.CfPers_Country cfPers_Country;

    private gr.ekt.cerif.schema.CfPers_OrgUnit cfPers_OrgUnit;

    private gr.ekt.cerif.schema.CfPers_Prize cfPers_Prize;

    private gr.ekt.cerif.schema.CfPers_ResPat cfPers_ResPat;

    private gr.ekt.cerif.schema.CfPers_ResProd cfPers_ResProd;

    private gr.ekt.cerif.schema.CfPers_ResPubl cfPers_ResPubl;

    private gr.ekt.cerif.schema.CfPers_Srv cfPers_Srv;

    private gr.ekt.cerif.schema.CfPrize cfPrize;

    private gr.ekt.cerif.schema.CfProj cfProj;

    private gr.ekt.cerif.schema.CfProj_Class cfProj_Class;

    private gr.ekt.cerif.schema.CfProjTitle cfProjTitle;

    private gr.ekt.cerif.schema.CfProjAbstr cfProjAbstr;

    private gr.ekt.cerif.schema.CfProjKeyw cfProjKeyw;

    private gr.ekt.cerif.schema.CfProj_Equip cfProj_Equip;

    private gr.ekt.cerif.schema.CfProj_Event cfProj_Event;

    private gr.ekt.cerif.schema.CfProj_Facil cfProj_Facil;

    private gr.ekt.cerif.schema.CfProj_Fund cfProj_Fund;

    private gr.ekt.cerif.schema.CfProj_OrgUnit cfProj_OrgUnit;

    private gr.ekt.cerif.schema.CfProj_Pers cfProj_Pers;

    private gr.ekt.cerif.schema.CfProj_Prize cfProj_Prize;

    private gr.ekt.cerif.schema.CfProj_ResPat cfProj_ResPat;

    private gr.ekt.cerif.schema.CfProj_Proj cfProj_Proj;

    private gr.ekt.cerif.schema.CfProj_ResProd cfProj_ResProd;

    private gr.ekt.cerif.schema.CfProj_ResPubl cfProj_ResPubl;

    private gr.ekt.cerif.schema.CfResPubl cfResPubl;

    private gr.ekt.cerif.schema.CfResPat cfResPat;

    private gr.ekt.cerif.schema.CfResPatTitle cfResPatTitle;

    private gr.ekt.cerif.schema.CfResPatAbstr cfResPatAbstr;

    private gr.ekt.cerif.schema.CfResPatKeyw cfResPatKeyw;

    private gr.ekt.cerif.schema.CfResPat_Class cfResPat_Class;

    private gr.ekt.cerif.schema.CfResProd cfResProd;

    private gr.ekt.cerif.schema.CfResProd_Class cfResProd_Class;

    private gr.ekt.cerif.schema.CfResProd_Fund cfResProd_Fund;

    private gr.ekt.cerif.schema.CfResProdName cfResProdName;

    private gr.ekt.cerif.schema.CfResProdDescr cfResProdDescr;

    private gr.ekt.cerif.schema.CfResProdKeyw cfResProdKeyw;

    private gr.ekt.cerif.schema.CfResPublTitle cfResPublTitle;

    private gr.ekt.cerif.schema.CfResPublAbstr cfResPublAbstr;

    private gr.ekt.cerif.schema.CfResPubl_ResPubl cfResPubl_ResPubl;

    private gr.ekt.cerif.schema.CfResPubl_Class cfResPubl_Class;

    private gr.ekt.cerif.schema.CfSrv cfSrv;

    private gr.ekt.cerif.schema.CfSrvName cfSrvName;

    private gr.ekt.cerif.schema.CfSrvDescr cfSrvDescr;

    private gr.ekt.cerif.schema.CfSrvKeyw cfSrvKeyw;

    private gr.ekt.cerif.schema.CfSrv_Class cfSrv_Class;

    private gr.ekt.cerif.schema.CfResPublKeyw cfResPublKeyw;

    private gr.ekt.cerif.schema.CfEAddr cfEAddr;

    private gr.ekt.cerif.schema.CfPAddr cfPAddr;

    private gr.ekt.cerif.schema.CfOrgUnit_PAddr cfOrgUnit_PAddr;

    private gr.ekt.cerif.schema.CfEAddr_Class cfEAddr_Class;

    private gr.ekt.cerif.schema.CfPers_PAddr cfPers_PAddr;

    private gr.ekt.cerif.schema.CfPAddr_Class cfPAddr_Class;

    private gr.ekt.cerif.schema.CfResPubl_Fund cfResPubl_Fund;

    private gr.ekt.cerif.schema.CfResPat_Fund cfResPat_Fund;

    private gr.ekt.cerif.schema.CfExpSkills cfExpSkills;

    private gr.ekt.cerif.schema.CfExpSkillsName cfExpSkillsName;

    private gr.ekt.cerif.schema.CfExpSkillsKeyw cfExpSkillsKeyw;

    private gr.ekt.cerif.schema.CfExpSkillsDescr cfExpSkillsDescr;

    private gr.ekt.cerif.schema.CfExpSkills_Class cfExpSkills_Class;

    private gr.ekt.cerif.schema.CfCurrency cfCurrency;

    private gr.ekt.cerif.schema.CfCurrencyName cfCurrencyName;

    private gr.ekt.cerif.schema.CfCurrencyEntName cfCurrencyEntName;

    private gr.ekt.cerif.schema.CfCountryName cfCountryName;

    private gr.ekt.cerif.schema.CfCV_Class cfCV_Class;

    private gr.ekt.cerif.schema.CfPrize_Class cfPrize_Class;

    private gr.ekt.cerif.schema.CfCountry_Class cfCountry_Class;

    private gr.ekt.cerif.schema.CfCurrency_Class cfCurrency_Class;

    private gr.ekt.cerif.schema.CfLang_Class cfLang_Class;

    private gr.ekt.cerif.schema.CfDC cfDC;

    private gr.ekt.cerif.schema.CfDCPublisher cfDCPublisher;

    private gr.ekt.cerif.schema.CfDCCreator cfDCCreator;

    private gr.ekt.cerif.schema.CfDCContributor cfDCContributor;

    private gr.ekt.cerif.schema.CfDCRelation cfDCRelation;

    private gr.ekt.cerif.schema.CfDCLanguage cfDCLanguage;

    private gr.ekt.cerif.schema.CfDCRightsHolder cfDCRightsHolder;

    private gr.ekt.cerif.schema.CfDCTitle cfDCTitle;

    private gr.ekt.cerif.schema.CfDCSubject cfDCSubject;

    private gr.ekt.cerif.schema.CfDCDescription cfDCDescription;

    private gr.ekt.cerif.schema.CfDCDate cfDCDate;

    private gr.ekt.cerif.schema.CfDCAudience cfDCAudience;

    private gr.ekt.cerif.schema.CfDCResourceType cfDCResourceType;

    private gr.ekt.cerif.schema.CfDCFormat cfDCFormat;

    private gr.ekt.cerif.schema.CfDCResourceIdentifier cfDCResourceIdentifier;

    private gr.ekt.cerif.schema.CfDCProvenance cfDCProvenance;

    private gr.ekt.cerif.schema.CfDCSource cfDCSource;

    private gr.ekt.cerif.schema.CfDCCoverage cfDCCoverage;

    private gr.ekt.cerif.schema.CfDCCoverageSpatial cfDCCoverageSpatial;

    private gr.ekt.cerif.schema.CfDCCoverageTemporal cfDCCoverageTemporal;

    private gr.ekt.cerif.schema.CfDCRightsMM cfDCRightsMM;

    private gr.ekt.cerif.schema.CfDCRightsMMAccessRights cfDCRightsMMAccessRights;

    private gr.ekt.cerif.schema.CfDCRightsMMLicense cfDCRightsMMLicense;

    private gr.ekt.cerif.schema.CfFDCRightsMMPrivacy cfFDCRightsMMPrivacy;

    private gr.ekt.cerif.schema.CfFDCRightsMMRights cfFDCRightsMMRights;

    private gr.ekt.cerif.schema.CfFDCRightsMMPricing cfFDCRightsMMPricing;

    private gr.ekt.cerif.schema.CfFDCRightsMMSecurity cfFDCRightsMMSecurity;

    private gr.ekt.cerif.schema.CfOrgUnit_DC cfOrgUnit_DC;

    private gr.ekt.cerif.schema.CfPers_DC cfPers_DC;

    private gr.ekt.cerif.schema.CfProj_DC cfProj_DC;

    private gr.ekt.cerif.schema.CfQual cfQual;

    private gr.ekt.cerif.schema.CfPers_Qual cfPers_Qual;

    private gr.ekt.cerif.schema.CfQualDescr cfQualDescr;

    private gr.ekt.cerif.schema.CfQualKeyw cfQualKeyw;

    private gr.ekt.cerif.schema.CfQual_Class cfQual_Class;

    private gr.ekt.cerif.schema.CfProj_Srv cfProj_Srv;

    private gr.ekt.cerif.schema.CfResPubl_DC cfResPubl_DC;

    private gr.ekt.cerif.schema.CfCite cfCite;

    private gr.ekt.cerif.schema.CfMetrics cfMetrics;

    private gr.ekt.cerif.schema.CfResPublBiblNote cfResPublBiblNote;

    private gr.ekt.cerif.schema.CfResPubl_Facil cfResPubl_Facil;

    private gr.ekt.cerif.schema.CfResPubl_Equip cfResPubl_Equip;

    private gr.ekt.cerif.schema.CfResPubl_ResProd cfResPubl_ResProd;

    private gr.ekt.cerif.schema.CfResPubl_ResPat cfResPubl_ResPat;

    private gr.ekt.cerif.schema.CfResPublNameAbbrev cfResPublNameAbbrev;

    private gr.ekt.cerif.schema.CfEvent_Event cfEvent_Event;

    private gr.ekt.cerif.schema.CfResPublSubtitle cfResPublSubtitle;

    private gr.ekt.cerif.schema.CfResPubl_Cite cfResPubl_Cite;

    private gr.ekt.cerif.schema.CfCite_Class cfCite_Class;

    private gr.ekt.cerif.schema.CfMetrics_Class cfMetrics_Class;

    private gr.ekt.cerif.schema.CfResPubl_Metrics cfResPubl_Metrics;

    private gr.ekt.cerif.schema.CfPersName cfPersName;

    private gr.ekt.cerif.schema.CfPersName_Pers cfPersName_Pers;

    private gr.ekt.cerif.schema.CfMetricsName cfMetricsName;

    private gr.ekt.cerif.schema.CfCiteTitle cfCiteTitle;

    private gr.ekt.cerif.schema.CfCiteDescr cfCiteDescr;

    private gr.ekt.cerif.schema.CfMetricsDescr cfMetricsDescr;

    private gr.ekt.cerif.schema.CfResProd_ResProd cfResProd_ResProd;

    private gr.ekt.cerif.schema.CfResPat_ResPat cfResPat_ResPat;

    private gr.ekt.cerif.schema.CfSrv_Fund cfSrv_Fund;

    private gr.ekt.cerif.schema.CfMetricsKeyw cfMetricsKeyw;

    private gr.ekt.cerif.schema.CfMedium cfMedium;

    private gr.ekt.cerif.schema.CfMediumTitle cfMediumTitle;

    private gr.ekt.cerif.schema.CfMediumDescr cfMediumDescr;

    private gr.ekt.cerif.schema.CfMediumKeyw cfMediumKeyw;

    private gr.ekt.cerif.schema.CfResPubl_Medium cfResPubl_Medium;

    private gr.ekt.cerif.schema.CfResPat_Medium cfResPat_Medium;

    private gr.ekt.cerif.schema.CfResProd_Medium cfResProd_Medium;

    private gr.ekt.cerif.schema.CfFacil_Medium cfFacil_Medium;

    private gr.ekt.cerif.schema.CfEquip_Medium cfEquip_Medium;

    private gr.ekt.cerif.schema.CfSrv_Medium cfSrv_Medium;

    private gr.ekt.cerif.schema.CfMedium_Medium cfMedium_Medium;

    private gr.ekt.cerif.schema.CfProj_Medium cfProj_Medium;

    private gr.ekt.cerif.schema.CfPers_Medium cfPers_Medium;

    private gr.ekt.cerif.schema.CfOrgUnit_Medium cfOrgUnit_Medium;

    private gr.ekt.cerif.schema.CfMedium_Class cfMedium_Class;

    private gr.ekt.cerif.schema.CfEvent_Medium cfEvent_Medium;

    private gr.ekt.cerif.schema.CfMedium_Fund cfMedium_Fund;

    private gr.ekt.cerif.schema.CfCite_Medium cfCite_Medium;

    private gr.ekt.cerif.schema.CfGeoBBox cfGeoBBox;

    private gr.ekt.cerif.schema.CfGeoBBoxName cfGeoBBoxName;

    private gr.ekt.cerif.schema.CfGeoBBoxDescr cfGeoBBoxDescr;

    private gr.ekt.cerif.schema.CfGeoBBoxKeyw cfGeoBBoxKeyw;

    private gr.ekt.cerif.schema.CfPAddr_GeoBBox cfPAddr_GeoBBox;

    private gr.ekt.cerif.schema.CfEquip_PAddr cfEquip_PAddr;

    private gr.ekt.cerif.schema.CfFacil_PAddr cfFacil_PAddr;

    private gr.ekt.cerif.schema.CfSrv_PAddr cfSrv_PAddr;

    private gr.ekt.cerif.schema.CfGeoBBox_Class cfGeoBBox_Class;

    private gr.ekt.cerif.schema.CfEquip_Equip cfEquip_Equip;

    private gr.ekt.cerif.schema.CfSrv_Srv cfSrv_Srv;

    private gr.ekt.cerif.schema.CfFacil_Facil cfFacil_Facil;

    private gr.ekt.cerif.schema.CfFacil_Equip cfFacil_Equip;

    private gr.ekt.cerif.schema.CfFacil_Srv cfFacil_Srv;

    private gr.ekt.cerif.schema.CfEquip_Srv cfEquip_Srv;

    private gr.ekt.cerif.schema.CfFacil_Event cfFacil_Event;

    private gr.ekt.cerif.schema.CfSrv_Event cfSrv_Event;

    private gr.ekt.cerif.schema.CfEquip_Event cfEquip_Event;

    private gr.ekt.cerif.schema.CfResPubl_Srv cfResPubl_Srv;

    private gr.ekt.cerif.schema.CfResPat_Facil cfResPat_Facil;

    private gr.ekt.cerif.schema.CfResPat_Srv cfResPat_Srv;

    private gr.ekt.cerif.schema.CfResPat_Equip cfResPat_Equip;

    private gr.ekt.cerif.schema.CfResProd_Facil cfResProd_Facil;

    private gr.ekt.cerif.schema.CfResProd_Srv cfResProd_Srv;

    private gr.ekt.cerif.schema.CfResProd_Equip cfResProd_Equip;

    private gr.ekt.cerif.schema.CfMeas cfMeas;

    private gr.ekt.cerif.schema.CfIndic cfIndic;

    private gr.ekt.cerif.schema.CfIndic_Meas cfIndic_Meas;

    private gr.ekt.cerif.schema.CfMeas_Class cfMeas_Class;

    private gr.ekt.cerif.schema.CfIndic_Class cfIndic_Class;

    private gr.ekt.cerif.schema.CfIndicName cfIndicName;

    private gr.ekt.cerif.schema.CfIndicDescr cfIndicDescr;

    private gr.ekt.cerif.schema.CfIndicKeyw cfIndicKeyw;

    private gr.ekt.cerif.schema.CfMeasName cfMeasName;

    private gr.ekt.cerif.schema.CfMeasDescr cfMeasDescr;

    private gr.ekt.cerif.schema.CfMeasKeyw cfMeasKeyw;

    private gr.ekt.cerif.schema.CfPers_Meas cfPers_Meas;

    private gr.ekt.cerif.schema.CfOrgUnit_Meas cfOrgUnit_Meas;

    private gr.ekt.cerif.schema.CfProj_Meas cfProj_Meas;

    private gr.ekt.cerif.schema.CfResPubl_Meas cfResPubl_Meas;

    private gr.ekt.cerif.schema.CfResPat_Meas cfResPat_Meas;

    private gr.ekt.cerif.schema.CfResProd_Meas cfResProd_Meas;

    private gr.ekt.cerif.schema.CfFacil_Meas cfFacil_Meas;

    private gr.ekt.cerif.schema.CfSrv_Meas cfSrv_Meas;

    private gr.ekt.cerif.schema.CfEquip_Meas cfEquip_Meas;

    private gr.ekt.cerif.schema.CfPers_Indic cfPers_Indic;

    private gr.ekt.cerif.schema.CfOrgUnit_Indic cfOrgUnit_Indic;

    private gr.ekt.cerif.schema.CfProj_Indic cfProj_Indic;

    private gr.ekt.cerif.schema.CfResPubl_Indic cfResPubl_Indic;

    private gr.ekt.cerif.schema.CfResPat_Indic cfResPat_Indic;

    private gr.ekt.cerif.schema.CfResProd_Indic cfResProd_Indic;

    private gr.ekt.cerif.schema.CfFacil_Indic cfFacil_Indic;

    private gr.ekt.cerif.schema.CfSrv_Indic cfSrv_Indic;

    private gr.ekt.cerif.schema.CfEquip_Indic cfEquip_Indic;

    private gr.ekt.cerif.schema.CfEvent_Meas cfEvent_Meas;

    private gr.ekt.cerif.schema.CfEvent_Indic cfEvent_Indic;

    private gr.ekt.cerif.schema.CfMedium_Indic cfMedium_Indic;

    private gr.ekt.cerif.schema.CfMedium_Meas cfMedium_Meas;

    private gr.ekt.cerif.schema.CfIndic_Indic cfIndic_Indic;

    private gr.ekt.cerif.schema.CfMeas_Meas cfMeas_Meas;

    private gr.ekt.cerif.schema.CfClassDef cfClassDef;

    private gr.ekt.cerif.schema.CfClassSchemeName cfClassSchemeName;

    private gr.ekt.cerif.schema.CfClassEx cfClassEx;

    private gr.ekt.cerif.schema.CfGeoBBox_GeoBBox cfGeoBBox_GeoBBox;

    private gr.ekt.cerif.schema.CfPrizeName cfPrizeName;

    private gr.ekt.cerif.schema.CfPrizeDescr cfPrizeDescr;

    private gr.ekt.cerif.schema.CfPrizeKeyw cfPrizeKeyw;

    private gr.ekt.cerif.schema.CfFund_Indic cfFund_Indic;

    private gr.ekt.cerif.schema.CfFund_Meas cfFund_Meas;

    private gr.ekt.cerif.schema.CfQualTitle cfQualTitle;

    private gr.ekt.cerif.schema.CfResPatVersInfo cfResPatVersInfo;

    private gr.ekt.cerif.schema.CfResProdVersInfo cfResProdVersInfo;

    private gr.ekt.cerif.schema.CfResPublVersInfo cfResPublVersInfo;

    private gr.ekt.cerif.schema.CfFedId cfFedId;

    private gr.ekt.cerif.schema.CfFedId_Class cfFedId_Class;

    private gr.ekt.cerif.schema.CfFedId_Srv cfFedId_Srv;

    private gr.ekt.cerif.schema.CfResProd_ResPat cfResProd_ResPat;

    private gr.ekt.cerif.schema.CfResProdAltName cfResProdAltName;

    private gr.ekt.cerif.schema.CfResProd_GeoBBox cfResProd_GeoBBox;

    private gr.ekt.cerif.schema.CfGeoBBox_Meas cfGeoBBox_Meas;

    public CERIFItem() {
        super();
    }

    /**
     * Returns the value of field 'cfCV'.
     * 
     * @return the value of field 'CfCV'.
     */
    public gr.ekt.cerif.schema.CfCV getCfCV() {
        return this.cfCV;
    }

    /**
     * Returns the value of field 'cfCV_Class'.
     * 
     * @return the value of field 'CfCV_Class'.
     */
    public gr.ekt.cerif.schema.CfCV_Class getCfCV_Class() {
        return this.cfCV_Class;
    }

    /**
     * Returns the value of field 'cfCite'.
     * 
     * @return the value of field 'CfCite'.
     */
    public gr.ekt.cerif.schema.CfCite getCfCite() {
        return this.cfCite;
    }

    /**
     * Returns the value of field 'cfCiteDescr'.
     * 
     * @return the value of field 'CfCiteDescr'.
     */
    public gr.ekt.cerif.schema.CfCiteDescr getCfCiteDescr() {
        return this.cfCiteDescr;
    }

    /**
     * Returns the value of field 'cfCiteTitle'.
     * 
     * @return the value of field 'CfCiteTitle'.
     */
    public gr.ekt.cerif.schema.CfCiteTitle getCfCiteTitle() {
        return this.cfCiteTitle;
    }

    /**
     * Returns the value of field 'cfCite_Class'.
     * 
     * @return the value of field 'CfCite_Class'.
     */
    public gr.ekt.cerif.schema.CfCite_Class getCfCite_Class() {
        return this.cfCite_Class;
    }

    /**
     * Returns the value of field 'cfCite_Medium'.
     * 
     * @return the value of field 'CfCite_Medium'.
     */
    public gr.ekt.cerif.schema.CfCite_Medium getCfCite_Medium() {
        return this.cfCite_Medium;
    }

    /**
     * Returns the value of field 'cfClass'.
     * 
     * @return the value of field 'CfClass'.
     */
    public gr.ekt.cerif.schema.CfClass getCfClass() {
        return this.cfClass;
    }

    /**
     * Returns the value of field 'cfClassDef'.
     * 
     * @return the value of field 'CfClassDef'.
     */
    public gr.ekt.cerif.schema.CfClassDef getCfClassDef() {
        return this.cfClassDef;
    }

    /**
     * Returns the value of field 'cfClassDescr'.
     * 
     * @return the value of field 'CfClassDescr'.
     */
    public gr.ekt.cerif.schema.CfClassDescr getCfClassDescr() {
        return this.cfClassDescr;
    }

    /**
     * Returns the value of field 'cfClassEx'.
     * 
     * @return the value of field 'CfClassEx'.
     */
    public gr.ekt.cerif.schema.CfClassEx getCfClassEx() {
        return this.cfClassEx;
    }

    /**
     * Returns the value of field 'cfClassScheme'.
     * 
     * @return the value of field 'CfClassScheme'.
     */
    public gr.ekt.cerif.schema.CfClassScheme getCfClassScheme() {
        return this.cfClassScheme;
    }

    /**
     * Returns the value of field 'cfClassSchemeDescr'.
     * 
     * @return the value of field 'CfClassSchemeDescr'.
     */
    public gr.ekt.cerif.schema.CfClassSchemeDescr getCfClassSchemeDescr() {
        return this.cfClassSchemeDescr;
    }

    /**
     * Returns the value of field 'cfClassSchemeName'.
     * 
     * @return the value of field 'CfClassSchemeName'.
     */
    public gr.ekt.cerif.schema.CfClassSchemeName getCfClassSchemeName() {
        return this.cfClassSchemeName;
    }

    /**
     * Returns the value of field 'cfClassScheme_ClassScheme'.
     * 
     * @return the value of field 'CfClassScheme_ClassScheme'.
     */
    public gr.ekt.cerif.schema.CfClassScheme_ClassScheme getCfClassScheme_ClassScheme() {
        return this.cfClassScheme_ClassScheme;
    }

    /**
     * Returns the value of field 'cfClassTerm'.
     * 
     * @return the value of field 'CfClassTerm'.
     */
    public gr.ekt.cerif.schema.CfClassTerm getCfClassTerm() {
        return this.cfClassTerm;
    }

    /**
     * Returns the value of field 'cfClass_Class'.
     * 
     * @return the value of field 'CfClass_Class'.
     */
    public gr.ekt.cerif.schema.CfClass_Class getCfClass_Class() {
        return this.cfClass_Class;
    }

    /**
     * Returns the value of field 'cfCountry'.
     * 
     * @return the value of field 'CfCountry'.
     */
    public gr.ekt.cerif.schema.CfCountry getCfCountry() {
        return this.cfCountry;
    }

    /**
     * Returns the value of field 'cfCountryName'.
     * 
     * @return the value of field 'CfCountryName'.
     */
    public gr.ekt.cerif.schema.CfCountryName getCfCountryName() {
        return this.cfCountryName;
    }

    /**
     * Returns the value of field 'cfCountry_Class'.
     * 
     * @return the value of field 'CfCountry_Class'.
     */
    public gr.ekt.cerif.schema.CfCountry_Class getCfCountry_Class() {
        return this.cfCountry_Class;
    }

    /**
     * Returns the value of field 'cfCurrency'.
     * 
     * @return the value of field 'CfCurrency'.
     */
    public gr.ekt.cerif.schema.CfCurrency getCfCurrency() {
        return this.cfCurrency;
    }

    /**
     * Returns the value of field 'cfCurrencyEntName'.
     * 
     * @return the value of field 'CfCurrencyEntName'.
     */
    public gr.ekt.cerif.schema.CfCurrencyEntName getCfCurrencyEntName() {
        return this.cfCurrencyEntName;
    }

    /**
     * Returns the value of field 'cfCurrencyName'.
     * 
     * @return the value of field 'CfCurrencyName'.
     */
    public gr.ekt.cerif.schema.CfCurrencyName getCfCurrencyName() {
        return this.cfCurrencyName;
    }

    /**
     * Returns the value of field 'cfCurrency_Class'.
     * 
     * @return the value of field 'CfCurrency_Class'.
     */
    public gr.ekt.cerif.schema.CfCurrency_Class getCfCurrency_Class() {
        return this.cfCurrency_Class;
    }

    /**
     * Returns the value of field 'cfDC'.
     * 
     * @return the value of field 'CfDC'.
     */
    public gr.ekt.cerif.schema.CfDC getCfDC() {
        return this.cfDC;
    }

    /**
     * Returns the value of field 'cfDCAudience'.
     * 
     * @return the value of field 'CfDCAudience'.
     */
    public gr.ekt.cerif.schema.CfDCAudience getCfDCAudience() {
        return this.cfDCAudience;
    }

    /**
     * Returns the value of field 'cfDCContributor'.
     * 
     * @return the value of field 'CfDCContributor'.
     */
    public gr.ekt.cerif.schema.CfDCContributor getCfDCContributor() {
        return this.cfDCContributor;
    }

    /**
     * Returns the value of field 'cfDCCoverage'.
     * 
     * @return the value of field 'CfDCCoverage'.
     */
    public gr.ekt.cerif.schema.CfDCCoverage getCfDCCoverage() {
        return this.cfDCCoverage;
    }

    /**
     * Returns the value of field 'cfDCCoverageSpatial'.
     * 
     * @return the value of field 'CfDCCoverageSpatial'.
     */
    public gr.ekt.cerif.schema.CfDCCoverageSpatial getCfDCCoverageSpatial() {
        return this.cfDCCoverageSpatial;
    }

    /**
     * Returns the value of field 'cfDCCoverageTemporal'.
     * 
     * @return the value of field 'CfDCCoverageTemporal'.
     */
    public gr.ekt.cerif.schema.CfDCCoverageTemporal getCfDCCoverageTemporal() {
        return this.cfDCCoverageTemporal;
    }

    /**
     * Returns the value of field 'cfDCCreator'.
     * 
     * @return the value of field 'CfDCCreator'.
     */
    public gr.ekt.cerif.schema.CfDCCreator getCfDCCreator() {
        return this.cfDCCreator;
    }

    /**
     * Returns the value of field 'cfDCDate'.
     * 
     * @return the value of field 'CfDCDate'.
     */
    public gr.ekt.cerif.schema.CfDCDate getCfDCDate() {
        return this.cfDCDate;
    }

    /**
     * Returns the value of field 'cfDCDescription'.
     * 
     * @return the value of field 'CfDCDescription'.
     */
    public gr.ekt.cerif.schema.CfDCDescription getCfDCDescription() {
        return this.cfDCDescription;
    }

    /**
     * Returns the value of field 'cfDCFormat'.
     * 
     * @return the value of field 'CfDCFormat'.
     */
    public gr.ekt.cerif.schema.CfDCFormat getCfDCFormat() {
        return this.cfDCFormat;
    }

    /**
     * Returns the value of field 'cfDCLanguage'.
     * 
     * @return the value of field 'CfDCLanguage'.
     */
    public gr.ekt.cerif.schema.CfDCLanguage getCfDCLanguage() {
        return this.cfDCLanguage;
    }

    /**
     * Returns the value of field 'cfDCProvenance'.
     * 
     * @return the value of field 'CfDCProvenance'.
     */
    public gr.ekt.cerif.schema.CfDCProvenance getCfDCProvenance() {
        return this.cfDCProvenance;
    }

    /**
     * Returns the value of field 'cfDCPublisher'.
     * 
     * @return the value of field 'CfDCPublisher'.
     */
    public gr.ekt.cerif.schema.CfDCPublisher getCfDCPublisher() {
        return this.cfDCPublisher;
    }

    /**
     * Returns the value of field 'cfDCRelation'.
     * 
     * @return the value of field 'CfDCRelation'.
     */
    public gr.ekt.cerif.schema.CfDCRelation getCfDCRelation() {
        return this.cfDCRelation;
    }

    /**
     * Returns the value of field 'cfDCResourceIdentifier'.
     * 
     * @return the value of field 'CfDCResourceIdentifier'.
     */
    public gr.ekt.cerif.schema.CfDCResourceIdentifier getCfDCResourceIdentifier() {
        return this.cfDCResourceIdentifier;
    }

    /**
     * Returns the value of field 'cfDCResourceType'.
     * 
     * @return the value of field 'CfDCResourceType'.
     */
    public gr.ekt.cerif.schema.CfDCResourceType getCfDCResourceType() {
        return this.cfDCResourceType;
    }

    /**
     * Returns the value of field 'cfDCRightsHolder'.
     * 
     * @return the value of field 'CfDCRightsHolder'.
     */
    public gr.ekt.cerif.schema.CfDCRightsHolder getCfDCRightsHolder() {
        return this.cfDCRightsHolder;
    }

    /**
     * Returns the value of field 'cfDCRightsMM'.
     * 
     * @return the value of field 'CfDCRightsMM'.
     */
    public gr.ekt.cerif.schema.CfDCRightsMM getCfDCRightsMM() {
        return this.cfDCRightsMM;
    }

    /**
     * Returns the value of field 'cfDCRightsMMAccessRights'.
     * 
     * @return the value of field 'CfDCRightsMMAccessRights'.
     */
    public gr.ekt.cerif.schema.CfDCRightsMMAccessRights getCfDCRightsMMAccessRights() {
        return this.cfDCRightsMMAccessRights;
    }

    /**
     * Returns the value of field 'cfDCRightsMMLicense'.
     * 
     * @return the value of field 'CfDCRightsMMLicense'.
     */
    public gr.ekt.cerif.schema.CfDCRightsMMLicense getCfDCRightsMMLicense() {
        return this.cfDCRightsMMLicense;
    }

    /**
     * Returns the value of field 'cfDCSource'.
     * 
     * @return the value of field 'CfDCSource'.
     */
    public gr.ekt.cerif.schema.CfDCSource getCfDCSource() {
        return this.cfDCSource;
    }

    /**
     * Returns the value of field 'cfDCSubject'.
     * 
     * @return the value of field 'CfDCSubject'.
     */
    public gr.ekt.cerif.schema.CfDCSubject getCfDCSubject() {
        return this.cfDCSubject;
    }

    /**
     * Returns the value of field 'cfDCTitle'.
     * 
     * @return the value of field 'CfDCTitle'.
     */
    public gr.ekt.cerif.schema.CfDCTitle getCfDCTitle() {
        return this.cfDCTitle;
    }

    /**
     * Returns the value of field 'cfEAddr'.
     * 
     * @return the value of field 'CfEAddr'.
     */
    public gr.ekt.cerif.schema.CfEAddr getCfEAddr() {
        return this.cfEAddr;
    }

    /**
     * Returns the value of field 'cfEAddr_Class'.
     * 
     * @return the value of field 'CfEAddr_Class'.
     */
    public gr.ekt.cerif.schema.CfEAddr_Class getCfEAddr_Class() {
        return this.cfEAddr_Class;
    }

    /**
     * Returns the value of field 'cfEquip'.
     * 
     * @return the value of field 'CfEquip'.
     */
    public gr.ekt.cerif.schema.CfEquip getCfEquip() {
        return this.cfEquip;
    }

    /**
     * Returns the value of field 'cfEquipDescr'.
     * 
     * @return the value of field 'CfEquipDescr'.
     */
    public gr.ekt.cerif.schema.CfEquipDescr getCfEquipDescr() {
        return this.cfEquipDescr;
    }

    /**
     * Returns the value of field 'cfEquipKeyw'.
     * 
     * @return the value of field 'CfEquipKeyw'.
     */
    public gr.ekt.cerif.schema.CfEquipKeyw getCfEquipKeyw() {
        return this.cfEquipKeyw;
    }

    /**
     * Returns the value of field 'cfEquipName'.
     * 
     * @return the value of field 'CfEquipName'.
     */
    public gr.ekt.cerif.schema.CfEquipName getCfEquipName() {
        return this.cfEquipName;
    }

    /**
     * Returns the value of field 'cfEquip_Class'.
     * 
     * @return the value of field 'CfEquip_Class'.
     */
    public gr.ekt.cerif.schema.CfEquip_Class getCfEquip_Class() {
        return this.cfEquip_Class;
    }

    /**
     * Returns the value of field 'cfEquip_Equip'.
     * 
     * @return the value of field 'CfEquip_Equip'.
     */
    public gr.ekt.cerif.schema.CfEquip_Equip getCfEquip_Equip() {
        return this.cfEquip_Equip;
    }

    /**
     * Returns the value of field 'cfEquip_Event'.
     * 
     * @return the value of field 'CfEquip_Event'.
     */
    public gr.ekt.cerif.schema.CfEquip_Event getCfEquip_Event() {
        return this.cfEquip_Event;
    }

    /**
     * Returns the value of field 'cfEquip_Fund'.
     * 
     * @return the value of field 'CfEquip_Fund'.
     */
    public gr.ekt.cerif.schema.CfEquip_Fund getCfEquip_Fund() {
        return this.cfEquip_Fund;
    }

    /**
     * Returns the value of field 'cfEquip_Indic'.
     * 
     * @return the value of field 'CfEquip_Indic'.
     */
    public gr.ekt.cerif.schema.CfEquip_Indic getCfEquip_Indic() {
        return this.cfEquip_Indic;
    }

    /**
     * Returns the value of field 'cfEquip_Meas'.
     * 
     * @return the value of field 'CfEquip_Meas'.
     */
    public gr.ekt.cerif.schema.CfEquip_Meas getCfEquip_Meas() {
        return this.cfEquip_Meas;
    }

    /**
     * Returns the value of field 'cfEquip_Medium'.
     * 
     * @return the value of field 'CfEquip_Medium'.
     */
    public gr.ekt.cerif.schema.CfEquip_Medium getCfEquip_Medium() {
        return this.cfEquip_Medium;
    }

    /**
     * Returns the value of field 'cfEquip_PAddr'.
     * 
     * @return the value of field 'CfEquip_PAddr'.
     */
    public gr.ekt.cerif.schema.CfEquip_PAddr getCfEquip_PAddr() {
        return this.cfEquip_PAddr;
    }

    /**
     * Returns the value of field 'cfEquip_Srv'.
     * 
     * @return the value of field 'CfEquip_Srv'.
     */
    public gr.ekt.cerif.schema.CfEquip_Srv getCfEquip_Srv() {
        return this.cfEquip_Srv;
    }

    /**
     * Returns the value of field 'cfEvent'.
     * 
     * @return the value of field 'CfEvent'.
     */
    public gr.ekt.cerif.schema.CfEvent getCfEvent() {
        return this.cfEvent;
    }

    /**
     * Returns the value of field 'cfEventDescr'.
     * 
     * @return the value of field 'CfEventDescr'.
     */
    public gr.ekt.cerif.schema.CfEventDescr getCfEventDescr() {
        return this.cfEventDescr;
    }

    /**
     * Returns the value of field 'cfEventKeyw'.
     * 
     * @return the value of field 'CfEventKeyw'.
     */
    public gr.ekt.cerif.schema.CfEventKeyw getCfEventKeyw() {
        return this.cfEventKeyw;
    }

    /**
     * Returns the value of field 'cfEventName'.
     * 
     * @return the value of field 'CfEventName'.
     */
    public gr.ekt.cerif.schema.CfEventName getCfEventName() {
        return this.cfEventName;
    }

    /**
     * Returns the value of field 'cfEvent_Class'.
     * 
     * @return the value of field 'CfEvent_Class'.
     */
    public gr.ekt.cerif.schema.CfEvent_Class getCfEvent_Class() {
        return this.cfEvent_Class;
    }

    /**
     * Returns the value of field 'cfEvent_Event'.
     * 
     * @return the value of field 'CfEvent_Event'.
     */
    public gr.ekt.cerif.schema.CfEvent_Event getCfEvent_Event() {
        return this.cfEvent_Event;
    }

    /**
     * Returns the value of field 'cfEvent_Fund'.
     * 
     * @return the value of field 'CfEvent_Fund'.
     */
    public gr.ekt.cerif.schema.CfEvent_Fund getCfEvent_Fund() {
        return this.cfEvent_Fund;
    }

    /**
     * Returns the value of field 'cfEvent_Indic'.
     * 
     * @return the value of field 'CfEvent_Indic'.
     */
    public gr.ekt.cerif.schema.CfEvent_Indic getCfEvent_Indic() {
        return this.cfEvent_Indic;
    }

    /**
     * Returns the value of field 'cfEvent_Meas'.
     * 
     * @return the value of field 'CfEvent_Meas'.
     */
    public gr.ekt.cerif.schema.CfEvent_Meas getCfEvent_Meas() {
        return this.cfEvent_Meas;
    }

    /**
     * Returns the value of field 'cfEvent_Medium'.
     * 
     * @return the value of field 'CfEvent_Medium'.
     */
    public gr.ekt.cerif.schema.CfEvent_Medium getCfEvent_Medium() {
        return this.cfEvent_Medium;
    }

    /**
     * Returns the value of field 'cfExpSkills'.
     * 
     * @return the value of field 'CfExpSkills'.
     */
    public gr.ekt.cerif.schema.CfExpSkills getCfExpSkills() {
        return this.cfExpSkills;
    }

    /**
     * Returns the value of field 'cfExpSkillsDescr'.
     * 
     * @return the value of field 'CfExpSkillsDescr'.
     */
    public gr.ekt.cerif.schema.CfExpSkillsDescr getCfExpSkillsDescr() {
        return this.cfExpSkillsDescr;
    }

    /**
     * Returns the value of field 'cfExpSkillsKeyw'.
     * 
     * @return the value of field 'CfExpSkillsKeyw'.
     */
    public gr.ekt.cerif.schema.CfExpSkillsKeyw getCfExpSkillsKeyw() {
        return this.cfExpSkillsKeyw;
    }

    /**
     * Returns the value of field 'cfExpSkillsName'.
     * 
     * @return the value of field 'CfExpSkillsName'.
     */
    public gr.ekt.cerif.schema.CfExpSkillsName getCfExpSkillsName() {
        return this.cfExpSkillsName;
    }

    /**
     * Returns the value of field 'cfExpSkills_Class'.
     * 
     * @return the value of field 'CfExpSkills_Class'.
     */
    public gr.ekt.cerif.schema.CfExpSkills_Class getCfExpSkills_Class() {
        return this.cfExpSkills_Class;
    }

    /**
     * Returns the value of field 'cfFDCRightsMMPricing'.
     * 
     * @return the value of field 'CfFDCRightsMMPricing'.
     */
    public gr.ekt.cerif.schema.CfFDCRightsMMPricing getCfFDCRightsMMPricing() {
        return this.cfFDCRightsMMPricing;
    }

    /**
     * Returns the value of field 'cfFDCRightsMMPrivacy'.
     * 
     * @return the value of field 'CfFDCRightsMMPrivacy'.
     */
    public gr.ekt.cerif.schema.CfFDCRightsMMPrivacy getCfFDCRightsMMPrivacy() {
        return this.cfFDCRightsMMPrivacy;
    }

    /**
     * Returns the value of field 'cfFDCRightsMMRights'.
     * 
     * @return the value of field 'CfFDCRightsMMRights'.
     */
    public gr.ekt.cerif.schema.CfFDCRightsMMRights getCfFDCRightsMMRights() {
        return this.cfFDCRightsMMRights;
    }

    /**
     * Returns the value of field 'cfFDCRightsMMSecurity'.
     * 
     * @return the value of field 'CfFDCRightsMMSecurity'.
     */
    public gr.ekt.cerif.schema.CfFDCRightsMMSecurity getCfFDCRightsMMSecurity() {
        return this.cfFDCRightsMMSecurity;
    }

    /**
     * Returns the value of field 'cfFacil'.
     * 
     * @return the value of field 'CfFacil'.
     */
    public gr.ekt.cerif.schema.CfFacil getCfFacil() {
        return this.cfFacil;
    }

    /**
     * Returns the value of field 'cfFacilDescr'.
     * 
     * @return the value of field 'CfFacilDescr'.
     */
    public gr.ekt.cerif.schema.CfFacilDescr getCfFacilDescr() {
        return this.cfFacilDescr;
    }

    /**
     * Returns the value of field 'cfFacilKeyw'.
     * 
     * @return the value of field 'CfFacilKeyw'.
     */
    public gr.ekt.cerif.schema.CfFacilKeyw getCfFacilKeyw() {
        return this.cfFacilKeyw;
    }

    /**
     * Returns the value of field 'cfFacilName'.
     * 
     * @return the value of field 'CfFacilName'.
     */
    public gr.ekt.cerif.schema.CfFacilName getCfFacilName() {
        return this.cfFacilName;
    }

    /**
     * Returns the value of field 'cfFacil_Class'.
     * 
     * @return the value of field 'CfFacil_Class'.
     */
    public gr.ekt.cerif.schema.CfFacil_Class getCfFacil_Class() {
        return this.cfFacil_Class;
    }

    /**
     * Returns the value of field 'cfFacil_Equip'.
     * 
     * @return the value of field 'CfFacil_Equip'.
     */
    public gr.ekt.cerif.schema.CfFacil_Equip getCfFacil_Equip() {
        return this.cfFacil_Equip;
    }

    /**
     * Returns the value of field 'cfFacil_Event'.
     * 
     * @return the value of field 'CfFacil_Event'.
     */
    public gr.ekt.cerif.schema.CfFacil_Event getCfFacil_Event() {
        return this.cfFacil_Event;
    }

    /**
     * Returns the value of field 'cfFacil_Facil'.
     * 
     * @return the value of field 'CfFacil_Facil'.
     */
    public gr.ekt.cerif.schema.CfFacil_Facil getCfFacil_Facil() {
        return this.cfFacil_Facil;
    }

    /**
     * Returns the value of field 'cfFacil_Fund'.
     * 
     * @return the value of field 'CfFacil_Fund'.
     */
    public gr.ekt.cerif.schema.CfFacil_Fund getCfFacil_Fund() {
        return this.cfFacil_Fund;
    }

    /**
     * Returns the value of field 'cfFacil_Indic'.
     * 
     * @return the value of field 'CfFacil_Indic'.
     */
    public gr.ekt.cerif.schema.CfFacil_Indic getCfFacil_Indic() {
        return this.cfFacil_Indic;
    }

    /**
     * Returns the value of field 'cfFacil_Meas'.
     * 
     * @return the value of field 'CfFacil_Meas'.
     */
    public gr.ekt.cerif.schema.CfFacil_Meas getCfFacil_Meas() {
        return this.cfFacil_Meas;
    }

    /**
     * Returns the value of field 'cfFacil_Medium'.
     * 
     * @return the value of field 'CfFacil_Medium'.
     */
    public gr.ekt.cerif.schema.CfFacil_Medium getCfFacil_Medium() {
        return this.cfFacil_Medium;
    }

    /**
     * Returns the value of field 'cfFacil_PAddr'.
     * 
     * @return the value of field 'CfFacil_PAddr'.
     */
    public gr.ekt.cerif.schema.CfFacil_PAddr getCfFacil_PAddr() {
        return this.cfFacil_PAddr;
    }

    /**
     * Returns the value of field 'cfFacil_Srv'.
     * 
     * @return the value of field 'CfFacil_Srv'.
     */
    public gr.ekt.cerif.schema.CfFacil_Srv getCfFacil_Srv() {
        return this.cfFacil_Srv;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfFedId_Class'.
     * 
     * @return the value of field 'CfFedId_Class'.
     */
    public gr.ekt.cerif.schema.CfFedId_Class getCfFedId_Class() {
        return this.cfFedId_Class;
    }

    /**
     * Returns the value of field 'cfFedId_Srv'.
     * 
     * @return the value of field 'CfFedId_Srv'.
     */
    public gr.ekt.cerif.schema.CfFedId_Srv getCfFedId_Srv() {
        return this.cfFedId_Srv;
    }

    /**
     * Returns the value of field 'cfFund'.
     * 
     * @return the value of field 'CfFund'.
     */
    public gr.ekt.cerif.schema.CfFund getCfFund() {
        return this.cfFund;
    }

    /**
     * Returns the value of field 'cfFundDescr'.
     * 
     * @return the value of field 'CfFundDescr'.
     */
    public gr.ekt.cerif.schema.CfFundDescr getCfFundDescr() {
        return this.cfFundDescr;
    }

    /**
     * Returns the value of field 'cfFundKeyw'.
     * 
     * @return the value of field 'CfFundKeyw'.
     */
    public gr.ekt.cerif.schema.CfFundKeyw getCfFundKeyw() {
        return this.cfFundKeyw;
    }

    /**
     * Returns the value of field 'cfFundName'.
     * 
     * @return the value of field 'CfFundName'.
     */
    public gr.ekt.cerif.schema.CfFundName getCfFundName() {
        return this.cfFundName;
    }

    /**
     * Returns the value of field 'cfFund_Class'.
     * 
     * @return the value of field 'CfFund_Class'.
     */
    public gr.ekt.cerif.schema.CfFund_Class getCfFund_Class() {
        return this.cfFund_Class;
    }

    /**
     * Returns the value of field 'cfFund_Fund'.
     * 
     * @return the value of field 'CfFund_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund_Fund getCfFund_Fund() {
        return this.cfFund_Fund;
    }

    /**
     * Returns the value of field 'cfFund_Indic'.
     * 
     * @return the value of field 'CfFund_Indic'.
     */
    public gr.ekt.cerif.schema.CfFund_Indic getCfFund_Indic() {
        return this.cfFund_Indic;
    }

    /**
     * Returns the value of field 'cfFund_Meas'.
     * 
     * @return the value of field 'CfFund_Meas'.
     */
    public gr.ekt.cerif.schema.CfFund_Meas getCfFund_Meas() {
        return this.cfFund_Meas;
    }

    /**
     * Returns the value of field 'cfGeoBBox'.
     * 
     * @return the value of field 'CfGeoBBox'.
     */
    public gr.ekt.cerif.schema.CfGeoBBox getCfGeoBBox() {
        return this.cfGeoBBox;
    }

    /**
     * Returns the value of field 'cfGeoBBoxDescr'.
     * 
     * @return the value of field 'CfGeoBBoxDescr'.
     */
    public gr.ekt.cerif.schema.CfGeoBBoxDescr getCfGeoBBoxDescr() {
        return this.cfGeoBBoxDescr;
    }

    /**
     * Returns the value of field 'cfGeoBBoxKeyw'.
     * 
     * @return the value of field 'CfGeoBBoxKeyw'.
     */
    public gr.ekt.cerif.schema.CfGeoBBoxKeyw getCfGeoBBoxKeyw() {
        return this.cfGeoBBoxKeyw;
    }

    /**
     * Returns the value of field 'cfGeoBBoxName'.
     * 
     * @return the value of field 'CfGeoBBoxName'.
     */
    public gr.ekt.cerif.schema.CfGeoBBoxName getCfGeoBBoxName() {
        return this.cfGeoBBoxName;
    }

    /**
     * Returns the value of field 'cfGeoBBox_Class'.
     * 
     * @return the value of field 'CfGeoBBox_Class'.
     */
    public gr.ekt.cerif.schema.CfGeoBBox_Class getCfGeoBBox_Class() {
        return this.cfGeoBBox_Class;
    }

    /**
     * Returns the value of field 'cfGeoBBox_GeoBBox'.
     * 
     * @return the value of field 'CfGeoBBox_GeoBBox'.
     */
    public gr.ekt.cerif.schema.CfGeoBBox_GeoBBox getCfGeoBBox_GeoBBox() {
        return this.cfGeoBBox_GeoBBox;
    }

    /**
     * Returns the value of field 'cfGeoBBox_Meas'.
     * 
     * @return the value of field 'CfGeoBBox_Meas'.
     */
    public gr.ekt.cerif.schema.CfGeoBBox_Meas getCfGeoBBox_Meas() {
        return this.cfGeoBBox_Meas;
    }

    /**
     * Returns the value of field 'cfIndic'.
     * 
     * @return the value of field 'CfIndic'.
     */
    public gr.ekt.cerif.schema.CfIndic getCfIndic() {
        return this.cfIndic;
    }

    /**
     * Returns the value of field 'cfIndicDescr'.
     * 
     * @return the value of field 'CfIndicDescr'.
     */
    public gr.ekt.cerif.schema.CfIndicDescr getCfIndicDescr() {
        return this.cfIndicDescr;
    }

    /**
     * Returns the value of field 'cfIndicKeyw'.
     * 
     * @return the value of field 'CfIndicKeyw'.
     */
    public gr.ekt.cerif.schema.CfIndicKeyw getCfIndicKeyw() {
        return this.cfIndicKeyw;
    }

    /**
     * Returns the value of field 'cfIndicName'.
     * 
     * @return the value of field 'CfIndicName'.
     */
    public gr.ekt.cerif.schema.CfIndicName getCfIndicName() {
        return this.cfIndicName;
    }

    /**
     * Returns the value of field 'cfIndic_Class'.
     * 
     * @return the value of field 'CfIndic_Class'.
     */
    public gr.ekt.cerif.schema.CfIndic_Class getCfIndic_Class() {
        return this.cfIndic_Class;
    }

    /**
     * Returns the value of field 'cfIndic_Indic'.
     * 
     * @return the value of field 'CfIndic_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic_Indic getCfIndic_Indic() {
        return this.cfIndic_Indic;
    }

    /**
     * Returns the value of field 'cfIndic_Meas'.
     * 
     * @return the value of field 'CfIndic_Meas'.
     */
    public gr.ekt.cerif.schema.CfIndic_Meas getCfIndic_Meas() {
        return this.cfIndic_Meas;
    }

    /**
     * Returns the value of field 'cfLang'.
     * 
     * @return the value of field 'CfLang'.
     */
    public gr.ekt.cerif.schema.CfLang getCfLang() {
        return this.cfLang;
    }

    /**
     * Returns the value of field 'cfLangName'.
     * 
     * @return the value of field 'CfLangName'.
     */
    public gr.ekt.cerif.schema.CfLangName getCfLangName() {
        return this.cfLangName;
    }

    /**
     * Returns the value of field 'cfLang_Class'.
     * 
     * @return the value of field 'CfLang_Class'.
     */
    public gr.ekt.cerif.schema.CfLang_Class getCfLang_Class() {
        return this.cfLang_Class;
    }

    /**
     * Returns the value of field 'cfMeas'.
     * 
     * @return the value of field 'CfMeas'.
     */
    public gr.ekt.cerif.schema.CfMeas getCfMeas() {
        return this.cfMeas;
    }

    /**
     * Returns the value of field 'cfMeasDescr'.
     * 
     * @return the value of field 'CfMeasDescr'.
     */
    public gr.ekt.cerif.schema.CfMeasDescr getCfMeasDescr() {
        return this.cfMeasDescr;
    }

    /**
     * Returns the value of field 'cfMeasKeyw'.
     * 
     * @return the value of field 'CfMeasKeyw'.
     */
    public gr.ekt.cerif.schema.CfMeasKeyw getCfMeasKeyw() {
        return this.cfMeasKeyw;
    }

    /**
     * Returns the value of field 'cfMeasName'.
     * 
     * @return the value of field 'CfMeasName'.
     */
    public gr.ekt.cerif.schema.CfMeasName getCfMeasName() {
        return this.cfMeasName;
    }

    /**
     * Returns the value of field 'cfMeas_Class'.
     * 
     * @return the value of field 'CfMeas_Class'.
     */
    public gr.ekt.cerif.schema.CfMeas_Class getCfMeas_Class() {
        return this.cfMeas_Class;
    }

    /**
     * Returns the value of field 'cfMeas_Meas'.
     * 
     * @return the value of field 'CfMeas_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas_Meas getCfMeas_Meas() {
        return this.cfMeas_Meas;
    }

    /**
     * Returns the value of field 'cfMedium'.
     * 
     * @return the value of field 'CfMedium'.
     */
    public gr.ekt.cerif.schema.CfMedium getCfMedium() {
        return this.cfMedium;
    }

    /**
     * Returns the value of field 'cfMediumDescr'.
     * 
     * @return the value of field 'CfMediumDescr'.
     */
    public gr.ekt.cerif.schema.CfMediumDescr getCfMediumDescr() {
        return this.cfMediumDescr;
    }

    /**
     * Returns the value of field 'cfMediumKeyw'.
     * 
     * @return the value of field 'CfMediumKeyw'.
     */
    public gr.ekt.cerif.schema.CfMediumKeyw getCfMediumKeyw() {
        return this.cfMediumKeyw;
    }

    /**
     * Returns the value of field 'cfMediumTitle'.
     * 
     * @return the value of field 'CfMediumTitle'.
     */
    public gr.ekt.cerif.schema.CfMediumTitle getCfMediumTitle() {
        return this.cfMediumTitle;
    }

    /**
     * Returns the value of field 'cfMedium_Class'.
     * 
     * @return the value of field 'CfMedium_Class'.
     */
    public gr.ekt.cerif.schema.CfMedium_Class getCfMedium_Class() {
        return this.cfMedium_Class;
    }

    /**
     * Returns the value of field 'cfMedium_Fund'.
     * 
     * @return the value of field 'CfMedium_Fund'.
     */
    public gr.ekt.cerif.schema.CfMedium_Fund getCfMedium_Fund() {
        return this.cfMedium_Fund;
    }

    /**
     * Returns the value of field 'cfMedium_Indic'.
     * 
     * @return the value of field 'CfMedium_Indic'.
     */
    public gr.ekt.cerif.schema.CfMedium_Indic getCfMedium_Indic() {
        return this.cfMedium_Indic;
    }

    /**
     * Returns the value of field 'cfMedium_Meas'.
     * 
     * @return the value of field 'CfMedium_Meas'.
     */
    public gr.ekt.cerif.schema.CfMedium_Meas getCfMedium_Meas() {
        return this.cfMedium_Meas;
    }

    /**
     * Returns the value of field 'cfMedium_Medium'.
     * 
     * @return the value of field 'CfMedium_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium_Medium getCfMedium_Medium() {
        return this.cfMedium_Medium;
    }

    /**
     * Returns the value of field 'cfMetrics'.
     * 
     * @return the value of field 'CfMetrics'.
     */
    public gr.ekt.cerif.schema.CfMetrics getCfMetrics() {
        return this.cfMetrics;
    }

    /**
     * Returns the value of field 'cfMetricsDescr'.
     * 
     * @return the value of field 'CfMetricsDescr'.
     */
    public gr.ekt.cerif.schema.CfMetricsDescr getCfMetricsDescr() {
        return this.cfMetricsDescr;
    }

    /**
     * Returns the value of field 'cfMetricsKeyw'.
     * 
     * @return the value of field 'CfMetricsKeyw'.
     */
    public gr.ekt.cerif.schema.CfMetricsKeyw getCfMetricsKeyw() {
        return this.cfMetricsKeyw;
    }

    /**
     * Returns the value of field 'cfMetricsName'.
     * 
     * @return the value of field 'CfMetricsName'.
     */
    public gr.ekt.cerif.schema.CfMetricsName getCfMetricsName() {
        return this.cfMetricsName;
    }

    /**
     * Returns the value of field 'cfMetrics_Class'.
     * 
     * @return the value of field 'CfMetrics_Class'.
     */
    public gr.ekt.cerif.schema.CfMetrics_Class getCfMetrics_Class() {
        return this.cfMetrics_Class;
    }

    /**
     * Returns the value of field 'cfOrgUnit'.
     * 
     * @return the value of field 'CfOrgUnit'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit getCfOrgUnit() {
        return this.cfOrgUnit;
    }

    /**
     * Returns the value of field 'cfOrgUnitKeyw'.
     * 
     * @return the value of field 'CfOrgUnitKeyw'.
     */
    public gr.ekt.cerif.schema.CfOrgUnitKeyw getCfOrgUnitKeyw() {
        return this.cfOrgUnitKeyw;
    }

    /**
     * Returns the value of field 'cfOrgUnitName'.
     * 
     * @return the value of field 'CfOrgUnitName'.
     */
    public gr.ekt.cerif.schema.CfOrgUnitName getCfOrgUnitName() {
        return this.cfOrgUnitName;
    }

    /**
     * Returns the value of field 'cfOrgUnitResAct'.
     * 
     * @return the value of field 'CfOrgUnitResAct'.
     */
    public gr.ekt.cerif.schema.CfOrgUnitResAct getCfOrgUnitResAct() {
        return this.cfOrgUnitResAct;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Class'.
     * 
     * @return the value of field 'CfOrgUnit_Class'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_Class getCfOrgUnit_Class() {
        return this.cfOrgUnit_Class;
    }

    /**
     * Returns the value of field 'cfOrgUnit_DC'.
     * 
     * @return the value of field 'CfOrgUnit_DC'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_DC getCfOrgUnit_DC() {
        return this.cfOrgUnit_DC;
    }

    /**
     * Returns the value of field 'cfOrgUnit_EAddr'.
     * 
     * @return the value of field 'CfOrgUnit_EAddr'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_EAddr getCfOrgUnit_EAddr() {
        return this.cfOrgUnit_EAddr;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Equip'.
     * 
     * @return the value of field 'CfOrgUnit_Equip'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_Equip getCfOrgUnit_Equip() {
        return this.cfOrgUnit_Equip;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Event'.
     * 
     * @return the value of field 'CfOrgUnit_Event'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_Event getCfOrgUnit_Event() {
        return this.cfOrgUnit_Event;
    }

    /**
     * Returns the value of field 'cfOrgUnit_ExpSkills'.
     * 
     * @return the value of field 'CfOrgUnit_ExpSkills'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_ExpSkills getCfOrgUnit_ExpSkills() {
        return this.cfOrgUnit_ExpSkills;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Facil'.
     * 
     * @return the value of field 'CfOrgUnit_Facil'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_Facil getCfOrgUnit_Facil() {
        return this.cfOrgUnit_Facil;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Fund'.
     * 
     * @return the value of field 'CfOrgUnit_Fund'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_Fund getCfOrgUnit_Fund() {
        return this.cfOrgUnit_Fund;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Indic'.
     * 
     * @return the value of field 'CfOrgUnit_Indic'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_Indic getCfOrgUnit_Indic() {
        return this.cfOrgUnit_Indic;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Meas'.
     * 
     * @return the value of field 'CfOrgUnit_Meas'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_Meas getCfOrgUnit_Meas() {
        return this.cfOrgUnit_Meas;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Medium'.
     * 
     * @return the value of field 'CfOrgUnit_Medium'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_Medium getCfOrgUnit_Medium() {
        return this.cfOrgUnit_Medium;
    }

    /**
     * Returns the value of field 'cfOrgUnit_OrgUnit'.
     * 
     * @return the value of field 'CfOrgUnit_OrgUnit'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_OrgUnit getCfOrgUnit_OrgUnit() {
        return this.cfOrgUnit_OrgUnit;
    }

    /**
     * Returns the value of field 'cfOrgUnit_PAddr'.
     * 
     * @return the value of field 'CfOrgUnit_PAddr'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_PAddr getCfOrgUnit_PAddr() {
        return this.cfOrgUnit_PAddr;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Prize'.
     * 
     * @return the value of field 'CfOrgUnit_Prize'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_Prize getCfOrgUnit_Prize() {
        return this.cfOrgUnit_Prize;
    }

    /**
     * Returns the value of field 'cfOrgUnit_ResPat'.
     * 
     * @return the value of field 'CfOrgUnit_ResPat'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_ResPat getCfOrgUnit_ResPat() {
        return this.cfOrgUnit_ResPat;
    }

    /**
     * Returns the value of field 'cfOrgUnit_ResProd'.
     * 
     * @return the value of field 'CfOrgUnit_ResProd'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_ResProd getCfOrgUnit_ResProd() {
        return this.cfOrgUnit_ResProd;
    }

    /**
     * Returns the value of field 'cfOrgUnit_ResPubl'.
     * 
     * @return the value of field 'CfOrgUnit_ResPubl'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_ResPubl getCfOrgUnit_ResPubl() {
        return this.cfOrgUnit_ResPubl;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Srv'.
     * 
     * @return the value of field 'CfOrgUnit_Srv'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit_Srv getCfOrgUnit_Srv() {
        return this.cfOrgUnit_Srv;
    }

    /**
     * Returns the value of field 'cfPAddr'.
     * 
     * @return the value of field 'CfPAddr'.
     */
    public gr.ekt.cerif.schema.CfPAddr getCfPAddr() {
        return this.cfPAddr;
    }

    /**
     * Returns the value of field 'cfPAddr_Class'.
     * 
     * @return the value of field 'CfPAddr_Class'.
     */
    public gr.ekt.cerif.schema.CfPAddr_Class getCfPAddr_Class() {
        return this.cfPAddr_Class;
    }

    /**
     * Returns the value of field 'cfPAddr_GeoBBox'.
     * 
     * @return the value of field 'CfPAddr_GeoBBox'.
     */
    public gr.ekt.cerif.schema.CfPAddr_GeoBBox getCfPAddr_GeoBBox() {
        return this.cfPAddr_GeoBBox;
    }

    /**
     * Returns the value of field 'cfPers'.
     * 
     * @return the value of field 'CfPers'.
     */
    public gr.ekt.cerif.schema.CfPers getCfPers() {
        return this.cfPers;
    }

    /**
     * Returns the value of field 'cfPersKeyw'.
     * 
     * @return the value of field 'CfPersKeyw'.
     */
    public gr.ekt.cerif.schema.CfPersKeyw getCfPersKeyw() {
        return this.cfPersKeyw;
    }

    /**
     * Returns the value of field 'cfPersName'.
     * 
     * @return the value of field 'CfPersName'.
     */
    public gr.ekt.cerif.schema.CfPersName getCfPersName() {
        return this.cfPersName;
    }

    /**
     * Returns the value of field 'cfPersName_Pers'.
     * 
     * @return the value of field 'CfPersName_Pers'.
     */
    public gr.ekt.cerif.schema.CfPersName_Pers getCfPersName_Pers() {
        return this.cfPersName_Pers;
    }

    /**
     * Returns the value of field 'cfPersResInt'.
     * 
     * @return the value of field 'CfPersResInt'.
     */
    public gr.ekt.cerif.schema.CfPersResInt getCfPersResInt() {
        return this.cfPersResInt;
    }

    /**
     * Returns the value of field 'cfPers_CV'.
     * 
     * @return the value of field 'CfPers_CV'.
     */
    public gr.ekt.cerif.schema.CfPers_CV getCfPers_CV() {
        return this.cfPers_CV;
    }

    /**
     * Returns the value of field 'cfPers_Class'.
     * 
     * @return the value of field 'CfPers_Class'.
     */
    public gr.ekt.cerif.schema.CfPers_Class getCfPers_Class() {
        return this.cfPers_Class;
    }

    /**
     * Returns the value of field 'cfPers_Country'.
     * 
     * @return the value of field 'CfPers_Country'.
     */
    public gr.ekt.cerif.schema.CfPers_Country getCfPers_Country() {
        return this.cfPers_Country;
    }

    /**
     * Returns the value of field 'cfPers_DC'.
     * 
     * @return the value of field 'CfPers_DC'.
     */
    public gr.ekt.cerif.schema.CfPers_DC getCfPers_DC() {
        return this.cfPers_DC;
    }

    /**
     * Returns the value of field 'cfPers_EAddr'.
     * 
     * @return the value of field 'CfPers_EAddr'.
     */
    public gr.ekt.cerif.schema.CfPers_EAddr getCfPers_EAddr() {
        return this.cfPers_EAddr;
    }

    /**
     * Returns the value of field 'cfPers_Equip'.
     * 
     * @return the value of field 'CfPers_Equip'.
     */
    public gr.ekt.cerif.schema.CfPers_Equip getCfPers_Equip() {
        return this.cfPers_Equip;
    }

    /**
     * Returns the value of field 'cfPers_Event'.
     * 
     * @return the value of field 'CfPers_Event'.
     */
    public gr.ekt.cerif.schema.CfPers_Event getCfPers_Event() {
        return this.cfPers_Event;
    }

    /**
     * Returns the value of field 'cfPers_ExpSkills'.
     * 
     * @return the value of field 'CfPers_ExpSkills'.
     */
    public gr.ekt.cerif.schema.CfPers_ExpSkills getCfPers_ExpSkills() {
        return this.cfPers_ExpSkills;
    }

    /**
     * Returns the value of field 'cfPers_Facil'.
     * 
     * @return the value of field 'CfPers_Facil'.
     */
    public gr.ekt.cerif.schema.CfPers_Facil getCfPers_Facil() {
        return this.cfPers_Facil;
    }

    /**
     * Returns the value of field 'cfPers_Fund'.
     * 
     * @return the value of field 'CfPers_Fund'.
     */
    public gr.ekt.cerif.schema.CfPers_Fund getCfPers_Fund() {
        return this.cfPers_Fund;
    }

    /**
     * Returns the value of field 'cfPers_Indic'.
     * 
     * @return the value of field 'CfPers_Indic'.
     */
    public gr.ekt.cerif.schema.CfPers_Indic getCfPers_Indic() {
        return this.cfPers_Indic;
    }

    /**
     * Returns the value of field 'cfPers_Lang'.
     * 
     * @return the value of field 'CfPers_Lang'.
     */
    public gr.ekt.cerif.schema.CfPers_Lang getCfPers_Lang() {
        return this.cfPers_Lang;
    }

    /**
     * Returns the value of field 'cfPers_Meas'.
     * 
     * @return the value of field 'CfPers_Meas'.
     */
    public gr.ekt.cerif.schema.CfPers_Meas getCfPers_Meas() {
        return this.cfPers_Meas;
    }

    /**
     * Returns the value of field 'cfPers_Medium'.
     * 
     * @return the value of field 'CfPers_Medium'.
     */
    public gr.ekt.cerif.schema.CfPers_Medium getCfPers_Medium() {
        return this.cfPers_Medium;
    }

    /**
     * Returns the value of field 'cfPers_OrgUnit'.
     * 
     * @return the value of field 'CfPers_OrgUnit'.
     */
    public gr.ekt.cerif.schema.CfPers_OrgUnit getCfPers_OrgUnit() {
        return this.cfPers_OrgUnit;
    }

    /**
     * Returns the value of field 'cfPers_PAddr'.
     * 
     * @return the value of field 'CfPers_PAddr'.
     */
    public gr.ekt.cerif.schema.CfPers_PAddr getCfPers_PAddr() {
        return this.cfPers_PAddr;
    }

    /**
     * Returns the value of field 'cfPers_Pers'.
     * 
     * @return the value of field 'CfPers_Pers'.
     */
    public gr.ekt.cerif.schema.CfPers_Pers getCfPers_Pers() {
        return this.cfPers_Pers;
    }

    /**
     * Returns the value of field 'cfPers_Prize'.
     * 
     * @return the value of field 'CfPers_Prize'.
     */
    public gr.ekt.cerif.schema.CfPers_Prize getCfPers_Prize() {
        return this.cfPers_Prize;
    }

    /**
     * Returns the value of field 'cfPers_Qual'.
     * 
     * @return the value of field 'CfPers_Qual'.
     */
    public gr.ekt.cerif.schema.CfPers_Qual getCfPers_Qual() {
        return this.cfPers_Qual;
    }

    /**
     * Returns the value of field 'cfPers_ResPat'.
     * 
     * @return the value of field 'CfPers_ResPat'.
     */
    public gr.ekt.cerif.schema.CfPers_ResPat getCfPers_ResPat() {
        return this.cfPers_ResPat;
    }

    /**
     * Returns the value of field 'cfPers_ResProd'.
     * 
     * @return the value of field 'CfPers_ResProd'.
     */
    public gr.ekt.cerif.schema.CfPers_ResProd getCfPers_ResProd() {
        return this.cfPers_ResProd;
    }

    /**
     * Returns the value of field 'cfPers_ResPubl'.
     * 
     * @return the value of field 'CfPers_ResPubl'.
     */
    public gr.ekt.cerif.schema.CfPers_ResPubl getCfPers_ResPubl() {
        return this.cfPers_ResPubl;
    }

    /**
     * Returns the value of field 'cfPers_Srv'.
     * 
     * @return the value of field 'CfPers_Srv'.
     */
    public gr.ekt.cerif.schema.CfPers_Srv getCfPers_Srv() {
        return this.cfPers_Srv;
    }

    /**
     * Returns the value of field 'cfPrize'.
     * 
     * @return the value of field 'CfPrize'.
     */
    public gr.ekt.cerif.schema.CfPrize getCfPrize() {
        return this.cfPrize;
    }

    /**
     * Returns the value of field 'cfPrizeDescr'.
     * 
     * @return the value of field 'CfPrizeDescr'.
     */
    public gr.ekt.cerif.schema.CfPrizeDescr getCfPrizeDescr() {
        return this.cfPrizeDescr;
    }

    /**
     * Returns the value of field 'cfPrizeKeyw'.
     * 
     * @return the value of field 'CfPrizeKeyw'.
     */
    public gr.ekt.cerif.schema.CfPrizeKeyw getCfPrizeKeyw() {
        return this.cfPrizeKeyw;
    }

    /**
     * Returns the value of field 'cfPrizeName'.
     * 
     * @return the value of field 'CfPrizeName'.
     */
    public gr.ekt.cerif.schema.CfPrizeName getCfPrizeName() {
        return this.cfPrizeName;
    }

    /**
     * Returns the value of field 'cfPrize_Class'.
     * 
     * @return the value of field 'CfPrize_Class'.
     */
    public gr.ekt.cerif.schema.CfPrize_Class getCfPrize_Class() {
        return this.cfPrize_Class;
    }

    /**
     * Returns the value of field 'cfProj'.
     * 
     * @return the value of field 'CfProj'.
     */
    public gr.ekt.cerif.schema.CfProj getCfProj() {
        return this.cfProj;
    }

    /**
     * Returns the value of field 'cfProjAbstr'.
     * 
     * @return the value of field 'CfProjAbstr'.
     */
    public gr.ekt.cerif.schema.CfProjAbstr getCfProjAbstr() {
        return this.cfProjAbstr;
    }

    /**
     * Returns the value of field 'cfProjKeyw'.
     * 
     * @return the value of field 'CfProjKeyw'.
     */
    public gr.ekt.cerif.schema.CfProjKeyw getCfProjKeyw() {
        return this.cfProjKeyw;
    }

    /**
     * Returns the value of field 'cfProjTitle'.
     * 
     * @return the value of field 'CfProjTitle'.
     */
    public gr.ekt.cerif.schema.CfProjTitle getCfProjTitle() {
        return this.cfProjTitle;
    }

    /**
     * Returns the value of field 'cfProj_Class'.
     * 
     * @return the value of field 'CfProj_Class'.
     */
    public gr.ekt.cerif.schema.CfProj_Class getCfProj_Class() {
        return this.cfProj_Class;
    }

    /**
     * Returns the value of field 'cfProj_DC'.
     * 
     * @return the value of field 'CfProj_DC'.
     */
    public gr.ekt.cerif.schema.CfProj_DC getCfProj_DC() {
        return this.cfProj_DC;
    }

    /**
     * Returns the value of field 'cfProj_Equip'.
     * 
     * @return the value of field 'CfProj_Equip'.
     */
    public gr.ekt.cerif.schema.CfProj_Equip getCfProj_Equip() {
        return this.cfProj_Equip;
    }

    /**
     * Returns the value of field 'cfProj_Event'.
     * 
     * @return the value of field 'CfProj_Event'.
     */
    public gr.ekt.cerif.schema.CfProj_Event getCfProj_Event() {
        return this.cfProj_Event;
    }

    /**
     * Returns the value of field 'cfProj_Facil'.
     * 
     * @return the value of field 'CfProj_Facil'.
     */
    public gr.ekt.cerif.schema.CfProj_Facil getCfProj_Facil() {
        return this.cfProj_Facil;
    }

    /**
     * Returns the value of field 'cfProj_Fund'.
     * 
     * @return the value of field 'CfProj_Fund'.
     */
    public gr.ekt.cerif.schema.CfProj_Fund getCfProj_Fund() {
        return this.cfProj_Fund;
    }

    /**
     * Returns the value of field 'cfProj_Indic'.
     * 
     * @return the value of field 'CfProj_Indic'.
     */
    public gr.ekt.cerif.schema.CfProj_Indic getCfProj_Indic() {
        return this.cfProj_Indic;
    }

    /**
     * Returns the value of field 'cfProj_Meas'.
     * 
     * @return the value of field 'CfProj_Meas'.
     */
    public gr.ekt.cerif.schema.CfProj_Meas getCfProj_Meas() {
        return this.cfProj_Meas;
    }

    /**
     * Returns the value of field 'cfProj_Medium'.
     * 
     * @return the value of field 'CfProj_Medium'.
     */
    public gr.ekt.cerif.schema.CfProj_Medium getCfProj_Medium() {
        return this.cfProj_Medium;
    }

    /**
     * Returns the value of field 'cfProj_OrgUnit'.
     * 
     * @return the value of field 'CfProj_OrgUnit'.
     */
    public gr.ekt.cerif.schema.CfProj_OrgUnit getCfProj_OrgUnit() {
        return this.cfProj_OrgUnit;
    }

    /**
     * Returns the value of field 'cfProj_Pers'.
     * 
     * @return the value of field 'CfProj_Pers'.
     */
    public gr.ekt.cerif.schema.CfProj_Pers getCfProj_Pers() {
        return this.cfProj_Pers;
    }

    /**
     * Returns the value of field 'cfProj_Prize'.
     * 
     * @return the value of field 'CfProj_Prize'.
     */
    public gr.ekt.cerif.schema.CfProj_Prize getCfProj_Prize() {
        return this.cfProj_Prize;
    }

    /**
     * Returns the value of field 'cfProj_Proj'.
     * 
     * @return the value of field 'CfProj_Proj'.
     */
    public gr.ekt.cerif.schema.CfProj_Proj getCfProj_Proj() {
        return this.cfProj_Proj;
    }

    /**
     * Returns the value of field 'cfProj_ResPat'.
     * 
     * @return the value of field 'CfProj_ResPat'.
     */
    public gr.ekt.cerif.schema.CfProj_ResPat getCfProj_ResPat() {
        return this.cfProj_ResPat;
    }

    /**
     * Returns the value of field 'cfProj_ResProd'.
     * 
     * @return the value of field 'CfProj_ResProd'.
     */
    public gr.ekt.cerif.schema.CfProj_ResProd getCfProj_ResProd() {
        return this.cfProj_ResProd;
    }

    /**
     * Returns the value of field 'cfProj_ResPubl'.
     * 
     * @return the value of field 'CfProj_ResPubl'.
     */
    public gr.ekt.cerif.schema.CfProj_ResPubl getCfProj_ResPubl() {
        return this.cfProj_ResPubl;
    }

    /**
     * Returns the value of field 'cfProj_Srv'.
     * 
     * @return the value of field 'CfProj_Srv'.
     */
    public gr.ekt.cerif.schema.CfProj_Srv getCfProj_Srv() {
        return this.cfProj_Srv;
    }

    /**
     * Returns the value of field 'cfQual'.
     * 
     * @return the value of field 'CfQual'.
     */
    public gr.ekt.cerif.schema.CfQual getCfQual() {
        return this.cfQual;
    }

    /**
     * Returns the value of field 'cfQualDescr'.
     * 
     * @return the value of field 'CfQualDescr'.
     */
    public gr.ekt.cerif.schema.CfQualDescr getCfQualDescr() {
        return this.cfQualDescr;
    }

    /**
     * Returns the value of field 'cfQualKeyw'.
     * 
     * @return the value of field 'CfQualKeyw'.
     */
    public gr.ekt.cerif.schema.CfQualKeyw getCfQualKeyw() {
        return this.cfQualKeyw;
    }

    /**
     * Returns the value of field 'cfQualTitle'.
     * 
     * @return the value of field 'CfQualTitle'.
     */
    public gr.ekt.cerif.schema.CfQualTitle getCfQualTitle() {
        return this.cfQualTitle;
    }

    /**
     * Returns the value of field 'cfQual_Class'.
     * 
     * @return the value of field 'CfQual_Class'.
     */
    public gr.ekt.cerif.schema.CfQual_Class getCfQual_Class() {
        return this.cfQual_Class;
    }

    /**
     * Returns the value of field 'cfResPat'.
     * 
     * @return the value of field 'CfResPat'.
     */
    public gr.ekt.cerif.schema.CfResPat getCfResPat() {
        return this.cfResPat;
    }

    /**
     * Returns the value of field 'cfResPatAbstr'.
     * 
     * @return the value of field 'CfResPatAbstr'.
     */
    public gr.ekt.cerif.schema.CfResPatAbstr getCfResPatAbstr() {
        return this.cfResPatAbstr;
    }

    /**
     * Returns the value of field 'cfResPatKeyw'.
     * 
     * @return the value of field 'CfResPatKeyw'.
     */
    public gr.ekt.cerif.schema.CfResPatKeyw getCfResPatKeyw() {
        return this.cfResPatKeyw;
    }

    /**
     * Returns the value of field 'cfResPatTitle'.
     * 
     * @return the value of field 'CfResPatTitle'.
     */
    public gr.ekt.cerif.schema.CfResPatTitle getCfResPatTitle() {
        return this.cfResPatTitle;
    }

    /**
     * Returns the value of field 'cfResPatVersInfo'.
     * 
     * @return the value of field 'CfResPatVersInfo'.
     */
    public gr.ekt.cerif.schema.CfResPatVersInfo getCfResPatVersInfo() {
        return this.cfResPatVersInfo;
    }

    /**
     * Returns the value of field 'cfResPat_Class'.
     * 
     * @return the value of field 'CfResPat_Class'.
     */
    public gr.ekt.cerif.schema.CfResPat_Class getCfResPat_Class() {
        return this.cfResPat_Class;
    }

    /**
     * Returns the value of field 'cfResPat_Equip'.
     * 
     * @return the value of field 'CfResPat_Equip'.
     */
    public gr.ekt.cerif.schema.CfResPat_Equip getCfResPat_Equip() {
        return this.cfResPat_Equip;
    }

    /**
     * Returns the value of field 'cfResPat_Facil'.
     * 
     * @return the value of field 'CfResPat_Facil'.
     */
    public gr.ekt.cerif.schema.CfResPat_Facil getCfResPat_Facil() {
        return this.cfResPat_Facil;
    }

    /**
     * Returns the value of field 'cfResPat_Fund'.
     * 
     * @return the value of field 'CfResPat_Fund'.
     */
    public gr.ekt.cerif.schema.CfResPat_Fund getCfResPat_Fund() {
        return this.cfResPat_Fund;
    }

    /**
     * Returns the value of field 'cfResPat_Indic'.
     * 
     * @return the value of field 'CfResPat_Indic'.
     */
    public gr.ekt.cerif.schema.CfResPat_Indic getCfResPat_Indic() {
        return this.cfResPat_Indic;
    }

    /**
     * Returns the value of field 'cfResPat_Meas'.
     * 
     * @return the value of field 'CfResPat_Meas'.
     */
    public gr.ekt.cerif.schema.CfResPat_Meas getCfResPat_Meas() {
        return this.cfResPat_Meas;
    }

    /**
     * Returns the value of field 'cfResPat_Medium'.
     * 
     * @return the value of field 'CfResPat_Medium'.
     */
    public gr.ekt.cerif.schema.CfResPat_Medium getCfResPat_Medium() {
        return this.cfResPat_Medium;
    }

    /**
     * Returns the value of field 'cfResPat_ResPat'.
     * 
     * @return the value of field 'CfResPat_ResPat'.
     */
    public gr.ekt.cerif.schema.CfResPat_ResPat getCfResPat_ResPat() {
        return this.cfResPat_ResPat;
    }

    /**
     * Returns the value of field 'cfResPat_Srv'.
     * 
     * @return the value of field 'CfResPat_Srv'.
     */
    public gr.ekt.cerif.schema.CfResPat_Srv getCfResPat_Srv() {
        return this.cfResPat_Srv;
    }

    /**
     * Returns the value of field 'cfResProd'.
     * 
     * @return the value of field 'CfResProd'.
     */
    public gr.ekt.cerif.schema.CfResProd getCfResProd() {
        return this.cfResProd;
    }

    /**
     * Returns the value of field 'cfResProdAltName'.
     * 
     * @return the value of field 'CfResProdAltName'.
     */
    public gr.ekt.cerif.schema.CfResProdAltName getCfResProdAltName() {
        return this.cfResProdAltName;
    }

    /**
     * Returns the value of field 'cfResProdDescr'.
     * 
     * @return the value of field 'CfResProdDescr'.
     */
    public gr.ekt.cerif.schema.CfResProdDescr getCfResProdDescr() {
        return this.cfResProdDescr;
    }

    /**
     * Returns the value of field 'cfResProdKeyw'.
     * 
     * @return the value of field 'CfResProdKeyw'.
     */
    public gr.ekt.cerif.schema.CfResProdKeyw getCfResProdKeyw() {
        return this.cfResProdKeyw;
    }

    /**
     * Returns the value of field 'cfResProdName'.
     * 
     * @return the value of field 'CfResProdName'.
     */
    public gr.ekt.cerif.schema.CfResProdName getCfResProdName() {
        return this.cfResProdName;
    }

    /**
     * Returns the value of field 'cfResProdVersInfo'.
     * 
     * @return the value of field 'CfResProdVersInfo'.
     */
    public gr.ekt.cerif.schema.CfResProdVersInfo getCfResProdVersInfo() {
        return this.cfResProdVersInfo;
    }

    /**
     * Returns the value of field 'cfResProd_Class'.
     * 
     * @return the value of field 'CfResProd_Class'.
     */
    public gr.ekt.cerif.schema.CfResProd_Class getCfResProd_Class() {
        return this.cfResProd_Class;
    }

    /**
     * Returns the value of field 'cfResProd_Equip'.
     * 
     * @return the value of field 'CfResProd_Equip'.
     */
    public gr.ekt.cerif.schema.CfResProd_Equip getCfResProd_Equip() {
        return this.cfResProd_Equip;
    }

    /**
     * Returns the value of field 'cfResProd_Facil'.
     * 
     * @return the value of field 'CfResProd_Facil'.
     */
    public gr.ekt.cerif.schema.CfResProd_Facil getCfResProd_Facil() {
        return this.cfResProd_Facil;
    }

    /**
     * Returns the value of field 'cfResProd_Fund'.
     * 
     * @return the value of field 'CfResProd_Fund'.
     */
    public gr.ekt.cerif.schema.CfResProd_Fund getCfResProd_Fund() {
        return this.cfResProd_Fund;
    }

    /**
     * Returns the value of field 'cfResProd_GeoBBox'.
     * 
     * @return the value of field 'CfResProd_GeoBBox'.
     */
    public gr.ekt.cerif.schema.CfResProd_GeoBBox getCfResProd_GeoBBox() {
        return this.cfResProd_GeoBBox;
    }

    /**
     * Returns the value of field 'cfResProd_Indic'.
     * 
     * @return the value of field 'CfResProd_Indic'.
     */
    public gr.ekt.cerif.schema.CfResProd_Indic getCfResProd_Indic() {
        return this.cfResProd_Indic;
    }

    /**
     * Returns the value of field 'cfResProd_Meas'.
     * 
     * @return the value of field 'CfResProd_Meas'.
     */
    public gr.ekt.cerif.schema.CfResProd_Meas getCfResProd_Meas() {
        return this.cfResProd_Meas;
    }

    /**
     * Returns the value of field 'cfResProd_Medium'.
     * 
     * @return the value of field 'CfResProd_Medium'.
     */
    public gr.ekt.cerif.schema.CfResProd_Medium getCfResProd_Medium() {
        return this.cfResProd_Medium;
    }

    /**
     * Returns the value of field 'cfResProd_ResPat'.
     * 
     * @return the value of field 'CfResProd_ResPat'.
     */
    public gr.ekt.cerif.schema.CfResProd_ResPat getCfResProd_ResPat() {
        return this.cfResProd_ResPat;
    }

    /**
     * Returns the value of field 'cfResProd_ResProd'.
     * 
     * @return the value of field 'CfResProd_ResProd'.
     */
    public gr.ekt.cerif.schema.CfResProd_ResProd getCfResProd_ResProd() {
        return this.cfResProd_ResProd;
    }

    /**
     * Returns the value of field 'cfResProd_Srv'.
     * 
     * @return the value of field 'CfResProd_Srv'.
     */
    public gr.ekt.cerif.schema.CfResProd_Srv getCfResProd_Srv() {
        return this.cfResProd_Srv;
    }

    /**
     * Returns the value of field 'cfResPubl'.
     * 
     * @return the value of field 'CfResPubl'.
     */
    public gr.ekt.cerif.schema.CfResPubl getCfResPubl() {
        return this.cfResPubl;
    }

    /**
     * Returns the value of field 'cfResPublAbstr'.
     * 
     * @return the value of field 'CfResPublAbstr'.
     */
    public gr.ekt.cerif.schema.CfResPublAbstr getCfResPublAbstr() {
        return this.cfResPublAbstr;
    }

    /**
     * Returns the value of field 'cfResPublBiblNote'.
     * 
     * @return the value of field 'CfResPublBiblNote'.
     */
    public gr.ekt.cerif.schema.CfResPublBiblNote getCfResPublBiblNote() {
        return this.cfResPublBiblNote;
    }

    /**
     * Returns the value of field 'cfResPublKeyw'.
     * 
     * @return the value of field 'CfResPublKeyw'.
     */
    public gr.ekt.cerif.schema.CfResPublKeyw getCfResPublKeyw() {
        return this.cfResPublKeyw;
    }

    /**
     * Returns the value of field 'cfResPublNameAbbrev'.
     * 
     * @return the value of field 'CfResPublNameAbbrev'.
     */
    public gr.ekt.cerif.schema.CfResPublNameAbbrev getCfResPublNameAbbrev() {
        return this.cfResPublNameAbbrev;
    }

    /**
     * Returns the value of field 'cfResPublSubtitle'.
     * 
     * @return the value of field 'CfResPublSubtitle'.
     */
    public gr.ekt.cerif.schema.CfResPublSubtitle getCfResPublSubtitle() {
        return this.cfResPublSubtitle;
    }

    /**
     * Returns the value of field 'cfResPublTitle'.
     * 
     * @return the value of field 'CfResPublTitle'.
     */
    public gr.ekt.cerif.schema.CfResPublTitle getCfResPublTitle() {
        return this.cfResPublTitle;
    }

    /**
     * Returns the value of field 'cfResPublVersInfo'.
     * 
     * @return the value of field 'CfResPublVersInfo'.
     */
    public gr.ekt.cerif.schema.CfResPublVersInfo getCfResPublVersInfo() {
        return this.cfResPublVersInfo;
    }

    /**
     * Returns the value of field 'cfResPubl_Cite'.
     * 
     * @return the value of field 'CfResPubl_Cite'.
     */
    public gr.ekt.cerif.schema.CfResPubl_Cite getCfResPubl_Cite() {
        return this.cfResPubl_Cite;
    }

    /**
     * Returns the value of field 'cfResPubl_Class'.
     * 
     * @return the value of field 'CfResPubl_Class'.
     */
    public gr.ekt.cerif.schema.CfResPubl_Class getCfResPubl_Class() {
        return this.cfResPubl_Class;
    }

    /**
     * Returns the value of field 'cfResPubl_DC'.
     * 
     * @return the value of field 'CfResPubl_DC'.
     */
    public gr.ekt.cerif.schema.CfResPubl_DC getCfResPubl_DC() {
        return this.cfResPubl_DC;
    }

    /**
     * Returns the value of field 'cfResPubl_Equip'.
     * 
     * @return the value of field 'CfResPubl_Equip'.
     */
    public gr.ekt.cerif.schema.CfResPubl_Equip getCfResPubl_Equip() {
        return this.cfResPubl_Equip;
    }

    /**
     * Returns the value of field 'cfResPubl_Event'.
     * 
     * @return the value of field 'CfResPubl_Event'.
     */
    public gr.ekt.cerif.schema.CfResPubl_Event getCfResPubl_Event() {
        return this.cfResPubl_Event;
    }

    /**
     * Returns the value of field 'cfResPubl_Facil'.
     * 
     * @return the value of field 'CfResPubl_Facil'.
     */
    public gr.ekt.cerif.schema.CfResPubl_Facil getCfResPubl_Facil() {
        return this.cfResPubl_Facil;
    }

    /**
     * Returns the value of field 'cfResPubl_Fund'.
     * 
     * @return the value of field 'CfResPubl_Fund'.
     */
    public gr.ekt.cerif.schema.CfResPubl_Fund getCfResPubl_Fund() {
        return this.cfResPubl_Fund;
    }

    /**
     * Returns the value of field 'cfResPubl_Indic'.
     * 
     * @return the value of field 'CfResPubl_Indic'.
     */
    public gr.ekt.cerif.schema.CfResPubl_Indic getCfResPubl_Indic() {
        return this.cfResPubl_Indic;
    }

    /**
     * Returns the value of field 'cfResPubl_Meas'.
     * 
     * @return the value of field 'CfResPubl_Meas'.
     */
    public gr.ekt.cerif.schema.CfResPubl_Meas getCfResPubl_Meas() {
        return this.cfResPubl_Meas;
    }

    /**
     * Returns the value of field 'cfResPubl_Medium'.
     * 
     * @return the value of field 'CfResPubl_Medium'.
     */
    public gr.ekt.cerif.schema.CfResPubl_Medium getCfResPubl_Medium() {
        return this.cfResPubl_Medium;
    }

    /**
     * Returns the value of field 'cfResPubl_Metrics'.
     * 
     * @return the value of field 'CfResPubl_Metrics'.
     */
    public gr.ekt.cerif.schema.CfResPubl_Metrics getCfResPubl_Metrics() {
        return this.cfResPubl_Metrics;
    }

    /**
     * Returns the value of field 'cfResPubl_ResPat'.
     * 
     * @return the value of field 'CfResPubl_ResPat'.
     */
    public gr.ekt.cerif.schema.CfResPubl_ResPat getCfResPubl_ResPat() {
        return this.cfResPubl_ResPat;
    }

    /**
     * Returns the value of field 'cfResPubl_ResProd'.
     * 
     * @return the value of field 'CfResPubl_ResProd'.
     */
    public gr.ekt.cerif.schema.CfResPubl_ResProd getCfResPubl_ResProd() {
        return this.cfResPubl_ResProd;
    }

    /**
     * Returns the value of field 'cfResPubl_ResPubl'.
     * 
     * @return the value of field 'CfResPubl_ResPubl'.
     */
    public gr.ekt.cerif.schema.CfResPubl_ResPubl getCfResPubl_ResPubl() {
        return this.cfResPubl_ResPubl;
    }

    /**
     * Returns the value of field 'cfResPubl_Srv'.
     * 
     * @return the value of field 'CfResPubl_Srv'.
     */
    public gr.ekt.cerif.schema.CfResPubl_Srv getCfResPubl_Srv() {
        return this.cfResPubl_Srv;
    }

    /**
     * Returns the value of field 'cfSrv'.
     * 
     * @return the value of field 'CfSrv'.
     */
    public gr.ekt.cerif.schema.CfSrv getCfSrv() {
        return this.cfSrv;
    }

    /**
     * Returns the value of field 'cfSrvDescr'.
     * 
     * @return the value of field 'CfSrvDescr'.
     */
    public gr.ekt.cerif.schema.CfSrvDescr getCfSrvDescr() {
        return this.cfSrvDescr;
    }

    /**
     * Returns the value of field 'cfSrvKeyw'.
     * 
     * @return the value of field 'CfSrvKeyw'.
     */
    public gr.ekt.cerif.schema.CfSrvKeyw getCfSrvKeyw() {
        return this.cfSrvKeyw;
    }

    /**
     * Returns the value of field 'cfSrvName'.
     * 
     * @return the value of field 'CfSrvName'.
     */
    public gr.ekt.cerif.schema.CfSrvName getCfSrvName() {
        return this.cfSrvName;
    }

    /**
     * Returns the value of field 'cfSrv_Class'.
     * 
     * @return the value of field 'CfSrv_Class'.
     */
    public gr.ekt.cerif.schema.CfSrv_Class getCfSrv_Class() {
        return this.cfSrv_Class;
    }

    /**
     * Returns the value of field 'cfSrv_Event'.
     * 
     * @return the value of field 'CfSrv_Event'.
     */
    public gr.ekt.cerif.schema.CfSrv_Event getCfSrv_Event() {
        return this.cfSrv_Event;
    }

    /**
     * Returns the value of field 'cfSrv_Fund'.
     * 
     * @return the value of field 'CfSrv_Fund'.
     */
    public gr.ekt.cerif.schema.CfSrv_Fund getCfSrv_Fund() {
        return this.cfSrv_Fund;
    }

    /**
     * Returns the value of field 'cfSrv_Indic'.
     * 
     * @return the value of field 'CfSrv_Indic'.
     */
    public gr.ekt.cerif.schema.CfSrv_Indic getCfSrv_Indic() {
        return this.cfSrv_Indic;
    }

    /**
     * Returns the value of field 'cfSrv_Meas'.
     * 
     * @return the value of field 'CfSrv_Meas'.
     */
    public gr.ekt.cerif.schema.CfSrv_Meas getCfSrv_Meas() {
        return this.cfSrv_Meas;
    }

    /**
     * Returns the value of field 'cfSrv_Medium'.
     * 
     * @return the value of field 'CfSrv_Medium'.
     */
    public gr.ekt.cerif.schema.CfSrv_Medium getCfSrv_Medium() {
        return this.cfSrv_Medium;
    }

    /**
     * Returns the value of field 'cfSrv_PAddr'.
     * 
     * @return the value of field 'CfSrv_PAddr'.
     */
    public gr.ekt.cerif.schema.CfSrv_PAddr getCfSrv_PAddr() {
        return this.cfSrv_PAddr;
    }

    /**
     * Returns the value of field 'cfSrv_Srv'.
     * 
     * @return the value of field 'CfSrv_Srv'.
     */
    public gr.ekt.cerif.schema.CfSrv_Srv getCfSrv_Srv() {
        return this.cfSrv_Srv;
    }

    /**
     * Returns the value of field 'choiceValue'. The field
     * 'choiceValue' has the following description: Internal choice
     * value storage
     * 
     * @return the value of field 'ChoiceValue'.
     */
    public java.lang.Object getChoiceValue() {
        return this._choiceValue;
    }

    /**
     * Sets the value of field 'cfCV'.
     * 
     * @param cfCV the value of field 'cfCV'.
     */
    public void setCfCV(final gr.ekt.cerif.schema.CfCV cfCV) {
        this.cfCV = cfCV;
        this._choiceValue = cfCV;
    }

    /**
     * Sets the value of field 'cfCV_Class'.
     * 
     * @param cfCV_Class the value of field 'cfCV_Class'.
     */
    public void setCfCV_Class(final gr.ekt.cerif.schema.CfCV_Class cfCV_Class) {
        this.cfCV_Class = cfCV_Class;
        this._choiceValue = cfCV_Class;
    }

    /**
     * Sets the value of field 'cfCite'.
     * 
     * @param cfCite the value of field 'cfCite'.
     */
    public void setCfCite(final gr.ekt.cerif.schema.CfCite cfCite) {
        this.cfCite = cfCite;
        this._choiceValue = cfCite;
    }

    /**
     * Sets the value of field 'cfCiteDescr'.
     * 
     * @param cfCiteDescr the value of field 'cfCiteDescr'.
     */
    public void setCfCiteDescr(final gr.ekt.cerif.schema.CfCiteDescr cfCiteDescr) {
        this.cfCiteDescr = cfCiteDescr;
        this._choiceValue = cfCiteDescr;
    }

    /**
     * Sets the value of field 'cfCiteTitle'.
     * 
     * @param cfCiteTitle the value of field 'cfCiteTitle'.
     */
    public void setCfCiteTitle(final gr.ekt.cerif.schema.CfCiteTitle cfCiteTitle) {
        this.cfCiteTitle = cfCiteTitle;
        this._choiceValue = cfCiteTitle;
    }

    /**
     * Sets the value of field 'cfCite_Class'.
     * 
     * @param cfCite_Class the value of field 'cfCite_Class'.
     */
    public void setCfCite_Class(final gr.ekt.cerif.schema.CfCite_Class cfCite_Class) {
        this.cfCite_Class = cfCite_Class;
        this._choiceValue = cfCite_Class;
    }

    /**
     * Sets the value of field 'cfCite_Medium'.
     * 
     * @param cfCite_Medium the value of field 'cfCite_Medium'.
     */
    public void setCfCite_Medium(final gr.ekt.cerif.schema.CfCite_Medium cfCite_Medium) {
        this.cfCite_Medium = cfCite_Medium;
        this._choiceValue = cfCite_Medium;
    }

    /**
     * Sets the value of field 'cfClass'.
     * 
     * @param cfClass the value of field 'cfClass'.
     */
    public void setCfClass(final gr.ekt.cerif.schema.CfClass cfClass) {
        this.cfClass = cfClass;
        this._choiceValue = cfClass;
    }

    /**
     * Sets the value of field 'cfClassDef'.
     * 
     * @param cfClassDef the value of field 'cfClassDef'.
     */
    public void setCfClassDef(final gr.ekt.cerif.schema.CfClassDef cfClassDef) {
        this.cfClassDef = cfClassDef;
        this._choiceValue = cfClassDef;
    }

    /**
     * Sets the value of field 'cfClassDescr'.
     * 
     * @param cfClassDescr the value of field 'cfClassDescr'.
     */
    public void setCfClassDescr(final gr.ekt.cerif.schema.CfClassDescr cfClassDescr) {
        this.cfClassDescr = cfClassDescr;
        this._choiceValue = cfClassDescr;
    }

    /**
     * Sets the value of field 'cfClassEx'.
     * 
     * @param cfClassEx the value of field 'cfClassEx'.
     */
    public void setCfClassEx(final gr.ekt.cerif.schema.CfClassEx cfClassEx) {
        this.cfClassEx = cfClassEx;
        this._choiceValue = cfClassEx;
    }

    /**
     * Sets the value of field 'cfClassScheme'.
     * 
     * @param cfClassScheme the value of field 'cfClassScheme'.
     */
    public void setCfClassScheme(final gr.ekt.cerif.schema.CfClassScheme cfClassScheme) {
        this.cfClassScheme = cfClassScheme;
        this._choiceValue = cfClassScheme;
    }

    /**
     * Sets the value of field 'cfClassSchemeDescr'.
     * 
     * @param cfClassSchemeDescr the value of field
     * 'cfClassSchemeDescr'.
     */
    public void setCfClassSchemeDescr(final gr.ekt.cerif.schema.CfClassSchemeDescr cfClassSchemeDescr) {
        this.cfClassSchemeDescr = cfClassSchemeDescr;
        this._choiceValue = cfClassSchemeDescr;
    }

    /**
     * Sets the value of field 'cfClassSchemeName'.
     * 
     * @param cfClassSchemeName the value of field
     * 'cfClassSchemeName'.
     */
    public void setCfClassSchemeName(final gr.ekt.cerif.schema.CfClassSchemeName cfClassSchemeName) {
        this.cfClassSchemeName = cfClassSchemeName;
        this._choiceValue = cfClassSchemeName;
    }

    /**
     * Sets the value of field 'cfClassScheme_ClassScheme'.
     * 
     * @param cfClassScheme_ClassScheme the value of field
     * 'cfClassScheme_ClassScheme'.
     */
    public void setCfClassScheme_ClassScheme(final gr.ekt.cerif.schema.CfClassScheme_ClassScheme cfClassScheme_ClassScheme) {
        this.cfClassScheme_ClassScheme = cfClassScheme_ClassScheme;
        this._choiceValue = cfClassScheme_ClassScheme;
    }

    /**
     * Sets the value of field 'cfClassTerm'.
     * 
     * @param cfClassTerm the value of field 'cfClassTerm'.
     */
    public void setCfClassTerm(final gr.ekt.cerif.schema.CfClassTerm cfClassTerm) {
        this.cfClassTerm = cfClassTerm;
        this._choiceValue = cfClassTerm;
    }

    /**
     * Sets the value of field 'cfClass_Class'.
     * 
     * @param cfClass_Class the value of field 'cfClass_Class'.
     */
    public void setCfClass_Class(final gr.ekt.cerif.schema.CfClass_Class cfClass_Class) {
        this.cfClass_Class = cfClass_Class;
        this._choiceValue = cfClass_Class;
    }

    /**
     * Sets the value of field 'cfCountry'.
     * 
     * @param cfCountry the value of field 'cfCountry'.
     */
    public void setCfCountry(final gr.ekt.cerif.schema.CfCountry cfCountry) {
        this.cfCountry = cfCountry;
        this._choiceValue = cfCountry;
    }

    /**
     * Sets the value of field 'cfCountryName'.
     * 
     * @param cfCountryName the value of field 'cfCountryName'.
     */
    public void setCfCountryName(final gr.ekt.cerif.schema.CfCountryName cfCountryName) {
        this.cfCountryName = cfCountryName;
        this._choiceValue = cfCountryName;
    }

    /**
     * Sets the value of field 'cfCountry_Class'.
     * 
     * @param cfCountry_Class the value of field 'cfCountry_Class'.
     */
    public void setCfCountry_Class(final gr.ekt.cerif.schema.CfCountry_Class cfCountry_Class) {
        this.cfCountry_Class = cfCountry_Class;
        this._choiceValue = cfCountry_Class;
    }

    /**
     * Sets the value of field 'cfCurrency'.
     * 
     * @param cfCurrency the value of field 'cfCurrency'.
     */
    public void setCfCurrency(final gr.ekt.cerif.schema.CfCurrency cfCurrency) {
        this.cfCurrency = cfCurrency;
        this._choiceValue = cfCurrency;
    }

    /**
     * Sets the value of field 'cfCurrencyEntName'.
     * 
     * @param cfCurrencyEntName the value of field
     * 'cfCurrencyEntName'.
     */
    public void setCfCurrencyEntName(final gr.ekt.cerif.schema.CfCurrencyEntName cfCurrencyEntName) {
        this.cfCurrencyEntName = cfCurrencyEntName;
        this._choiceValue = cfCurrencyEntName;
    }

    /**
     * Sets the value of field 'cfCurrencyName'.
     * 
     * @param cfCurrencyName the value of field 'cfCurrencyName'.
     */
    public void setCfCurrencyName(final gr.ekt.cerif.schema.CfCurrencyName cfCurrencyName) {
        this.cfCurrencyName = cfCurrencyName;
        this._choiceValue = cfCurrencyName;
    }

    /**
     * Sets the value of field 'cfCurrency_Class'.
     * 
     * @param cfCurrency_Class the value of field 'cfCurrency_Class'
     */
    public void setCfCurrency_Class(final gr.ekt.cerif.schema.CfCurrency_Class cfCurrency_Class) {
        this.cfCurrency_Class = cfCurrency_Class;
        this._choiceValue = cfCurrency_Class;
    }

    /**
     * Sets the value of field 'cfDC'.
     * 
     * @param cfDC the value of field 'cfDC'.
     */
    public void setCfDC(final gr.ekt.cerif.schema.CfDC cfDC) {
        this.cfDC = cfDC;
        this._choiceValue = cfDC;
    }

    /**
     * Sets the value of field 'cfDCAudience'.
     * 
     * @param cfDCAudience the value of field 'cfDCAudience'.
     */
    public void setCfDCAudience(final gr.ekt.cerif.schema.CfDCAudience cfDCAudience) {
        this.cfDCAudience = cfDCAudience;
        this._choiceValue = cfDCAudience;
    }

    /**
     * Sets the value of field 'cfDCContributor'.
     * 
     * @param cfDCContributor the value of field 'cfDCContributor'.
     */
    public void setCfDCContributor(final gr.ekt.cerif.schema.CfDCContributor cfDCContributor) {
        this.cfDCContributor = cfDCContributor;
        this._choiceValue = cfDCContributor;
    }

    /**
     * Sets the value of field 'cfDCCoverage'.
     * 
     * @param cfDCCoverage the value of field 'cfDCCoverage'.
     */
    public void setCfDCCoverage(final gr.ekt.cerif.schema.CfDCCoverage cfDCCoverage) {
        this.cfDCCoverage = cfDCCoverage;
        this._choiceValue = cfDCCoverage;
    }

    /**
     * Sets the value of field 'cfDCCoverageSpatial'.
     * 
     * @param cfDCCoverageSpatial the value of field
     * 'cfDCCoverageSpatial'.
     */
    public void setCfDCCoverageSpatial(final gr.ekt.cerif.schema.CfDCCoverageSpatial cfDCCoverageSpatial) {
        this.cfDCCoverageSpatial = cfDCCoverageSpatial;
        this._choiceValue = cfDCCoverageSpatial;
    }

    /**
     * Sets the value of field 'cfDCCoverageTemporal'.
     * 
     * @param cfDCCoverageTemporal the value of field
     * 'cfDCCoverageTemporal'.
     */
    public void setCfDCCoverageTemporal(final gr.ekt.cerif.schema.CfDCCoverageTemporal cfDCCoverageTemporal) {
        this.cfDCCoverageTemporal = cfDCCoverageTemporal;
        this._choiceValue = cfDCCoverageTemporal;
    }

    /**
     * Sets the value of field 'cfDCCreator'.
     * 
     * @param cfDCCreator the value of field 'cfDCCreator'.
     */
    public void setCfDCCreator(final gr.ekt.cerif.schema.CfDCCreator cfDCCreator) {
        this.cfDCCreator = cfDCCreator;
        this._choiceValue = cfDCCreator;
    }

    /**
     * Sets the value of field 'cfDCDate'.
     * 
     * @param cfDCDate the value of field 'cfDCDate'.
     */
    public void setCfDCDate(final gr.ekt.cerif.schema.CfDCDate cfDCDate) {
        this.cfDCDate = cfDCDate;
        this._choiceValue = cfDCDate;
    }

    /**
     * Sets the value of field 'cfDCDescription'.
     * 
     * @param cfDCDescription the value of field 'cfDCDescription'.
     */
    public void setCfDCDescription(final gr.ekt.cerif.schema.CfDCDescription cfDCDescription) {
        this.cfDCDescription = cfDCDescription;
        this._choiceValue = cfDCDescription;
    }

    /**
     * Sets the value of field 'cfDCFormat'.
     * 
     * @param cfDCFormat the value of field 'cfDCFormat'.
     */
    public void setCfDCFormat(final gr.ekt.cerif.schema.CfDCFormat cfDCFormat) {
        this.cfDCFormat = cfDCFormat;
        this._choiceValue = cfDCFormat;
    }

    /**
     * Sets the value of field 'cfDCLanguage'.
     * 
     * @param cfDCLanguage the value of field 'cfDCLanguage'.
     */
    public void setCfDCLanguage(final gr.ekt.cerif.schema.CfDCLanguage cfDCLanguage) {
        this.cfDCLanguage = cfDCLanguage;
        this._choiceValue = cfDCLanguage;
    }

    /**
     * Sets the value of field 'cfDCProvenance'.
     * 
     * @param cfDCProvenance the value of field 'cfDCProvenance'.
     */
    public void setCfDCProvenance(final gr.ekt.cerif.schema.CfDCProvenance cfDCProvenance) {
        this.cfDCProvenance = cfDCProvenance;
        this._choiceValue = cfDCProvenance;
    }

    /**
     * Sets the value of field 'cfDCPublisher'.
     * 
     * @param cfDCPublisher the value of field 'cfDCPublisher'.
     */
    public void setCfDCPublisher(final gr.ekt.cerif.schema.CfDCPublisher cfDCPublisher) {
        this.cfDCPublisher = cfDCPublisher;
        this._choiceValue = cfDCPublisher;
    }

    /**
     * Sets the value of field 'cfDCRelation'.
     * 
     * @param cfDCRelation the value of field 'cfDCRelation'.
     */
    public void setCfDCRelation(final gr.ekt.cerif.schema.CfDCRelation cfDCRelation) {
        this.cfDCRelation = cfDCRelation;
        this._choiceValue = cfDCRelation;
    }

    /**
     * Sets the value of field 'cfDCResourceIdentifier'.
     * 
     * @param cfDCResourceIdentifier the value of field
     * 'cfDCResourceIdentifier'.
     */
    public void setCfDCResourceIdentifier(final gr.ekt.cerif.schema.CfDCResourceIdentifier cfDCResourceIdentifier) {
        this.cfDCResourceIdentifier = cfDCResourceIdentifier;
        this._choiceValue = cfDCResourceIdentifier;
    }

    /**
     * Sets the value of field 'cfDCResourceType'.
     * 
     * @param cfDCResourceType the value of field 'cfDCResourceType'
     */
    public void setCfDCResourceType(final gr.ekt.cerif.schema.CfDCResourceType cfDCResourceType) {
        this.cfDCResourceType = cfDCResourceType;
        this._choiceValue = cfDCResourceType;
    }

    /**
     * Sets the value of field 'cfDCRightsHolder'.
     * 
     * @param cfDCRightsHolder the value of field 'cfDCRightsHolder'
     */
    public void setCfDCRightsHolder(final gr.ekt.cerif.schema.CfDCRightsHolder cfDCRightsHolder) {
        this.cfDCRightsHolder = cfDCRightsHolder;
        this._choiceValue = cfDCRightsHolder;
    }

    /**
     * Sets the value of field 'cfDCRightsMM'.
     * 
     * @param cfDCRightsMM the value of field 'cfDCRightsMM'.
     */
    public void setCfDCRightsMM(final gr.ekt.cerif.schema.CfDCRightsMM cfDCRightsMM) {
        this.cfDCRightsMM = cfDCRightsMM;
        this._choiceValue = cfDCRightsMM;
    }

    /**
     * Sets the value of field 'cfDCRightsMMAccessRights'.
     * 
     * @param cfDCRightsMMAccessRights the value of field
     * 'cfDCRightsMMAccessRights'.
     */
    public void setCfDCRightsMMAccessRights(final gr.ekt.cerif.schema.CfDCRightsMMAccessRights cfDCRightsMMAccessRights) {
        this.cfDCRightsMMAccessRights = cfDCRightsMMAccessRights;
        this._choiceValue = cfDCRightsMMAccessRights;
    }

    /**
     * Sets the value of field 'cfDCRightsMMLicense'.
     * 
     * @param cfDCRightsMMLicense the value of field
     * 'cfDCRightsMMLicense'.
     */
    public void setCfDCRightsMMLicense(final gr.ekt.cerif.schema.CfDCRightsMMLicense cfDCRightsMMLicense) {
        this.cfDCRightsMMLicense = cfDCRightsMMLicense;
        this._choiceValue = cfDCRightsMMLicense;
    }

    /**
     * Sets the value of field 'cfDCSource'.
     * 
     * @param cfDCSource the value of field 'cfDCSource'.
     */
    public void setCfDCSource(final gr.ekt.cerif.schema.CfDCSource cfDCSource) {
        this.cfDCSource = cfDCSource;
        this._choiceValue = cfDCSource;
    }

    /**
     * Sets the value of field 'cfDCSubject'.
     * 
     * @param cfDCSubject the value of field 'cfDCSubject'.
     */
    public void setCfDCSubject(final gr.ekt.cerif.schema.CfDCSubject cfDCSubject) {
        this.cfDCSubject = cfDCSubject;
        this._choiceValue = cfDCSubject;
    }

    /**
     * Sets the value of field 'cfDCTitle'.
     * 
     * @param cfDCTitle the value of field 'cfDCTitle'.
     */
    public void setCfDCTitle(final gr.ekt.cerif.schema.CfDCTitle cfDCTitle) {
        this.cfDCTitle = cfDCTitle;
        this._choiceValue = cfDCTitle;
    }

    /**
     * Sets the value of field 'cfEAddr'.
     * 
     * @param cfEAddr the value of field 'cfEAddr'.
     */
    public void setCfEAddr(final gr.ekt.cerif.schema.CfEAddr cfEAddr) {
        this.cfEAddr = cfEAddr;
        this._choiceValue = cfEAddr;
    }

    /**
     * Sets the value of field 'cfEAddr_Class'.
     * 
     * @param cfEAddr_Class the value of field 'cfEAddr_Class'.
     */
    public void setCfEAddr_Class(final gr.ekt.cerif.schema.CfEAddr_Class cfEAddr_Class) {
        this.cfEAddr_Class = cfEAddr_Class;
        this._choiceValue = cfEAddr_Class;
    }

    /**
     * Sets the value of field 'cfEquip'.
     * 
     * @param cfEquip the value of field 'cfEquip'.
     */
    public void setCfEquip(final gr.ekt.cerif.schema.CfEquip cfEquip) {
        this.cfEquip = cfEquip;
        this._choiceValue = cfEquip;
    }

    /**
     * Sets the value of field 'cfEquipDescr'.
     * 
     * @param cfEquipDescr the value of field 'cfEquipDescr'.
     */
    public void setCfEquipDescr(final gr.ekt.cerif.schema.CfEquipDescr cfEquipDescr) {
        this.cfEquipDescr = cfEquipDescr;
        this._choiceValue = cfEquipDescr;
    }

    /**
     * Sets the value of field 'cfEquipKeyw'.
     * 
     * @param cfEquipKeyw the value of field 'cfEquipKeyw'.
     */
    public void setCfEquipKeyw(final gr.ekt.cerif.schema.CfEquipKeyw cfEquipKeyw) {
        this.cfEquipKeyw = cfEquipKeyw;
        this._choiceValue = cfEquipKeyw;
    }

    /**
     * Sets the value of field 'cfEquipName'.
     * 
     * @param cfEquipName the value of field 'cfEquipName'.
     */
    public void setCfEquipName(final gr.ekt.cerif.schema.CfEquipName cfEquipName) {
        this.cfEquipName = cfEquipName;
        this._choiceValue = cfEquipName;
    }

    /**
     * Sets the value of field 'cfEquip_Class'.
     * 
     * @param cfEquip_Class the value of field 'cfEquip_Class'.
     */
    public void setCfEquip_Class(final gr.ekt.cerif.schema.CfEquip_Class cfEquip_Class) {
        this.cfEquip_Class = cfEquip_Class;
        this._choiceValue = cfEquip_Class;
    }

    /**
     * Sets the value of field 'cfEquip_Equip'.
     * 
     * @param cfEquip_Equip the value of field 'cfEquip_Equip'.
     */
    public void setCfEquip_Equip(final gr.ekt.cerif.schema.CfEquip_Equip cfEquip_Equip) {
        this.cfEquip_Equip = cfEquip_Equip;
        this._choiceValue = cfEquip_Equip;
    }

    /**
     * Sets the value of field 'cfEquip_Event'.
     * 
     * @param cfEquip_Event the value of field 'cfEquip_Event'.
     */
    public void setCfEquip_Event(final gr.ekt.cerif.schema.CfEquip_Event cfEquip_Event) {
        this.cfEquip_Event = cfEquip_Event;
        this._choiceValue = cfEquip_Event;
    }

    /**
     * Sets the value of field 'cfEquip_Fund'.
     * 
     * @param cfEquip_Fund the value of field 'cfEquip_Fund'.
     */
    public void setCfEquip_Fund(final gr.ekt.cerif.schema.CfEquip_Fund cfEquip_Fund) {
        this.cfEquip_Fund = cfEquip_Fund;
        this._choiceValue = cfEquip_Fund;
    }

    /**
     * Sets the value of field 'cfEquip_Indic'.
     * 
     * @param cfEquip_Indic the value of field 'cfEquip_Indic'.
     */
    public void setCfEquip_Indic(final gr.ekt.cerif.schema.CfEquip_Indic cfEquip_Indic) {
        this.cfEquip_Indic = cfEquip_Indic;
        this._choiceValue = cfEquip_Indic;
    }

    /**
     * Sets the value of field 'cfEquip_Meas'.
     * 
     * @param cfEquip_Meas the value of field 'cfEquip_Meas'.
     */
    public void setCfEquip_Meas(final gr.ekt.cerif.schema.CfEquip_Meas cfEquip_Meas) {
        this.cfEquip_Meas = cfEquip_Meas;
        this._choiceValue = cfEquip_Meas;
    }

    /**
     * Sets the value of field 'cfEquip_Medium'.
     * 
     * @param cfEquip_Medium the value of field 'cfEquip_Medium'.
     */
    public void setCfEquip_Medium(final gr.ekt.cerif.schema.CfEquip_Medium cfEquip_Medium) {
        this.cfEquip_Medium = cfEquip_Medium;
        this._choiceValue = cfEquip_Medium;
    }

    /**
     * Sets the value of field 'cfEquip_PAddr'.
     * 
     * @param cfEquip_PAddr the value of field 'cfEquip_PAddr'.
     */
    public void setCfEquip_PAddr(final gr.ekt.cerif.schema.CfEquip_PAddr cfEquip_PAddr) {
        this.cfEquip_PAddr = cfEquip_PAddr;
        this._choiceValue = cfEquip_PAddr;
    }

    /**
     * Sets the value of field 'cfEquip_Srv'.
     * 
     * @param cfEquip_Srv the value of field 'cfEquip_Srv'.
     */
    public void setCfEquip_Srv(final gr.ekt.cerif.schema.CfEquip_Srv cfEquip_Srv) {
        this.cfEquip_Srv = cfEquip_Srv;
        this._choiceValue = cfEquip_Srv;
    }

    /**
     * Sets the value of field 'cfEvent'.
     * 
     * @param cfEvent the value of field 'cfEvent'.
     */
    public void setCfEvent(final gr.ekt.cerif.schema.CfEvent cfEvent) {
        this.cfEvent = cfEvent;
        this._choiceValue = cfEvent;
    }

    /**
     * Sets the value of field 'cfEventDescr'.
     * 
     * @param cfEventDescr the value of field 'cfEventDescr'.
     */
    public void setCfEventDescr(final gr.ekt.cerif.schema.CfEventDescr cfEventDescr) {
        this.cfEventDescr = cfEventDescr;
        this._choiceValue = cfEventDescr;
    }

    /**
     * Sets the value of field 'cfEventKeyw'.
     * 
     * @param cfEventKeyw the value of field 'cfEventKeyw'.
     */
    public void setCfEventKeyw(final gr.ekt.cerif.schema.CfEventKeyw cfEventKeyw) {
        this.cfEventKeyw = cfEventKeyw;
        this._choiceValue = cfEventKeyw;
    }

    /**
     * Sets the value of field 'cfEventName'.
     * 
     * @param cfEventName the value of field 'cfEventName'.
     */
    public void setCfEventName(final gr.ekt.cerif.schema.CfEventName cfEventName) {
        this.cfEventName = cfEventName;
        this._choiceValue = cfEventName;
    }

    /**
     * Sets the value of field 'cfEvent_Class'.
     * 
     * @param cfEvent_Class the value of field 'cfEvent_Class'.
     */
    public void setCfEvent_Class(final gr.ekt.cerif.schema.CfEvent_Class cfEvent_Class) {
        this.cfEvent_Class = cfEvent_Class;
        this._choiceValue = cfEvent_Class;
    }

    /**
     * Sets the value of field 'cfEvent_Event'.
     * 
     * @param cfEvent_Event the value of field 'cfEvent_Event'.
     */
    public void setCfEvent_Event(final gr.ekt.cerif.schema.CfEvent_Event cfEvent_Event) {
        this.cfEvent_Event = cfEvent_Event;
        this._choiceValue = cfEvent_Event;
    }

    /**
     * Sets the value of field 'cfEvent_Fund'.
     * 
     * @param cfEvent_Fund the value of field 'cfEvent_Fund'.
     */
    public void setCfEvent_Fund(final gr.ekt.cerif.schema.CfEvent_Fund cfEvent_Fund) {
        this.cfEvent_Fund = cfEvent_Fund;
        this._choiceValue = cfEvent_Fund;
    }

    /**
     * Sets the value of field 'cfEvent_Indic'.
     * 
     * @param cfEvent_Indic the value of field 'cfEvent_Indic'.
     */
    public void setCfEvent_Indic(final gr.ekt.cerif.schema.CfEvent_Indic cfEvent_Indic) {
        this.cfEvent_Indic = cfEvent_Indic;
        this._choiceValue = cfEvent_Indic;
    }

    /**
     * Sets the value of field 'cfEvent_Meas'.
     * 
     * @param cfEvent_Meas the value of field 'cfEvent_Meas'.
     */
    public void setCfEvent_Meas(final gr.ekt.cerif.schema.CfEvent_Meas cfEvent_Meas) {
        this.cfEvent_Meas = cfEvent_Meas;
        this._choiceValue = cfEvent_Meas;
    }

    /**
     * Sets the value of field 'cfEvent_Medium'.
     * 
     * @param cfEvent_Medium the value of field 'cfEvent_Medium'.
     */
    public void setCfEvent_Medium(final gr.ekt.cerif.schema.CfEvent_Medium cfEvent_Medium) {
        this.cfEvent_Medium = cfEvent_Medium;
        this._choiceValue = cfEvent_Medium;
    }

    /**
     * Sets the value of field 'cfExpSkills'.
     * 
     * @param cfExpSkills the value of field 'cfExpSkills'.
     */
    public void setCfExpSkills(final gr.ekt.cerif.schema.CfExpSkills cfExpSkills) {
        this.cfExpSkills = cfExpSkills;
        this._choiceValue = cfExpSkills;
    }

    /**
     * Sets the value of field 'cfExpSkillsDescr'.
     * 
     * @param cfExpSkillsDescr the value of field 'cfExpSkillsDescr'
     */
    public void setCfExpSkillsDescr(final gr.ekt.cerif.schema.CfExpSkillsDescr cfExpSkillsDescr) {
        this.cfExpSkillsDescr = cfExpSkillsDescr;
        this._choiceValue = cfExpSkillsDescr;
    }

    /**
     * Sets the value of field 'cfExpSkillsKeyw'.
     * 
     * @param cfExpSkillsKeyw the value of field 'cfExpSkillsKeyw'.
     */
    public void setCfExpSkillsKeyw(final gr.ekt.cerif.schema.CfExpSkillsKeyw cfExpSkillsKeyw) {
        this.cfExpSkillsKeyw = cfExpSkillsKeyw;
        this._choiceValue = cfExpSkillsKeyw;
    }

    /**
     * Sets the value of field 'cfExpSkillsName'.
     * 
     * @param cfExpSkillsName the value of field 'cfExpSkillsName'.
     */
    public void setCfExpSkillsName(final gr.ekt.cerif.schema.CfExpSkillsName cfExpSkillsName) {
        this.cfExpSkillsName = cfExpSkillsName;
        this._choiceValue = cfExpSkillsName;
    }

    /**
     * Sets the value of field 'cfExpSkills_Class'.
     * 
     * @param cfExpSkills_Class the value of field
     * 'cfExpSkills_Class'.
     */
    public void setCfExpSkills_Class(final gr.ekt.cerif.schema.CfExpSkills_Class cfExpSkills_Class) {
        this.cfExpSkills_Class = cfExpSkills_Class;
        this._choiceValue = cfExpSkills_Class;
    }

    /**
     * Sets the value of field 'cfFDCRightsMMPricing'.
     * 
     * @param cfFDCRightsMMPricing the value of field
     * 'cfFDCRightsMMPricing'.
     */
    public void setCfFDCRightsMMPricing(final gr.ekt.cerif.schema.CfFDCRightsMMPricing cfFDCRightsMMPricing) {
        this.cfFDCRightsMMPricing = cfFDCRightsMMPricing;
        this._choiceValue = cfFDCRightsMMPricing;
    }

    /**
     * Sets the value of field 'cfFDCRightsMMPrivacy'.
     * 
     * @param cfFDCRightsMMPrivacy the value of field
     * 'cfFDCRightsMMPrivacy'.
     */
    public void setCfFDCRightsMMPrivacy(final gr.ekt.cerif.schema.CfFDCRightsMMPrivacy cfFDCRightsMMPrivacy) {
        this.cfFDCRightsMMPrivacy = cfFDCRightsMMPrivacy;
        this._choiceValue = cfFDCRightsMMPrivacy;
    }

    /**
     * Sets the value of field 'cfFDCRightsMMRights'.
     * 
     * @param cfFDCRightsMMRights the value of field
     * 'cfFDCRightsMMRights'.
     */
    public void setCfFDCRightsMMRights(final gr.ekt.cerif.schema.CfFDCRightsMMRights cfFDCRightsMMRights) {
        this.cfFDCRightsMMRights = cfFDCRightsMMRights;
        this._choiceValue = cfFDCRightsMMRights;
    }

    /**
     * Sets the value of field 'cfFDCRightsMMSecurity'.
     * 
     * @param cfFDCRightsMMSecurity the value of field
     * 'cfFDCRightsMMSecurity'.
     */
    public void setCfFDCRightsMMSecurity(final gr.ekt.cerif.schema.CfFDCRightsMMSecurity cfFDCRightsMMSecurity) {
        this.cfFDCRightsMMSecurity = cfFDCRightsMMSecurity;
        this._choiceValue = cfFDCRightsMMSecurity;
    }

    /**
     * Sets the value of field 'cfFacil'.
     * 
     * @param cfFacil the value of field 'cfFacil'.
     */
    public void setCfFacil(final gr.ekt.cerif.schema.CfFacil cfFacil) {
        this.cfFacil = cfFacil;
        this._choiceValue = cfFacil;
    }

    /**
     * Sets the value of field 'cfFacilDescr'.
     * 
     * @param cfFacilDescr the value of field 'cfFacilDescr'.
     */
    public void setCfFacilDescr(final gr.ekt.cerif.schema.CfFacilDescr cfFacilDescr) {
        this.cfFacilDescr = cfFacilDescr;
        this._choiceValue = cfFacilDescr;
    }

    /**
     * Sets the value of field 'cfFacilKeyw'.
     * 
     * @param cfFacilKeyw the value of field 'cfFacilKeyw'.
     */
    public void setCfFacilKeyw(final gr.ekt.cerif.schema.CfFacilKeyw cfFacilKeyw) {
        this.cfFacilKeyw = cfFacilKeyw;
        this._choiceValue = cfFacilKeyw;
    }

    /**
     * Sets the value of field 'cfFacilName'.
     * 
     * @param cfFacilName the value of field 'cfFacilName'.
     */
    public void setCfFacilName(final gr.ekt.cerif.schema.CfFacilName cfFacilName) {
        this.cfFacilName = cfFacilName;
        this._choiceValue = cfFacilName;
    }

    /**
     * Sets the value of field 'cfFacil_Class'.
     * 
     * @param cfFacil_Class the value of field 'cfFacil_Class'.
     */
    public void setCfFacil_Class(final gr.ekt.cerif.schema.CfFacil_Class cfFacil_Class) {
        this.cfFacil_Class = cfFacil_Class;
        this._choiceValue = cfFacil_Class;
    }

    /**
     * Sets the value of field 'cfFacil_Equip'.
     * 
     * @param cfFacil_Equip the value of field 'cfFacil_Equip'.
     */
    public void setCfFacil_Equip(final gr.ekt.cerif.schema.CfFacil_Equip cfFacil_Equip) {
        this.cfFacil_Equip = cfFacil_Equip;
        this._choiceValue = cfFacil_Equip;
    }

    /**
     * Sets the value of field 'cfFacil_Event'.
     * 
     * @param cfFacil_Event the value of field 'cfFacil_Event'.
     */
    public void setCfFacil_Event(final gr.ekt.cerif.schema.CfFacil_Event cfFacil_Event) {
        this.cfFacil_Event = cfFacil_Event;
        this._choiceValue = cfFacil_Event;
    }

    /**
     * Sets the value of field 'cfFacil_Facil'.
     * 
     * @param cfFacil_Facil the value of field 'cfFacil_Facil'.
     */
    public void setCfFacil_Facil(final gr.ekt.cerif.schema.CfFacil_Facil cfFacil_Facil) {
        this.cfFacil_Facil = cfFacil_Facil;
        this._choiceValue = cfFacil_Facil;
    }

    /**
     * Sets the value of field 'cfFacil_Fund'.
     * 
     * @param cfFacil_Fund the value of field 'cfFacil_Fund'.
     */
    public void setCfFacil_Fund(final gr.ekt.cerif.schema.CfFacil_Fund cfFacil_Fund) {
        this.cfFacil_Fund = cfFacil_Fund;
        this._choiceValue = cfFacil_Fund;
    }

    /**
     * Sets the value of field 'cfFacil_Indic'.
     * 
     * @param cfFacil_Indic the value of field 'cfFacil_Indic'.
     */
    public void setCfFacil_Indic(final gr.ekt.cerif.schema.CfFacil_Indic cfFacil_Indic) {
        this.cfFacil_Indic = cfFacil_Indic;
        this._choiceValue = cfFacil_Indic;
    }

    /**
     * Sets the value of field 'cfFacil_Meas'.
     * 
     * @param cfFacil_Meas the value of field 'cfFacil_Meas'.
     */
    public void setCfFacil_Meas(final gr.ekt.cerif.schema.CfFacil_Meas cfFacil_Meas) {
        this.cfFacil_Meas = cfFacil_Meas;
        this._choiceValue = cfFacil_Meas;
    }

    /**
     * Sets the value of field 'cfFacil_Medium'.
     * 
     * @param cfFacil_Medium the value of field 'cfFacil_Medium'.
     */
    public void setCfFacil_Medium(final gr.ekt.cerif.schema.CfFacil_Medium cfFacil_Medium) {
        this.cfFacil_Medium = cfFacil_Medium;
        this._choiceValue = cfFacil_Medium;
    }

    /**
     * Sets the value of field 'cfFacil_PAddr'.
     * 
     * @param cfFacil_PAddr the value of field 'cfFacil_PAddr'.
     */
    public void setCfFacil_PAddr(final gr.ekt.cerif.schema.CfFacil_PAddr cfFacil_PAddr) {
        this.cfFacil_PAddr = cfFacil_PAddr;
        this._choiceValue = cfFacil_PAddr;
    }

    /**
     * Sets the value of field 'cfFacil_Srv'.
     * 
     * @param cfFacil_Srv the value of field 'cfFacil_Srv'.
     */
    public void setCfFacil_Srv(final gr.ekt.cerif.schema.CfFacil_Srv cfFacil_Srv) {
        this.cfFacil_Srv = cfFacil_Srv;
        this._choiceValue = cfFacil_Srv;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfFedId cfFedId) {
        this.cfFedId = cfFedId;
        this._choiceValue = cfFedId;
    }

    /**
     * Sets the value of field 'cfFedId_Class'.
     * 
     * @param cfFedId_Class the value of field 'cfFedId_Class'.
     */
    public void setCfFedId_Class(final gr.ekt.cerif.schema.CfFedId_Class cfFedId_Class) {
        this.cfFedId_Class = cfFedId_Class;
        this._choiceValue = cfFedId_Class;
    }

    /**
     * Sets the value of field 'cfFedId_Srv'.
     * 
     * @param cfFedId_Srv the value of field 'cfFedId_Srv'.
     */
    public void setCfFedId_Srv(final gr.ekt.cerif.schema.CfFedId_Srv cfFedId_Srv) {
        this.cfFedId_Srv = cfFedId_Srv;
        this._choiceValue = cfFedId_Srv;
    }

    /**
     * Sets the value of field 'cfFund'.
     * 
     * @param cfFund the value of field 'cfFund'.
     */
    public void setCfFund(final gr.ekt.cerif.schema.CfFund cfFund) {
        this.cfFund = cfFund;
        this._choiceValue = cfFund;
    }

    /**
     * Sets the value of field 'cfFundDescr'.
     * 
     * @param cfFundDescr the value of field 'cfFundDescr'.
     */
    public void setCfFundDescr(final gr.ekt.cerif.schema.CfFundDescr cfFundDescr) {
        this.cfFundDescr = cfFundDescr;
        this._choiceValue = cfFundDescr;
    }

    /**
     * Sets the value of field 'cfFundKeyw'.
     * 
     * @param cfFundKeyw the value of field 'cfFundKeyw'.
     */
    public void setCfFundKeyw(final gr.ekt.cerif.schema.CfFundKeyw cfFundKeyw) {
        this.cfFundKeyw = cfFundKeyw;
        this._choiceValue = cfFundKeyw;
    }

    /**
     * Sets the value of field 'cfFundName'.
     * 
     * @param cfFundName the value of field 'cfFundName'.
     */
    public void setCfFundName(final gr.ekt.cerif.schema.CfFundName cfFundName) {
        this.cfFundName = cfFundName;
        this._choiceValue = cfFundName;
    }

    /**
     * Sets the value of field 'cfFund_Class'.
     * 
     * @param cfFund_Class the value of field 'cfFund_Class'.
     */
    public void setCfFund_Class(final gr.ekt.cerif.schema.CfFund_Class cfFund_Class) {
        this.cfFund_Class = cfFund_Class;
        this._choiceValue = cfFund_Class;
    }

    /**
     * Sets the value of field 'cfFund_Fund'.
     * 
     * @param cfFund_Fund the value of field 'cfFund_Fund'.
     */
    public void setCfFund_Fund(final gr.ekt.cerif.schema.CfFund_Fund cfFund_Fund) {
        this.cfFund_Fund = cfFund_Fund;
        this._choiceValue = cfFund_Fund;
    }

    /**
     * Sets the value of field 'cfFund_Indic'.
     * 
     * @param cfFund_Indic the value of field 'cfFund_Indic'.
     */
    public void setCfFund_Indic(final gr.ekt.cerif.schema.CfFund_Indic cfFund_Indic) {
        this.cfFund_Indic = cfFund_Indic;
        this._choiceValue = cfFund_Indic;
    }

    /**
     * Sets the value of field 'cfFund_Meas'.
     * 
     * @param cfFund_Meas the value of field 'cfFund_Meas'.
     */
    public void setCfFund_Meas(final gr.ekt.cerif.schema.CfFund_Meas cfFund_Meas) {
        this.cfFund_Meas = cfFund_Meas;
        this._choiceValue = cfFund_Meas;
    }

    /**
     * Sets the value of field 'cfGeoBBox'.
     * 
     * @param cfGeoBBox the value of field 'cfGeoBBox'.
     */
    public void setCfGeoBBox(final gr.ekt.cerif.schema.CfGeoBBox cfGeoBBox) {
        this.cfGeoBBox = cfGeoBBox;
        this._choiceValue = cfGeoBBox;
    }

    /**
     * Sets the value of field 'cfGeoBBoxDescr'.
     * 
     * @param cfGeoBBoxDescr the value of field 'cfGeoBBoxDescr'.
     */
    public void setCfGeoBBoxDescr(final gr.ekt.cerif.schema.CfGeoBBoxDescr cfGeoBBoxDescr) {
        this.cfGeoBBoxDescr = cfGeoBBoxDescr;
        this._choiceValue = cfGeoBBoxDescr;
    }

    /**
     * Sets the value of field 'cfGeoBBoxKeyw'.
     * 
     * @param cfGeoBBoxKeyw the value of field 'cfGeoBBoxKeyw'.
     */
    public void setCfGeoBBoxKeyw(final gr.ekt.cerif.schema.CfGeoBBoxKeyw cfGeoBBoxKeyw) {
        this.cfGeoBBoxKeyw = cfGeoBBoxKeyw;
        this._choiceValue = cfGeoBBoxKeyw;
    }

    /**
     * Sets the value of field 'cfGeoBBoxName'.
     * 
     * @param cfGeoBBoxName the value of field 'cfGeoBBoxName'.
     */
    public void setCfGeoBBoxName(final gr.ekt.cerif.schema.CfGeoBBoxName cfGeoBBoxName) {
        this.cfGeoBBoxName = cfGeoBBoxName;
        this._choiceValue = cfGeoBBoxName;
    }

    /**
     * Sets the value of field 'cfGeoBBox_Class'.
     * 
     * @param cfGeoBBox_Class the value of field 'cfGeoBBox_Class'.
     */
    public void setCfGeoBBox_Class(final gr.ekt.cerif.schema.CfGeoBBox_Class cfGeoBBox_Class) {
        this.cfGeoBBox_Class = cfGeoBBox_Class;
        this._choiceValue = cfGeoBBox_Class;
    }

    /**
     * Sets the value of field 'cfGeoBBox_GeoBBox'.
     * 
     * @param cfGeoBBox_GeoBBox the value of field
     * 'cfGeoBBox_GeoBBox'.
     */
    public void setCfGeoBBox_GeoBBox(final gr.ekt.cerif.schema.CfGeoBBox_GeoBBox cfGeoBBox_GeoBBox) {
        this.cfGeoBBox_GeoBBox = cfGeoBBox_GeoBBox;
        this._choiceValue = cfGeoBBox_GeoBBox;
    }

    /**
     * Sets the value of field 'cfGeoBBox_Meas'.
     * 
     * @param cfGeoBBox_Meas the value of field 'cfGeoBBox_Meas'.
     */
    public void setCfGeoBBox_Meas(final gr.ekt.cerif.schema.CfGeoBBox_Meas cfGeoBBox_Meas) {
        this.cfGeoBBox_Meas = cfGeoBBox_Meas;
        this._choiceValue = cfGeoBBox_Meas;
    }

    /**
     * Sets the value of field 'cfIndic'.
     * 
     * @param cfIndic the value of field 'cfIndic'.
     */
    public void setCfIndic(final gr.ekt.cerif.schema.CfIndic cfIndic) {
        this.cfIndic = cfIndic;
        this._choiceValue = cfIndic;
    }

    /**
     * Sets the value of field 'cfIndicDescr'.
     * 
     * @param cfIndicDescr the value of field 'cfIndicDescr'.
     */
    public void setCfIndicDescr(final gr.ekt.cerif.schema.CfIndicDescr cfIndicDescr) {
        this.cfIndicDescr = cfIndicDescr;
        this._choiceValue = cfIndicDescr;
    }

    /**
     * Sets the value of field 'cfIndicKeyw'.
     * 
     * @param cfIndicKeyw the value of field 'cfIndicKeyw'.
     */
    public void setCfIndicKeyw(final gr.ekt.cerif.schema.CfIndicKeyw cfIndicKeyw) {
        this.cfIndicKeyw = cfIndicKeyw;
        this._choiceValue = cfIndicKeyw;
    }

    /**
     * Sets the value of field 'cfIndicName'.
     * 
     * @param cfIndicName the value of field 'cfIndicName'.
     */
    public void setCfIndicName(final gr.ekt.cerif.schema.CfIndicName cfIndicName) {
        this.cfIndicName = cfIndicName;
        this._choiceValue = cfIndicName;
    }

    /**
     * Sets the value of field 'cfIndic_Class'.
     * 
     * @param cfIndic_Class the value of field 'cfIndic_Class'.
     */
    public void setCfIndic_Class(final gr.ekt.cerif.schema.CfIndic_Class cfIndic_Class) {
        this.cfIndic_Class = cfIndic_Class;
        this._choiceValue = cfIndic_Class;
    }

    /**
     * Sets the value of field 'cfIndic_Indic'.
     * 
     * @param cfIndic_Indic the value of field 'cfIndic_Indic'.
     */
    public void setCfIndic_Indic(final gr.ekt.cerif.schema.CfIndic_Indic cfIndic_Indic) {
        this.cfIndic_Indic = cfIndic_Indic;
        this._choiceValue = cfIndic_Indic;
    }

    /**
     * Sets the value of field 'cfIndic_Meas'.
     * 
     * @param cfIndic_Meas the value of field 'cfIndic_Meas'.
     */
    public void setCfIndic_Meas(final gr.ekt.cerif.schema.CfIndic_Meas cfIndic_Meas) {
        this.cfIndic_Meas = cfIndic_Meas;
        this._choiceValue = cfIndic_Meas;
    }

    /**
     * Sets the value of field 'cfLang'.
     * 
     * @param cfLang the value of field 'cfLang'.
     */
    public void setCfLang(final gr.ekt.cerif.schema.CfLang cfLang) {
        this.cfLang = cfLang;
        this._choiceValue = cfLang;
    }

    /**
     * Sets the value of field 'cfLangName'.
     * 
     * @param cfLangName the value of field 'cfLangName'.
     */
    public void setCfLangName(final gr.ekt.cerif.schema.CfLangName cfLangName) {
        this.cfLangName = cfLangName;
        this._choiceValue = cfLangName;
    }

    /**
     * Sets the value of field 'cfLang_Class'.
     * 
     * @param cfLang_Class the value of field 'cfLang_Class'.
     */
    public void setCfLang_Class(final gr.ekt.cerif.schema.CfLang_Class cfLang_Class) {
        this.cfLang_Class = cfLang_Class;
        this._choiceValue = cfLang_Class;
    }

    /**
     * Sets the value of field 'cfMeas'.
     * 
     * @param cfMeas the value of field 'cfMeas'.
     */
    public void setCfMeas(final gr.ekt.cerif.schema.CfMeas cfMeas) {
        this.cfMeas = cfMeas;
        this._choiceValue = cfMeas;
    }

    /**
     * Sets the value of field 'cfMeasDescr'.
     * 
     * @param cfMeasDescr the value of field 'cfMeasDescr'.
     */
    public void setCfMeasDescr(final gr.ekt.cerif.schema.CfMeasDescr cfMeasDescr) {
        this.cfMeasDescr = cfMeasDescr;
        this._choiceValue = cfMeasDescr;
    }

    /**
     * Sets the value of field 'cfMeasKeyw'.
     * 
     * @param cfMeasKeyw the value of field 'cfMeasKeyw'.
     */
    public void setCfMeasKeyw(final gr.ekt.cerif.schema.CfMeasKeyw cfMeasKeyw) {
        this.cfMeasKeyw = cfMeasKeyw;
        this._choiceValue = cfMeasKeyw;
    }

    /**
     * Sets the value of field 'cfMeasName'.
     * 
     * @param cfMeasName the value of field 'cfMeasName'.
     */
    public void setCfMeasName(final gr.ekt.cerif.schema.CfMeasName cfMeasName) {
        this.cfMeasName = cfMeasName;
        this._choiceValue = cfMeasName;
    }

    /**
     * Sets the value of field 'cfMeas_Class'.
     * 
     * @param cfMeas_Class the value of field 'cfMeas_Class'.
     */
    public void setCfMeas_Class(final gr.ekt.cerif.schema.CfMeas_Class cfMeas_Class) {
        this.cfMeas_Class = cfMeas_Class;
        this._choiceValue = cfMeas_Class;
    }

    /**
     * Sets the value of field 'cfMeas_Meas'.
     * 
     * @param cfMeas_Meas the value of field 'cfMeas_Meas'.
     */
    public void setCfMeas_Meas(final gr.ekt.cerif.schema.CfMeas_Meas cfMeas_Meas) {
        this.cfMeas_Meas = cfMeas_Meas;
        this._choiceValue = cfMeas_Meas;
    }

    /**
     * Sets the value of field 'cfMedium'.
     * 
     * @param cfMedium the value of field 'cfMedium'.
     */
    public void setCfMedium(final gr.ekt.cerif.schema.CfMedium cfMedium) {
        this.cfMedium = cfMedium;
        this._choiceValue = cfMedium;
    }

    /**
     * Sets the value of field 'cfMediumDescr'.
     * 
     * @param cfMediumDescr the value of field 'cfMediumDescr'.
     */
    public void setCfMediumDescr(final gr.ekt.cerif.schema.CfMediumDescr cfMediumDescr) {
        this.cfMediumDescr = cfMediumDescr;
        this._choiceValue = cfMediumDescr;
    }

    /**
     * Sets the value of field 'cfMediumKeyw'.
     * 
     * @param cfMediumKeyw the value of field 'cfMediumKeyw'.
     */
    public void setCfMediumKeyw(final gr.ekt.cerif.schema.CfMediumKeyw cfMediumKeyw) {
        this.cfMediumKeyw = cfMediumKeyw;
        this._choiceValue = cfMediumKeyw;
    }

    /**
     * Sets the value of field 'cfMediumTitle'.
     * 
     * @param cfMediumTitle the value of field 'cfMediumTitle'.
     */
    public void setCfMediumTitle(final gr.ekt.cerif.schema.CfMediumTitle cfMediumTitle) {
        this.cfMediumTitle = cfMediumTitle;
        this._choiceValue = cfMediumTitle;
    }

    /**
     * Sets the value of field 'cfMedium_Class'.
     * 
     * @param cfMedium_Class the value of field 'cfMedium_Class'.
     */
    public void setCfMedium_Class(final gr.ekt.cerif.schema.CfMedium_Class cfMedium_Class) {
        this.cfMedium_Class = cfMedium_Class;
        this._choiceValue = cfMedium_Class;
    }

    /**
     * Sets the value of field 'cfMedium_Fund'.
     * 
     * @param cfMedium_Fund the value of field 'cfMedium_Fund'.
     */
    public void setCfMedium_Fund(final gr.ekt.cerif.schema.CfMedium_Fund cfMedium_Fund) {
        this.cfMedium_Fund = cfMedium_Fund;
        this._choiceValue = cfMedium_Fund;
    }

    /**
     * Sets the value of field 'cfMedium_Indic'.
     * 
     * @param cfMedium_Indic the value of field 'cfMedium_Indic'.
     */
    public void setCfMedium_Indic(final gr.ekt.cerif.schema.CfMedium_Indic cfMedium_Indic) {
        this.cfMedium_Indic = cfMedium_Indic;
        this._choiceValue = cfMedium_Indic;
    }

    /**
     * Sets the value of field 'cfMedium_Meas'.
     * 
     * @param cfMedium_Meas the value of field 'cfMedium_Meas'.
     */
    public void setCfMedium_Meas(final gr.ekt.cerif.schema.CfMedium_Meas cfMedium_Meas) {
        this.cfMedium_Meas = cfMedium_Meas;
        this._choiceValue = cfMedium_Meas;
    }

    /**
     * Sets the value of field 'cfMedium_Medium'.
     * 
     * @param cfMedium_Medium the value of field 'cfMedium_Medium'.
     */
    public void setCfMedium_Medium(final gr.ekt.cerif.schema.CfMedium_Medium cfMedium_Medium) {
        this.cfMedium_Medium = cfMedium_Medium;
        this._choiceValue = cfMedium_Medium;
    }

    /**
     * Sets the value of field 'cfMetrics'.
     * 
     * @param cfMetrics the value of field 'cfMetrics'.
     */
    public void setCfMetrics(final gr.ekt.cerif.schema.CfMetrics cfMetrics) {
        this.cfMetrics = cfMetrics;
        this._choiceValue = cfMetrics;
    }

    /**
     * Sets the value of field 'cfMetricsDescr'.
     * 
     * @param cfMetricsDescr the value of field 'cfMetricsDescr'.
     */
    public void setCfMetricsDescr(final gr.ekt.cerif.schema.CfMetricsDescr cfMetricsDescr) {
        this.cfMetricsDescr = cfMetricsDescr;
        this._choiceValue = cfMetricsDescr;
    }

    /**
     * Sets the value of field 'cfMetricsKeyw'.
     * 
     * @param cfMetricsKeyw the value of field 'cfMetricsKeyw'.
     */
    public void setCfMetricsKeyw(final gr.ekt.cerif.schema.CfMetricsKeyw cfMetricsKeyw) {
        this.cfMetricsKeyw = cfMetricsKeyw;
        this._choiceValue = cfMetricsKeyw;
    }

    /**
     * Sets the value of field 'cfMetricsName'.
     * 
     * @param cfMetricsName the value of field 'cfMetricsName'.
     */
    public void setCfMetricsName(final gr.ekt.cerif.schema.CfMetricsName cfMetricsName) {
        this.cfMetricsName = cfMetricsName;
        this._choiceValue = cfMetricsName;
    }

    /**
     * Sets the value of field 'cfMetrics_Class'.
     * 
     * @param cfMetrics_Class the value of field 'cfMetrics_Class'.
     */
    public void setCfMetrics_Class(final gr.ekt.cerif.schema.CfMetrics_Class cfMetrics_Class) {
        this.cfMetrics_Class = cfMetrics_Class;
        this._choiceValue = cfMetrics_Class;
    }

    /**
     * Sets the value of field 'cfOrgUnit'.
     * 
     * @param cfOrgUnit the value of field 'cfOrgUnit'.
     */
    public void setCfOrgUnit(final gr.ekt.cerif.schema.CfOrgUnit cfOrgUnit) {
        this.cfOrgUnit = cfOrgUnit;
        this._choiceValue = cfOrgUnit;
    }

    /**
     * Sets the value of field 'cfOrgUnitKeyw'.
     * 
     * @param cfOrgUnitKeyw the value of field 'cfOrgUnitKeyw'.
     */
    public void setCfOrgUnitKeyw(final gr.ekt.cerif.schema.CfOrgUnitKeyw cfOrgUnitKeyw) {
        this.cfOrgUnitKeyw = cfOrgUnitKeyw;
        this._choiceValue = cfOrgUnitKeyw;
    }

    /**
     * Sets the value of field 'cfOrgUnitName'.
     * 
     * @param cfOrgUnitName the value of field 'cfOrgUnitName'.
     */
    public void setCfOrgUnitName(final gr.ekt.cerif.schema.CfOrgUnitName cfOrgUnitName) {
        this.cfOrgUnitName = cfOrgUnitName;
        this._choiceValue = cfOrgUnitName;
    }

    /**
     * Sets the value of field 'cfOrgUnitResAct'.
     * 
     * @param cfOrgUnitResAct the value of field 'cfOrgUnitResAct'.
     */
    public void setCfOrgUnitResAct(final gr.ekt.cerif.schema.CfOrgUnitResAct cfOrgUnitResAct) {
        this.cfOrgUnitResAct = cfOrgUnitResAct;
        this._choiceValue = cfOrgUnitResAct;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Class'.
     * 
     * @param cfOrgUnit_Class the value of field 'cfOrgUnit_Class'.
     */
    public void setCfOrgUnit_Class(final gr.ekt.cerif.schema.CfOrgUnit_Class cfOrgUnit_Class) {
        this.cfOrgUnit_Class = cfOrgUnit_Class;
        this._choiceValue = cfOrgUnit_Class;
    }

    /**
     * Sets the value of field 'cfOrgUnit_DC'.
     * 
     * @param cfOrgUnit_DC the value of field 'cfOrgUnit_DC'.
     */
    public void setCfOrgUnit_DC(final gr.ekt.cerif.schema.CfOrgUnit_DC cfOrgUnit_DC) {
        this.cfOrgUnit_DC = cfOrgUnit_DC;
        this._choiceValue = cfOrgUnit_DC;
    }

    /**
     * Sets the value of field 'cfOrgUnit_EAddr'.
     * 
     * @param cfOrgUnit_EAddr the value of field 'cfOrgUnit_EAddr'.
     */
    public void setCfOrgUnit_EAddr(final gr.ekt.cerif.schema.CfOrgUnit_EAddr cfOrgUnit_EAddr) {
        this.cfOrgUnit_EAddr = cfOrgUnit_EAddr;
        this._choiceValue = cfOrgUnit_EAddr;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Equip'.
     * 
     * @param cfOrgUnit_Equip the value of field 'cfOrgUnit_Equip'.
     */
    public void setCfOrgUnit_Equip(final gr.ekt.cerif.schema.CfOrgUnit_Equip cfOrgUnit_Equip) {
        this.cfOrgUnit_Equip = cfOrgUnit_Equip;
        this._choiceValue = cfOrgUnit_Equip;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Event'.
     * 
     * @param cfOrgUnit_Event the value of field 'cfOrgUnit_Event'.
     */
    public void setCfOrgUnit_Event(final gr.ekt.cerif.schema.CfOrgUnit_Event cfOrgUnit_Event) {
        this.cfOrgUnit_Event = cfOrgUnit_Event;
        this._choiceValue = cfOrgUnit_Event;
    }

    /**
     * Sets the value of field 'cfOrgUnit_ExpSkills'.
     * 
     * @param cfOrgUnit_ExpSkills the value of field
     * 'cfOrgUnit_ExpSkills'.
     */
    public void setCfOrgUnit_ExpSkills(final gr.ekt.cerif.schema.CfOrgUnit_ExpSkills cfOrgUnit_ExpSkills) {
        this.cfOrgUnit_ExpSkills = cfOrgUnit_ExpSkills;
        this._choiceValue = cfOrgUnit_ExpSkills;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Facil'.
     * 
     * @param cfOrgUnit_Facil the value of field 'cfOrgUnit_Facil'.
     */
    public void setCfOrgUnit_Facil(final gr.ekt.cerif.schema.CfOrgUnit_Facil cfOrgUnit_Facil) {
        this.cfOrgUnit_Facil = cfOrgUnit_Facil;
        this._choiceValue = cfOrgUnit_Facil;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Fund'.
     * 
     * @param cfOrgUnit_Fund the value of field 'cfOrgUnit_Fund'.
     */
    public void setCfOrgUnit_Fund(final gr.ekt.cerif.schema.CfOrgUnit_Fund cfOrgUnit_Fund) {
        this.cfOrgUnit_Fund = cfOrgUnit_Fund;
        this._choiceValue = cfOrgUnit_Fund;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Indic'.
     * 
     * @param cfOrgUnit_Indic the value of field 'cfOrgUnit_Indic'.
     */
    public void setCfOrgUnit_Indic(final gr.ekt.cerif.schema.CfOrgUnit_Indic cfOrgUnit_Indic) {
        this.cfOrgUnit_Indic = cfOrgUnit_Indic;
        this._choiceValue = cfOrgUnit_Indic;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Meas'.
     * 
     * @param cfOrgUnit_Meas the value of field 'cfOrgUnit_Meas'.
     */
    public void setCfOrgUnit_Meas(final gr.ekt.cerif.schema.CfOrgUnit_Meas cfOrgUnit_Meas) {
        this.cfOrgUnit_Meas = cfOrgUnit_Meas;
        this._choiceValue = cfOrgUnit_Meas;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Medium'.
     * 
     * @param cfOrgUnit_Medium the value of field 'cfOrgUnit_Medium'
     */
    public void setCfOrgUnit_Medium(final gr.ekt.cerif.schema.CfOrgUnit_Medium cfOrgUnit_Medium) {
        this.cfOrgUnit_Medium = cfOrgUnit_Medium;
        this._choiceValue = cfOrgUnit_Medium;
    }

    /**
     * Sets the value of field 'cfOrgUnit_OrgUnit'.
     * 
     * @param cfOrgUnit_OrgUnit the value of field
     * 'cfOrgUnit_OrgUnit'.
     */
    public void setCfOrgUnit_OrgUnit(final gr.ekt.cerif.schema.CfOrgUnit_OrgUnit cfOrgUnit_OrgUnit) {
        this.cfOrgUnit_OrgUnit = cfOrgUnit_OrgUnit;
        this._choiceValue = cfOrgUnit_OrgUnit;
    }

    /**
     * Sets the value of field 'cfOrgUnit_PAddr'.
     * 
     * @param cfOrgUnit_PAddr the value of field 'cfOrgUnit_PAddr'.
     */
    public void setCfOrgUnit_PAddr(final gr.ekt.cerif.schema.CfOrgUnit_PAddr cfOrgUnit_PAddr) {
        this.cfOrgUnit_PAddr = cfOrgUnit_PAddr;
        this._choiceValue = cfOrgUnit_PAddr;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Prize'.
     * 
     * @param cfOrgUnit_Prize the value of field 'cfOrgUnit_Prize'.
     */
    public void setCfOrgUnit_Prize(final gr.ekt.cerif.schema.CfOrgUnit_Prize cfOrgUnit_Prize) {
        this.cfOrgUnit_Prize = cfOrgUnit_Prize;
        this._choiceValue = cfOrgUnit_Prize;
    }

    /**
     * Sets the value of field 'cfOrgUnit_ResPat'.
     * 
     * @param cfOrgUnit_ResPat the value of field 'cfOrgUnit_ResPat'
     */
    public void setCfOrgUnit_ResPat(final gr.ekt.cerif.schema.CfOrgUnit_ResPat cfOrgUnit_ResPat) {
        this.cfOrgUnit_ResPat = cfOrgUnit_ResPat;
        this._choiceValue = cfOrgUnit_ResPat;
    }

    /**
     * Sets the value of field 'cfOrgUnit_ResProd'.
     * 
     * @param cfOrgUnit_ResProd the value of field
     * 'cfOrgUnit_ResProd'.
     */
    public void setCfOrgUnit_ResProd(final gr.ekt.cerif.schema.CfOrgUnit_ResProd cfOrgUnit_ResProd) {
        this.cfOrgUnit_ResProd = cfOrgUnit_ResProd;
        this._choiceValue = cfOrgUnit_ResProd;
    }

    /**
     * Sets the value of field 'cfOrgUnit_ResPubl'.
     * 
     * @param cfOrgUnit_ResPubl the value of field
     * 'cfOrgUnit_ResPubl'.
     */
    public void setCfOrgUnit_ResPubl(final gr.ekt.cerif.schema.CfOrgUnit_ResPubl cfOrgUnit_ResPubl) {
        this.cfOrgUnit_ResPubl = cfOrgUnit_ResPubl;
        this._choiceValue = cfOrgUnit_ResPubl;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Srv'.
     * 
     * @param cfOrgUnit_Srv the value of field 'cfOrgUnit_Srv'.
     */
    public void setCfOrgUnit_Srv(final gr.ekt.cerif.schema.CfOrgUnit_Srv cfOrgUnit_Srv) {
        this.cfOrgUnit_Srv = cfOrgUnit_Srv;
        this._choiceValue = cfOrgUnit_Srv;
    }

    /**
     * Sets the value of field 'cfPAddr'.
     * 
     * @param cfPAddr the value of field 'cfPAddr'.
     */
    public void setCfPAddr(final gr.ekt.cerif.schema.CfPAddr cfPAddr) {
        this.cfPAddr = cfPAddr;
        this._choiceValue = cfPAddr;
    }

    /**
     * Sets the value of field 'cfPAddr_Class'.
     * 
     * @param cfPAddr_Class the value of field 'cfPAddr_Class'.
     */
    public void setCfPAddr_Class(final gr.ekt.cerif.schema.CfPAddr_Class cfPAddr_Class) {
        this.cfPAddr_Class = cfPAddr_Class;
        this._choiceValue = cfPAddr_Class;
    }

    /**
     * Sets the value of field 'cfPAddr_GeoBBox'.
     * 
     * @param cfPAddr_GeoBBox the value of field 'cfPAddr_GeoBBox'.
     */
    public void setCfPAddr_GeoBBox(final gr.ekt.cerif.schema.CfPAddr_GeoBBox cfPAddr_GeoBBox) {
        this.cfPAddr_GeoBBox = cfPAddr_GeoBBox;
        this._choiceValue = cfPAddr_GeoBBox;
    }

    /**
     * Sets the value of field 'cfPers'.
     * 
     * @param cfPers the value of field 'cfPers'.
     */
    public void setCfPers(final gr.ekt.cerif.schema.CfPers cfPers) {
        this.cfPers = cfPers;
        this._choiceValue = cfPers;
    }

    /**
     * Sets the value of field 'cfPersKeyw'.
     * 
     * @param cfPersKeyw the value of field 'cfPersKeyw'.
     */
    public void setCfPersKeyw(final gr.ekt.cerif.schema.CfPersKeyw cfPersKeyw) {
        this.cfPersKeyw = cfPersKeyw;
        this._choiceValue = cfPersKeyw;
    }

    /**
     * Sets the value of field 'cfPersName'.
     * 
     * @param cfPersName the value of field 'cfPersName'.
     */
    public void setCfPersName(final gr.ekt.cerif.schema.CfPersName cfPersName) {
        this.cfPersName = cfPersName;
        this._choiceValue = cfPersName;
    }

    /**
     * Sets the value of field 'cfPersName_Pers'.
     * 
     * @param cfPersName_Pers the value of field 'cfPersName_Pers'.
     */
    public void setCfPersName_Pers(final gr.ekt.cerif.schema.CfPersName_Pers cfPersName_Pers) {
        this.cfPersName_Pers = cfPersName_Pers;
        this._choiceValue = cfPersName_Pers;
    }

    /**
     * Sets the value of field 'cfPersResInt'.
     * 
     * @param cfPersResInt the value of field 'cfPersResInt'.
     */
    public void setCfPersResInt(final gr.ekt.cerif.schema.CfPersResInt cfPersResInt) {
        this.cfPersResInt = cfPersResInt;
        this._choiceValue = cfPersResInt;
    }

    /**
     * Sets the value of field 'cfPers_CV'.
     * 
     * @param cfPers_CV the value of field 'cfPers_CV'.
     */
    public void setCfPers_CV(final gr.ekt.cerif.schema.CfPers_CV cfPers_CV) {
        this.cfPers_CV = cfPers_CV;
        this._choiceValue = cfPers_CV;
    }

    /**
     * Sets the value of field 'cfPers_Class'.
     * 
     * @param cfPers_Class the value of field 'cfPers_Class'.
     */
    public void setCfPers_Class(final gr.ekt.cerif.schema.CfPers_Class cfPers_Class) {
        this.cfPers_Class = cfPers_Class;
        this._choiceValue = cfPers_Class;
    }

    /**
     * Sets the value of field 'cfPers_Country'.
     * 
     * @param cfPers_Country the value of field 'cfPers_Country'.
     */
    public void setCfPers_Country(final gr.ekt.cerif.schema.CfPers_Country cfPers_Country) {
        this.cfPers_Country = cfPers_Country;
        this._choiceValue = cfPers_Country;
    }

    /**
     * Sets the value of field 'cfPers_DC'.
     * 
     * @param cfPers_DC the value of field 'cfPers_DC'.
     */
    public void setCfPers_DC(final gr.ekt.cerif.schema.CfPers_DC cfPers_DC) {
        this.cfPers_DC = cfPers_DC;
        this._choiceValue = cfPers_DC;
    }

    /**
     * Sets the value of field 'cfPers_EAddr'.
     * 
     * @param cfPers_EAddr the value of field 'cfPers_EAddr'.
     */
    public void setCfPers_EAddr(final gr.ekt.cerif.schema.CfPers_EAddr cfPers_EAddr) {
        this.cfPers_EAddr = cfPers_EAddr;
        this._choiceValue = cfPers_EAddr;
    }

    /**
     * Sets the value of field 'cfPers_Equip'.
     * 
     * @param cfPers_Equip the value of field 'cfPers_Equip'.
     */
    public void setCfPers_Equip(final gr.ekt.cerif.schema.CfPers_Equip cfPers_Equip) {
        this.cfPers_Equip = cfPers_Equip;
        this._choiceValue = cfPers_Equip;
    }

    /**
     * Sets the value of field 'cfPers_Event'.
     * 
     * @param cfPers_Event the value of field 'cfPers_Event'.
     */
    public void setCfPers_Event(final gr.ekt.cerif.schema.CfPers_Event cfPers_Event) {
        this.cfPers_Event = cfPers_Event;
        this._choiceValue = cfPers_Event;
    }

    /**
     * Sets the value of field 'cfPers_ExpSkills'.
     * 
     * @param cfPers_ExpSkills the value of field 'cfPers_ExpSkills'
     */
    public void setCfPers_ExpSkills(final gr.ekt.cerif.schema.CfPers_ExpSkills cfPers_ExpSkills) {
        this.cfPers_ExpSkills = cfPers_ExpSkills;
        this._choiceValue = cfPers_ExpSkills;
    }

    /**
     * Sets the value of field 'cfPers_Facil'.
     * 
     * @param cfPers_Facil the value of field 'cfPers_Facil'.
     */
    public void setCfPers_Facil(final gr.ekt.cerif.schema.CfPers_Facil cfPers_Facil) {
        this.cfPers_Facil = cfPers_Facil;
        this._choiceValue = cfPers_Facil;
    }

    /**
     * Sets the value of field 'cfPers_Fund'.
     * 
     * @param cfPers_Fund the value of field 'cfPers_Fund'.
     */
    public void setCfPers_Fund(final gr.ekt.cerif.schema.CfPers_Fund cfPers_Fund) {
        this.cfPers_Fund = cfPers_Fund;
        this._choiceValue = cfPers_Fund;
    }

    /**
     * Sets the value of field 'cfPers_Indic'.
     * 
     * @param cfPers_Indic the value of field 'cfPers_Indic'.
     */
    public void setCfPers_Indic(final gr.ekt.cerif.schema.CfPers_Indic cfPers_Indic) {
        this.cfPers_Indic = cfPers_Indic;
        this._choiceValue = cfPers_Indic;
    }

    /**
     * Sets the value of field 'cfPers_Lang'.
     * 
     * @param cfPers_Lang the value of field 'cfPers_Lang'.
     */
    public void setCfPers_Lang(final gr.ekt.cerif.schema.CfPers_Lang cfPers_Lang) {
        this.cfPers_Lang = cfPers_Lang;
        this._choiceValue = cfPers_Lang;
    }

    /**
     * Sets the value of field 'cfPers_Meas'.
     * 
     * @param cfPers_Meas the value of field 'cfPers_Meas'.
     */
    public void setCfPers_Meas(final gr.ekt.cerif.schema.CfPers_Meas cfPers_Meas) {
        this.cfPers_Meas = cfPers_Meas;
        this._choiceValue = cfPers_Meas;
    }

    /**
     * Sets the value of field 'cfPers_Medium'.
     * 
     * @param cfPers_Medium the value of field 'cfPers_Medium'.
     */
    public void setCfPers_Medium(final gr.ekt.cerif.schema.CfPers_Medium cfPers_Medium) {
        this.cfPers_Medium = cfPers_Medium;
        this._choiceValue = cfPers_Medium;
    }

    /**
     * Sets the value of field 'cfPers_OrgUnit'.
     * 
     * @param cfPers_OrgUnit the value of field 'cfPers_OrgUnit'.
     */
    public void setCfPers_OrgUnit(final gr.ekt.cerif.schema.CfPers_OrgUnit cfPers_OrgUnit) {
        this.cfPers_OrgUnit = cfPers_OrgUnit;
        this._choiceValue = cfPers_OrgUnit;
    }

    /**
     * Sets the value of field 'cfPers_PAddr'.
     * 
     * @param cfPers_PAddr the value of field 'cfPers_PAddr'.
     */
    public void setCfPers_PAddr(final gr.ekt.cerif.schema.CfPers_PAddr cfPers_PAddr) {
        this.cfPers_PAddr = cfPers_PAddr;
        this._choiceValue = cfPers_PAddr;
    }

    /**
     * Sets the value of field 'cfPers_Pers'.
     * 
     * @param cfPers_Pers the value of field 'cfPers_Pers'.
     */
    public void setCfPers_Pers(final gr.ekt.cerif.schema.CfPers_Pers cfPers_Pers) {
        this.cfPers_Pers = cfPers_Pers;
        this._choiceValue = cfPers_Pers;
    }

    /**
     * Sets the value of field 'cfPers_Prize'.
     * 
     * @param cfPers_Prize the value of field 'cfPers_Prize'.
     */
    public void setCfPers_Prize(final gr.ekt.cerif.schema.CfPers_Prize cfPers_Prize) {
        this.cfPers_Prize = cfPers_Prize;
        this._choiceValue = cfPers_Prize;
    }

    /**
     * Sets the value of field 'cfPers_Qual'.
     * 
     * @param cfPers_Qual the value of field 'cfPers_Qual'.
     */
    public void setCfPers_Qual(final gr.ekt.cerif.schema.CfPers_Qual cfPers_Qual) {
        this.cfPers_Qual = cfPers_Qual;
        this._choiceValue = cfPers_Qual;
    }

    /**
     * Sets the value of field 'cfPers_ResPat'.
     * 
     * @param cfPers_ResPat the value of field 'cfPers_ResPat'.
     */
    public void setCfPers_ResPat(final gr.ekt.cerif.schema.CfPers_ResPat cfPers_ResPat) {
        this.cfPers_ResPat = cfPers_ResPat;
        this._choiceValue = cfPers_ResPat;
    }

    /**
     * Sets the value of field 'cfPers_ResProd'.
     * 
     * @param cfPers_ResProd the value of field 'cfPers_ResProd'.
     */
    public void setCfPers_ResProd(final gr.ekt.cerif.schema.CfPers_ResProd cfPers_ResProd) {
        this.cfPers_ResProd = cfPers_ResProd;
        this._choiceValue = cfPers_ResProd;
    }

    /**
     * Sets the value of field 'cfPers_ResPubl'.
     * 
     * @param cfPers_ResPubl the value of field 'cfPers_ResPubl'.
     */
    public void setCfPers_ResPubl(final gr.ekt.cerif.schema.CfPers_ResPubl cfPers_ResPubl) {
        this.cfPers_ResPubl = cfPers_ResPubl;
        this._choiceValue = cfPers_ResPubl;
    }

    /**
     * Sets the value of field 'cfPers_Srv'.
     * 
     * @param cfPers_Srv the value of field 'cfPers_Srv'.
     */
    public void setCfPers_Srv(final gr.ekt.cerif.schema.CfPers_Srv cfPers_Srv) {
        this.cfPers_Srv = cfPers_Srv;
        this._choiceValue = cfPers_Srv;
    }

    /**
     * Sets the value of field 'cfPrize'.
     * 
     * @param cfPrize the value of field 'cfPrize'.
     */
    public void setCfPrize(final gr.ekt.cerif.schema.CfPrize cfPrize) {
        this.cfPrize = cfPrize;
        this._choiceValue = cfPrize;
    }

    /**
     * Sets the value of field 'cfPrizeDescr'.
     * 
     * @param cfPrizeDescr the value of field 'cfPrizeDescr'.
     */
    public void setCfPrizeDescr(final gr.ekt.cerif.schema.CfPrizeDescr cfPrizeDescr) {
        this.cfPrizeDescr = cfPrizeDescr;
        this._choiceValue = cfPrizeDescr;
    }

    /**
     * Sets the value of field 'cfPrizeKeyw'.
     * 
     * @param cfPrizeKeyw the value of field 'cfPrizeKeyw'.
     */
    public void setCfPrizeKeyw(final gr.ekt.cerif.schema.CfPrizeKeyw cfPrizeKeyw) {
        this.cfPrizeKeyw = cfPrizeKeyw;
        this._choiceValue = cfPrizeKeyw;
    }

    /**
     * Sets the value of field 'cfPrizeName'.
     * 
     * @param cfPrizeName the value of field 'cfPrizeName'.
     */
    public void setCfPrizeName(final gr.ekt.cerif.schema.CfPrizeName cfPrizeName) {
        this.cfPrizeName = cfPrizeName;
        this._choiceValue = cfPrizeName;
    }

    /**
     * Sets the value of field 'cfPrize_Class'.
     * 
     * @param cfPrize_Class the value of field 'cfPrize_Class'.
     */
    public void setCfPrize_Class(final gr.ekt.cerif.schema.CfPrize_Class cfPrize_Class) {
        this.cfPrize_Class = cfPrize_Class;
        this._choiceValue = cfPrize_Class;
    }

    /**
     * Sets the value of field 'cfProj'.
     * 
     * @param cfProj the value of field 'cfProj'.
     */
    public void setCfProj(final gr.ekt.cerif.schema.CfProj cfProj) {
        this.cfProj = cfProj;
        this._choiceValue = cfProj;
    }

    /**
     * Sets the value of field 'cfProjAbstr'.
     * 
     * @param cfProjAbstr the value of field 'cfProjAbstr'.
     */
    public void setCfProjAbstr(final gr.ekt.cerif.schema.CfProjAbstr cfProjAbstr) {
        this.cfProjAbstr = cfProjAbstr;
        this._choiceValue = cfProjAbstr;
    }

    /**
     * Sets the value of field 'cfProjKeyw'.
     * 
     * @param cfProjKeyw the value of field 'cfProjKeyw'.
     */
    public void setCfProjKeyw(final gr.ekt.cerif.schema.CfProjKeyw cfProjKeyw) {
        this.cfProjKeyw = cfProjKeyw;
        this._choiceValue = cfProjKeyw;
    }

    /**
     * Sets the value of field 'cfProjTitle'.
     * 
     * @param cfProjTitle the value of field 'cfProjTitle'.
     */
    public void setCfProjTitle(final gr.ekt.cerif.schema.CfProjTitle cfProjTitle) {
        this.cfProjTitle = cfProjTitle;
        this._choiceValue = cfProjTitle;
    }

    /**
     * Sets the value of field 'cfProj_Class'.
     * 
     * @param cfProj_Class the value of field 'cfProj_Class'.
     */
    public void setCfProj_Class(final gr.ekt.cerif.schema.CfProj_Class cfProj_Class) {
        this.cfProj_Class = cfProj_Class;
        this._choiceValue = cfProj_Class;
    }

    /**
     * Sets the value of field 'cfProj_DC'.
     * 
     * @param cfProj_DC the value of field 'cfProj_DC'.
     */
    public void setCfProj_DC(final gr.ekt.cerif.schema.CfProj_DC cfProj_DC) {
        this.cfProj_DC = cfProj_DC;
        this._choiceValue = cfProj_DC;
    }

    /**
     * Sets the value of field 'cfProj_Equip'.
     * 
     * @param cfProj_Equip the value of field 'cfProj_Equip'.
     */
    public void setCfProj_Equip(final gr.ekt.cerif.schema.CfProj_Equip cfProj_Equip) {
        this.cfProj_Equip = cfProj_Equip;
        this._choiceValue = cfProj_Equip;
    }

    /**
     * Sets the value of field 'cfProj_Event'.
     * 
     * @param cfProj_Event the value of field 'cfProj_Event'.
     */
    public void setCfProj_Event(final gr.ekt.cerif.schema.CfProj_Event cfProj_Event) {
        this.cfProj_Event = cfProj_Event;
        this._choiceValue = cfProj_Event;
    }

    /**
     * Sets the value of field 'cfProj_Facil'.
     * 
     * @param cfProj_Facil the value of field 'cfProj_Facil'.
     */
    public void setCfProj_Facil(final gr.ekt.cerif.schema.CfProj_Facil cfProj_Facil) {
        this.cfProj_Facil = cfProj_Facil;
        this._choiceValue = cfProj_Facil;
    }

    /**
     * Sets the value of field 'cfProj_Fund'.
     * 
     * @param cfProj_Fund the value of field 'cfProj_Fund'.
     */
    public void setCfProj_Fund(final gr.ekt.cerif.schema.CfProj_Fund cfProj_Fund) {
        this.cfProj_Fund = cfProj_Fund;
        this._choiceValue = cfProj_Fund;
    }

    /**
     * Sets the value of field 'cfProj_Indic'.
     * 
     * @param cfProj_Indic the value of field 'cfProj_Indic'.
     */
    public void setCfProj_Indic(final gr.ekt.cerif.schema.CfProj_Indic cfProj_Indic) {
        this.cfProj_Indic = cfProj_Indic;
        this._choiceValue = cfProj_Indic;
    }

    /**
     * Sets the value of field 'cfProj_Meas'.
     * 
     * @param cfProj_Meas the value of field 'cfProj_Meas'.
     */
    public void setCfProj_Meas(final gr.ekt.cerif.schema.CfProj_Meas cfProj_Meas) {
        this.cfProj_Meas = cfProj_Meas;
        this._choiceValue = cfProj_Meas;
    }

    /**
     * Sets the value of field 'cfProj_Medium'.
     * 
     * @param cfProj_Medium the value of field 'cfProj_Medium'.
     */
    public void setCfProj_Medium(final gr.ekt.cerif.schema.CfProj_Medium cfProj_Medium) {
        this.cfProj_Medium = cfProj_Medium;
        this._choiceValue = cfProj_Medium;
    }

    /**
     * Sets the value of field 'cfProj_OrgUnit'.
     * 
     * @param cfProj_OrgUnit the value of field 'cfProj_OrgUnit'.
     */
    public void setCfProj_OrgUnit(final gr.ekt.cerif.schema.CfProj_OrgUnit cfProj_OrgUnit) {
        this.cfProj_OrgUnit = cfProj_OrgUnit;
        this._choiceValue = cfProj_OrgUnit;
    }

    /**
     * Sets the value of field 'cfProj_Pers'.
     * 
     * @param cfProj_Pers the value of field 'cfProj_Pers'.
     */
    public void setCfProj_Pers(final gr.ekt.cerif.schema.CfProj_Pers cfProj_Pers) {
        this.cfProj_Pers = cfProj_Pers;
        this._choiceValue = cfProj_Pers;
    }

    /**
     * Sets the value of field 'cfProj_Prize'.
     * 
     * @param cfProj_Prize the value of field 'cfProj_Prize'.
     */
    public void setCfProj_Prize(final gr.ekt.cerif.schema.CfProj_Prize cfProj_Prize) {
        this.cfProj_Prize = cfProj_Prize;
        this._choiceValue = cfProj_Prize;
    }

    /**
     * Sets the value of field 'cfProj_Proj'.
     * 
     * @param cfProj_Proj the value of field 'cfProj_Proj'.
     */
    public void setCfProj_Proj(final gr.ekt.cerif.schema.CfProj_Proj cfProj_Proj) {
        this.cfProj_Proj = cfProj_Proj;
        this._choiceValue = cfProj_Proj;
    }

    /**
     * Sets the value of field 'cfProj_ResPat'.
     * 
     * @param cfProj_ResPat the value of field 'cfProj_ResPat'.
     */
    public void setCfProj_ResPat(final gr.ekt.cerif.schema.CfProj_ResPat cfProj_ResPat) {
        this.cfProj_ResPat = cfProj_ResPat;
        this._choiceValue = cfProj_ResPat;
    }

    /**
     * Sets the value of field 'cfProj_ResProd'.
     * 
     * @param cfProj_ResProd the value of field 'cfProj_ResProd'.
     */
    public void setCfProj_ResProd(final gr.ekt.cerif.schema.CfProj_ResProd cfProj_ResProd) {
        this.cfProj_ResProd = cfProj_ResProd;
        this._choiceValue = cfProj_ResProd;
    }

    /**
     * Sets the value of field 'cfProj_ResPubl'.
     * 
     * @param cfProj_ResPubl the value of field 'cfProj_ResPubl'.
     */
    public void setCfProj_ResPubl(final gr.ekt.cerif.schema.CfProj_ResPubl cfProj_ResPubl) {
        this.cfProj_ResPubl = cfProj_ResPubl;
        this._choiceValue = cfProj_ResPubl;
    }

    /**
     * Sets the value of field 'cfProj_Srv'.
     * 
     * @param cfProj_Srv the value of field 'cfProj_Srv'.
     */
    public void setCfProj_Srv(final gr.ekt.cerif.schema.CfProj_Srv cfProj_Srv) {
        this.cfProj_Srv = cfProj_Srv;
        this._choiceValue = cfProj_Srv;
    }

    /**
     * Sets the value of field 'cfQual'.
     * 
     * @param cfQual the value of field 'cfQual'.
     */
    public void setCfQual(final gr.ekt.cerif.schema.CfQual cfQual) {
        this.cfQual = cfQual;
        this._choiceValue = cfQual;
    }

    /**
     * Sets the value of field 'cfQualDescr'.
     * 
     * @param cfQualDescr the value of field 'cfQualDescr'.
     */
    public void setCfQualDescr(final gr.ekt.cerif.schema.CfQualDescr cfQualDescr) {
        this.cfQualDescr = cfQualDescr;
        this._choiceValue = cfQualDescr;
    }

    /**
     * Sets the value of field 'cfQualKeyw'.
     * 
     * @param cfQualKeyw the value of field 'cfQualKeyw'.
     */
    public void setCfQualKeyw(final gr.ekt.cerif.schema.CfQualKeyw cfQualKeyw) {
        this.cfQualKeyw = cfQualKeyw;
        this._choiceValue = cfQualKeyw;
    }

    /**
     * Sets the value of field 'cfQualTitle'.
     * 
     * @param cfQualTitle the value of field 'cfQualTitle'.
     */
    public void setCfQualTitle(final gr.ekt.cerif.schema.CfQualTitle cfQualTitle) {
        this.cfQualTitle = cfQualTitle;
        this._choiceValue = cfQualTitle;
    }

    /**
     * Sets the value of field 'cfQual_Class'.
     * 
     * @param cfQual_Class the value of field 'cfQual_Class'.
     */
    public void setCfQual_Class(final gr.ekt.cerif.schema.CfQual_Class cfQual_Class) {
        this.cfQual_Class = cfQual_Class;
        this._choiceValue = cfQual_Class;
    }

    /**
     * Sets the value of field 'cfResPat'.
     * 
     * @param cfResPat the value of field 'cfResPat'.
     */
    public void setCfResPat(final gr.ekt.cerif.schema.CfResPat cfResPat) {
        this.cfResPat = cfResPat;
        this._choiceValue = cfResPat;
    }

    /**
     * Sets the value of field 'cfResPatAbstr'.
     * 
     * @param cfResPatAbstr the value of field 'cfResPatAbstr'.
     */
    public void setCfResPatAbstr(final gr.ekt.cerif.schema.CfResPatAbstr cfResPatAbstr) {
        this.cfResPatAbstr = cfResPatAbstr;
        this._choiceValue = cfResPatAbstr;
    }

    /**
     * Sets the value of field 'cfResPatKeyw'.
     * 
     * @param cfResPatKeyw the value of field 'cfResPatKeyw'.
     */
    public void setCfResPatKeyw(final gr.ekt.cerif.schema.CfResPatKeyw cfResPatKeyw) {
        this.cfResPatKeyw = cfResPatKeyw;
        this._choiceValue = cfResPatKeyw;
    }

    /**
     * Sets the value of field 'cfResPatTitle'.
     * 
     * @param cfResPatTitle the value of field 'cfResPatTitle'.
     */
    public void setCfResPatTitle(final gr.ekt.cerif.schema.CfResPatTitle cfResPatTitle) {
        this.cfResPatTitle = cfResPatTitle;
        this._choiceValue = cfResPatTitle;
    }

    /**
     * Sets the value of field 'cfResPatVersInfo'.
     * 
     * @param cfResPatVersInfo the value of field 'cfResPatVersInfo'
     */
    public void setCfResPatVersInfo(final gr.ekt.cerif.schema.CfResPatVersInfo cfResPatVersInfo) {
        this.cfResPatVersInfo = cfResPatVersInfo;
        this._choiceValue = cfResPatVersInfo;
    }

    /**
     * Sets the value of field 'cfResPat_Class'.
     * 
     * @param cfResPat_Class the value of field 'cfResPat_Class'.
     */
    public void setCfResPat_Class(final gr.ekt.cerif.schema.CfResPat_Class cfResPat_Class) {
        this.cfResPat_Class = cfResPat_Class;
        this._choiceValue = cfResPat_Class;
    }

    /**
     * Sets the value of field 'cfResPat_Equip'.
     * 
     * @param cfResPat_Equip the value of field 'cfResPat_Equip'.
     */
    public void setCfResPat_Equip(final gr.ekt.cerif.schema.CfResPat_Equip cfResPat_Equip) {
        this.cfResPat_Equip = cfResPat_Equip;
        this._choiceValue = cfResPat_Equip;
    }

    /**
     * Sets the value of field 'cfResPat_Facil'.
     * 
     * @param cfResPat_Facil the value of field 'cfResPat_Facil'.
     */
    public void setCfResPat_Facil(final gr.ekt.cerif.schema.CfResPat_Facil cfResPat_Facil) {
        this.cfResPat_Facil = cfResPat_Facil;
        this._choiceValue = cfResPat_Facil;
    }

    /**
     * Sets the value of field 'cfResPat_Fund'.
     * 
     * @param cfResPat_Fund the value of field 'cfResPat_Fund'.
     */
    public void setCfResPat_Fund(final gr.ekt.cerif.schema.CfResPat_Fund cfResPat_Fund) {
        this.cfResPat_Fund = cfResPat_Fund;
        this._choiceValue = cfResPat_Fund;
    }

    /**
     * Sets the value of field 'cfResPat_Indic'.
     * 
     * @param cfResPat_Indic the value of field 'cfResPat_Indic'.
     */
    public void setCfResPat_Indic(final gr.ekt.cerif.schema.CfResPat_Indic cfResPat_Indic) {
        this.cfResPat_Indic = cfResPat_Indic;
        this._choiceValue = cfResPat_Indic;
    }

    /**
     * Sets the value of field 'cfResPat_Meas'.
     * 
     * @param cfResPat_Meas the value of field 'cfResPat_Meas'.
     */
    public void setCfResPat_Meas(final gr.ekt.cerif.schema.CfResPat_Meas cfResPat_Meas) {
        this.cfResPat_Meas = cfResPat_Meas;
        this._choiceValue = cfResPat_Meas;
    }

    /**
     * Sets the value of field 'cfResPat_Medium'.
     * 
     * @param cfResPat_Medium the value of field 'cfResPat_Medium'.
     */
    public void setCfResPat_Medium(final gr.ekt.cerif.schema.CfResPat_Medium cfResPat_Medium) {
        this.cfResPat_Medium = cfResPat_Medium;
        this._choiceValue = cfResPat_Medium;
    }

    /**
     * Sets the value of field 'cfResPat_ResPat'.
     * 
     * @param cfResPat_ResPat the value of field 'cfResPat_ResPat'.
     */
    public void setCfResPat_ResPat(final gr.ekt.cerif.schema.CfResPat_ResPat cfResPat_ResPat) {
        this.cfResPat_ResPat = cfResPat_ResPat;
        this._choiceValue = cfResPat_ResPat;
    }

    /**
     * Sets the value of field 'cfResPat_Srv'.
     * 
     * @param cfResPat_Srv the value of field 'cfResPat_Srv'.
     */
    public void setCfResPat_Srv(final gr.ekt.cerif.schema.CfResPat_Srv cfResPat_Srv) {
        this.cfResPat_Srv = cfResPat_Srv;
        this._choiceValue = cfResPat_Srv;
    }

    /**
     * Sets the value of field 'cfResProd'.
     * 
     * @param cfResProd the value of field 'cfResProd'.
     */
    public void setCfResProd(final gr.ekt.cerif.schema.CfResProd cfResProd) {
        this.cfResProd = cfResProd;
        this._choiceValue = cfResProd;
    }

    /**
     * Sets the value of field 'cfResProdAltName'.
     * 
     * @param cfResProdAltName the value of field 'cfResProdAltName'
     */
    public void setCfResProdAltName(final gr.ekt.cerif.schema.CfResProdAltName cfResProdAltName) {
        this.cfResProdAltName = cfResProdAltName;
        this._choiceValue = cfResProdAltName;
    }

    /**
     * Sets the value of field 'cfResProdDescr'.
     * 
     * @param cfResProdDescr the value of field 'cfResProdDescr'.
     */
    public void setCfResProdDescr(final gr.ekt.cerif.schema.CfResProdDescr cfResProdDescr) {
        this.cfResProdDescr = cfResProdDescr;
        this._choiceValue = cfResProdDescr;
    }

    /**
     * Sets the value of field 'cfResProdKeyw'.
     * 
     * @param cfResProdKeyw the value of field 'cfResProdKeyw'.
     */
    public void setCfResProdKeyw(final gr.ekt.cerif.schema.CfResProdKeyw cfResProdKeyw) {
        this.cfResProdKeyw = cfResProdKeyw;
        this._choiceValue = cfResProdKeyw;
    }

    /**
     * Sets the value of field 'cfResProdName'.
     * 
     * @param cfResProdName the value of field 'cfResProdName'.
     */
    public void setCfResProdName(final gr.ekt.cerif.schema.CfResProdName cfResProdName) {
        this.cfResProdName = cfResProdName;
        this._choiceValue = cfResProdName;
    }

    /**
     * Sets the value of field 'cfResProdVersInfo'.
     * 
     * @param cfResProdVersInfo the value of field
     * 'cfResProdVersInfo'.
     */
    public void setCfResProdVersInfo(final gr.ekt.cerif.schema.CfResProdVersInfo cfResProdVersInfo) {
        this.cfResProdVersInfo = cfResProdVersInfo;
        this._choiceValue = cfResProdVersInfo;
    }

    /**
     * Sets the value of field 'cfResProd_Class'.
     * 
     * @param cfResProd_Class the value of field 'cfResProd_Class'.
     */
    public void setCfResProd_Class(final gr.ekt.cerif.schema.CfResProd_Class cfResProd_Class) {
        this.cfResProd_Class = cfResProd_Class;
        this._choiceValue = cfResProd_Class;
    }

    /**
     * Sets the value of field 'cfResProd_Equip'.
     * 
     * @param cfResProd_Equip the value of field 'cfResProd_Equip'.
     */
    public void setCfResProd_Equip(final gr.ekt.cerif.schema.CfResProd_Equip cfResProd_Equip) {
        this.cfResProd_Equip = cfResProd_Equip;
        this._choiceValue = cfResProd_Equip;
    }

    /**
     * Sets the value of field 'cfResProd_Facil'.
     * 
     * @param cfResProd_Facil the value of field 'cfResProd_Facil'.
     */
    public void setCfResProd_Facil(final gr.ekt.cerif.schema.CfResProd_Facil cfResProd_Facil) {
        this.cfResProd_Facil = cfResProd_Facil;
        this._choiceValue = cfResProd_Facil;
    }

    /**
     * Sets the value of field 'cfResProd_Fund'.
     * 
     * @param cfResProd_Fund the value of field 'cfResProd_Fund'.
     */
    public void setCfResProd_Fund(final gr.ekt.cerif.schema.CfResProd_Fund cfResProd_Fund) {
        this.cfResProd_Fund = cfResProd_Fund;
        this._choiceValue = cfResProd_Fund;
    }

    /**
     * Sets the value of field 'cfResProd_GeoBBox'.
     * 
     * @param cfResProd_GeoBBox the value of field
     * 'cfResProd_GeoBBox'.
     */
    public void setCfResProd_GeoBBox(final gr.ekt.cerif.schema.CfResProd_GeoBBox cfResProd_GeoBBox) {
        this.cfResProd_GeoBBox = cfResProd_GeoBBox;
        this._choiceValue = cfResProd_GeoBBox;
    }

    /**
     * Sets the value of field 'cfResProd_Indic'.
     * 
     * @param cfResProd_Indic the value of field 'cfResProd_Indic'.
     */
    public void setCfResProd_Indic(final gr.ekt.cerif.schema.CfResProd_Indic cfResProd_Indic) {
        this.cfResProd_Indic = cfResProd_Indic;
        this._choiceValue = cfResProd_Indic;
    }

    /**
     * Sets the value of field 'cfResProd_Meas'.
     * 
     * @param cfResProd_Meas the value of field 'cfResProd_Meas'.
     */
    public void setCfResProd_Meas(final gr.ekt.cerif.schema.CfResProd_Meas cfResProd_Meas) {
        this.cfResProd_Meas = cfResProd_Meas;
        this._choiceValue = cfResProd_Meas;
    }

    /**
     * Sets the value of field 'cfResProd_Medium'.
     * 
     * @param cfResProd_Medium the value of field 'cfResProd_Medium'
     */
    public void setCfResProd_Medium(final gr.ekt.cerif.schema.CfResProd_Medium cfResProd_Medium) {
        this.cfResProd_Medium = cfResProd_Medium;
        this._choiceValue = cfResProd_Medium;
    }

    /**
     * Sets the value of field 'cfResProd_ResPat'.
     * 
     * @param cfResProd_ResPat the value of field 'cfResProd_ResPat'
     */
    public void setCfResProd_ResPat(final gr.ekt.cerif.schema.CfResProd_ResPat cfResProd_ResPat) {
        this.cfResProd_ResPat = cfResProd_ResPat;
        this._choiceValue = cfResProd_ResPat;
    }

    /**
     * Sets the value of field 'cfResProd_ResProd'.
     * 
     * @param cfResProd_ResProd the value of field
     * 'cfResProd_ResProd'.
     */
    public void setCfResProd_ResProd(final gr.ekt.cerif.schema.CfResProd_ResProd cfResProd_ResProd) {
        this.cfResProd_ResProd = cfResProd_ResProd;
        this._choiceValue = cfResProd_ResProd;
    }

    /**
     * Sets the value of field 'cfResProd_Srv'.
     * 
     * @param cfResProd_Srv the value of field 'cfResProd_Srv'.
     */
    public void setCfResProd_Srv(final gr.ekt.cerif.schema.CfResProd_Srv cfResProd_Srv) {
        this.cfResProd_Srv = cfResProd_Srv;
        this._choiceValue = cfResProd_Srv;
    }

    /**
     * Sets the value of field 'cfResPubl'.
     * 
     * @param cfResPubl the value of field 'cfResPubl'.
     */
    public void setCfResPubl(final gr.ekt.cerif.schema.CfResPubl cfResPubl) {
        this.cfResPubl = cfResPubl;
        this._choiceValue = cfResPubl;
    }

    /**
     * Sets the value of field 'cfResPublAbstr'.
     * 
     * @param cfResPublAbstr the value of field 'cfResPublAbstr'.
     */
    public void setCfResPublAbstr(final gr.ekt.cerif.schema.CfResPublAbstr cfResPublAbstr) {
        this.cfResPublAbstr = cfResPublAbstr;
        this._choiceValue = cfResPublAbstr;
    }

    /**
     * Sets the value of field 'cfResPublBiblNote'.
     * 
     * @param cfResPublBiblNote the value of field
     * 'cfResPublBiblNote'.
     */
    public void setCfResPublBiblNote(final gr.ekt.cerif.schema.CfResPublBiblNote cfResPublBiblNote) {
        this.cfResPublBiblNote = cfResPublBiblNote;
        this._choiceValue = cfResPublBiblNote;
    }

    /**
     * Sets the value of field 'cfResPublKeyw'.
     * 
     * @param cfResPublKeyw the value of field 'cfResPublKeyw'.
     */
    public void setCfResPublKeyw(final gr.ekt.cerif.schema.CfResPublKeyw cfResPublKeyw) {
        this.cfResPublKeyw = cfResPublKeyw;
        this._choiceValue = cfResPublKeyw;
    }

    /**
     * Sets the value of field 'cfResPublNameAbbrev'.
     * 
     * @param cfResPublNameAbbrev the value of field
     * 'cfResPublNameAbbrev'.
     */
    public void setCfResPublNameAbbrev(final gr.ekt.cerif.schema.CfResPublNameAbbrev cfResPublNameAbbrev) {
        this.cfResPublNameAbbrev = cfResPublNameAbbrev;
        this._choiceValue = cfResPublNameAbbrev;
    }

    /**
     * Sets the value of field 'cfResPublSubtitle'.
     * 
     * @param cfResPublSubtitle the value of field
     * 'cfResPublSubtitle'.
     */
    public void setCfResPublSubtitle(final gr.ekt.cerif.schema.CfResPublSubtitle cfResPublSubtitle) {
        this.cfResPublSubtitle = cfResPublSubtitle;
        this._choiceValue = cfResPublSubtitle;
    }

    /**
     * Sets the value of field 'cfResPublTitle'.
     * 
     * @param cfResPublTitle the value of field 'cfResPublTitle'.
     */
    public void setCfResPublTitle(final gr.ekt.cerif.schema.CfResPublTitle cfResPublTitle) {
        this.cfResPublTitle = cfResPublTitle;
        this._choiceValue = cfResPublTitle;
    }

    /**
     * Sets the value of field 'cfResPublVersInfo'.
     * 
     * @param cfResPublVersInfo the value of field
     * 'cfResPublVersInfo'.
     */
    public void setCfResPublVersInfo(final gr.ekt.cerif.schema.CfResPublVersInfo cfResPublVersInfo) {
        this.cfResPublVersInfo = cfResPublVersInfo;
        this._choiceValue = cfResPublVersInfo;
    }

    /**
     * Sets the value of field 'cfResPubl_Cite'.
     * 
     * @param cfResPubl_Cite the value of field 'cfResPubl_Cite'.
     */
    public void setCfResPubl_Cite(final gr.ekt.cerif.schema.CfResPubl_Cite cfResPubl_Cite) {
        this.cfResPubl_Cite = cfResPubl_Cite;
        this._choiceValue = cfResPubl_Cite;
    }

    /**
     * Sets the value of field 'cfResPubl_Class'.
     * 
     * @param cfResPubl_Class the value of field 'cfResPubl_Class'.
     */
    public void setCfResPubl_Class(final gr.ekt.cerif.schema.CfResPubl_Class cfResPubl_Class) {
        this.cfResPubl_Class = cfResPubl_Class;
        this._choiceValue = cfResPubl_Class;
    }

    /**
     * Sets the value of field 'cfResPubl_DC'.
     * 
     * @param cfResPubl_DC the value of field 'cfResPubl_DC'.
     */
    public void setCfResPubl_DC(final gr.ekt.cerif.schema.CfResPubl_DC cfResPubl_DC) {
        this.cfResPubl_DC = cfResPubl_DC;
        this._choiceValue = cfResPubl_DC;
    }

    /**
     * Sets the value of field 'cfResPubl_Equip'.
     * 
     * @param cfResPubl_Equip the value of field 'cfResPubl_Equip'.
     */
    public void setCfResPubl_Equip(final gr.ekt.cerif.schema.CfResPubl_Equip cfResPubl_Equip) {
        this.cfResPubl_Equip = cfResPubl_Equip;
        this._choiceValue = cfResPubl_Equip;
    }

    /**
     * Sets the value of field 'cfResPubl_Event'.
     * 
     * @param cfResPubl_Event the value of field 'cfResPubl_Event'.
     */
    public void setCfResPubl_Event(final gr.ekt.cerif.schema.CfResPubl_Event cfResPubl_Event) {
        this.cfResPubl_Event = cfResPubl_Event;
        this._choiceValue = cfResPubl_Event;
    }

    /**
     * Sets the value of field 'cfResPubl_Facil'.
     * 
     * @param cfResPubl_Facil the value of field 'cfResPubl_Facil'.
     */
    public void setCfResPubl_Facil(final gr.ekt.cerif.schema.CfResPubl_Facil cfResPubl_Facil) {
        this.cfResPubl_Facil = cfResPubl_Facil;
        this._choiceValue = cfResPubl_Facil;
    }

    /**
     * Sets the value of field 'cfResPubl_Fund'.
     * 
     * @param cfResPubl_Fund the value of field 'cfResPubl_Fund'.
     */
    public void setCfResPubl_Fund(final gr.ekt.cerif.schema.CfResPubl_Fund cfResPubl_Fund) {
        this.cfResPubl_Fund = cfResPubl_Fund;
        this._choiceValue = cfResPubl_Fund;
    }

    /**
     * Sets the value of field 'cfResPubl_Indic'.
     * 
     * @param cfResPubl_Indic the value of field 'cfResPubl_Indic'.
     */
    public void setCfResPubl_Indic(final gr.ekt.cerif.schema.CfResPubl_Indic cfResPubl_Indic) {
        this.cfResPubl_Indic = cfResPubl_Indic;
        this._choiceValue = cfResPubl_Indic;
    }

    /**
     * Sets the value of field 'cfResPubl_Meas'.
     * 
     * @param cfResPubl_Meas the value of field 'cfResPubl_Meas'.
     */
    public void setCfResPubl_Meas(final gr.ekt.cerif.schema.CfResPubl_Meas cfResPubl_Meas) {
        this.cfResPubl_Meas = cfResPubl_Meas;
        this._choiceValue = cfResPubl_Meas;
    }

    /**
     * Sets the value of field 'cfResPubl_Medium'.
     * 
     * @param cfResPubl_Medium the value of field 'cfResPubl_Medium'
     */
    public void setCfResPubl_Medium(final gr.ekt.cerif.schema.CfResPubl_Medium cfResPubl_Medium) {
        this.cfResPubl_Medium = cfResPubl_Medium;
        this._choiceValue = cfResPubl_Medium;
    }

    /**
     * Sets the value of field 'cfResPubl_Metrics'.
     * 
     * @param cfResPubl_Metrics the value of field
     * 'cfResPubl_Metrics'.
     */
    public void setCfResPubl_Metrics(final gr.ekt.cerif.schema.CfResPubl_Metrics cfResPubl_Metrics) {
        this.cfResPubl_Metrics = cfResPubl_Metrics;
        this._choiceValue = cfResPubl_Metrics;
    }

    /**
     * Sets the value of field 'cfResPubl_ResPat'.
     * 
     * @param cfResPubl_ResPat the value of field 'cfResPubl_ResPat'
     */
    public void setCfResPubl_ResPat(final gr.ekt.cerif.schema.CfResPubl_ResPat cfResPubl_ResPat) {
        this.cfResPubl_ResPat = cfResPubl_ResPat;
        this._choiceValue = cfResPubl_ResPat;
    }

    /**
     * Sets the value of field 'cfResPubl_ResProd'.
     * 
     * @param cfResPubl_ResProd the value of field
     * 'cfResPubl_ResProd'.
     */
    public void setCfResPubl_ResProd(final gr.ekt.cerif.schema.CfResPubl_ResProd cfResPubl_ResProd) {
        this.cfResPubl_ResProd = cfResPubl_ResProd;
        this._choiceValue = cfResPubl_ResProd;
    }

    /**
     * Sets the value of field 'cfResPubl_ResPubl'.
     * 
     * @param cfResPubl_ResPubl the value of field
     * 'cfResPubl_ResPubl'.
     */
    public void setCfResPubl_ResPubl(final gr.ekt.cerif.schema.CfResPubl_ResPubl cfResPubl_ResPubl) {
        this.cfResPubl_ResPubl = cfResPubl_ResPubl;
        this._choiceValue = cfResPubl_ResPubl;
    }

    /**
     * Sets the value of field 'cfResPubl_Srv'.
     * 
     * @param cfResPubl_Srv the value of field 'cfResPubl_Srv'.
     */
    public void setCfResPubl_Srv(final gr.ekt.cerif.schema.CfResPubl_Srv cfResPubl_Srv) {
        this.cfResPubl_Srv = cfResPubl_Srv;
        this._choiceValue = cfResPubl_Srv;
    }

    /**
     * Sets the value of field 'cfSrv'.
     * 
     * @param cfSrv the value of field 'cfSrv'.
     */
    public void setCfSrv(final gr.ekt.cerif.schema.CfSrv cfSrv) {
        this.cfSrv = cfSrv;
        this._choiceValue = cfSrv;
    }

    /**
     * Sets the value of field 'cfSrvDescr'.
     * 
     * @param cfSrvDescr the value of field 'cfSrvDescr'.
     */
    public void setCfSrvDescr(final gr.ekt.cerif.schema.CfSrvDescr cfSrvDescr) {
        this.cfSrvDescr = cfSrvDescr;
        this._choiceValue = cfSrvDescr;
    }

    /**
     * Sets the value of field 'cfSrvKeyw'.
     * 
     * @param cfSrvKeyw the value of field 'cfSrvKeyw'.
     */
    public void setCfSrvKeyw(final gr.ekt.cerif.schema.CfSrvKeyw cfSrvKeyw) {
        this.cfSrvKeyw = cfSrvKeyw;
        this._choiceValue = cfSrvKeyw;
    }

    /**
     * Sets the value of field 'cfSrvName'.
     * 
     * @param cfSrvName the value of field 'cfSrvName'.
     */
    public void setCfSrvName(final gr.ekt.cerif.schema.CfSrvName cfSrvName) {
        this.cfSrvName = cfSrvName;
        this._choiceValue = cfSrvName;
    }

    /**
     * Sets the value of field 'cfSrv_Class'.
     * 
     * @param cfSrv_Class the value of field 'cfSrv_Class'.
     */
    public void setCfSrv_Class(final gr.ekt.cerif.schema.CfSrv_Class cfSrv_Class) {
        this.cfSrv_Class = cfSrv_Class;
        this._choiceValue = cfSrv_Class;
    }

    /**
     * Sets the value of field 'cfSrv_Event'.
     * 
     * @param cfSrv_Event the value of field 'cfSrv_Event'.
     */
    public void setCfSrv_Event(final gr.ekt.cerif.schema.CfSrv_Event cfSrv_Event) {
        this.cfSrv_Event = cfSrv_Event;
        this._choiceValue = cfSrv_Event;
    }

    /**
     * Sets the value of field 'cfSrv_Fund'.
     * 
     * @param cfSrv_Fund the value of field 'cfSrv_Fund'.
     */
    public void setCfSrv_Fund(final gr.ekt.cerif.schema.CfSrv_Fund cfSrv_Fund) {
        this.cfSrv_Fund = cfSrv_Fund;
        this._choiceValue = cfSrv_Fund;
    }

    /**
     * Sets the value of field 'cfSrv_Indic'.
     * 
     * @param cfSrv_Indic the value of field 'cfSrv_Indic'.
     */
    public void setCfSrv_Indic(final gr.ekt.cerif.schema.CfSrv_Indic cfSrv_Indic) {
        this.cfSrv_Indic = cfSrv_Indic;
        this._choiceValue = cfSrv_Indic;
    }

    /**
     * Sets the value of field 'cfSrv_Meas'.
     * 
     * @param cfSrv_Meas the value of field 'cfSrv_Meas'.
     */
    public void setCfSrv_Meas(final gr.ekt.cerif.schema.CfSrv_Meas cfSrv_Meas) {
        this.cfSrv_Meas = cfSrv_Meas;
        this._choiceValue = cfSrv_Meas;
    }

    /**
     * Sets the value of field 'cfSrv_Medium'.
     * 
     * @param cfSrv_Medium the value of field 'cfSrv_Medium'.
     */
    public void setCfSrv_Medium(final gr.ekt.cerif.schema.CfSrv_Medium cfSrv_Medium) {
        this.cfSrv_Medium = cfSrv_Medium;
        this._choiceValue = cfSrv_Medium;
    }

    /**
     * Sets the value of field 'cfSrv_PAddr'.
     * 
     * @param cfSrv_PAddr the value of field 'cfSrv_PAddr'.
     */
    public void setCfSrv_PAddr(final gr.ekt.cerif.schema.CfSrv_PAddr cfSrv_PAddr) {
        this.cfSrv_PAddr = cfSrv_PAddr;
        this._choiceValue = cfSrv_PAddr;
    }

    /**
     * Sets the value of field 'cfSrv_Srv'.
     * 
     * @param cfSrv_Srv the value of field 'cfSrv_Srv'.
     */
    public void setCfSrv_Srv(final gr.ekt.cerif.schema.CfSrv_Srv cfSrv_Srv) {
        this.cfSrv_Srv = cfSrv_Srv;
        this._choiceValue = cfSrv_Srv;
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfCV__Type implements java.io.Serializable {

    private java.lang.String cfCVId;

    private java.lang.String cfCVDoc;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfCV__TypeChoice cfCV__TypeChoice;

    public CfCV__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfCVDoc'.
     * 
     * @return the value of field 'CfCVDoc'.
     */
    public java.lang.String getCfCVDoc() {
        return this.cfCVDoc;
    }

    /**
     * Returns the value of field 'cfCVId'.
     * 
     * @return the value of field 'CfCVId'.
     */
    public java.lang.String getCfCVId() {
        return this.cfCVId;
    }

    /**
     * Returns the value of field 'cfCV__TypeChoice'.
     * 
     * @return the value of field 'CfCV__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfCV__TypeChoice getCfCV__TypeChoice() {
        return this.cfCV__TypeChoice;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCVDoc'.
     * 
     * @param cfCVDoc the value of field 'cfCVDoc'.
     */
    public void setCfCVDoc(final java.lang.String cfCVDoc) {
        this.cfCVDoc = cfCVDoc;
    }

    /**
     * Sets the value of field 'cfCVId'.
     * 
     * @param cfCVId the value of field 'cfCVId'.
     */
    public void setCfCVId(final java.lang.String cfCVId) {
        this.cfCVId = cfCVId;
    }

    /**
     * Sets the value of field 'cfCV__TypeChoice'.
     * 
     * @param cfCV__TypeChoice the value of field 'cfCV__TypeChoice'
     */
    public void setCfCV__TypeChoice(final gr.ekt.cerif.schema.CfCV__TypeChoice cfCV__TypeChoice) {
        this.cfCV__TypeChoice = cfCV__TypeChoice;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfCV__Type
     */
    public static gr.ekt.cerif.schema.CfCV__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfCV__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfCV__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

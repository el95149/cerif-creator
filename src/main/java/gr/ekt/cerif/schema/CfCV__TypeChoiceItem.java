/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfCV__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfCV__TypeCfPers_CV cfPers_CV;

    private gr.ekt.cerif.schema.CfCV__TypeCfCV_Class cfCV_Class;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfCV__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfCV_Class'.
     * 
     * @return the value of field 'CfCV_Class'.
     */
    public gr.ekt.cerif.schema.CfCV__TypeCfCV_Class getCfCV_Class() {
        return this.cfCV_Class;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfPers_CV'.
     * 
     * @return the value of field 'CfPers_CV'.
     */
    public gr.ekt.cerif.schema.CfCV__TypeCfPers_CV getCfPers_CV() {
        return this.cfPers_CV;
    }

    /**
     * Sets the value of field 'cfCV_Class'.
     * 
     * @param cfCV_Class the value of field 'cfCV_Class'.
     */
    public void setCfCV_Class(final gr.ekt.cerif.schema.CfCV__TypeCfCV_Class cfCV_Class) {
        this.cfCV_Class = cfCV_Class;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfPers_CV'.
     * 
     * @param cfPers_CV the value of field 'cfPers_CV'.
     */
    public void setCfPers_CV(final gr.ekt.cerif.schema.CfCV__TypeCfPers_CV cfPers_CV) {
        this.cfPers_CV = cfPers_CV;
    }

}

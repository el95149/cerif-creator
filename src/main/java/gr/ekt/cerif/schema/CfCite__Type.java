/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfCite__Type implements java.io.Serializable {

    private java.lang.String cfCiteId;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfCite__TypeChoice cfCite__TypeChoice;

    public CfCite__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfCiteId'.
     * 
     * @return the value of field 'CfCiteId'.
     */
    public java.lang.String getCfCiteId() {
        return this.cfCiteId;
    }

    /**
     * Returns the value of field 'cfCite__TypeChoice'.
     * 
     * @return the value of field 'CfCite__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfCite__TypeChoice getCfCite__TypeChoice() {
        return this.cfCite__TypeChoice;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCiteId'.
     * 
     * @param cfCiteId the value of field 'cfCiteId'.
     */
    public void setCfCiteId(final java.lang.String cfCiteId) {
        this.cfCiteId = cfCiteId;
    }

    /**
     * Sets the value of field 'cfCite__TypeChoice'.
     * 
     * @param cfCite__TypeChoice the value of field
     * 'cfCite__TypeChoice'.
     */
    public void setCfCite__TypeChoice(final gr.ekt.cerif.schema.CfCite__TypeChoice cfCite__TypeChoice) {
        this.cfCite__TypeChoice = cfCite__TypeChoice;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfCite__Type
     */
    public static gr.ekt.cerif.schema.CfCite__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfCite__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfCite__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfCite__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfTitle cfTitle;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfCite__TypeCfResPubl_Cite cfResPubl_Cite;

    private gr.ekt.cerif.schema.CfCite__TypeCfCite_Class cfCite_Class;

    private gr.ekt.cerif.schema.CfCite__TypeCfCite_Medium cfCite_Medium;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfCite__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfCite_Class'.
     * 
     * @return the value of field 'CfCite_Class'.
     */
    public gr.ekt.cerif.schema.CfCite__TypeCfCite_Class getCfCite_Class() {
        return this.cfCite_Class;
    }

    /**
     * Returns the value of field 'cfCite_Medium'.
     * 
     * @return the value of field 'CfCite_Medium'.
     */
    public gr.ekt.cerif.schema.CfCite__TypeCfCite_Medium getCfCite_Medium() {
        return this.cfCite_Medium;
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfResPubl_Cite'.
     * 
     * @return the value of field 'CfResPubl_Cite'.
     */
    public gr.ekt.cerif.schema.CfCite__TypeCfResPubl_Cite getCfResPubl_Cite() {
        return this.cfResPubl_Cite;
    }

    /**
     * Returns the value of field 'cfTitle'.
     * 
     * @return the value of field 'CfTitle'.
     */
    public gr.ekt.cerif.schema.CfTitle getCfTitle() {
        return this.cfTitle;
    }

    /**
     * Sets the value of field 'cfCite_Class'.
     * 
     * @param cfCite_Class the value of field 'cfCite_Class'.
     */
    public void setCfCite_Class(final gr.ekt.cerif.schema.CfCite__TypeCfCite_Class cfCite_Class) {
        this.cfCite_Class = cfCite_Class;
    }

    /**
     * Sets the value of field 'cfCite_Medium'.
     * 
     * @param cfCite_Medium the value of field 'cfCite_Medium'.
     */
    public void setCfCite_Medium(final gr.ekt.cerif.schema.CfCite__TypeCfCite_Medium cfCite_Medium) {
        this.cfCite_Medium = cfCite_Medium;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfResPubl_Cite'.
     * 
     * @param cfResPubl_Cite the value of field 'cfResPubl_Cite'.
     */
    public void setCfResPubl_Cite(final gr.ekt.cerif.schema.CfCite__TypeCfResPubl_Cite cfResPubl_Cite) {
        this.cfResPubl_Cite = cfResPubl_Cite;
    }

    /**
     * Sets the value of field 'cfTitle'.
     * 
     * @param cfTitle the value of field 'cfTitle'.
     */
    public void setCfTitle(final gr.ekt.cerif.schema.CfTitle cfTitle) {
        this.cfTitle = cfTitle;
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfClassChoice implements java.io.Serializable {

    private java.util.Vector<gr.ekt.cerif.schema.CfClassChoiceItem> _items;

    public CfClassChoice() {
        super();
        this._items = new java.util.Vector<gr.ekt.cerif.schema.CfClassChoiceItem>();
    }

    /**
     * 
     * 
     * @param vCfClassChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfClassChoiceItem(final gr.ekt.cerif.schema.CfClassChoiceItem vCfClassChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.addElement(vCfClassChoiceItem);
    }

    /**
     * 
     * 
     * @param index
     * @param vCfClassChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfClassChoiceItem(final int index,final gr.ekt.cerif.schema.CfClassChoiceItem vCfClassChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.add(index, vCfClassChoiceItem);
    }

    /**
     * Method enumerateCfClassChoiceItem.
     * 
     * @return an Enumeration over all
     * gr.ekt.cerif.schema.CfClassChoiceItem elements
     */
    public java.util.Enumeration<? extends gr.ekt.cerif.schema.CfClassChoiceItem> enumerateCfClassChoiceItem() {
        return this._items.elements();
    }

    /**
     * Method getCfClassChoiceItem.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * gr.ekt.cerif.schema.CfClassChoiceItem at the given index
     */
    public gr.ekt.cerif.schema.CfClassChoiceItem getCfClassChoiceItem(final int index) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("getCfClassChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        return _items.get(index);
    }

    /**
     * Method getCfClassChoiceItem.Returns the contents of the
     * collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public gr.ekt.cerif.schema.CfClassChoiceItem[] getCfClassChoiceItem() {
        gr.ekt.cerif.schema.CfClassChoiceItem[] array = new gr.ekt.cerif.schema.CfClassChoiceItem[0];
        return this._items.toArray(array);
    }

    /**
     * Method getCfClassChoiceItemCount.
     * 
     * @return the size of this collection
     */
    public int getCfClassChoiceItemCount() {
        return this._items.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllCfClassChoiceItem() {
        this._items.clear();
    }

    /**
     * Method removeCfClassChoiceItem.
     * 
     * @param vCfClassChoiceItem
     * @return true if the object was removed from the collection.
     */
    public boolean removeCfClassChoiceItem(final gr.ekt.cerif.schema.CfClassChoiceItem vCfClassChoiceItem) {
        boolean removed = _items.remove(vCfClassChoiceItem);
        return removed;
    }

    /**
     * Method removeCfClassChoiceItemAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public gr.ekt.cerif.schema.CfClassChoiceItem removeCfClassChoiceItemAt(final int index) {
        java.lang.Object obj = this._items.remove(index);
        return (gr.ekt.cerif.schema.CfClassChoiceItem) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vCfClassChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setCfClassChoiceItem(final int index,final gr.ekt.cerif.schema.CfClassChoiceItem vCfClassChoiceItem) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("setCfClassChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        this._items.set(index, vCfClassChoiceItem);
    }

    /**
     * 
     * 
     * @param vCfClassChoiceItemArray
     */
    public void setCfClassChoiceItem(final gr.ekt.cerif.schema.CfClassChoiceItem[] vCfClassChoiceItemArray) {
        //-- copy array
        _items.clear();

        for (int i = 0; i < vCfClassChoiceItemArray.length; i++) {
                this._items.add(vCfClassChoiceItemArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfClassChoice
     */
    public static gr.ekt.cerif.schema.CfClassChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfClassChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfClassChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfClassChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfDescrSrc cfDescrSrc;

    private gr.ekt.cerif.schema.CfTerm cfTerm;

    private gr.ekt.cerif.schema.CfTermSrc cfTermSrc;

    private gr.ekt.cerif.schema.CfRoleExpr cfRoleExpr;

    private gr.ekt.cerif.schema.CfRoleExprOpp cfRoleExprOpp;

    private gr.ekt.cerif.schema.CfDef cfDef;

    private gr.ekt.cerif.schema.CfDefSrc cfDefSrc;

    private gr.ekt.cerif.schema.CfEx cfEx;

    private gr.ekt.cerif.schema.CfExSrc cfExSrc;

    private gr.ekt.cerif.schema.CfClassScheme__TypeCfClassCfClass_Class cfClass_Class;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfClassChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfClass_Class'.
     * 
     * @return the value of field 'CfClass_Class'.
     */
    public gr.ekt.cerif.schema.CfClassScheme__TypeCfClassCfClass_Class getCfClass_Class() {
        return this.cfClass_Class;
    }

    /**
     * Returns the value of field 'cfDef'.
     * 
     * @return the value of field 'CfDef'.
     */
    public gr.ekt.cerif.schema.CfDef getCfDef() {
        return this.cfDef;
    }

    /**
     * Returns the value of field 'cfDefSrc'.
     * 
     * @return the value of field 'CfDefSrc'.
     */
    public gr.ekt.cerif.schema.CfDefSrc getCfDefSrc() {
        return this.cfDefSrc;
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfDescrSrc'.
     * 
     * @return the value of field 'CfDescrSrc'.
     */
    public gr.ekt.cerif.schema.CfDescrSrc getCfDescrSrc() {
        return this.cfDescrSrc;
    }

    /**
     * Returns the value of field 'cfEx'.
     * 
     * @return the value of field 'CfEx'.
     */
    public gr.ekt.cerif.schema.CfEx getCfEx() {
        return this.cfEx;
    }

    /**
     * Returns the value of field 'cfExSrc'.
     * 
     * @return the value of field 'CfExSrc'.
     */
    public gr.ekt.cerif.schema.CfExSrc getCfExSrc() {
        return this.cfExSrc;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfRoleExpr'.
     * 
     * @return the value of field 'CfRoleExpr'.
     */
    public gr.ekt.cerif.schema.CfRoleExpr getCfRoleExpr() {
        return this.cfRoleExpr;
    }

    /**
     * Returns the value of field 'cfRoleExprOpp'.
     * 
     * @return the value of field 'CfRoleExprOpp'.
     */
    public gr.ekt.cerif.schema.CfRoleExprOpp getCfRoleExprOpp() {
        return this.cfRoleExprOpp;
    }

    /**
     * Returns the value of field 'cfTerm'.
     * 
     * @return the value of field 'CfTerm'.
     */
    public gr.ekt.cerif.schema.CfTerm getCfTerm() {
        return this.cfTerm;
    }

    /**
     * Returns the value of field 'cfTermSrc'.
     * 
     * @return the value of field 'CfTermSrc'.
     */
    public gr.ekt.cerif.schema.CfTermSrc getCfTermSrc() {
        return this.cfTermSrc;
    }

    /**
     * Sets the value of field 'cfClass_Class'.
     * 
     * @param cfClass_Class the value of field 'cfClass_Class'.
     */
    public void setCfClass_Class(final gr.ekt.cerif.schema.CfClassScheme__TypeCfClassCfClass_Class cfClass_Class) {
        this.cfClass_Class = cfClass_Class;
    }

    /**
     * Sets the value of field 'cfDef'.
     * 
     * @param cfDef the value of field 'cfDef'.
     */
    public void setCfDef(final gr.ekt.cerif.schema.CfDef cfDef) {
        this.cfDef = cfDef;
    }

    /**
     * Sets the value of field 'cfDefSrc'.
     * 
     * @param cfDefSrc the value of field 'cfDefSrc'.
     */
    public void setCfDefSrc(final gr.ekt.cerif.schema.CfDefSrc cfDefSrc) {
        this.cfDefSrc = cfDefSrc;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfDescrSrc'.
     * 
     * @param cfDescrSrc the value of field 'cfDescrSrc'.
     */
    public void setCfDescrSrc(final gr.ekt.cerif.schema.CfDescrSrc cfDescrSrc) {
        this.cfDescrSrc = cfDescrSrc;
    }

    /**
     * Sets the value of field 'cfEx'.
     * 
     * @param cfEx the value of field 'cfEx'.
     */
    public void setCfEx(final gr.ekt.cerif.schema.CfEx cfEx) {
        this.cfEx = cfEx;
    }

    /**
     * Sets the value of field 'cfExSrc'.
     * 
     * @param cfExSrc the value of field 'cfExSrc'.
     */
    public void setCfExSrc(final gr.ekt.cerif.schema.CfExSrc cfExSrc) {
        this.cfExSrc = cfExSrc;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfRoleExpr'.
     * 
     * @param cfRoleExpr the value of field 'cfRoleExpr'.
     */
    public void setCfRoleExpr(final gr.ekt.cerif.schema.CfRoleExpr cfRoleExpr) {
        this.cfRoleExpr = cfRoleExpr;
    }

    /**
     * Sets the value of field 'cfRoleExprOpp'.
     * 
     * @param cfRoleExprOpp the value of field 'cfRoleExprOpp'.
     */
    public void setCfRoleExprOpp(final gr.ekt.cerif.schema.CfRoleExprOpp cfRoleExprOpp) {
        this.cfRoleExprOpp = cfRoleExprOpp;
    }

    /**
     * Sets the value of field 'cfTerm'.
     * 
     * @param cfTerm the value of field 'cfTerm'.
     */
    public void setCfTerm(final gr.ekt.cerif.schema.CfTerm cfTerm) {
        this.cfTerm = cfTerm;
    }

    /**
     * Sets the value of field 'cfTermSrc'.
     * 
     * @param cfTermSrc the value of field 'cfTermSrc'.
     */
    public void setCfTermSrc(final gr.ekt.cerif.schema.CfTermSrc cfTermSrc) {
        this.cfTermSrc = cfTermSrc;
    }

}

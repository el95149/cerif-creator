/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfClassScheme__TypeCfClass implements java.io.Serializable {

    private java.lang.String cfClassId;

    private java.util.Date cfStartDate;

    private java.util.Date cfEndDate;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfClassChoice cfClassChoice;

    public CfClassScheme__TypeCfClass() {
        super();
    }

    /**
     * Returns the value of field 'cfClassChoice'.
     * 
     * @return the value of field 'CfClassChoice'.
     */
    public gr.ekt.cerif.schema.CfClassChoice getCfClassChoice() {
        return this.cfClassChoice;
    }

    /**
     * Returns the value of field 'cfClassId'.
     * 
     * @return the value of field 'CfClassId'.
     */
    public java.lang.String getCfClassId() {
        return this.cfClassId;
    }

    /**
     * Returns the value of field 'cfEndDate'.
     * 
     * @return the value of field 'CfEndDate'.
     */
    public java.util.Date getCfEndDate() {
        return this.cfEndDate;
    }

    /**
     * Returns the value of field 'cfStartDate'.
     * 
     * @return the value of field 'CfStartDate'.
     */
    public java.util.Date getCfStartDate() {
        return this.cfStartDate;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfClassChoice'.
     * 
     * @param cfClassChoice the value of field 'cfClassChoice'.
     */
    public void setCfClassChoice(final gr.ekt.cerif.schema.CfClassChoice cfClassChoice) {
        this.cfClassChoice = cfClassChoice;
    }

    /**
     * Sets the value of field 'cfClassId'.
     * 
     * @param cfClassId the value of field 'cfClassId'.
     */
    public void setCfClassId(final java.lang.String cfClassId) {
        this.cfClassId = cfClassId;
    }

    /**
     * Sets the value of field 'cfEndDate'.
     * 
     * @param cfEndDate the value of field 'cfEndDate'.
     */
    public void setCfEndDate(final java.util.Date cfEndDate) {
        this.cfEndDate = cfEndDate;
    }

    /**
     * Sets the value of field 'cfStartDate'.
     * 
     * @param cfStartDate the value of field 'cfStartDate'.
     */
    public void setCfStartDate(final java.util.Date cfStartDate) {
        this.cfStartDate = cfStartDate;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfClassScheme__TypeCfClass
     */
    public static gr.ekt.cerif.schema.CfClassScheme__TypeCfClass unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfClassScheme__TypeCfClass) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfClassScheme__TypeCfClass.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

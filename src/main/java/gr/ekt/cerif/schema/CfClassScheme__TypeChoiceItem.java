/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfClassScheme__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfDescrSrc cfDescrSrc;

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfNameSrc cfNameSrc;

    private gr.ekt.cerif.schema.CfClassScheme__TypeCfClassScheme_ClassScheme cfClassScheme_ClassScheme;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    private gr.ekt.cerif.schema.CfClassScheme__TypeCfClass cfClass;

    public CfClassScheme__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfClass'.
     * 
     * @return the value of field 'CfClass'.
     */
    public gr.ekt.cerif.schema.CfClassScheme__TypeCfClass getCfClass() {
        return this.cfClass;
    }

    /**
     * Returns the value of field 'cfClassScheme_ClassScheme'.
     * 
     * @return the value of field 'CfClassScheme_ClassScheme'.
     */
    public gr.ekt.cerif.schema.CfClassScheme__TypeCfClassScheme_ClassScheme getCfClassScheme_ClassScheme() {
        return this.cfClassScheme_ClassScheme;
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfDescrSrc'.
     * 
     * @return the value of field 'CfDescrSrc'.
     */
    public gr.ekt.cerif.schema.CfDescrSrc getCfDescrSrc() {
        return this.cfDescrSrc;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfNameSrc'.
     * 
     * @return the value of field 'CfNameSrc'.
     */
    public gr.ekt.cerif.schema.CfNameSrc getCfNameSrc() {
        return this.cfNameSrc;
    }

    /**
     * Sets the value of field 'cfClass'.
     * 
     * @param cfClass the value of field 'cfClass'.
     */
    public void setCfClass(final gr.ekt.cerif.schema.CfClassScheme__TypeCfClass cfClass) {
        this.cfClass = cfClass;
    }

    /**
     * Sets the value of field 'cfClassScheme_ClassScheme'.
     * 
     * @param cfClassScheme_ClassScheme the value of field
     * 'cfClassScheme_ClassScheme'.
     */
    public void setCfClassScheme_ClassScheme(final gr.ekt.cerif.schema.CfClassScheme__TypeCfClassScheme_ClassScheme cfClassScheme_ClassScheme) {
        this.cfClassScheme_ClassScheme = cfClassScheme_ClassScheme;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfDescrSrc'.
     * 
     * @param cfDescrSrc the value of field 'cfDescrSrc'.
     */
    public void setCfDescrSrc(final gr.ekt.cerif.schema.CfDescrSrc cfDescrSrc) {
        this.cfDescrSrc = cfDescrSrc;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfNameSrc'.
     * 
     * @param cfNameSrc the value of field 'cfNameSrc'.
     */
    public void setCfNameSrc(final gr.ekt.cerif.schema.CfNameSrc cfNameSrc) {
        this.cfNameSrc = cfNameSrc;
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfClassTerm__Type implements java.io.Serializable {

    private java.lang.String cfClassId;

    private java.lang.String cfClassSchemeId;

    private gr.ekt.cerif.schema.CfTerm cfTerm;

    private gr.ekt.cerif.schema.CfTermSrc cfTermSrc;

    private gr.ekt.cerif.schema.CfRoleExpr cfRoleExpr;

    private gr.ekt.cerif.schema.CfRoleExprOpp cfRoleExprOpp;

    public CfClassTerm__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfClassId'.
     * 
     * @return the value of field 'CfClassId'.
     */
    public java.lang.String getCfClassId() {
        return this.cfClassId;
    }

    /**
     * Returns the value of field 'cfClassSchemeId'.
     * 
     * @return the value of field 'CfClassSchemeId'.
     */
    public java.lang.String getCfClassSchemeId() {
        return this.cfClassSchemeId;
    }

    /**
     * Returns the value of field 'cfRoleExpr'.
     * 
     * @return the value of field 'CfRoleExpr'.
     */
    public gr.ekt.cerif.schema.CfRoleExpr getCfRoleExpr() {
        return this.cfRoleExpr;
    }

    /**
     * Returns the value of field 'cfRoleExprOpp'.
     * 
     * @return the value of field 'CfRoleExprOpp'.
     */
    public gr.ekt.cerif.schema.CfRoleExprOpp getCfRoleExprOpp() {
        return this.cfRoleExprOpp;
    }

    /**
     * Returns the value of field 'cfTerm'.
     * 
     * @return the value of field 'CfTerm'.
     */
    public gr.ekt.cerif.schema.CfTerm getCfTerm() {
        return this.cfTerm;
    }

    /**
     * Returns the value of field 'cfTermSrc'.
     * 
     * @return the value of field 'CfTermSrc'.
     */
    public gr.ekt.cerif.schema.CfTermSrc getCfTermSrc() {
        return this.cfTermSrc;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfClassId'.
     * 
     * @param cfClassId the value of field 'cfClassId'.
     */
    public void setCfClassId(final java.lang.String cfClassId) {
        this.cfClassId = cfClassId;
    }

    /**
     * Sets the value of field 'cfClassSchemeId'.
     * 
     * @param cfClassSchemeId the value of field 'cfClassSchemeId'.
     */
    public void setCfClassSchemeId(final java.lang.String cfClassSchemeId) {
        this.cfClassSchemeId = cfClassSchemeId;
    }

    /**
     * Sets the value of field 'cfRoleExpr'.
     * 
     * @param cfRoleExpr the value of field 'cfRoleExpr'.
     */
    public void setCfRoleExpr(final gr.ekt.cerif.schema.CfRoleExpr cfRoleExpr) {
        this.cfRoleExpr = cfRoleExpr;
    }

    /**
     * Sets the value of field 'cfRoleExprOpp'.
     * 
     * @param cfRoleExprOpp the value of field 'cfRoleExprOpp'.
     */
    public void setCfRoleExprOpp(final gr.ekt.cerif.schema.CfRoleExprOpp cfRoleExprOpp) {
        this.cfRoleExprOpp = cfRoleExprOpp;
    }

    /**
     * Sets the value of field 'cfTerm'.
     * 
     * @param cfTerm the value of field 'cfTerm'.
     */
    public void setCfTerm(final gr.ekt.cerif.schema.CfTerm cfTerm) {
        this.cfTerm = cfTerm;
    }

    /**
     * Sets the value of field 'cfTermSrc'.
     * 
     * @param cfTermSrc the value of field 'cfTermSrc'.
     */
    public void setCfTermSrc(final gr.ekt.cerif.schema.CfTermSrc cfTermSrc) {
        this.cfTermSrc = cfTermSrc;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfClassTerm__Type
     */
    public static gr.ekt.cerif.schema.CfClassTerm__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfClassTerm__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfClassTerm__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

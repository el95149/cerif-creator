/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfClass_Class2Choice implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfClass_Class2ChoiceSequence cfClass_Class2ChoiceSequence;

    private gr.ekt.cerif.schema.CfClass_Class2ChoiceSequence2 cfClass_Class2ChoiceSequence2;

    public CfClass_Class2Choice() {
        super();
    }

    /**
     * Returns the value of field 'cfClass_Class2ChoiceSequence'.
     * 
     * @return the value of field 'CfClass_Class2ChoiceSequence'.
     */
    public gr.ekt.cerif.schema.CfClass_Class2ChoiceSequence getCfClass_Class2ChoiceSequence() {
        return this.cfClass_Class2ChoiceSequence;
    }

    /**
     * Returns the value of field 'cfClass_Class2ChoiceSequence2'.
     * 
     * @return the value of field 'CfClass_Class2ChoiceSequence2'.
     */
    public gr.ekt.cerif.schema.CfClass_Class2ChoiceSequence2 getCfClass_Class2ChoiceSequence2() {
        return this.cfClass_Class2ChoiceSequence2;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfClass_Class2ChoiceSequence'.
     * 
     * @param cfClass_Class2ChoiceSequence the value of field
     * 'cfClass_Class2ChoiceSequence'.
     */
    public void setCfClass_Class2ChoiceSequence(final gr.ekt.cerif.schema.CfClass_Class2ChoiceSequence cfClass_Class2ChoiceSequence) {
        this.cfClass_Class2ChoiceSequence = cfClass_Class2ChoiceSequence;
    }

    /**
     * Sets the value of field 'cfClass_Class2ChoiceSequence2'.
     * 
     * @param cfClass_Class2ChoiceSequence2 the value of field
     * 'cfClass_Class2ChoiceSequence2'.
     */
    public void setCfClass_Class2ChoiceSequence2(final gr.ekt.cerif.schema.CfClass_Class2ChoiceSequence2 cfClass_Class2ChoiceSequence2) {
        this.cfClass_Class2ChoiceSequence2 = cfClass_Class2ChoiceSequence2;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfClass_Class2Choice
     */
    public static gr.ekt.cerif.schema.CfClass_Class2Choice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfClass_Class2Choice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfClass_Class2Choice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

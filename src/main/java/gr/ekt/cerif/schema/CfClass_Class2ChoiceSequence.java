/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfClass_Class2ChoiceSequence implements java.io.Serializable {

    private java.lang.String cfClassId2;

    private java.lang.String cfClassSchemeId2;

    public CfClass_Class2ChoiceSequence() {
        super();
    }

    /**
     * Returns the value of field 'cfClassId2'.
     * 
     * @return the value of field 'CfClassId2'.
     */
    public java.lang.String getCfClassId2() {
        return this.cfClassId2;
    }

    /**
     * Returns the value of field 'cfClassSchemeId2'.
     * 
     * @return the value of field 'CfClassSchemeId2'.
     */
    public java.lang.String getCfClassSchemeId2() {
        return this.cfClassSchemeId2;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfClassId2'.
     * 
     * @param cfClassId2 the value of field 'cfClassId2'.
     */
    public void setCfClassId2(final java.lang.String cfClassId2) {
        this.cfClassId2 = cfClassId2;
    }

    /**
     * Sets the value of field 'cfClassSchemeId2'.
     * 
     * @param cfClassSchemeId2 the value of field 'cfClassSchemeId2'
     */
    public void setCfClassSchemeId2(final java.lang.String cfClassSchemeId2) {
        this.cfClassSchemeId2 = cfClassSchemeId2;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfClass_Class2ChoiceSequence
     */
    public static gr.ekt.cerif.schema.CfClass_Class2ChoiceSequence unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfClass_Class2ChoiceSequence) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfClass_Class2ChoiceSequence.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

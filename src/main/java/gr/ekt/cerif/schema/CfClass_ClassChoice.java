/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfClass_ClassChoice implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfClass_ClassChoiceSequence cfClass_ClassChoiceSequence;

    private gr.ekt.cerif.schema.CfClass_ClassChoiceSequence2 cfClass_ClassChoiceSequence2;

    public CfClass_ClassChoice() {
        super();
    }

    /**
     * Returns the value of field 'cfClass_ClassChoiceSequence'.
     * 
     * @return the value of field 'CfClass_ClassChoiceSequence'.
     */
    public gr.ekt.cerif.schema.CfClass_ClassChoiceSequence getCfClass_ClassChoiceSequence() {
        return this.cfClass_ClassChoiceSequence;
    }

    /**
     * Returns the value of field 'cfClass_ClassChoiceSequence2'.
     * 
     * @return the value of field 'CfClass_ClassChoiceSequence2'.
     */
    public gr.ekt.cerif.schema.CfClass_ClassChoiceSequence2 getCfClass_ClassChoiceSequence2() {
        return this.cfClass_ClassChoiceSequence2;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfClass_ClassChoiceSequence'.
     * 
     * @param cfClass_ClassChoiceSequence the value of field
     * 'cfClass_ClassChoiceSequence'.
     */
    public void setCfClass_ClassChoiceSequence(final gr.ekt.cerif.schema.CfClass_ClassChoiceSequence cfClass_ClassChoiceSequence) {
        this.cfClass_ClassChoiceSequence = cfClass_ClassChoiceSequence;
    }

    /**
     * Sets the value of field 'cfClass_ClassChoiceSequence2'.
     * 
     * @param cfClass_ClassChoiceSequence2 the value of field
     * 'cfClass_ClassChoiceSequence2'.
     */
    public void setCfClass_ClassChoiceSequence2(final gr.ekt.cerif.schema.CfClass_ClassChoiceSequence2 cfClass_ClassChoiceSequence2) {
        this.cfClass_ClassChoiceSequence2 = cfClass_ClassChoiceSequence2;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfClass_ClassChoice
     */
    public static gr.ekt.cerif.schema.CfClass_ClassChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfClass_ClassChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfClass_ClassChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfCoreClassWithFraction__Group implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfCoreClass__Group cfCoreClass__Group;

    private float cfFraction;

    /**
     * Keeps track of whether primitive field cfFraction has been
     * set already.
     */
    private boolean hascfFraction;

    public CfCoreClassWithFraction__Group() {
        super();
    }

    /**
     */
    public void deleteCfFraction() {
        this.hascfFraction= false;
    }

    /**
     * Returns the value of field 'cfCoreClass__Group'.
     * 
     * @return the value of field 'CfCoreClass__Group'.
     */
    public gr.ekt.cerif.schema.CfCoreClass__Group getCfCoreClass__Group() {
        return this.cfCoreClass__Group;
    }

    /**
     * Returns the value of field 'cfFraction'.
     * 
     * @return the value of field 'CfFraction'.
     */
    public float getCfFraction() {
        return this.cfFraction;
    }

    /**
     * Method hasCfFraction.
     * 
     * @return true if at least one CfFraction has been added
     */
    public boolean hasCfFraction() {
        return this.hascfFraction;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCoreClass__Group'.
     * 
     * @param cfCoreClass__Group the value of field
     * 'cfCoreClass__Group'.
     */
    public void setCfCoreClass__Group(final gr.ekt.cerif.schema.CfCoreClass__Group cfCoreClass__Group) {
        this.cfCoreClass__Group = cfCoreClass__Group;
    }

    /**
     * Sets the value of field 'cfFraction'.
     * 
     * @param cfFraction the value of field 'cfFraction'.
     */
    public void setCfFraction(final float cfFraction) {
        this.cfFraction = cfFraction;
        this.hascfFraction = true;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfCoreClassWithFraction__Group
     */
    public static gr.ekt.cerif.schema.CfCoreClassWithFraction__Group unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfCoreClassWithFraction__Group) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfCoreClassWithFraction__Group.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

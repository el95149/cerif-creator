/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfCoreClass__Group implements java.io.Serializable {

    private java.lang.String cfClassId;

    private java.lang.String cfClassSchemeId;

    private java.util.Date cfStartDate;

    private java.util.Date cfEndDate;

    public CfCoreClass__Group() {
        super();
    }

    /**
     * Returns the value of field 'cfClassId'.
     * 
     * @return the value of field 'CfClassId'.
     */
    public java.lang.String getCfClassId() {
        return this.cfClassId;
    }

    /**
     * Returns the value of field 'cfClassSchemeId'.
     * 
     * @return the value of field 'CfClassSchemeId'.
     */
    public java.lang.String getCfClassSchemeId() {
        return this.cfClassSchemeId;
    }

    /**
     * Returns the value of field 'cfEndDate'.
     * 
     * @return the value of field 'CfEndDate'.
     */
    public java.util.Date getCfEndDate() {
        return this.cfEndDate;
    }

    /**
     * Returns the value of field 'cfStartDate'.
     * 
     * @return the value of field 'CfStartDate'.
     */
    public java.util.Date getCfStartDate() {
        return this.cfStartDate;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfClassId'.
     * 
     * @param cfClassId the value of field 'cfClassId'.
     */
    public void setCfClassId(final java.lang.String cfClassId) {
        this.cfClassId = cfClassId;
    }

    /**
     * Sets the value of field 'cfClassSchemeId'.
     * 
     * @param cfClassSchemeId the value of field 'cfClassSchemeId'.
     */
    public void setCfClassSchemeId(final java.lang.String cfClassSchemeId) {
        this.cfClassSchemeId = cfClassSchemeId;
    }

    /**
     * Sets the value of field 'cfEndDate'.
     * 
     * @param cfEndDate the value of field 'cfEndDate'.
     */
    public void setCfEndDate(final java.util.Date cfEndDate) {
        this.cfEndDate = cfEndDate;
    }

    /**
     * Sets the value of field 'cfStartDate'.
     * 
     * @param cfStartDate the value of field 'cfStartDate'.
     */
    public void setCfStartDate(final java.util.Date cfStartDate) {
        this.cfStartDate = cfStartDate;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfCoreClass__Grou
     */
    public static gr.ekt.cerif.schema.CfCoreClass__Group unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfCoreClass__Group) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfCoreClass__Group.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

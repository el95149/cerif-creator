/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfCountry__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfCountry__TypeCfPers_Country cfPers_Country;

    private gr.ekt.cerif.schema.CfCountry__TypeCfCountry_Class cfCountry_Class;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfCountry__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfCountry_Class'.
     * 
     * @return the value of field 'CfCountry_Class'.
     */
    public gr.ekt.cerif.schema.CfCountry__TypeCfCountry_Class getCfCountry_Class() {
        return this.cfCountry_Class;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfPers_Country'.
     * 
     * @return the value of field 'CfPers_Country'.
     */
    public gr.ekt.cerif.schema.CfCountry__TypeCfPers_Country getCfPers_Country() {
        return this.cfPers_Country;
    }

    /**
     * Sets the value of field 'cfCountry_Class'.
     * 
     * @param cfCountry_Class the value of field 'cfCountry_Class'.
     */
    public void setCfCountry_Class(final gr.ekt.cerif.schema.CfCountry__TypeCfCountry_Class cfCountry_Class) {
        this.cfCountry_Class = cfCountry_Class;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfPers_Country'.
     * 
     * @param cfPers_Country the value of field 'cfPers_Country'.
     */
    public void setCfPers_Country(final gr.ekt.cerif.schema.CfCountry__TypeCfPers_Country cfPers_Country) {
        this.cfPers_Country = cfPers_Country;
    }

}

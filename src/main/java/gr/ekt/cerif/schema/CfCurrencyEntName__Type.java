/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfCurrencyEntName__Type implements java.io.Serializable {

    private java.lang.String cfCurrCode;

    private gr.ekt.cerif.schema.CfEntName cfEntName;

    public CfCurrencyEntName__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfCurrCode'.
     * 
     * @return the value of field 'CfCurrCode'.
     */
    public java.lang.String getCfCurrCode() {
        return this.cfCurrCode;
    }

    /**
     * Returns the value of field 'cfEntName'.
     * 
     * @return the value of field 'CfEntName'.
     */
    public gr.ekt.cerif.schema.CfEntName getCfEntName() {
        return this.cfEntName;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCurrCode'.
     * 
     * @param cfCurrCode the value of field 'cfCurrCode'.
     */
    public void setCfCurrCode(final java.lang.String cfCurrCode) {
        this.cfCurrCode = cfCurrCode;
    }

    /**
     * Sets the value of field 'cfEntName'.
     * 
     * @param cfEntName the value of field 'cfEntName'.
     */
    public void setCfEntName(final gr.ekt.cerif.schema.CfEntName cfEntName) {
        this.cfEntName = cfEntName;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfCurrencyEntName__Type
     */
    public static gr.ekt.cerif.schema.CfCurrencyEntName__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfCurrencyEntName__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfCurrencyEntName__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

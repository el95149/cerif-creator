/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfCurrency__Type implements java.io.Serializable {

    private java.lang.String cfCurrCode;

    private java.lang.String cfNumCurrCode;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfCurrency__TypeChoice cfCurrency__TypeChoice;

    public CfCurrency__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfCurrCode'.
     * 
     * @return the value of field 'CfCurrCode'.
     */
    public java.lang.String getCfCurrCode() {
        return this.cfCurrCode;
    }

    /**
     * Returns the value of field 'cfCurrency__TypeChoice'.
     * 
     * @return the value of field 'CfCurrency__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfCurrency__TypeChoice getCfCurrency__TypeChoice() {
        return this.cfCurrency__TypeChoice;
    }

    /**
     * Returns the value of field 'cfNumCurrCode'.
     * 
     * @return the value of field 'CfNumCurrCode'.
     */
    public java.lang.String getCfNumCurrCode() {
        return this.cfNumCurrCode;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCurrCode'.
     * 
     * @param cfCurrCode the value of field 'cfCurrCode'.
     */
    public void setCfCurrCode(final java.lang.String cfCurrCode) {
        this.cfCurrCode = cfCurrCode;
    }

    /**
     * Sets the value of field 'cfCurrency__TypeChoice'.
     * 
     * @param cfCurrency__TypeChoice the value of field
     * 'cfCurrency__TypeChoice'.
     */
    public void setCfCurrency__TypeChoice(final gr.ekt.cerif.schema.CfCurrency__TypeChoice cfCurrency__TypeChoice) {
        this.cfCurrency__TypeChoice = cfCurrency__TypeChoice;
    }

    /**
     * Sets the value of field 'cfNumCurrCode'.
     * 
     * @param cfNumCurrCode the value of field 'cfNumCurrCode'.
     */
    public void setCfNumCurrCode(final java.lang.String cfNumCurrCode) {
        this.cfNumCurrCode = cfNumCurrCode;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfCurrency__Type
     */
    public static gr.ekt.cerif.schema.CfCurrency__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfCurrency__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfCurrency__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfCurrency__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfEntName cfEntName;

    private gr.ekt.cerif.schema.CfCurrency__TypeCfCurrency_Class cfCurrency_Class;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfCurrency__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfCurrency_Class'.
     * 
     * @return the value of field 'CfCurrency_Class'.
     */
    public gr.ekt.cerif.schema.CfCurrency__TypeCfCurrency_Class getCfCurrency_Class() {
        return this.cfCurrency_Class;
    }

    /**
     * Returns the value of field 'cfEntName'.
     * 
     * @return the value of field 'CfEntName'.
     */
    public gr.ekt.cerif.schema.CfEntName getCfEntName() {
        return this.cfEntName;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Sets the value of field 'cfCurrency_Class'.
     * 
     * @param cfCurrency_Class the value of field 'cfCurrency_Class'
     */
    public void setCfCurrency_Class(final gr.ekt.cerif.schema.CfCurrency__TypeCfCurrency_Class cfCurrency_Class) {
        this.cfCurrency_Class = cfCurrency_Class;
    }

    /**
     * Sets the value of field 'cfEntName'.
     * 
     * @param cfEntName the value of field 'cfEntName'.
     */
    public void setCfEntName(final gr.ekt.cerif.schema.CfEntName cfEntName) {
        this.cfEntName = cfEntName;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

}

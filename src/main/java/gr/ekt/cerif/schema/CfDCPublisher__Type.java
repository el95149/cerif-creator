/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfDCPublisher__Type implements java.io.Serializable {

    private java.lang.String cfDCId;

    private java.lang.String cfDCScheme;

    private java.lang.String cfDCLangTag;

    private java.lang.String cfDCTrans;

    private java.lang.String cfDCType;

    private java.lang.String cfDCValue;

    public CfDCPublisher__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfDCId'.
     * 
     * @return the value of field 'CfDCId'.
     */
    public java.lang.String getCfDCId() {
        return this.cfDCId;
    }

    /**
     * Returns the value of field 'cfDCLangTag'.
     * 
     * @return the value of field 'CfDCLangTag'.
     */
    public java.lang.String getCfDCLangTag() {
        return this.cfDCLangTag;
    }

    /**
     * Returns the value of field 'cfDCScheme'.
     * 
     * @return the value of field 'CfDCScheme'.
     */
    public java.lang.String getCfDCScheme() {
        return this.cfDCScheme;
    }

    /**
     * Returns the value of field 'cfDCTrans'.
     * 
     * @return the value of field 'CfDCTrans'.
     */
    public java.lang.String getCfDCTrans() {
        return this.cfDCTrans;
    }

    /**
     * Returns the value of field 'cfDCType'.
     * 
     * @return the value of field 'CfDCType'.
     */
    public java.lang.String getCfDCType() {
        return this.cfDCType;
    }

    /**
     * Returns the value of field 'cfDCValue'.
     * 
     * @return the value of field 'CfDCValue'.
     */
    public java.lang.String getCfDCValue() {
        return this.cfDCValue;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfDCId'.
     * 
     * @param cfDCId the value of field 'cfDCId'.
     */
    public void setCfDCId(final java.lang.String cfDCId) {
        this.cfDCId = cfDCId;
    }

    /**
     * Sets the value of field 'cfDCLangTag'.
     * 
     * @param cfDCLangTag the value of field 'cfDCLangTag'.
     */
    public void setCfDCLangTag(final java.lang.String cfDCLangTag) {
        this.cfDCLangTag = cfDCLangTag;
    }

    /**
     * Sets the value of field 'cfDCScheme'.
     * 
     * @param cfDCScheme the value of field 'cfDCScheme'.
     */
    public void setCfDCScheme(final java.lang.String cfDCScheme) {
        this.cfDCScheme = cfDCScheme;
    }

    /**
     * Sets the value of field 'cfDCTrans'.
     * 
     * @param cfDCTrans the value of field 'cfDCTrans'.
     */
    public void setCfDCTrans(final java.lang.String cfDCTrans) {
        this.cfDCTrans = cfDCTrans;
    }

    /**
     * Sets the value of field 'cfDCType'.
     * 
     * @param cfDCType the value of field 'cfDCType'.
     */
    public void setCfDCType(final java.lang.String cfDCType) {
        this.cfDCType = cfDCType;
    }

    /**
     * Sets the value of field 'cfDCValue'.
     * 
     * @param cfDCValue the value of field 'cfDCValue'.
     */
    public void setCfDCValue(final java.lang.String cfDCValue) {
        this.cfDCValue = cfDCValue;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfDCPublisher__Type
     */
    public static gr.ekt.cerif.schema.CfDCPublisher__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfDCPublisher__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfDCPublisher__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

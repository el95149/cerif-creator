/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfDCRelation__Type implements java.io.Serializable {

    private java.lang.String cfDCId1;

    private java.lang.String cfDCId2;

    private java.lang.String cfDCScheme1;

    private java.lang.String cfDCScheme2;

    private java.lang.String cfDCLangTag;

    private java.lang.String cfDCTrans;

    private java.util.Date cfDCStartDate;

    private java.util.Date cfDCEndDate;

    private java.lang.String cfDCType;

    private java.lang.String cfDCValue;

    public CfDCRelation__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfDCEndDate'.
     * 
     * @return the value of field 'CfDCEndDate'.
     */
    public java.util.Date getCfDCEndDate() {
        return this.cfDCEndDate;
    }

    /**
     * Returns the value of field 'cfDCId1'.
     * 
     * @return the value of field 'CfDCId1'.
     */
    public java.lang.String getCfDCId1() {
        return this.cfDCId1;
    }

    /**
     * Returns the value of field 'cfDCId2'.
     * 
     * @return the value of field 'CfDCId2'.
     */
    public java.lang.String getCfDCId2() {
        return this.cfDCId2;
    }

    /**
     * Returns the value of field 'cfDCLangTag'.
     * 
     * @return the value of field 'CfDCLangTag'.
     */
    public java.lang.String getCfDCLangTag() {
        return this.cfDCLangTag;
    }

    /**
     * Returns the value of field 'cfDCScheme1'.
     * 
     * @return the value of field 'CfDCScheme1'.
     */
    public java.lang.String getCfDCScheme1() {
        return this.cfDCScheme1;
    }

    /**
     * Returns the value of field 'cfDCScheme2'.
     * 
     * @return the value of field 'CfDCScheme2'.
     */
    public java.lang.String getCfDCScheme2() {
        return this.cfDCScheme2;
    }

    /**
     * Returns the value of field 'cfDCStartDate'.
     * 
     * @return the value of field 'CfDCStartDate'.
     */
    public java.util.Date getCfDCStartDate() {
        return this.cfDCStartDate;
    }

    /**
     * Returns the value of field 'cfDCTrans'.
     * 
     * @return the value of field 'CfDCTrans'.
     */
    public java.lang.String getCfDCTrans() {
        return this.cfDCTrans;
    }

    /**
     * Returns the value of field 'cfDCType'.
     * 
     * @return the value of field 'CfDCType'.
     */
    public java.lang.String getCfDCType() {
        return this.cfDCType;
    }

    /**
     * Returns the value of field 'cfDCValue'.
     * 
     * @return the value of field 'CfDCValue'.
     */
    public java.lang.String getCfDCValue() {
        return this.cfDCValue;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfDCEndDate'.
     * 
     * @param cfDCEndDate the value of field 'cfDCEndDate'.
     */
    public void setCfDCEndDate(final java.util.Date cfDCEndDate) {
        this.cfDCEndDate = cfDCEndDate;
    }

    /**
     * Sets the value of field 'cfDCId1'.
     * 
     * @param cfDCId1 the value of field 'cfDCId1'.
     */
    public void setCfDCId1(final java.lang.String cfDCId1) {
        this.cfDCId1 = cfDCId1;
    }

    /**
     * Sets the value of field 'cfDCId2'.
     * 
     * @param cfDCId2 the value of field 'cfDCId2'.
     */
    public void setCfDCId2(final java.lang.String cfDCId2) {
        this.cfDCId2 = cfDCId2;
    }

    /**
     * Sets the value of field 'cfDCLangTag'.
     * 
     * @param cfDCLangTag the value of field 'cfDCLangTag'.
     */
    public void setCfDCLangTag(final java.lang.String cfDCLangTag) {
        this.cfDCLangTag = cfDCLangTag;
    }

    /**
     * Sets the value of field 'cfDCScheme1'.
     * 
     * @param cfDCScheme1 the value of field 'cfDCScheme1'.
     */
    public void setCfDCScheme1(final java.lang.String cfDCScheme1) {
        this.cfDCScheme1 = cfDCScheme1;
    }

    /**
     * Sets the value of field 'cfDCScheme2'.
     * 
     * @param cfDCScheme2 the value of field 'cfDCScheme2'.
     */
    public void setCfDCScheme2(final java.lang.String cfDCScheme2) {
        this.cfDCScheme2 = cfDCScheme2;
    }

    /**
     * Sets the value of field 'cfDCStartDate'.
     * 
     * @param cfDCStartDate the value of field 'cfDCStartDate'.
     */
    public void setCfDCStartDate(final java.util.Date cfDCStartDate) {
        this.cfDCStartDate = cfDCStartDate;
    }

    /**
     * Sets the value of field 'cfDCTrans'.
     * 
     * @param cfDCTrans the value of field 'cfDCTrans'.
     */
    public void setCfDCTrans(final java.lang.String cfDCTrans) {
        this.cfDCTrans = cfDCTrans;
    }

    /**
     * Sets the value of field 'cfDCType'.
     * 
     * @param cfDCType the value of field 'cfDCType'.
     */
    public void setCfDCType(final java.lang.String cfDCType) {
        this.cfDCType = cfDCType;
    }

    /**
     * Sets the value of field 'cfDCValue'.
     * 
     * @param cfDCValue the value of field 'cfDCValue'.
     */
    public void setCfDCValue(final java.lang.String cfDCValue) {
        this.cfDCValue = cfDCValue;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfDCRelation__Typ
     */
    public static gr.ekt.cerif.schema.CfDCRelation__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfDCRelation__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfDCRelation__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfDC__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfDC__TypeCfOrgUnit_DC cfOrgUnit_DC;

    private gr.ekt.cerif.schema.CfDC__TypeCfPers_DC cfPers_DC;

    private gr.ekt.cerif.schema.CfDC__TypeCfProj_DC cfProj_DC;

    private gr.ekt.cerif.schema.CfDC__TypeCfResPubl_DC cfResPubl_DC;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfDC__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfOrgUnit_DC'.
     * 
     * @return the value of field 'CfOrgUnit_DC'.
     */
    public gr.ekt.cerif.schema.CfDC__TypeCfOrgUnit_DC getCfOrgUnit_DC() {
        return this.cfOrgUnit_DC;
    }

    /**
     * Returns the value of field 'cfPers_DC'.
     * 
     * @return the value of field 'CfPers_DC'.
     */
    public gr.ekt.cerif.schema.CfDC__TypeCfPers_DC getCfPers_DC() {
        return this.cfPers_DC;
    }

    /**
     * Returns the value of field 'cfProj_DC'.
     * 
     * @return the value of field 'CfProj_DC'.
     */
    public gr.ekt.cerif.schema.CfDC__TypeCfProj_DC getCfProj_DC() {
        return this.cfProj_DC;
    }

    /**
     * Returns the value of field 'cfResPubl_DC'.
     * 
     * @return the value of field 'CfResPubl_DC'.
     */
    public gr.ekt.cerif.schema.CfDC__TypeCfResPubl_DC getCfResPubl_DC() {
        return this.cfResPubl_DC;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfOrgUnit_DC'.
     * 
     * @param cfOrgUnit_DC the value of field 'cfOrgUnit_DC'.
     */
    public void setCfOrgUnit_DC(final gr.ekt.cerif.schema.CfDC__TypeCfOrgUnit_DC cfOrgUnit_DC) {
        this.cfOrgUnit_DC = cfOrgUnit_DC;
    }

    /**
     * Sets the value of field 'cfPers_DC'.
     * 
     * @param cfPers_DC the value of field 'cfPers_DC'.
     */
    public void setCfPers_DC(final gr.ekt.cerif.schema.CfDC__TypeCfPers_DC cfPers_DC) {
        this.cfPers_DC = cfPers_DC;
    }

    /**
     * Sets the value of field 'cfProj_DC'.
     * 
     * @param cfProj_DC the value of field 'cfProj_DC'.
     */
    public void setCfProj_DC(final gr.ekt.cerif.schema.CfDC__TypeCfProj_DC cfProj_DC) {
        this.cfProj_DC = cfProj_DC;
    }

    /**
     * Sets the value of field 'cfResPubl_DC'.
     * 
     * @param cfResPubl_DC the value of field 'cfResPubl_DC'.
     */
    public void setCfResPubl_DC(final gr.ekt.cerif.schema.CfDC__TypeCfResPubl_DC cfResPubl_DC) {
        this.cfResPubl_DC = cfResPubl_DC;
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfEAddr__Type implements java.io.Serializable {

    private java.lang.String cfEAddrId;

    private java.lang.String cfPAddrId;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfEAddr__TypeChoice cfEAddr__TypeChoice;

    public CfEAddr__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfEAddrId'.
     * 
     * @return the value of field 'CfEAddrId'.
     */
    public java.lang.String getCfEAddrId() {
        return this.cfEAddrId;
    }

    /**
     * Returns the value of field 'cfEAddr__TypeChoice'.
     * 
     * @return the value of field 'CfEAddr__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfEAddr__TypeChoice getCfEAddr__TypeChoice() {
        return this.cfEAddr__TypeChoice;
    }

    /**
     * Returns the value of field 'cfPAddrId'.
     * 
     * @return the value of field 'CfPAddrId'.
     */
    public java.lang.String getCfPAddrId() {
        return this.cfPAddrId;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfEAddrId'.
     * 
     * @param cfEAddrId the value of field 'cfEAddrId'.
     */
    public void setCfEAddrId(final java.lang.String cfEAddrId) {
        this.cfEAddrId = cfEAddrId;
    }

    /**
     * Sets the value of field 'cfEAddr__TypeChoice'.
     * 
     * @param cfEAddr__TypeChoice the value of field
     * 'cfEAddr__TypeChoice'.
     */
    public void setCfEAddr__TypeChoice(final gr.ekt.cerif.schema.CfEAddr__TypeChoice cfEAddr__TypeChoice) {
        this.cfEAddr__TypeChoice = cfEAddr__TypeChoice;
    }

    /**
     * Sets the value of field 'cfPAddrId'.
     * 
     * @param cfPAddrId the value of field 'cfPAddrId'.
     */
    public void setCfPAddrId(final java.lang.String cfPAddrId) {
        this.cfPAddrId = cfPAddrId;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfEAddr__Type
     */
    public static gr.ekt.cerif.schema.CfEAddr__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfEAddr__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfEAddr__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

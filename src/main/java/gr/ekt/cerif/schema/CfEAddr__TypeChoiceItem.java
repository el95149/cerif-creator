/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfEAddr__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfEAddr__TypeCfOrgUnit_EAddr cfOrgUnit_EAddr;

    private gr.ekt.cerif.schema.CfEAddr__TypeCfPers_EAddr cfPers_EAddr;

    private gr.ekt.cerif.schema.CfEAddr__TypeCfEAddr_Class cfEAddr_Class;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfEAddr__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfEAddr_Class'.
     * 
     * @return the value of field 'CfEAddr_Class'.
     */
    public gr.ekt.cerif.schema.CfEAddr__TypeCfEAddr_Class getCfEAddr_Class() {
        return this.cfEAddr_Class;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfOrgUnit_EAddr'.
     * 
     * @return the value of field 'CfOrgUnit_EAddr'.
     */
    public gr.ekt.cerif.schema.CfEAddr__TypeCfOrgUnit_EAddr getCfOrgUnit_EAddr() {
        return this.cfOrgUnit_EAddr;
    }

    /**
     * Returns the value of field 'cfPers_EAddr'.
     * 
     * @return the value of field 'CfPers_EAddr'.
     */
    public gr.ekt.cerif.schema.CfEAddr__TypeCfPers_EAddr getCfPers_EAddr() {
        return this.cfPers_EAddr;
    }

    /**
     * Sets the value of field 'cfEAddr_Class'.
     * 
     * @param cfEAddr_Class the value of field 'cfEAddr_Class'.
     */
    public void setCfEAddr_Class(final gr.ekt.cerif.schema.CfEAddr__TypeCfEAddr_Class cfEAddr_Class) {
        this.cfEAddr_Class = cfEAddr_Class;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfOrgUnit_EAddr'.
     * 
     * @param cfOrgUnit_EAddr the value of field 'cfOrgUnit_EAddr'.
     */
    public void setCfOrgUnit_EAddr(final gr.ekt.cerif.schema.CfEAddr__TypeCfOrgUnit_EAddr cfOrgUnit_EAddr) {
        this.cfOrgUnit_EAddr = cfOrgUnit_EAddr;
    }

    /**
     * Sets the value of field 'cfPers_EAddr'.
     * 
     * @param cfPers_EAddr the value of field 'cfPers_EAddr'.
     */
    public void setCfPers_EAddr(final gr.ekt.cerif.schema.CfEAddr__TypeCfPers_EAddr cfPers_EAddr) {
        this.cfPers_EAddr = cfPers_EAddr;
    }

}

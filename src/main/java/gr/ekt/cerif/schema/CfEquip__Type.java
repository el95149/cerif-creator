/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfEquip__Type implements java.io.Serializable {

    private java.lang.String cfEquipId;

    private java.lang.String cfAcro;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfEquip__TypeChoice cfEquip__TypeChoice;

    public CfEquip__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfAcro'.
     * 
     * @return the value of field 'CfAcro'.
     */
    public java.lang.String getCfAcro() {
        return this.cfAcro;
    }

    /**
     * Returns the value of field 'cfEquipId'.
     * 
     * @return the value of field 'CfEquipId'.
     */
    public java.lang.String getCfEquipId() {
        return this.cfEquipId;
    }

    /**
     * Returns the value of field 'cfEquip__TypeChoice'.
     * 
     * @return the value of field 'CfEquip__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeChoice getCfEquip__TypeChoice() {
        return this.cfEquip__TypeChoice;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfAcro'.
     * 
     * @param cfAcro the value of field 'cfAcro'.
     */
    public void setCfAcro(final java.lang.String cfAcro) {
        this.cfAcro = cfAcro;
    }

    /**
     * Sets the value of field 'cfEquipId'.
     * 
     * @param cfEquipId the value of field 'cfEquipId'.
     */
    public void setCfEquipId(final java.lang.String cfEquipId) {
        this.cfEquipId = cfEquipId;
    }

    /**
     * Sets the value of field 'cfEquip__TypeChoice'.
     * 
     * @param cfEquip__TypeChoice the value of field
     * 'cfEquip__TypeChoice'.
     */
    public void setCfEquip__TypeChoice(final gr.ekt.cerif.schema.CfEquip__TypeChoice cfEquip__TypeChoice) {
        this.cfEquip__TypeChoice = cfEquip__TypeChoice;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfEquip__Type
     */
    public static gr.ekt.cerif.schema.CfEquip__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfEquip__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfEquip__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

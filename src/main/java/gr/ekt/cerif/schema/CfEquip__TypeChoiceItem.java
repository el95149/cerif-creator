/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfEquip__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Class cfEquip_Class;

    private gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Fund cfEquip_Fund;

    private gr.ekt.cerif.schema.CfEquip__TypeCfOrgUnit_Equip cfOrgUnit_Equip;

    private gr.ekt.cerif.schema.CfEquip__TypeCfPers_Equip cfPers_Equip;

    private gr.ekt.cerif.schema.CfEquip__TypeCfProj_Equip cfProj_Equip;

    private gr.ekt.cerif.schema.CfEquip__TypeCfResPubl_Equip cfResPubl_Equip;

    private gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Medium cfEquip_Medium;

    private gr.ekt.cerif.schema.CfEquip__TypeCfEquip_PAddr cfEquip_PAddr;

    private gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Equip cfEquip_Equip;

    private gr.ekt.cerif.schema.CfEquip__TypeCfFacil_Equip cfFacil_Equip;

    private gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Srv cfEquip_Srv;

    private gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Event cfEquip_Event;

    private gr.ekt.cerif.schema.CfEquip__TypeCfResPat_Equip cfResPat_Equip;

    private gr.ekt.cerif.schema.CfEquip__TypeCfResProd_Equip cfResProd_Equip;

    private gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Meas cfEquip_Meas;

    private gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Indic cfEquip_Indic;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfEquip__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfEquip_Class'.
     * 
     * @return the value of field 'CfEquip_Class'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Class getCfEquip_Class() {
        return this.cfEquip_Class;
    }

    /**
     * Returns the value of field 'cfEquip_Equip'.
     * 
     * @return the value of field 'CfEquip_Equip'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Equip getCfEquip_Equip() {
        return this.cfEquip_Equip;
    }

    /**
     * Returns the value of field 'cfEquip_Event'.
     * 
     * @return the value of field 'CfEquip_Event'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Event getCfEquip_Event() {
        return this.cfEquip_Event;
    }

    /**
     * Returns the value of field 'cfEquip_Fund'.
     * 
     * @return the value of field 'CfEquip_Fund'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Fund getCfEquip_Fund() {
        return this.cfEquip_Fund;
    }

    /**
     * Returns the value of field 'cfEquip_Indic'.
     * 
     * @return the value of field 'CfEquip_Indic'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Indic getCfEquip_Indic() {
        return this.cfEquip_Indic;
    }

    /**
     * Returns the value of field 'cfEquip_Meas'.
     * 
     * @return the value of field 'CfEquip_Meas'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Meas getCfEquip_Meas() {
        return this.cfEquip_Meas;
    }

    /**
     * Returns the value of field 'cfEquip_Medium'.
     * 
     * @return the value of field 'CfEquip_Medium'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Medium getCfEquip_Medium() {
        return this.cfEquip_Medium;
    }

    /**
     * Returns the value of field 'cfEquip_PAddr'.
     * 
     * @return the value of field 'CfEquip_PAddr'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfEquip_PAddr getCfEquip_PAddr() {
        return this.cfEquip_PAddr;
    }

    /**
     * Returns the value of field 'cfEquip_Srv'.
     * 
     * @return the value of field 'CfEquip_Srv'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Srv getCfEquip_Srv() {
        return this.cfEquip_Srv;
    }

    /**
     * Returns the value of field 'cfFacil_Equip'.
     * 
     * @return the value of field 'CfFacil_Equip'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfFacil_Equip getCfFacil_Equip() {
        return this.cfFacil_Equip;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Equip'.
     * 
     * @return the value of field 'CfOrgUnit_Equip'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfOrgUnit_Equip getCfOrgUnit_Equip() {
        return this.cfOrgUnit_Equip;
    }

    /**
     * Returns the value of field 'cfPers_Equip'.
     * 
     * @return the value of field 'CfPers_Equip'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfPers_Equip getCfPers_Equip() {
        return this.cfPers_Equip;
    }

    /**
     * Returns the value of field 'cfProj_Equip'.
     * 
     * @return the value of field 'CfProj_Equip'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfProj_Equip getCfProj_Equip() {
        return this.cfProj_Equip;
    }

    /**
     * Returns the value of field 'cfResPat_Equip'.
     * 
     * @return the value of field 'CfResPat_Equip'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfResPat_Equip getCfResPat_Equip() {
        return this.cfResPat_Equip;
    }

    /**
     * Returns the value of field 'cfResProd_Equip'.
     * 
     * @return the value of field 'CfResProd_Equip'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfResProd_Equip getCfResProd_Equip() {
        return this.cfResProd_Equip;
    }

    /**
     * Returns the value of field 'cfResPubl_Equip'.
     * 
     * @return the value of field 'CfResPubl_Equip'.
     */
    public gr.ekt.cerif.schema.CfEquip__TypeCfResPubl_Equip getCfResPubl_Equip() {
        return this.cfResPubl_Equip;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfEquip_Class'.
     * 
     * @param cfEquip_Class the value of field 'cfEquip_Class'.
     */
    public void setCfEquip_Class(final gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Class cfEquip_Class) {
        this.cfEquip_Class = cfEquip_Class;
    }

    /**
     * Sets the value of field 'cfEquip_Equip'.
     * 
     * @param cfEquip_Equip the value of field 'cfEquip_Equip'.
     */
    public void setCfEquip_Equip(final gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Equip cfEquip_Equip) {
        this.cfEquip_Equip = cfEquip_Equip;
    }

    /**
     * Sets the value of field 'cfEquip_Event'.
     * 
     * @param cfEquip_Event the value of field 'cfEquip_Event'.
     */
    public void setCfEquip_Event(final gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Event cfEquip_Event) {
        this.cfEquip_Event = cfEquip_Event;
    }

    /**
     * Sets the value of field 'cfEquip_Fund'.
     * 
     * @param cfEquip_Fund the value of field 'cfEquip_Fund'.
     */
    public void setCfEquip_Fund(final gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Fund cfEquip_Fund) {
        this.cfEquip_Fund = cfEquip_Fund;
    }

    /**
     * Sets the value of field 'cfEquip_Indic'.
     * 
     * @param cfEquip_Indic the value of field 'cfEquip_Indic'.
     */
    public void setCfEquip_Indic(final gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Indic cfEquip_Indic) {
        this.cfEquip_Indic = cfEquip_Indic;
    }

    /**
     * Sets the value of field 'cfEquip_Meas'.
     * 
     * @param cfEquip_Meas the value of field 'cfEquip_Meas'.
     */
    public void setCfEquip_Meas(final gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Meas cfEquip_Meas) {
        this.cfEquip_Meas = cfEquip_Meas;
    }

    /**
     * Sets the value of field 'cfEquip_Medium'.
     * 
     * @param cfEquip_Medium the value of field 'cfEquip_Medium'.
     */
    public void setCfEquip_Medium(final gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Medium cfEquip_Medium) {
        this.cfEquip_Medium = cfEquip_Medium;
    }

    /**
     * Sets the value of field 'cfEquip_PAddr'.
     * 
     * @param cfEquip_PAddr the value of field 'cfEquip_PAddr'.
     */
    public void setCfEquip_PAddr(final gr.ekt.cerif.schema.CfEquip__TypeCfEquip_PAddr cfEquip_PAddr) {
        this.cfEquip_PAddr = cfEquip_PAddr;
    }

    /**
     * Sets the value of field 'cfEquip_Srv'.
     * 
     * @param cfEquip_Srv the value of field 'cfEquip_Srv'.
     */
    public void setCfEquip_Srv(final gr.ekt.cerif.schema.CfEquip__TypeCfEquip_Srv cfEquip_Srv) {
        this.cfEquip_Srv = cfEquip_Srv;
    }

    /**
     * Sets the value of field 'cfFacil_Equip'.
     * 
     * @param cfFacil_Equip the value of field 'cfFacil_Equip'.
     */
    public void setCfFacil_Equip(final gr.ekt.cerif.schema.CfEquip__TypeCfFacil_Equip cfFacil_Equip) {
        this.cfFacil_Equip = cfFacil_Equip;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Equip'.
     * 
     * @param cfOrgUnit_Equip the value of field 'cfOrgUnit_Equip'.
     */
    public void setCfOrgUnit_Equip(final gr.ekt.cerif.schema.CfEquip__TypeCfOrgUnit_Equip cfOrgUnit_Equip) {
        this.cfOrgUnit_Equip = cfOrgUnit_Equip;
    }

    /**
     * Sets the value of field 'cfPers_Equip'.
     * 
     * @param cfPers_Equip the value of field 'cfPers_Equip'.
     */
    public void setCfPers_Equip(final gr.ekt.cerif.schema.CfEquip__TypeCfPers_Equip cfPers_Equip) {
        this.cfPers_Equip = cfPers_Equip;
    }

    /**
     * Sets the value of field 'cfProj_Equip'.
     * 
     * @param cfProj_Equip the value of field 'cfProj_Equip'.
     */
    public void setCfProj_Equip(final gr.ekt.cerif.schema.CfEquip__TypeCfProj_Equip cfProj_Equip) {
        this.cfProj_Equip = cfProj_Equip;
    }

    /**
     * Sets the value of field 'cfResPat_Equip'.
     * 
     * @param cfResPat_Equip the value of field 'cfResPat_Equip'.
     */
    public void setCfResPat_Equip(final gr.ekt.cerif.schema.CfEquip__TypeCfResPat_Equip cfResPat_Equip) {
        this.cfResPat_Equip = cfResPat_Equip;
    }

    /**
     * Sets the value of field 'cfResProd_Equip'.
     * 
     * @param cfResProd_Equip the value of field 'cfResProd_Equip'.
     */
    public void setCfResProd_Equip(final gr.ekt.cerif.schema.CfEquip__TypeCfResProd_Equip cfResProd_Equip) {
        this.cfResProd_Equip = cfResProd_Equip;
    }

    /**
     * Sets the value of field 'cfResPubl_Equip'.
     * 
     * @param cfResPubl_Equip the value of field 'cfResPubl_Equip'.
     */
    public void setCfResPubl_Equip(final gr.ekt.cerif.schema.CfEquip__TypeCfResPubl_Equip cfResPubl_Equip) {
        this.cfResPubl_Equip = cfResPubl_Equip;
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfEvent__Type implements java.io.Serializable {

    private java.lang.String cfEventId;

    private java.lang.String cfCountryCode;

    private java.lang.String cfCityTown;

    private java.lang.String cfFeeOrFree;

    private org.exolab.castor.types.Date cfStartDate;

    private org.exolab.castor.types.Date cfEndDate;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfEvent__TypeChoice cfEvent__TypeChoice;

    public CfEvent__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfCityTown'.
     * 
     * @return the value of field 'CfCityTown'.
     */
    public java.lang.String getCfCityTown() {
        return this.cfCityTown;
    }

    /**
     * Returns the value of field 'cfCountryCode'.
     * 
     * @return the value of field 'CfCountryCode'.
     */
    public java.lang.String getCfCountryCode() {
        return this.cfCountryCode;
    }

    /**
     * Returns the value of field 'cfEndDate'.
     * 
     * @return the value of field 'CfEndDate'.
     */
    public org.exolab.castor.types.Date getCfEndDate() {
        return this.cfEndDate;
    }

    /**
     * Returns the value of field 'cfEventId'.
     * 
     * @return the value of field 'CfEventId'.
     */
    public java.lang.String getCfEventId() {
        return this.cfEventId;
    }

    /**
     * Returns the value of field 'cfEvent__TypeChoice'.
     * 
     * @return the value of field 'CfEvent__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeChoice getCfEvent__TypeChoice() {
        return this.cfEvent__TypeChoice;
    }

    /**
     * Returns the value of field 'cfFeeOrFree'.
     * 
     * @return the value of field 'CfFeeOrFree'.
     */
    public java.lang.String getCfFeeOrFree() {
        return this.cfFeeOrFree;
    }

    /**
     * Returns the value of field 'cfStartDate'.
     * 
     * @return the value of field 'CfStartDate'.
     */
    public org.exolab.castor.types.Date getCfStartDate() {
        return this.cfStartDate;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCityTown'.
     * 
     * @param cfCityTown the value of field 'cfCityTown'.
     */
    public void setCfCityTown(final java.lang.String cfCityTown) {
        this.cfCityTown = cfCityTown;
    }

    /**
     * Sets the value of field 'cfCountryCode'.
     * 
     * @param cfCountryCode the value of field 'cfCountryCode'.
     */
    public void setCfCountryCode(final java.lang.String cfCountryCode) {
        this.cfCountryCode = cfCountryCode;
    }

    /**
     * Sets the value of field 'cfEndDate'.
     * 
     * @param cfEndDate the value of field 'cfEndDate'.
     */
    public void setCfEndDate(final org.exolab.castor.types.Date cfEndDate) {
        this.cfEndDate = cfEndDate;
    }

    /**
     * Sets the value of field 'cfEventId'.
     * 
     * @param cfEventId the value of field 'cfEventId'.
     */
    public void setCfEventId(final java.lang.String cfEventId) {
        this.cfEventId = cfEventId;
    }

    /**
     * Sets the value of field 'cfEvent__TypeChoice'.
     * 
     * @param cfEvent__TypeChoice the value of field
     * 'cfEvent__TypeChoice'.
     */
    public void setCfEvent__TypeChoice(final gr.ekt.cerif.schema.CfEvent__TypeChoice cfEvent__TypeChoice) {
        this.cfEvent__TypeChoice = cfEvent__TypeChoice;
    }

    /**
     * Sets the value of field 'cfFeeOrFree'.
     * 
     * @param cfFeeOrFree the value of field 'cfFeeOrFree'.
     */
    public void setCfFeeOrFree(final java.lang.String cfFeeOrFree) {
        this.cfFeeOrFree = cfFeeOrFree;
    }

    /**
     * Sets the value of field 'cfStartDate'.
     * 
     * @param cfStartDate the value of field 'cfStartDate'.
     */
    public void setCfStartDate(final org.exolab.castor.types.Date cfStartDate) {
        this.cfStartDate = cfStartDate;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfEvent__Type
     */
    public static gr.ekt.cerif.schema.CfEvent__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfEvent__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfEvent__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfEvent__TypeChoice implements java.io.Serializable {

    private java.util.Vector<gr.ekt.cerif.schema.CfEvent__TypeChoiceItem> _items;

    public CfEvent__TypeChoice() {
        super();
        this._items = new java.util.Vector<gr.ekt.cerif.schema.CfEvent__TypeChoiceItem>();
    }

    /**
     * 
     * 
     * @param vCfEvent__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfEvent__TypeChoiceItem(final gr.ekt.cerif.schema.CfEvent__TypeChoiceItem vCfEvent__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.addElement(vCfEvent__TypeChoiceItem);
    }

    /**
     * 
     * 
     * @param index
     * @param vCfEvent__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfEvent__TypeChoiceItem(final int index,final gr.ekt.cerif.schema.CfEvent__TypeChoiceItem vCfEvent__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.add(index, vCfEvent__TypeChoiceItem);
    }

    /**
     * Method enumerateCfEvent__TypeChoiceItem.
     * 
     * @return an Enumeration over all
     * gr.ekt.cerif.schema.CfEvent__TypeChoiceItem elements
     */
    public java.util.Enumeration<? extends gr.ekt.cerif.schema.CfEvent__TypeChoiceItem> enumerateCfEvent__TypeChoiceItem() {
        return this._items.elements();
    }

    /**
     * Method getCfEvent__TypeChoiceItem.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * gr.ekt.cerif.schema.CfEvent__TypeChoiceItem at the given inde
     */
    public gr.ekt.cerif.schema.CfEvent__TypeChoiceItem getCfEvent__TypeChoiceItem(final int index) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("getCfEvent__TypeChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        return _items.get(index);
    }

    /**
     * Method getCfEvent__TypeChoiceItem.Returns the contents of
     * the collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public gr.ekt.cerif.schema.CfEvent__TypeChoiceItem[] getCfEvent__TypeChoiceItem() {
        gr.ekt.cerif.schema.CfEvent__TypeChoiceItem[] array = new gr.ekt.cerif.schema.CfEvent__TypeChoiceItem[0];
        return this._items.toArray(array);
    }

    /**
     * Method getCfEvent__TypeChoiceItemCount.
     * 
     * @return the size of this collection
     */
    public int getCfEvent__TypeChoiceItemCount() {
        return this._items.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllCfEvent__TypeChoiceItem() {
        this._items.clear();
    }

    /**
     * Method removeCfEvent__TypeChoiceItem.
     * 
     * @param vCfEvent__TypeChoiceItem
     * @return true if the object was removed from the collection.
     */
    public boolean removeCfEvent__TypeChoiceItem(final gr.ekt.cerif.schema.CfEvent__TypeChoiceItem vCfEvent__TypeChoiceItem) {
        boolean removed = _items.remove(vCfEvent__TypeChoiceItem);
        return removed;
    }

    /**
     * Method removeCfEvent__TypeChoiceItemAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public gr.ekt.cerif.schema.CfEvent__TypeChoiceItem removeCfEvent__TypeChoiceItemAt(final int index) {
        java.lang.Object obj = this._items.remove(index);
        return (gr.ekt.cerif.schema.CfEvent__TypeChoiceItem) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vCfEvent__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setCfEvent__TypeChoiceItem(final int index,final gr.ekt.cerif.schema.CfEvent__TypeChoiceItem vCfEvent__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("setCfEvent__TypeChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        this._items.set(index, vCfEvent__TypeChoiceItem);
    }

    /**
     * 
     * 
     * @param vCfEvent__TypeChoiceItemArray
     */
    public void setCfEvent__TypeChoiceItem(final gr.ekt.cerif.schema.CfEvent__TypeChoiceItem[] vCfEvent__TypeChoiceItemArray) {
        //-- copy array
        _items.clear();

        for (int i = 0; i < vCfEvent__TypeChoiceItemArray.length; i++) {
                this._items.add(vCfEvent__TypeChoiceItemArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfEvent__TypeChoice
     */
    public static gr.ekt.cerif.schema.CfEvent__TypeChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfEvent__TypeChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfEvent__TypeChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfEvent__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Class cfEvent_Class;

    private gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Fund cfEvent_Fund;

    private gr.ekt.cerif.schema.CfEvent__TypeCfResPubl_Event cfResPubl_Event;

    private gr.ekt.cerif.schema.CfEvent__TypeCfOrgUnit_Event cfOrgUnit_Event;

    private gr.ekt.cerif.schema.CfEvent__TypeCfPers_Event cfPers_Event;

    private gr.ekt.cerif.schema.CfEvent__TypeCfProj_Event cfProj_Event;

    private gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Event cfEvent_Event;

    private gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Medium cfEvent_Medium;

    private gr.ekt.cerif.schema.CfEvent__TypeCfFacil_Event cfFacil_Event;

    private gr.ekt.cerif.schema.CfEvent__TypeCfSrv_Event cfSrv_Event;

    private gr.ekt.cerif.schema.CfEvent__TypeCfEquip_Event cfEquip_Event;

    private gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Meas cfEvent_Meas;

    private gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Indic cfEvent_Indic;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfEvent__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfEquip_Event'.
     * 
     * @return the value of field 'CfEquip_Event'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfEquip_Event getCfEquip_Event() {
        return this.cfEquip_Event;
    }

    /**
     * Returns the value of field 'cfEvent_Class'.
     * 
     * @return the value of field 'CfEvent_Class'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Class getCfEvent_Class() {
        return this.cfEvent_Class;
    }

    /**
     * Returns the value of field 'cfEvent_Event'.
     * 
     * @return the value of field 'CfEvent_Event'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Event getCfEvent_Event() {
        return this.cfEvent_Event;
    }

    /**
     * Returns the value of field 'cfEvent_Fund'.
     * 
     * @return the value of field 'CfEvent_Fund'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Fund getCfEvent_Fund() {
        return this.cfEvent_Fund;
    }

    /**
     * Returns the value of field 'cfEvent_Indic'.
     * 
     * @return the value of field 'CfEvent_Indic'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Indic getCfEvent_Indic() {
        return this.cfEvent_Indic;
    }

    /**
     * Returns the value of field 'cfEvent_Meas'.
     * 
     * @return the value of field 'CfEvent_Meas'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Meas getCfEvent_Meas() {
        return this.cfEvent_Meas;
    }

    /**
     * Returns the value of field 'cfEvent_Medium'.
     * 
     * @return the value of field 'CfEvent_Medium'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Medium getCfEvent_Medium() {
        return this.cfEvent_Medium;
    }

    /**
     * Returns the value of field 'cfFacil_Event'.
     * 
     * @return the value of field 'CfFacil_Event'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfFacil_Event getCfFacil_Event() {
        return this.cfFacil_Event;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Event'.
     * 
     * @return the value of field 'CfOrgUnit_Event'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfOrgUnit_Event getCfOrgUnit_Event() {
        return this.cfOrgUnit_Event;
    }

    /**
     * Returns the value of field 'cfPers_Event'.
     * 
     * @return the value of field 'CfPers_Event'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfPers_Event getCfPers_Event() {
        return this.cfPers_Event;
    }

    /**
     * Returns the value of field 'cfProj_Event'.
     * 
     * @return the value of field 'CfProj_Event'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfProj_Event getCfProj_Event() {
        return this.cfProj_Event;
    }

    /**
     * Returns the value of field 'cfResPubl_Event'.
     * 
     * @return the value of field 'CfResPubl_Event'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfResPubl_Event getCfResPubl_Event() {
        return this.cfResPubl_Event;
    }

    /**
     * Returns the value of field 'cfSrv_Event'.
     * 
     * @return the value of field 'CfSrv_Event'.
     */
    public gr.ekt.cerif.schema.CfEvent__TypeCfSrv_Event getCfSrv_Event() {
        return this.cfSrv_Event;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfEquip_Event'.
     * 
     * @param cfEquip_Event the value of field 'cfEquip_Event'.
     */
    public void setCfEquip_Event(final gr.ekt.cerif.schema.CfEvent__TypeCfEquip_Event cfEquip_Event) {
        this.cfEquip_Event = cfEquip_Event;
    }

    /**
     * Sets the value of field 'cfEvent_Class'.
     * 
     * @param cfEvent_Class the value of field 'cfEvent_Class'.
     */
    public void setCfEvent_Class(final gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Class cfEvent_Class) {
        this.cfEvent_Class = cfEvent_Class;
    }

    /**
     * Sets the value of field 'cfEvent_Event'.
     * 
     * @param cfEvent_Event the value of field 'cfEvent_Event'.
     */
    public void setCfEvent_Event(final gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Event cfEvent_Event) {
        this.cfEvent_Event = cfEvent_Event;
    }

    /**
     * Sets the value of field 'cfEvent_Fund'.
     * 
     * @param cfEvent_Fund the value of field 'cfEvent_Fund'.
     */
    public void setCfEvent_Fund(final gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Fund cfEvent_Fund) {
        this.cfEvent_Fund = cfEvent_Fund;
    }

    /**
     * Sets the value of field 'cfEvent_Indic'.
     * 
     * @param cfEvent_Indic the value of field 'cfEvent_Indic'.
     */
    public void setCfEvent_Indic(final gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Indic cfEvent_Indic) {
        this.cfEvent_Indic = cfEvent_Indic;
    }

    /**
     * Sets the value of field 'cfEvent_Meas'.
     * 
     * @param cfEvent_Meas the value of field 'cfEvent_Meas'.
     */
    public void setCfEvent_Meas(final gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Meas cfEvent_Meas) {
        this.cfEvent_Meas = cfEvent_Meas;
    }

    /**
     * Sets the value of field 'cfEvent_Medium'.
     * 
     * @param cfEvent_Medium the value of field 'cfEvent_Medium'.
     */
    public void setCfEvent_Medium(final gr.ekt.cerif.schema.CfEvent__TypeCfEvent_Medium cfEvent_Medium) {
        this.cfEvent_Medium = cfEvent_Medium;
    }

    /**
     * Sets the value of field 'cfFacil_Event'.
     * 
     * @param cfFacil_Event the value of field 'cfFacil_Event'.
     */
    public void setCfFacil_Event(final gr.ekt.cerif.schema.CfEvent__TypeCfFacil_Event cfFacil_Event) {
        this.cfFacil_Event = cfFacil_Event;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Event'.
     * 
     * @param cfOrgUnit_Event the value of field 'cfOrgUnit_Event'.
     */
    public void setCfOrgUnit_Event(final gr.ekt.cerif.schema.CfEvent__TypeCfOrgUnit_Event cfOrgUnit_Event) {
        this.cfOrgUnit_Event = cfOrgUnit_Event;
    }

    /**
     * Sets the value of field 'cfPers_Event'.
     * 
     * @param cfPers_Event the value of field 'cfPers_Event'.
     */
    public void setCfPers_Event(final gr.ekt.cerif.schema.CfEvent__TypeCfPers_Event cfPers_Event) {
        this.cfPers_Event = cfPers_Event;
    }

    /**
     * Sets the value of field 'cfProj_Event'.
     * 
     * @param cfProj_Event the value of field 'cfProj_Event'.
     */
    public void setCfProj_Event(final gr.ekt.cerif.schema.CfEvent__TypeCfProj_Event cfProj_Event) {
        this.cfProj_Event = cfProj_Event;
    }

    /**
     * Sets the value of field 'cfResPubl_Event'.
     * 
     * @param cfResPubl_Event the value of field 'cfResPubl_Event'.
     */
    public void setCfResPubl_Event(final gr.ekt.cerif.schema.CfEvent__TypeCfResPubl_Event cfResPubl_Event) {
        this.cfResPubl_Event = cfResPubl_Event;
    }

    /**
     * Sets the value of field 'cfSrv_Event'.
     * 
     * @param cfSrv_Event the value of field 'cfSrv_Event'.
     */
    public void setCfSrv_Event(final gr.ekt.cerif.schema.CfEvent__TypeCfSrv_Event cfSrv_Event) {
        this.cfSrv_Event = cfSrv_Event;
    }

}

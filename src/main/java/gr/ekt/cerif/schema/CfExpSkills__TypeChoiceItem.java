/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfExpSkills__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfExpSkills__TypeCfOrgUnit_ExpSkills cfOrgUnit_ExpSkills;

    private gr.ekt.cerif.schema.CfExpSkills__TypeCfPers_ExpSkills cfPers_ExpSkills;

    private gr.ekt.cerif.schema.CfExpSkills__TypeCfExpSkills_Class cfExpSkills_Class;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfExpSkills__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfExpSkills_Class'.
     * 
     * @return the value of field 'CfExpSkills_Class'.
     */
    public gr.ekt.cerif.schema.CfExpSkills__TypeCfExpSkills_Class getCfExpSkills_Class() {
        return this.cfExpSkills_Class;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfOrgUnit_ExpSkills'.
     * 
     * @return the value of field 'CfOrgUnit_ExpSkills'.
     */
    public gr.ekt.cerif.schema.CfExpSkills__TypeCfOrgUnit_ExpSkills getCfOrgUnit_ExpSkills() {
        return this.cfOrgUnit_ExpSkills;
    }

    /**
     * Returns the value of field 'cfPers_ExpSkills'.
     * 
     * @return the value of field 'CfPers_ExpSkills'.
     */
    public gr.ekt.cerif.schema.CfExpSkills__TypeCfPers_ExpSkills getCfPers_ExpSkills() {
        return this.cfPers_ExpSkills;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfExpSkills_Class'.
     * 
     * @param cfExpSkills_Class the value of field
     * 'cfExpSkills_Class'.
     */
    public void setCfExpSkills_Class(final gr.ekt.cerif.schema.CfExpSkills__TypeCfExpSkills_Class cfExpSkills_Class) {
        this.cfExpSkills_Class = cfExpSkills_Class;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfOrgUnit_ExpSkills'.
     * 
     * @param cfOrgUnit_ExpSkills the value of field
     * 'cfOrgUnit_ExpSkills'.
     */
    public void setCfOrgUnit_ExpSkills(final gr.ekt.cerif.schema.CfExpSkills__TypeCfOrgUnit_ExpSkills cfOrgUnit_ExpSkills) {
        this.cfOrgUnit_ExpSkills = cfOrgUnit_ExpSkills;
    }

    /**
     * Sets the value of field 'cfPers_ExpSkills'.
     * 
     * @param cfPers_ExpSkills the value of field 'cfPers_ExpSkills'
     */
    public void setCfPers_ExpSkills(final gr.ekt.cerif.schema.CfExpSkills__TypeCfPers_ExpSkills cfPers_ExpSkills) {
        this.cfPers_ExpSkills = cfPers_ExpSkills;
    }

}

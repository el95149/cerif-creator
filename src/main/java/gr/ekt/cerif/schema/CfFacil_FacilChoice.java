/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfFacil_FacilChoice implements java.io.Serializable {

    private java.lang.String cfFacilId2;

    private java.lang.String cfFacilId1;

    public CfFacil_FacilChoice() {
        super();
    }

    /**
     * Returns the value of field 'cfFacilId1'.
     * 
     * @return the value of field 'CfFacilId1'.
     */
    public java.lang.String getCfFacilId1() {
        return this.cfFacilId1;
    }

    /**
     * Returns the value of field 'cfFacilId2'.
     * 
     * @return the value of field 'CfFacilId2'.
     */
    public java.lang.String getCfFacilId2() {
        return this.cfFacilId2;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfFacilId1'.
     * 
     * @param cfFacilId1 the value of field 'cfFacilId1'.
     */
    public void setCfFacilId1(final java.lang.String cfFacilId1) {
        this.cfFacilId1 = cfFacilId1;
    }

    /**
     * Sets the value of field 'cfFacilId2'.
     * 
     * @param cfFacilId2 the value of field 'cfFacilId2'.
     */
    public void setCfFacilId2(final java.lang.String cfFacilId2) {
        this.cfFacilId2 = cfFacilId2;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfFacil_FacilChoice
     */
    public static gr.ekt.cerif.schema.CfFacil_FacilChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfFacil_FacilChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfFacil_FacilChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfFacil__TypeChoice implements java.io.Serializable {

    private java.util.Vector<gr.ekt.cerif.schema.CfFacil__TypeChoiceItem> _items;

    public CfFacil__TypeChoice() {
        super();
        this._items = new java.util.Vector<gr.ekt.cerif.schema.CfFacil__TypeChoiceItem>();
    }

    /**
     * 
     * 
     * @param vCfFacil__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfFacil__TypeChoiceItem(final gr.ekt.cerif.schema.CfFacil__TypeChoiceItem vCfFacil__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.addElement(vCfFacil__TypeChoiceItem);
    }

    /**
     * 
     * 
     * @param index
     * @param vCfFacil__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfFacil__TypeChoiceItem(final int index,final gr.ekt.cerif.schema.CfFacil__TypeChoiceItem vCfFacil__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.add(index, vCfFacil__TypeChoiceItem);
    }

    /**
     * Method enumerateCfFacil__TypeChoiceItem.
     * 
     * @return an Enumeration over all
     * gr.ekt.cerif.schema.CfFacil__TypeChoiceItem elements
     */
    public java.util.Enumeration<? extends gr.ekt.cerif.schema.CfFacil__TypeChoiceItem> enumerateCfFacil__TypeChoiceItem() {
        return this._items.elements();
    }

    /**
     * Method getCfFacil__TypeChoiceItem.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * gr.ekt.cerif.schema.CfFacil__TypeChoiceItem at the given inde
     */
    public gr.ekt.cerif.schema.CfFacil__TypeChoiceItem getCfFacil__TypeChoiceItem(final int index) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("getCfFacil__TypeChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        return _items.get(index);
    }

    /**
     * Method getCfFacil__TypeChoiceItem.Returns the contents of
     * the collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public gr.ekt.cerif.schema.CfFacil__TypeChoiceItem[] getCfFacil__TypeChoiceItem() {
        gr.ekt.cerif.schema.CfFacil__TypeChoiceItem[] array = new gr.ekt.cerif.schema.CfFacil__TypeChoiceItem[0];
        return this._items.toArray(array);
    }

    /**
     * Method getCfFacil__TypeChoiceItemCount.
     * 
     * @return the size of this collection
     */
    public int getCfFacil__TypeChoiceItemCount() {
        return this._items.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllCfFacil__TypeChoiceItem() {
        this._items.clear();
    }

    /**
     * Method removeCfFacil__TypeChoiceItem.
     * 
     * @param vCfFacil__TypeChoiceItem
     * @return true if the object was removed from the collection.
     */
    public boolean removeCfFacil__TypeChoiceItem(final gr.ekt.cerif.schema.CfFacil__TypeChoiceItem vCfFacil__TypeChoiceItem) {
        boolean removed = _items.remove(vCfFacil__TypeChoiceItem);
        return removed;
    }

    /**
     * Method removeCfFacil__TypeChoiceItemAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public gr.ekt.cerif.schema.CfFacil__TypeChoiceItem removeCfFacil__TypeChoiceItemAt(final int index) {
        java.lang.Object obj = this._items.remove(index);
        return (gr.ekt.cerif.schema.CfFacil__TypeChoiceItem) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vCfFacil__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setCfFacil__TypeChoiceItem(final int index,final gr.ekt.cerif.schema.CfFacil__TypeChoiceItem vCfFacil__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("setCfFacil__TypeChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        this._items.set(index, vCfFacil__TypeChoiceItem);
    }

    /**
     * 
     * 
     * @param vCfFacil__TypeChoiceItemArray
     */
    public void setCfFacil__TypeChoiceItem(final gr.ekt.cerif.schema.CfFacil__TypeChoiceItem[] vCfFacil__TypeChoiceItemArray) {
        //-- copy array
        _items.clear();

        for (int i = 0; i < vCfFacil__TypeChoiceItemArray.length; i++) {
                this._items.add(vCfFacil__TypeChoiceItemArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfFacil__TypeChoice
     */
    public static gr.ekt.cerif.schema.CfFacil__TypeChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfFacil__TypeChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfFacil__TypeChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfFacil__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Class cfFacil_Class;

    private gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Fund cfFacil_Fund;

    private gr.ekt.cerif.schema.CfFacil__TypeCfOrgUnit_Facil cfOrgUnit_Facil;

    private gr.ekt.cerif.schema.CfFacil__TypeCfPers_Facil cfPers_Facil;

    private gr.ekt.cerif.schema.CfFacil__TypeCfProj_Facil cfProj_Facil;

    private gr.ekt.cerif.schema.CfFacil__TypeCfResPubl_Facil cfResPubl_Facil;

    private gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Medium cfFacil_Medium;

    private gr.ekt.cerif.schema.CfFacil__TypeCfFacil_PAddr cfFacil_PAddr;

    private gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Facil cfFacil_Facil;

    private gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Equip cfFacil_Equip;

    private gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Srv cfFacil_Srv;

    private gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Event cfFacil_Event;

    private gr.ekt.cerif.schema.CfFacil__TypeCfResPat_Facil cfResPat_Facil;

    private gr.ekt.cerif.schema.CfFacil__TypeCfResProd_Facil cfResProd_Facil;

    private gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Meas cfFacil_Meas;

    private gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Indic cfFacil_Indic;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfFacil__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfFacil_Class'.
     * 
     * @return the value of field 'CfFacil_Class'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Class getCfFacil_Class() {
        return this.cfFacil_Class;
    }

    /**
     * Returns the value of field 'cfFacil_Equip'.
     * 
     * @return the value of field 'CfFacil_Equip'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Equip getCfFacil_Equip() {
        return this.cfFacil_Equip;
    }

    /**
     * Returns the value of field 'cfFacil_Event'.
     * 
     * @return the value of field 'CfFacil_Event'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Event getCfFacil_Event() {
        return this.cfFacil_Event;
    }

    /**
     * Returns the value of field 'cfFacil_Facil'.
     * 
     * @return the value of field 'CfFacil_Facil'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Facil getCfFacil_Facil() {
        return this.cfFacil_Facil;
    }

    /**
     * Returns the value of field 'cfFacil_Fund'.
     * 
     * @return the value of field 'CfFacil_Fund'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Fund getCfFacil_Fund() {
        return this.cfFacil_Fund;
    }

    /**
     * Returns the value of field 'cfFacil_Indic'.
     * 
     * @return the value of field 'CfFacil_Indic'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Indic getCfFacil_Indic() {
        return this.cfFacil_Indic;
    }

    /**
     * Returns the value of field 'cfFacil_Meas'.
     * 
     * @return the value of field 'CfFacil_Meas'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Meas getCfFacil_Meas() {
        return this.cfFacil_Meas;
    }

    /**
     * Returns the value of field 'cfFacil_Medium'.
     * 
     * @return the value of field 'CfFacil_Medium'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Medium getCfFacil_Medium() {
        return this.cfFacil_Medium;
    }

    /**
     * Returns the value of field 'cfFacil_PAddr'.
     * 
     * @return the value of field 'CfFacil_PAddr'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfFacil_PAddr getCfFacil_PAddr() {
        return this.cfFacil_PAddr;
    }

    /**
     * Returns the value of field 'cfFacil_Srv'.
     * 
     * @return the value of field 'CfFacil_Srv'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Srv getCfFacil_Srv() {
        return this.cfFacil_Srv;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Facil'.
     * 
     * @return the value of field 'CfOrgUnit_Facil'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfOrgUnit_Facil getCfOrgUnit_Facil() {
        return this.cfOrgUnit_Facil;
    }

    /**
     * Returns the value of field 'cfPers_Facil'.
     * 
     * @return the value of field 'CfPers_Facil'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfPers_Facil getCfPers_Facil() {
        return this.cfPers_Facil;
    }

    /**
     * Returns the value of field 'cfProj_Facil'.
     * 
     * @return the value of field 'CfProj_Facil'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfProj_Facil getCfProj_Facil() {
        return this.cfProj_Facil;
    }

    /**
     * Returns the value of field 'cfResPat_Facil'.
     * 
     * @return the value of field 'CfResPat_Facil'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfResPat_Facil getCfResPat_Facil() {
        return this.cfResPat_Facil;
    }

    /**
     * Returns the value of field 'cfResProd_Facil'.
     * 
     * @return the value of field 'CfResProd_Facil'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfResProd_Facil getCfResProd_Facil() {
        return this.cfResProd_Facil;
    }

    /**
     * Returns the value of field 'cfResPubl_Facil'.
     * 
     * @return the value of field 'CfResPubl_Facil'.
     */
    public gr.ekt.cerif.schema.CfFacil__TypeCfResPubl_Facil getCfResPubl_Facil() {
        return this.cfResPubl_Facil;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfFacil_Class'.
     * 
     * @param cfFacil_Class the value of field 'cfFacil_Class'.
     */
    public void setCfFacil_Class(final gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Class cfFacil_Class) {
        this.cfFacil_Class = cfFacil_Class;
    }

    /**
     * Sets the value of field 'cfFacil_Equip'.
     * 
     * @param cfFacil_Equip the value of field 'cfFacil_Equip'.
     */
    public void setCfFacil_Equip(final gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Equip cfFacil_Equip) {
        this.cfFacil_Equip = cfFacil_Equip;
    }

    /**
     * Sets the value of field 'cfFacil_Event'.
     * 
     * @param cfFacil_Event the value of field 'cfFacil_Event'.
     */
    public void setCfFacil_Event(final gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Event cfFacil_Event) {
        this.cfFacil_Event = cfFacil_Event;
    }

    /**
     * Sets the value of field 'cfFacil_Facil'.
     * 
     * @param cfFacil_Facil the value of field 'cfFacil_Facil'.
     */
    public void setCfFacil_Facil(final gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Facil cfFacil_Facil) {
        this.cfFacil_Facil = cfFacil_Facil;
    }

    /**
     * Sets the value of field 'cfFacil_Fund'.
     * 
     * @param cfFacil_Fund the value of field 'cfFacil_Fund'.
     */
    public void setCfFacil_Fund(final gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Fund cfFacil_Fund) {
        this.cfFacil_Fund = cfFacil_Fund;
    }

    /**
     * Sets the value of field 'cfFacil_Indic'.
     * 
     * @param cfFacil_Indic the value of field 'cfFacil_Indic'.
     */
    public void setCfFacil_Indic(final gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Indic cfFacil_Indic) {
        this.cfFacil_Indic = cfFacil_Indic;
    }

    /**
     * Sets the value of field 'cfFacil_Meas'.
     * 
     * @param cfFacil_Meas the value of field 'cfFacil_Meas'.
     */
    public void setCfFacil_Meas(final gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Meas cfFacil_Meas) {
        this.cfFacil_Meas = cfFacil_Meas;
    }

    /**
     * Sets the value of field 'cfFacil_Medium'.
     * 
     * @param cfFacil_Medium the value of field 'cfFacil_Medium'.
     */
    public void setCfFacil_Medium(final gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Medium cfFacil_Medium) {
        this.cfFacil_Medium = cfFacil_Medium;
    }

    /**
     * Sets the value of field 'cfFacil_PAddr'.
     * 
     * @param cfFacil_PAddr the value of field 'cfFacil_PAddr'.
     */
    public void setCfFacil_PAddr(final gr.ekt.cerif.schema.CfFacil__TypeCfFacil_PAddr cfFacil_PAddr) {
        this.cfFacil_PAddr = cfFacil_PAddr;
    }

    /**
     * Sets the value of field 'cfFacil_Srv'.
     * 
     * @param cfFacil_Srv the value of field 'cfFacil_Srv'.
     */
    public void setCfFacil_Srv(final gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Srv cfFacil_Srv) {
        this.cfFacil_Srv = cfFacil_Srv;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Facil'.
     * 
     * @param cfOrgUnit_Facil the value of field 'cfOrgUnit_Facil'.
     */
    public void setCfOrgUnit_Facil(final gr.ekt.cerif.schema.CfFacil__TypeCfOrgUnit_Facil cfOrgUnit_Facil) {
        this.cfOrgUnit_Facil = cfOrgUnit_Facil;
    }

    /**
     * Sets the value of field 'cfPers_Facil'.
     * 
     * @param cfPers_Facil the value of field 'cfPers_Facil'.
     */
    public void setCfPers_Facil(final gr.ekt.cerif.schema.CfFacil__TypeCfPers_Facil cfPers_Facil) {
        this.cfPers_Facil = cfPers_Facil;
    }

    /**
     * Sets the value of field 'cfProj_Facil'.
     * 
     * @param cfProj_Facil the value of field 'cfProj_Facil'.
     */
    public void setCfProj_Facil(final gr.ekt.cerif.schema.CfFacil__TypeCfProj_Facil cfProj_Facil) {
        this.cfProj_Facil = cfProj_Facil;
    }

    /**
     * Sets the value of field 'cfResPat_Facil'.
     * 
     * @param cfResPat_Facil the value of field 'cfResPat_Facil'.
     */
    public void setCfResPat_Facil(final gr.ekt.cerif.schema.CfFacil__TypeCfResPat_Facil cfResPat_Facil) {
        this.cfResPat_Facil = cfResPat_Facil;
    }

    /**
     * Sets the value of field 'cfResProd_Facil'.
     * 
     * @param cfResProd_Facil the value of field 'cfResProd_Facil'.
     */
    public void setCfResProd_Facil(final gr.ekt.cerif.schema.CfFacil__TypeCfResProd_Facil cfResProd_Facil) {
        this.cfResProd_Facil = cfResProd_Facil;
    }

    /**
     * Sets the value of field 'cfResPubl_Facil'.
     * 
     * @param cfResPubl_Facil the value of field 'cfResPubl_Facil'.
     */
    public void setCfResPubl_Facil(final gr.ekt.cerif.schema.CfFacil__TypeCfResPubl_Facil cfResPubl_Facil) {
        this.cfResPubl_Facil = cfResPubl_Facil;
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfFedId__EmbType implements java.io.Serializable {

    private java.lang.String cfFedIdId;

    private java.lang.String cfFedId;

    private java.lang.String cfClassId;

    private java.lang.String cfClassSchemeId;

    private java.util.Date cfStartDate;

    private java.util.Date cfEndDate;

    private gr.ekt.cerif.schema.CfFedId__EmbTypeChoice cfFedId__EmbTypeChoice;

    public CfFedId__EmbType() {
        super();
    }

    /**
     * Returns the value of field 'cfClassId'.
     * 
     * @return the value of field 'CfClassId'.
     */
    public java.lang.String getCfClassId() {
        return this.cfClassId;
    }

    /**
     * Returns the value of field 'cfClassSchemeId'.
     * 
     * @return the value of field 'CfClassSchemeId'.
     */
    public java.lang.String getCfClassSchemeId() {
        return this.cfClassSchemeId;
    }

    /**
     * Returns the value of field 'cfEndDate'.
     * 
     * @return the value of field 'CfEndDate'.
     */
    public java.util.Date getCfEndDate() {
        return this.cfEndDate;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public java.lang.String getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfFedIdId'.
     * 
     * @return the value of field 'CfFedIdId'.
     */
    public java.lang.String getCfFedIdId() {
        return this.cfFedIdId;
    }

    /**
     * Returns the value of field 'cfFedId__EmbTypeChoice'.
     * 
     * @return the value of field 'CfFedId__EmbTypeChoice'.
     */
    public gr.ekt.cerif.schema.CfFedId__EmbTypeChoice getCfFedId__EmbTypeChoice() {
        return this.cfFedId__EmbTypeChoice;
    }

    /**
     * Returns the value of field 'cfStartDate'.
     * 
     * @return the value of field 'CfStartDate'.
     */
    public java.util.Date getCfStartDate() {
        return this.cfStartDate;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfClassId'.
     * 
     * @param cfClassId the value of field 'cfClassId'.
     */
    public void setCfClassId(final java.lang.String cfClassId) {
        this.cfClassId = cfClassId;
    }

    /**
     * Sets the value of field 'cfClassSchemeId'.
     * 
     * @param cfClassSchemeId the value of field 'cfClassSchemeId'.
     */
    public void setCfClassSchemeId(final java.lang.String cfClassSchemeId) {
        this.cfClassSchemeId = cfClassSchemeId;
    }

    /**
     * Sets the value of field 'cfEndDate'.
     * 
     * @param cfEndDate the value of field 'cfEndDate'.
     */
    public void setCfEndDate(final java.util.Date cfEndDate) {
        this.cfEndDate = cfEndDate;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final java.lang.String cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfFedIdId'.
     * 
     * @param cfFedIdId the value of field 'cfFedIdId'.
     */
    public void setCfFedIdId(final java.lang.String cfFedIdId) {
        this.cfFedIdId = cfFedIdId;
    }

    /**
     * Sets the value of field 'cfFedId__EmbTypeChoice'.
     * 
     * @param cfFedId__EmbTypeChoice the value of field
     * 'cfFedId__EmbTypeChoice'.
     */
    public void setCfFedId__EmbTypeChoice(final gr.ekt.cerif.schema.CfFedId__EmbTypeChoice cfFedId__EmbTypeChoice) {
        this.cfFedId__EmbTypeChoice = cfFedId__EmbTypeChoice;
    }

    /**
     * Sets the value of field 'cfStartDate'.
     * 
     * @param cfStartDate the value of field 'cfStartDate'.
     */
    public void setCfStartDate(final java.util.Date cfStartDate) {
        this.cfStartDate = cfStartDate;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfFedId__EmbType
     */
    public static gr.ekt.cerif.schema.CfFedId__EmbType unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfFedId__EmbType) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfFedId__EmbType.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

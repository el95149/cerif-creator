/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfFedId__EmbTypeChoice implements java.io.Serializable {

    private java.util.Vector<gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem> _items;

    public CfFedId__EmbTypeChoice() {
        super();
        this._items = new java.util.Vector<gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem>();
    }

    /**
     * 
     * 
     * @param vCfFedId__EmbTypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfFedId__EmbTypeChoiceItem(final gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem vCfFedId__EmbTypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.addElement(vCfFedId__EmbTypeChoiceItem);
    }

    /**
     * 
     * 
     * @param index
     * @param vCfFedId__EmbTypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfFedId__EmbTypeChoiceItem(final int index,final gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem vCfFedId__EmbTypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.add(index, vCfFedId__EmbTypeChoiceItem);
    }

    /**
     * Method enumerateCfFedId__EmbTypeChoiceItem.
     * 
     * @return an Enumeration over all
     * gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem elements
     */
    public java.util.Enumeration<? extends gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem> enumerateCfFedId__EmbTypeChoiceItem() {
        return this._items.elements();
    }

    /**
     * Method getCfFedId__EmbTypeChoiceItem.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem at the given
     * index
     */
    public gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem getCfFedId__EmbTypeChoiceItem(final int index) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("getCfFedId__EmbTypeChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        return _items.get(index);
    }

    /**
     * Method getCfFedId__EmbTypeChoiceItem.Returns the contents of
     * the collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem[] getCfFedId__EmbTypeChoiceItem() {
        gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem[] array = new gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem[0];
        return this._items.toArray(array);
    }

    /**
     * Method getCfFedId__EmbTypeChoiceItemCount.
     * 
     * @return the size of this collection
     */
    public int getCfFedId__EmbTypeChoiceItemCount() {
        return this._items.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllCfFedId__EmbTypeChoiceItem() {
        this._items.clear();
    }

    /**
     * Method removeCfFedId__EmbTypeChoiceItem.
     * 
     * @param vCfFedId__EmbTypeChoiceItem
     * @return true if the object was removed from the collection.
     */
    public boolean removeCfFedId__EmbTypeChoiceItem(final gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem vCfFedId__EmbTypeChoiceItem) {
        boolean removed = _items.remove(vCfFedId__EmbTypeChoiceItem);
        return removed;
    }

    /**
     * Method removeCfFedId__EmbTypeChoiceItemAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem removeCfFedId__EmbTypeChoiceItemAt(final int index) {
        java.lang.Object obj = this._items.remove(index);
        return (gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vCfFedId__EmbTypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setCfFedId__EmbTypeChoiceItem(final int index,final gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem vCfFedId__EmbTypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("setCfFedId__EmbTypeChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        this._items.set(index, vCfFedId__EmbTypeChoiceItem);
    }

    /**
     * 
     * 
     * @param vCfFedId__EmbTypeChoiceItemArray
     */
    public void setCfFedId__EmbTypeChoiceItem(final gr.ekt.cerif.schema.CfFedId__EmbTypeChoiceItem[] vCfFedId__EmbTypeChoiceItemArray) {
        //-- copy array
        _items.clear();

        for (int i = 0; i < vCfFedId__EmbTypeChoiceItemArray.length; i++) {
                this._items.add(vCfFedId__EmbTypeChoiceItemArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfFedId__EmbTypeChoice
     */
    public static gr.ekt.cerif.schema.CfFedId__EmbTypeChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfFedId__EmbTypeChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfFedId__EmbTypeChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

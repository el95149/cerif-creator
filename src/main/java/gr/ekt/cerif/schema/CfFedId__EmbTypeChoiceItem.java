/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfFedId__EmbTypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfFedId__EmbTypeCfFedId_Class cfFedId_Class;

    private gr.ekt.cerif.schema.CfFedId__EmbTypeCfFedId_Srv cfFedId_Srv;

    public CfFedId__EmbTypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfFedId_Class'.
     * 
     * @return the value of field 'CfFedId_Class'.
     */
    public gr.ekt.cerif.schema.CfFedId__EmbTypeCfFedId_Class getCfFedId_Class() {
        return this.cfFedId_Class;
    }

    /**
     * Returns the value of field 'cfFedId_Srv'.
     * 
     * @return the value of field 'CfFedId_Srv'.
     */
    public gr.ekt.cerif.schema.CfFedId__EmbTypeCfFedId_Srv getCfFedId_Srv() {
        return this.cfFedId_Srv;
    }

    /**
     * Sets the value of field 'cfFedId_Class'.
     * 
     * @param cfFedId_Class the value of field 'cfFedId_Class'.
     */
    public void setCfFedId_Class(final gr.ekt.cerif.schema.CfFedId__EmbTypeCfFedId_Class cfFedId_Class) {
        this.cfFedId_Class = cfFedId_Class;
    }

    /**
     * Sets the value of field 'cfFedId_Srv'.
     * 
     * @param cfFedId_Srv the value of field 'cfFedId_Srv'.
     */
    public void setCfFedId_Srv(final gr.ekt.cerif.schema.CfFedId__EmbTypeCfFedId_Srv cfFedId_Srv) {
        this.cfFedId_Srv = cfFedId_Srv;
    }

}

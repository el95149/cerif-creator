/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfFedId__Type implements java.io.Serializable {

    private java.lang.String cfFedIdId;

    private java.lang.String cfInstId;

    private java.lang.String cfFedId;

    private gr.ekt.cerif.schema.CfCoreClass__Group cfCoreClass__Group;

    private gr.ekt.cerif.schema.CfFedId__TypeChoice cfFedId__TypeChoice;

    public CfFedId__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfCoreClass__Group'.
     * 
     * @return the value of field 'CfCoreClass__Group'.
     */
    public gr.ekt.cerif.schema.CfCoreClass__Group getCfCoreClass__Group() {
        return this.cfCoreClass__Group;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public java.lang.String getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfFedIdId'.
     * 
     * @return the value of field 'CfFedIdId'.
     */
    public java.lang.String getCfFedIdId() {
        return this.cfFedIdId;
    }

    /**
     * Returns the value of field 'cfFedId__TypeChoice'.
     * 
     * @return the value of field 'CfFedId__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfFedId__TypeChoice getCfFedId__TypeChoice() {
        return this.cfFedId__TypeChoice;
    }

    /**
     * Returns the value of field 'cfInstId'.
     * 
     * @return the value of field 'CfInstId'.
     */
    public java.lang.String getCfInstId() {
        return this.cfInstId;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCoreClass__Group'.
     * 
     * @param cfCoreClass__Group the value of field
     * 'cfCoreClass__Group'.
     */
    public void setCfCoreClass__Group(final gr.ekt.cerif.schema.CfCoreClass__Group cfCoreClass__Group) {
        this.cfCoreClass__Group = cfCoreClass__Group;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final java.lang.String cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfFedIdId'.
     * 
     * @param cfFedIdId the value of field 'cfFedIdId'.
     */
    public void setCfFedIdId(final java.lang.String cfFedIdId) {
        this.cfFedIdId = cfFedIdId;
    }

    /**
     * Sets the value of field 'cfFedId__TypeChoice'.
     * 
     * @param cfFedId__TypeChoice the value of field
     * 'cfFedId__TypeChoice'.
     */
    public void setCfFedId__TypeChoice(final gr.ekt.cerif.schema.CfFedId__TypeChoice cfFedId__TypeChoice) {
        this.cfFedId__TypeChoice = cfFedId__TypeChoice;
    }

    /**
     * Sets the value of field 'cfInstId'.
     * 
     * @param cfInstId the value of field 'cfInstId'.
     */
    public void setCfInstId(final java.lang.String cfInstId) {
        this.cfInstId = cfInstId;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfFedId__Type
     */
    public static gr.ekt.cerif.schema.CfFedId__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfFedId__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfFedId__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

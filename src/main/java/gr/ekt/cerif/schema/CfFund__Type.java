/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfFund__Type implements java.io.Serializable {

    private java.lang.String cfFundId;

    private org.exolab.castor.types.Date cfStartDate;

    private org.exolab.castor.types.Date cfEndDate;

    private java.lang.String cfAcro;

    private gr.ekt.cerif.schema.CfAmount cfAmount;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfFund__TypeChoice cfFund__TypeChoice;

    public CfFund__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfAcro'.
     * 
     * @return the value of field 'CfAcro'.
     */
    public java.lang.String getCfAcro() {
        return this.cfAcro;
    }

    /**
     * Returns the value of field 'cfAmount'.
     * 
     * @return the value of field 'CfAmount'.
     */
    public gr.ekt.cerif.schema.CfAmount getCfAmount() {
        return this.cfAmount;
    }

    /**
     * Returns the value of field 'cfEndDate'.
     * 
     * @return the value of field 'CfEndDate'.
     */
    public org.exolab.castor.types.Date getCfEndDate() {
        return this.cfEndDate;
    }

    /**
     * Returns the value of field 'cfFundId'.
     * 
     * @return the value of field 'CfFundId'.
     */
    public java.lang.String getCfFundId() {
        return this.cfFundId;
    }

    /**
     * Returns the value of field 'cfFund__TypeChoice'.
     * 
     * @return the value of field 'CfFund__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeChoice getCfFund__TypeChoice() {
        return this.cfFund__TypeChoice;
    }

    /**
     * Returns the value of field 'cfStartDate'.
     * 
     * @return the value of field 'CfStartDate'.
     */
    public org.exolab.castor.types.Date getCfStartDate() {
        return this.cfStartDate;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfAcro'.
     * 
     * @param cfAcro the value of field 'cfAcro'.
     */
    public void setCfAcro(final java.lang.String cfAcro) {
        this.cfAcro = cfAcro;
    }

    /**
     * Sets the value of field 'cfAmount'.
     * 
     * @param cfAmount the value of field 'cfAmount'.
     */
    public void setCfAmount(final gr.ekt.cerif.schema.CfAmount cfAmount) {
        this.cfAmount = cfAmount;
    }

    /**
     * Sets the value of field 'cfEndDate'.
     * 
     * @param cfEndDate the value of field 'cfEndDate'.
     */
    public void setCfEndDate(final org.exolab.castor.types.Date cfEndDate) {
        this.cfEndDate = cfEndDate;
    }

    /**
     * Sets the value of field 'cfFundId'.
     * 
     * @param cfFundId the value of field 'cfFundId'.
     */
    public void setCfFundId(final java.lang.String cfFundId) {
        this.cfFundId = cfFundId;
    }

    /**
     * Sets the value of field 'cfFund__TypeChoice'.
     * 
     * @param cfFund__TypeChoice the value of field
     * 'cfFund__TypeChoice'.
     */
    public void setCfFund__TypeChoice(final gr.ekt.cerif.schema.CfFund__TypeChoice cfFund__TypeChoice) {
        this.cfFund__TypeChoice = cfFund__TypeChoice;
    }

    /**
     * Sets the value of field 'cfStartDate'.
     * 
     * @param cfStartDate the value of field 'cfStartDate'.
     */
    public void setCfStartDate(final org.exolab.castor.types.Date cfStartDate) {
        this.cfStartDate = cfStartDate;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfFund__Type
     */
    public static gr.ekt.cerif.schema.CfFund__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfFund__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfFund__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

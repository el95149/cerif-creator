/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfFund__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfFund__TypeCfEquip_Fund cfEquip_Fund;

    private gr.ekt.cerif.schema.CfFund__TypeCfEvent_Fund cfEvent_Fund;

    private gr.ekt.cerif.schema.CfFund__TypeCfFacil_Fund cfFacil_Fund;

    private gr.ekt.cerif.schema.CfFund__TypeCfFund_Class cfFund_Class;

    private gr.ekt.cerif.schema.CfFund__TypeCfFund_Fund cfFund_Fund;

    private gr.ekt.cerif.schema.CfFund__TypeCfOrgUnit_Fund cfOrgUnit_Fund;

    private gr.ekt.cerif.schema.CfFund__TypeCfPers_Fund cfPers_Fund;

    private gr.ekt.cerif.schema.CfFund__TypeCfProj_Fund cfProj_Fund;

    private gr.ekt.cerif.schema.CfFund__TypeCfResProd_Fund cfResProd_Fund;

    private gr.ekt.cerif.schema.CfFund__TypeCfResPubl_Fund cfResPubl_Fund;

    private gr.ekt.cerif.schema.CfFund__TypeCfResPat_Fund cfResPat_Fund;

    private gr.ekt.cerif.schema.CfFund__TypeCfSrv_Fund cfSrv_Fund;

    private gr.ekt.cerif.schema.CfFund__TypeCfMedium_Fund cfMedium_Fund;

    private gr.ekt.cerif.schema.CfFund__TypeCfFund_Indic cfFund_Indic;

    private gr.ekt.cerif.schema.CfFund__TypeCfFund_Meas cfFund_Meas;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfFund__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfEquip_Fund'.
     * 
     * @return the value of field 'CfEquip_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfEquip_Fund getCfEquip_Fund() {
        return this.cfEquip_Fund;
    }

    /**
     * Returns the value of field 'cfEvent_Fund'.
     * 
     * @return the value of field 'CfEvent_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfEvent_Fund getCfEvent_Fund() {
        return this.cfEvent_Fund;
    }

    /**
     * Returns the value of field 'cfFacil_Fund'.
     * 
     * @return the value of field 'CfFacil_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfFacil_Fund getCfFacil_Fund() {
        return this.cfFacil_Fund;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfFund_Class'.
     * 
     * @return the value of field 'CfFund_Class'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfFund_Class getCfFund_Class() {
        return this.cfFund_Class;
    }

    /**
     * Returns the value of field 'cfFund_Fund'.
     * 
     * @return the value of field 'CfFund_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfFund_Fund getCfFund_Fund() {
        return this.cfFund_Fund;
    }

    /**
     * Returns the value of field 'cfFund_Indic'.
     * 
     * @return the value of field 'CfFund_Indic'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfFund_Indic getCfFund_Indic() {
        return this.cfFund_Indic;
    }

    /**
     * Returns the value of field 'cfFund_Meas'.
     * 
     * @return the value of field 'CfFund_Meas'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfFund_Meas getCfFund_Meas() {
        return this.cfFund_Meas;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfMedium_Fund'.
     * 
     * @return the value of field 'CfMedium_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfMedium_Fund getCfMedium_Fund() {
        return this.cfMedium_Fund;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Fund'.
     * 
     * @return the value of field 'CfOrgUnit_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfOrgUnit_Fund getCfOrgUnit_Fund() {
        return this.cfOrgUnit_Fund;
    }

    /**
     * Returns the value of field 'cfPers_Fund'.
     * 
     * @return the value of field 'CfPers_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfPers_Fund getCfPers_Fund() {
        return this.cfPers_Fund;
    }

    /**
     * Returns the value of field 'cfProj_Fund'.
     * 
     * @return the value of field 'CfProj_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfProj_Fund getCfProj_Fund() {
        return this.cfProj_Fund;
    }

    /**
     * Returns the value of field 'cfResPat_Fund'.
     * 
     * @return the value of field 'CfResPat_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfResPat_Fund getCfResPat_Fund() {
        return this.cfResPat_Fund;
    }

    /**
     * Returns the value of field 'cfResProd_Fund'.
     * 
     * @return the value of field 'CfResProd_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfResProd_Fund getCfResProd_Fund() {
        return this.cfResProd_Fund;
    }

    /**
     * Returns the value of field 'cfResPubl_Fund'.
     * 
     * @return the value of field 'CfResPubl_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfResPubl_Fund getCfResPubl_Fund() {
        return this.cfResPubl_Fund;
    }

    /**
     * Returns the value of field 'cfSrv_Fund'.
     * 
     * @return the value of field 'CfSrv_Fund'.
     */
    public gr.ekt.cerif.schema.CfFund__TypeCfSrv_Fund getCfSrv_Fund() {
        return this.cfSrv_Fund;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfEquip_Fund'.
     * 
     * @param cfEquip_Fund the value of field 'cfEquip_Fund'.
     */
    public void setCfEquip_Fund(final gr.ekt.cerif.schema.CfFund__TypeCfEquip_Fund cfEquip_Fund) {
        this.cfEquip_Fund = cfEquip_Fund;
    }

    /**
     * Sets the value of field 'cfEvent_Fund'.
     * 
     * @param cfEvent_Fund the value of field 'cfEvent_Fund'.
     */
    public void setCfEvent_Fund(final gr.ekt.cerif.schema.CfFund__TypeCfEvent_Fund cfEvent_Fund) {
        this.cfEvent_Fund = cfEvent_Fund;
    }

    /**
     * Sets the value of field 'cfFacil_Fund'.
     * 
     * @param cfFacil_Fund the value of field 'cfFacil_Fund'.
     */
    public void setCfFacil_Fund(final gr.ekt.cerif.schema.CfFund__TypeCfFacil_Fund cfFacil_Fund) {
        this.cfFacil_Fund = cfFacil_Fund;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfFund_Class'.
     * 
     * @param cfFund_Class the value of field 'cfFund_Class'.
     */
    public void setCfFund_Class(final gr.ekt.cerif.schema.CfFund__TypeCfFund_Class cfFund_Class) {
        this.cfFund_Class = cfFund_Class;
    }

    /**
     * Sets the value of field 'cfFund_Fund'.
     * 
     * @param cfFund_Fund the value of field 'cfFund_Fund'.
     */
    public void setCfFund_Fund(final gr.ekt.cerif.schema.CfFund__TypeCfFund_Fund cfFund_Fund) {
        this.cfFund_Fund = cfFund_Fund;
    }

    /**
     * Sets the value of field 'cfFund_Indic'.
     * 
     * @param cfFund_Indic the value of field 'cfFund_Indic'.
     */
    public void setCfFund_Indic(final gr.ekt.cerif.schema.CfFund__TypeCfFund_Indic cfFund_Indic) {
        this.cfFund_Indic = cfFund_Indic;
    }

    /**
     * Sets the value of field 'cfFund_Meas'.
     * 
     * @param cfFund_Meas the value of field 'cfFund_Meas'.
     */
    public void setCfFund_Meas(final gr.ekt.cerif.schema.CfFund__TypeCfFund_Meas cfFund_Meas) {
        this.cfFund_Meas = cfFund_Meas;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfMedium_Fund'.
     * 
     * @param cfMedium_Fund the value of field 'cfMedium_Fund'.
     */
    public void setCfMedium_Fund(final gr.ekt.cerif.schema.CfFund__TypeCfMedium_Fund cfMedium_Fund) {
        this.cfMedium_Fund = cfMedium_Fund;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Fund'.
     * 
     * @param cfOrgUnit_Fund the value of field 'cfOrgUnit_Fund'.
     */
    public void setCfOrgUnit_Fund(final gr.ekt.cerif.schema.CfFund__TypeCfOrgUnit_Fund cfOrgUnit_Fund) {
        this.cfOrgUnit_Fund = cfOrgUnit_Fund;
    }

    /**
     * Sets the value of field 'cfPers_Fund'.
     * 
     * @param cfPers_Fund the value of field 'cfPers_Fund'.
     */
    public void setCfPers_Fund(final gr.ekt.cerif.schema.CfFund__TypeCfPers_Fund cfPers_Fund) {
        this.cfPers_Fund = cfPers_Fund;
    }

    /**
     * Sets the value of field 'cfProj_Fund'.
     * 
     * @param cfProj_Fund the value of field 'cfProj_Fund'.
     */
    public void setCfProj_Fund(final gr.ekt.cerif.schema.CfFund__TypeCfProj_Fund cfProj_Fund) {
        this.cfProj_Fund = cfProj_Fund;
    }

    /**
     * Sets the value of field 'cfResPat_Fund'.
     * 
     * @param cfResPat_Fund the value of field 'cfResPat_Fund'.
     */
    public void setCfResPat_Fund(final gr.ekt.cerif.schema.CfFund__TypeCfResPat_Fund cfResPat_Fund) {
        this.cfResPat_Fund = cfResPat_Fund;
    }

    /**
     * Sets the value of field 'cfResProd_Fund'.
     * 
     * @param cfResProd_Fund the value of field 'cfResProd_Fund'.
     */
    public void setCfResProd_Fund(final gr.ekt.cerif.schema.CfFund__TypeCfResProd_Fund cfResProd_Fund) {
        this.cfResProd_Fund = cfResProd_Fund;
    }

    /**
     * Sets the value of field 'cfResPubl_Fund'.
     * 
     * @param cfResPubl_Fund the value of field 'cfResPubl_Fund'.
     */
    public void setCfResPubl_Fund(final gr.ekt.cerif.schema.CfFund__TypeCfResPubl_Fund cfResPubl_Fund) {
        this.cfResPubl_Fund = cfResPubl_Fund;
    }

    /**
     * Sets the value of field 'cfSrv_Fund'.
     * 
     * @param cfSrv_Fund the value of field 'cfSrv_Fund'.
     */
    public void setCfSrv_Fund(final gr.ekt.cerif.schema.CfFund__TypeCfSrv_Fund cfSrv_Fund) {
        this.cfSrv_Fund = cfSrv_Fund;
    }

}

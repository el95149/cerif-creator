/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfGeoBBox__Type implements java.io.Serializable {

    private java.lang.String cfGeoBBoxId;

    private float cfWBLong;

    /**
     * Keeps track of whether primitive field cfWBLong has been set
     * already.
     */
    private boolean hascfWBLong;

    private float cfEBLong;

    /**
     * Keeps track of whether primitive field cfEBLong has been set
     * already.
     */
    private boolean hascfEBLong;

    private float cfSBLat;

    /**
     * Keeps track of whether primitive field cfSBLat has been set
     * already.
     */
    private boolean hascfSBLat;

    private float cfNBLat;

    /**
     * Keeps track of whether primitive field cfNBLat has been set
     * already.
     */
    private boolean hascfNBLat;

    private float cfMinElev;

    /**
     * Keeps track of whether primitive field cfMinElev has been
     * set already.
     */
    private boolean hascfMinElev;

    private float cfMaxElev;

    /**
     * Keeps track of whether primitive field cfMaxElev has been
     * set already.
     */
    private boolean hascfMaxElev;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfGeoBBox__TypeChoice cfGeoBBox__TypeChoice;

    public CfGeoBBox__Type() {
        super();
    }

    /**
     */
    public void deleteCfEBLong() {
        this.hascfEBLong= false;
    }

    /**
     */
    public void deleteCfMaxElev() {
        this.hascfMaxElev= false;
    }

    /**
     */
    public void deleteCfMinElev() {
        this.hascfMinElev= false;
    }

    /**
     */
    public void deleteCfNBLat() {
        this.hascfNBLat= false;
    }

    /**
     */
    public void deleteCfSBLat() {
        this.hascfSBLat= false;
    }

    /**
     */
    public void deleteCfWBLong() {
        this.hascfWBLong= false;
    }

    /**
     * Returns the value of field 'cfEBLong'.
     * 
     * @return the value of field 'CfEBLong'.
     */
    public float getCfEBLong() {
        return this.cfEBLong;
    }

    /**
     * Returns the value of field 'cfGeoBBoxId'.
     * 
     * @return the value of field 'CfGeoBBoxId'.
     */
    public java.lang.String getCfGeoBBoxId() {
        return this.cfGeoBBoxId;
    }

    /**
     * Returns the value of field 'cfGeoBBox__TypeChoice'.
     * 
     * @return the value of field 'CfGeoBBox__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfGeoBBox__TypeChoice getCfGeoBBox__TypeChoice() {
        return this.cfGeoBBox__TypeChoice;
    }

    /**
     * Returns the value of field 'cfMaxElev'.
     * 
     * @return the value of field 'CfMaxElev'.
     */
    public float getCfMaxElev() {
        return this.cfMaxElev;
    }

    /**
     * Returns the value of field 'cfMinElev'.
     * 
     * @return the value of field 'CfMinElev'.
     */
    public float getCfMinElev() {
        return this.cfMinElev;
    }

    /**
     * Returns the value of field 'cfNBLat'.
     * 
     * @return the value of field 'CfNBLat'.
     */
    public float getCfNBLat() {
        return this.cfNBLat;
    }

    /**
     * Returns the value of field 'cfSBLat'.
     * 
     * @return the value of field 'CfSBLat'.
     */
    public float getCfSBLat() {
        return this.cfSBLat;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Returns the value of field 'cfWBLong'.
     * 
     * @return the value of field 'CfWBLong'.
     */
    public float getCfWBLong() {
        return this.cfWBLong;
    }

    /**
     * Method hasCfEBLong.
     * 
     * @return true if at least one CfEBLong has been added
     */
    public boolean hasCfEBLong() {
        return this.hascfEBLong;
    }

    /**
     * Method hasCfMaxElev.
     * 
     * @return true if at least one CfMaxElev has been added
     */
    public boolean hasCfMaxElev() {
        return this.hascfMaxElev;
    }

    /**
     * Method hasCfMinElev.
     * 
     * @return true if at least one CfMinElev has been added
     */
    public boolean hasCfMinElev() {
        return this.hascfMinElev;
    }

    /**
     * Method hasCfNBLat.
     * 
     * @return true if at least one CfNBLat has been added
     */
    public boolean hasCfNBLat() {
        return this.hascfNBLat;
    }

    /**
     * Method hasCfSBLat.
     * 
     * @return true if at least one CfSBLat has been added
     */
    public boolean hasCfSBLat() {
        return this.hascfSBLat;
    }

    /**
     * Method hasCfWBLong.
     * 
     * @return true if at least one CfWBLong has been added
     */
    public boolean hasCfWBLong() {
        return this.hascfWBLong;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfEBLong'.
     * 
     * @param cfEBLong the value of field 'cfEBLong'.
     */
    public void setCfEBLong(final float cfEBLong) {
        this.cfEBLong = cfEBLong;
        this.hascfEBLong = true;
    }

    /**
     * Sets the value of field 'cfGeoBBoxId'.
     * 
     * @param cfGeoBBoxId the value of field 'cfGeoBBoxId'.
     */
    public void setCfGeoBBoxId(final java.lang.String cfGeoBBoxId) {
        this.cfGeoBBoxId = cfGeoBBoxId;
    }

    /**
     * Sets the value of field 'cfGeoBBox__TypeChoice'.
     * 
     * @param cfGeoBBox__TypeChoice the value of field
     * 'cfGeoBBox__TypeChoice'.
     */
    public void setCfGeoBBox__TypeChoice(final gr.ekt.cerif.schema.CfGeoBBox__TypeChoice cfGeoBBox__TypeChoice) {
        this.cfGeoBBox__TypeChoice = cfGeoBBox__TypeChoice;
    }

    /**
     * Sets the value of field 'cfMaxElev'.
     * 
     * @param cfMaxElev the value of field 'cfMaxElev'.
     */
    public void setCfMaxElev(final float cfMaxElev) {
        this.cfMaxElev = cfMaxElev;
        this.hascfMaxElev = true;
    }

    /**
     * Sets the value of field 'cfMinElev'.
     * 
     * @param cfMinElev the value of field 'cfMinElev'.
     */
    public void setCfMinElev(final float cfMinElev) {
        this.cfMinElev = cfMinElev;
        this.hascfMinElev = true;
    }

    /**
     * Sets the value of field 'cfNBLat'.
     * 
     * @param cfNBLat the value of field 'cfNBLat'.
     */
    public void setCfNBLat(final float cfNBLat) {
        this.cfNBLat = cfNBLat;
        this.hascfNBLat = true;
    }

    /**
     * Sets the value of field 'cfSBLat'.
     * 
     * @param cfSBLat the value of field 'cfSBLat'.
     */
    public void setCfSBLat(final float cfSBLat) {
        this.cfSBLat = cfSBLat;
        this.hascfSBLat = true;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Sets the value of field 'cfWBLong'.
     * 
     * @param cfWBLong the value of field 'cfWBLong'.
     */
    public void setCfWBLong(final float cfWBLong) {
        this.cfWBLong = cfWBLong;
        this.hascfWBLong = true;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfGeoBBox__Type
     */
    public static gr.ekt.cerif.schema.CfGeoBBox__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfGeoBBox__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfGeoBBox__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

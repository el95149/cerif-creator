/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfGeoBBox__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfGeoBBox__TypeCfPAddr_GeoBBox cfPAddr_GeoBBox;

    private gr.ekt.cerif.schema.CfGeoBBox__TypeCfGeoBBox_Class cfGeoBBox_Class;

    private gr.ekt.cerif.schema.CfGeoBBox__TypeCfGeoBBox_GeoBBox cfGeoBBox_GeoBBox;

    private gr.ekt.cerif.schema.CfGeoBBox__TypeCfResProd_GeoBBox cfResProd_GeoBBox;

    private gr.ekt.cerif.schema.CfGeoBBox__TypeCfGeoBBox_Meas cfGeoBBox_Meas;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfGeoBBox__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfGeoBBox_Class'.
     * 
     * @return the value of field 'CfGeoBBox_Class'.
     */
    public gr.ekt.cerif.schema.CfGeoBBox__TypeCfGeoBBox_Class getCfGeoBBox_Class() {
        return this.cfGeoBBox_Class;
    }

    /**
     * Returns the value of field 'cfGeoBBox_GeoBBox'.
     * 
     * @return the value of field 'CfGeoBBox_GeoBBox'.
     */
    public gr.ekt.cerif.schema.CfGeoBBox__TypeCfGeoBBox_GeoBBox getCfGeoBBox_GeoBBox() {
        return this.cfGeoBBox_GeoBBox;
    }

    /**
     * Returns the value of field 'cfGeoBBox_Meas'.
     * 
     * @return the value of field 'CfGeoBBox_Meas'.
     */
    public gr.ekt.cerif.schema.CfGeoBBox__TypeCfGeoBBox_Meas getCfGeoBBox_Meas() {
        return this.cfGeoBBox_Meas;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfPAddr_GeoBBox'.
     * 
     * @return the value of field 'CfPAddr_GeoBBox'.
     */
    public gr.ekt.cerif.schema.CfGeoBBox__TypeCfPAddr_GeoBBox getCfPAddr_GeoBBox() {
        return this.cfPAddr_GeoBBox;
    }

    /**
     * Returns the value of field 'cfResProd_GeoBBox'.
     * 
     * @return the value of field 'CfResProd_GeoBBox'.
     */
    public gr.ekt.cerif.schema.CfGeoBBox__TypeCfResProd_GeoBBox getCfResProd_GeoBBox() {
        return this.cfResProd_GeoBBox;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfGeoBBox_Class'.
     * 
     * @param cfGeoBBox_Class the value of field 'cfGeoBBox_Class'.
     */
    public void setCfGeoBBox_Class(final gr.ekt.cerif.schema.CfGeoBBox__TypeCfGeoBBox_Class cfGeoBBox_Class) {
        this.cfGeoBBox_Class = cfGeoBBox_Class;
    }

    /**
     * Sets the value of field 'cfGeoBBox_GeoBBox'.
     * 
     * @param cfGeoBBox_GeoBBox the value of field
     * 'cfGeoBBox_GeoBBox'.
     */
    public void setCfGeoBBox_GeoBBox(final gr.ekt.cerif.schema.CfGeoBBox__TypeCfGeoBBox_GeoBBox cfGeoBBox_GeoBBox) {
        this.cfGeoBBox_GeoBBox = cfGeoBBox_GeoBBox;
    }

    /**
     * Sets the value of field 'cfGeoBBox_Meas'.
     * 
     * @param cfGeoBBox_Meas the value of field 'cfGeoBBox_Meas'.
     */
    public void setCfGeoBBox_Meas(final gr.ekt.cerif.schema.CfGeoBBox__TypeCfGeoBBox_Meas cfGeoBBox_Meas) {
        this.cfGeoBBox_Meas = cfGeoBBox_Meas;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfPAddr_GeoBBox'.
     * 
     * @param cfPAddr_GeoBBox the value of field 'cfPAddr_GeoBBox'.
     */
    public void setCfPAddr_GeoBBox(final gr.ekt.cerif.schema.CfGeoBBox__TypeCfPAddr_GeoBBox cfPAddr_GeoBBox) {
        this.cfPAddr_GeoBBox = cfPAddr_GeoBBox;
    }

    /**
     * Sets the value of field 'cfResProd_GeoBBox'.
     * 
     * @param cfResProd_GeoBBox the value of field
     * 'cfResProd_GeoBBox'.
     */
    public void setCfResProd_GeoBBox(final gr.ekt.cerif.schema.CfGeoBBox__TypeCfResProd_GeoBBox cfResProd_GeoBBox) {
        this.cfResProd_GeoBBox = cfResProd_GeoBBox;
    }

}

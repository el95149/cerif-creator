/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfIndic_IndicChoice implements java.io.Serializable {

    private java.lang.String cfIndicId2;

    private java.lang.String cfIndicId1;

    public CfIndic_IndicChoice() {
        super();
    }

    /**
     * Returns the value of field 'cfIndicId1'.
     * 
     * @return the value of field 'CfIndicId1'.
     */
    public java.lang.String getCfIndicId1() {
        return this.cfIndicId1;
    }

    /**
     * Returns the value of field 'cfIndicId2'.
     * 
     * @return the value of field 'CfIndicId2'.
     */
    public java.lang.String getCfIndicId2() {
        return this.cfIndicId2;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfIndicId1'.
     * 
     * @param cfIndicId1 the value of field 'cfIndicId1'.
     */
    public void setCfIndicId1(final java.lang.String cfIndicId1) {
        this.cfIndicId1 = cfIndicId1;
    }

    /**
     * Sets the value of field 'cfIndicId2'.
     * 
     * @param cfIndicId2 the value of field 'cfIndicId2'.
     */
    public void setCfIndicId2(final java.lang.String cfIndicId2) {
        this.cfIndicId2 = cfIndicId2;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfIndic_IndicChoice
     */
    public static gr.ekt.cerif.schema.CfIndic_IndicChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfIndic_IndicChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfIndic_IndicChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

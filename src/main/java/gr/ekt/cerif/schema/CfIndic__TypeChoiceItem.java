/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfIndic__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfIndic__TypeCfIndic_Meas cfIndic_Meas;

    private gr.ekt.cerif.schema.CfIndic__TypeCfIndic_Class cfIndic_Class;

    private gr.ekt.cerif.schema.CfIndic__TypeCfPers_Indic cfPers_Indic;

    private gr.ekt.cerif.schema.CfIndic__TypeCfOrgUnit_Indic cfOrgUnit_Indic;

    private gr.ekt.cerif.schema.CfIndic__TypeCfProj_Indic cfProj_Indic;

    private gr.ekt.cerif.schema.CfIndic__TypeCfResPubl_Indic cfResPubl_Indic;

    private gr.ekt.cerif.schema.CfIndic__TypeCfResPat_Indic cfResPat_Indic;

    private gr.ekt.cerif.schema.CfIndic__TypeCfResProd_Indic cfResProd_Indic;

    private gr.ekt.cerif.schema.CfIndic__TypeCfFacil_Indic cfFacil_Indic;

    private gr.ekt.cerif.schema.CfIndic__TypeCfSrv_Indic cfSrv_Indic;

    private gr.ekt.cerif.schema.CfIndic__TypeCfEquip_Indic cfEquip_Indic;

    private gr.ekt.cerif.schema.CfIndic__TypeCfEvent_Indic cfEvent_Indic;

    private gr.ekt.cerif.schema.CfIndic__TypeCfMedium_Indic cfMedium_Indic;

    private gr.ekt.cerif.schema.CfIndic__TypeCfIndic_Indic cfIndic_Indic;

    private gr.ekt.cerif.schema.CfIndic__TypeCfFund_Indic cfFund_Indic;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfIndic__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfEquip_Indic'.
     * 
     * @return the value of field 'CfEquip_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfEquip_Indic getCfEquip_Indic() {
        return this.cfEquip_Indic;
    }

    /**
     * Returns the value of field 'cfEvent_Indic'.
     * 
     * @return the value of field 'CfEvent_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfEvent_Indic getCfEvent_Indic() {
        return this.cfEvent_Indic;
    }

    /**
     * Returns the value of field 'cfFacil_Indic'.
     * 
     * @return the value of field 'CfFacil_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfFacil_Indic getCfFacil_Indic() {
        return this.cfFacil_Indic;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfFund_Indic'.
     * 
     * @return the value of field 'CfFund_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfFund_Indic getCfFund_Indic() {
        return this.cfFund_Indic;
    }

    /**
     * Returns the value of field 'cfIndic_Class'.
     * 
     * @return the value of field 'CfIndic_Class'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfIndic_Class getCfIndic_Class() {
        return this.cfIndic_Class;
    }

    /**
     * Returns the value of field 'cfIndic_Indic'.
     * 
     * @return the value of field 'CfIndic_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfIndic_Indic getCfIndic_Indic() {
        return this.cfIndic_Indic;
    }

    /**
     * Returns the value of field 'cfIndic_Meas'.
     * 
     * @return the value of field 'CfIndic_Meas'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfIndic_Meas getCfIndic_Meas() {
        return this.cfIndic_Meas;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfMedium_Indic'.
     * 
     * @return the value of field 'CfMedium_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfMedium_Indic getCfMedium_Indic() {
        return this.cfMedium_Indic;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Indic'.
     * 
     * @return the value of field 'CfOrgUnit_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfOrgUnit_Indic getCfOrgUnit_Indic() {
        return this.cfOrgUnit_Indic;
    }

    /**
     * Returns the value of field 'cfPers_Indic'.
     * 
     * @return the value of field 'CfPers_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfPers_Indic getCfPers_Indic() {
        return this.cfPers_Indic;
    }

    /**
     * Returns the value of field 'cfProj_Indic'.
     * 
     * @return the value of field 'CfProj_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfProj_Indic getCfProj_Indic() {
        return this.cfProj_Indic;
    }

    /**
     * Returns the value of field 'cfResPat_Indic'.
     * 
     * @return the value of field 'CfResPat_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfResPat_Indic getCfResPat_Indic() {
        return this.cfResPat_Indic;
    }

    /**
     * Returns the value of field 'cfResProd_Indic'.
     * 
     * @return the value of field 'CfResProd_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfResProd_Indic getCfResProd_Indic() {
        return this.cfResProd_Indic;
    }

    /**
     * Returns the value of field 'cfResPubl_Indic'.
     * 
     * @return the value of field 'CfResPubl_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfResPubl_Indic getCfResPubl_Indic() {
        return this.cfResPubl_Indic;
    }

    /**
     * Returns the value of field 'cfSrv_Indic'.
     * 
     * @return the value of field 'CfSrv_Indic'.
     */
    public gr.ekt.cerif.schema.CfIndic__TypeCfSrv_Indic getCfSrv_Indic() {
        return this.cfSrv_Indic;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfEquip_Indic'.
     * 
     * @param cfEquip_Indic the value of field 'cfEquip_Indic'.
     */
    public void setCfEquip_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfEquip_Indic cfEquip_Indic) {
        this.cfEquip_Indic = cfEquip_Indic;
    }

    /**
     * Sets the value of field 'cfEvent_Indic'.
     * 
     * @param cfEvent_Indic the value of field 'cfEvent_Indic'.
     */
    public void setCfEvent_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfEvent_Indic cfEvent_Indic) {
        this.cfEvent_Indic = cfEvent_Indic;
    }

    /**
     * Sets the value of field 'cfFacil_Indic'.
     * 
     * @param cfFacil_Indic the value of field 'cfFacil_Indic'.
     */
    public void setCfFacil_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfFacil_Indic cfFacil_Indic) {
        this.cfFacil_Indic = cfFacil_Indic;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfFund_Indic'.
     * 
     * @param cfFund_Indic the value of field 'cfFund_Indic'.
     */
    public void setCfFund_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfFund_Indic cfFund_Indic) {
        this.cfFund_Indic = cfFund_Indic;
    }

    /**
     * Sets the value of field 'cfIndic_Class'.
     * 
     * @param cfIndic_Class the value of field 'cfIndic_Class'.
     */
    public void setCfIndic_Class(final gr.ekt.cerif.schema.CfIndic__TypeCfIndic_Class cfIndic_Class) {
        this.cfIndic_Class = cfIndic_Class;
    }

    /**
     * Sets the value of field 'cfIndic_Indic'.
     * 
     * @param cfIndic_Indic the value of field 'cfIndic_Indic'.
     */
    public void setCfIndic_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfIndic_Indic cfIndic_Indic) {
        this.cfIndic_Indic = cfIndic_Indic;
    }

    /**
     * Sets the value of field 'cfIndic_Meas'.
     * 
     * @param cfIndic_Meas the value of field 'cfIndic_Meas'.
     */
    public void setCfIndic_Meas(final gr.ekt.cerif.schema.CfIndic__TypeCfIndic_Meas cfIndic_Meas) {
        this.cfIndic_Meas = cfIndic_Meas;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfMedium_Indic'.
     * 
     * @param cfMedium_Indic the value of field 'cfMedium_Indic'.
     */
    public void setCfMedium_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfMedium_Indic cfMedium_Indic) {
        this.cfMedium_Indic = cfMedium_Indic;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Indic'.
     * 
     * @param cfOrgUnit_Indic the value of field 'cfOrgUnit_Indic'.
     */
    public void setCfOrgUnit_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfOrgUnit_Indic cfOrgUnit_Indic) {
        this.cfOrgUnit_Indic = cfOrgUnit_Indic;
    }

    /**
     * Sets the value of field 'cfPers_Indic'.
     * 
     * @param cfPers_Indic the value of field 'cfPers_Indic'.
     */
    public void setCfPers_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfPers_Indic cfPers_Indic) {
        this.cfPers_Indic = cfPers_Indic;
    }

    /**
     * Sets the value of field 'cfProj_Indic'.
     * 
     * @param cfProj_Indic the value of field 'cfProj_Indic'.
     */
    public void setCfProj_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfProj_Indic cfProj_Indic) {
        this.cfProj_Indic = cfProj_Indic;
    }

    /**
     * Sets the value of field 'cfResPat_Indic'.
     * 
     * @param cfResPat_Indic the value of field 'cfResPat_Indic'.
     */
    public void setCfResPat_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfResPat_Indic cfResPat_Indic) {
        this.cfResPat_Indic = cfResPat_Indic;
    }

    /**
     * Sets the value of field 'cfResProd_Indic'.
     * 
     * @param cfResProd_Indic the value of field 'cfResProd_Indic'.
     */
    public void setCfResProd_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfResProd_Indic cfResProd_Indic) {
        this.cfResProd_Indic = cfResProd_Indic;
    }

    /**
     * Sets the value of field 'cfResPubl_Indic'.
     * 
     * @param cfResPubl_Indic the value of field 'cfResPubl_Indic'.
     */
    public void setCfResPubl_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfResPubl_Indic cfResPubl_Indic) {
        this.cfResPubl_Indic = cfResPubl_Indic;
    }

    /**
     * Sets the value of field 'cfSrv_Indic'.
     * 
     * @param cfSrv_Indic the value of field 'cfSrv_Indic'.
     */
    public void setCfSrv_Indic(final gr.ekt.cerif.schema.CfIndic__TypeCfSrv_Indic cfSrv_Indic) {
        this.cfSrv_Indic = cfSrv_Indic;
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfLang__TypeCfPers_Lang implements java.io.Serializable {

    private java.lang.String cfPersId;

    private gr.ekt.cerif.schema.CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group;

    private java.lang.String cfSkillReading;

    private java.lang.String cfSkillSpeaking;

    private java.lang.String cfSkillWriting;

    public CfLang__TypeCfPers_Lang() {
        super();
    }

    /**
     * Returns the value of field 'cfCoreClassWithFraction__Group'.
     * 
     * @return the value of field 'CfCoreClassWithFraction__Group'.
     */
    public gr.ekt.cerif.schema.CfCoreClassWithFraction__Group getCfCoreClassWithFraction__Group() {
        return this.cfCoreClassWithFraction__Group;
    }

    /**
     * Returns the value of field 'cfPersId'.
     * 
     * @return the value of field 'CfPersId'.
     */
    public java.lang.String getCfPersId() {
        return this.cfPersId;
    }

    /**
     * Returns the value of field 'cfSkillReading'.
     * 
     * @return the value of field 'CfSkillReading'.
     */
    public java.lang.String getCfSkillReading() {
        return this.cfSkillReading;
    }

    /**
     * Returns the value of field 'cfSkillSpeaking'.
     * 
     * @return the value of field 'CfSkillSpeaking'.
     */
    public java.lang.String getCfSkillSpeaking() {
        return this.cfSkillSpeaking;
    }

    /**
     * Returns the value of field 'cfSkillWriting'.
     * 
     * @return the value of field 'CfSkillWriting'.
     */
    public java.lang.String getCfSkillWriting() {
        return this.cfSkillWriting;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCoreClassWithFraction__Group'.
     * 
     * @param cfCoreClassWithFraction__Group the value of field
     * 'cfCoreClassWithFraction__Group'.
     */
    public void setCfCoreClassWithFraction__Group(final gr.ekt.cerif.schema.CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group) {
        this.cfCoreClassWithFraction__Group = cfCoreClassWithFraction__Group;
    }

    /**
     * Sets the value of field 'cfPersId'.
     * 
     * @param cfPersId the value of field 'cfPersId'.
     */
    public void setCfPersId(final java.lang.String cfPersId) {
        this.cfPersId = cfPersId;
    }

    /**
     * Sets the value of field 'cfSkillReading'.
     * 
     * @param cfSkillReading the value of field 'cfSkillReading'.
     */
    public void setCfSkillReading(final java.lang.String cfSkillReading) {
        this.cfSkillReading = cfSkillReading;
    }

    /**
     * Sets the value of field 'cfSkillSpeaking'.
     * 
     * @param cfSkillSpeaking the value of field 'cfSkillSpeaking'.
     */
    public void setCfSkillSpeaking(final java.lang.String cfSkillSpeaking) {
        this.cfSkillSpeaking = cfSkillSpeaking;
    }

    /**
     * Sets the value of field 'cfSkillWriting'.
     * 
     * @param cfSkillWriting the value of field 'cfSkillWriting'.
     */
    public void setCfSkillWriting(final java.lang.String cfSkillWriting) {
        this.cfSkillWriting = cfSkillWriting;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfLang__TypeCfPers_Lang
     */
    public static gr.ekt.cerif.schema.CfLang__TypeCfPers_Lang unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfLang__TypeCfPers_Lang) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfLang__TypeCfPers_Lang.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

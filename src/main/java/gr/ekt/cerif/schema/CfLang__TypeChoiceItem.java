/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfLang__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfLang__TypeCfPers_Lang cfPers_Lang;

    private gr.ekt.cerif.schema.CfLang__TypeCfLang_Class cfLang_Class;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfLang__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfLang_Class'.
     * 
     * @return the value of field 'CfLang_Class'.
     */
    public gr.ekt.cerif.schema.CfLang__TypeCfLang_Class getCfLang_Class() {
        return this.cfLang_Class;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfPers_Lang'.
     * 
     * @return the value of field 'CfPers_Lang'.
     */
    public gr.ekt.cerif.schema.CfLang__TypeCfPers_Lang getCfPers_Lang() {
        return this.cfPers_Lang;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfLang_Class'.
     * 
     * @param cfLang_Class the value of field 'cfLang_Class'.
     */
    public void setCfLang_Class(final gr.ekt.cerif.schema.CfLang__TypeCfLang_Class cfLang_Class) {
        this.cfLang_Class = cfLang_Class;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfPers_Lang'.
     * 
     * @param cfPers_Lang the value of field 'cfPers_Lang'.
     */
    public void setCfPers_Lang(final gr.ekt.cerif.schema.CfLang__TypeCfPers_Lang cfPers_Lang) {
        this.cfPers_Lang = cfPers_Lang;
    }

}

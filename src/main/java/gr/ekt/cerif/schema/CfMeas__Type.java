/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfMeas__Type implements java.io.Serializable {

    private java.lang.String cfMeasId;

    private int cfCountInt;

    /**
     * Keeps track of whether primitive field cfCountInt has been
     * set already.
     */
    private boolean hascfCountInt;

    private float cfValFloatP;

    /**
     * Keeps track of whether primitive field cfValFloatP has been
     * set already.
     */
    private boolean hascfValFloatP;

    private float cfValJudgeNum;

    /**
     * Keeps track of whether primitive field cfValJudgeNum has
     * been set already.
     */
    private boolean hascfValJudgeNum;

    private int cfCountIntChange;

    /**
     * Keeps track of whether primitive field cfCountIntChange has
     * been set already.
     */
    private boolean hascfCountIntChange;

    private float cfCountFloatPChange;

    /**
     * Keeps track of whether primitive field cfCountFloatPChange
     * has been set already.
     */
    private boolean hascfCountFloatPChange;

    private float cfValJudgeNumChange;

    /**
     * Keeps track of whether primitive field cfValJudgeNumChange
     * has been set already.
     */
    private boolean hascfValJudgeNumChange;

    private java.lang.String cfValJudgeText;

    private java.lang.String cfValJudgeTextChange;

    private java.util.Date cfDateTime;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfMeas__TypeChoice cfMeas__TypeChoice;

    public CfMeas__Type() {
        super();
    }

    /**
     */
    public void deleteCfCountFloatPChange() {
        this.hascfCountFloatPChange= false;
    }

    /**
     */
    public void deleteCfCountInt() {
        this.hascfCountInt= false;
    }

    /**
     */
    public void deleteCfCountIntChange() {
        this.hascfCountIntChange= false;
    }

    /**
     */
    public void deleteCfValFloatP() {
        this.hascfValFloatP= false;
    }

    /**
     */
    public void deleteCfValJudgeNum() {
        this.hascfValJudgeNum= false;
    }

    /**
     */
    public void deleteCfValJudgeNumChange() {
        this.hascfValJudgeNumChange= false;
    }

    /**
     * Returns the value of field 'cfCountFloatPChange'.
     * 
     * @return the value of field 'CfCountFloatPChange'.
     */
    public float getCfCountFloatPChange() {
        return this.cfCountFloatPChange;
    }

    /**
     * Returns the value of field 'cfCountInt'.
     * 
     * @return the value of field 'CfCountInt'.
     */
    public int getCfCountInt() {
        return this.cfCountInt;
    }

    /**
     * Returns the value of field 'cfCountIntChange'.
     * 
     * @return the value of field 'CfCountIntChange'.
     */
    public int getCfCountIntChange() {
        return this.cfCountIntChange;
    }

    /**
     * Returns the value of field 'cfDateTime'.
     * 
     * @return the value of field 'CfDateTime'.
     */
    public java.util.Date getCfDateTime() {
        return this.cfDateTime;
    }

    /**
     * Returns the value of field 'cfMeasId'.
     * 
     * @return the value of field 'CfMeasId'.
     */
    public java.lang.String getCfMeasId() {
        return this.cfMeasId;
    }

    /**
     * Returns the value of field 'cfMeas__TypeChoice'.
     * 
     * @return the value of field 'CfMeas__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeChoice getCfMeas__TypeChoice() {
        return this.cfMeas__TypeChoice;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Returns the value of field 'cfValFloatP'.
     * 
     * @return the value of field 'CfValFloatP'.
     */
    public float getCfValFloatP() {
        return this.cfValFloatP;
    }

    /**
     * Returns the value of field 'cfValJudgeNum'.
     * 
     * @return the value of field 'CfValJudgeNum'.
     */
    public float getCfValJudgeNum() {
        return this.cfValJudgeNum;
    }

    /**
     * Returns the value of field 'cfValJudgeNumChange'.
     * 
     * @return the value of field 'CfValJudgeNumChange'.
     */
    public float getCfValJudgeNumChange() {
        return this.cfValJudgeNumChange;
    }

    /**
     * Returns the value of field 'cfValJudgeText'.
     * 
     * @return the value of field 'CfValJudgeText'.
     */
    public java.lang.String getCfValJudgeText() {
        return this.cfValJudgeText;
    }

    /**
     * Returns the value of field 'cfValJudgeTextChange'.
     * 
     * @return the value of field 'CfValJudgeTextChange'.
     */
    public java.lang.String getCfValJudgeTextChange() {
        return this.cfValJudgeTextChange;
    }

    /**
     * Method hasCfCountFloatPChange.
     * 
     * @return true if at least one CfCountFloatPChange has been
     * added
     */
    public boolean hasCfCountFloatPChange() {
        return this.hascfCountFloatPChange;
    }

    /**
     * Method hasCfCountInt.
     * 
     * @return true if at least one CfCountInt has been added
     */
    public boolean hasCfCountInt() {
        return this.hascfCountInt;
    }

    /**
     * Method hasCfCountIntChange.
     * 
     * @return true if at least one CfCountIntChange has been added
     */
    public boolean hasCfCountIntChange() {
        return this.hascfCountIntChange;
    }

    /**
     * Method hasCfValFloatP.
     * 
     * @return true if at least one CfValFloatP has been added
     */
    public boolean hasCfValFloatP() {
        return this.hascfValFloatP;
    }

    /**
     * Method hasCfValJudgeNum.
     * 
     * @return true if at least one CfValJudgeNum has been added
     */
    public boolean hasCfValJudgeNum() {
        return this.hascfValJudgeNum;
    }

    /**
     * Method hasCfValJudgeNumChange.
     * 
     * @return true if at least one CfValJudgeNumChange has been
     * added
     */
    public boolean hasCfValJudgeNumChange() {
        return this.hascfValJudgeNumChange;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCountFloatPChange'.
     * 
     * @param cfCountFloatPChange the value of field
     * 'cfCountFloatPChange'.
     */
    public void setCfCountFloatPChange(final float cfCountFloatPChange) {
        this.cfCountFloatPChange = cfCountFloatPChange;
        this.hascfCountFloatPChange = true;
    }

    /**
     * Sets the value of field 'cfCountInt'.
     * 
     * @param cfCountInt the value of field 'cfCountInt'.
     */
    public void setCfCountInt(final int cfCountInt) {
        this.cfCountInt = cfCountInt;
        this.hascfCountInt = true;
    }

    /**
     * Sets the value of field 'cfCountIntChange'.
     * 
     * @param cfCountIntChange the value of field 'cfCountIntChange'
     */
    public void setCfCountIntChange(final int cfCountIntChange) {
        this.cfCountIntChange = cfCountIntChange;
        this.hascfCountIntChange = true;
    }

    /**
     * Sets the value of field 'cfDateTime'.
     * 
     * @param cfDateTime the value of field 'cfDateTime'.
     */
    public void setCfDateTime(final java.util.Date cfDateTime) {
        this.cfDateTime = cfDateTime;
    }

    /**
     * Sets the value of field 'cfMeasId'.
     * 
     * @param cfMeasId the value of field 'cfMeasId'.
     */
    public void setCfMeasId(final java.lang.String cfMeasId) {
        this.cfMeasId = cfMeasId;
    }

    /**
     * Sets the value of field 'cfMeas__TypeChoice'.
     * 
     * @param cfMeas__TypeChoice the value of field
     * 'cfMeas__TypeChoice'.
     */
    public void setCfMeas__TypeChoice(final gr.ekt.cerif.schema.CfMeas__TypeChoice cfMeas__TypeChoice) {
        this.cfMeas__TypeChoice = cfMeas__TypeChoice;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Sets the value of field 'cfValFloatP'.
     * 
     * @param cfValFloatP the value of field 'cfValFloatP'.
     */
    public void setCfValFloatP(final float cfValFloatP) {
        this.cfValFloatP = cfValFloatP;
        this.hascfValFloatP = true;
    }

    /**
     * Sets the value of field 'cfValJudgeNum'.
     * 
     * @param cfValJudgeNum the value of field 'cfValJudgeNum'.
     */
    public void setCfValJudgeNum(final float cfValJudgeNum) {
        this.cfValJudgeNum = cfValJudgeNum;
        this.hascfValJudgeNum = true;
    }

    /**
     * Sets the value of field 'cfValJudgeNumChange'.
     * 
     * @param cfValJudgeNumChange the value of field
     * 'cfValJudgeNumChange'.
     */
    public void setCfValJudgeNumChange(final float cfValJudgeNumChange) {
        this.cfValJudgeNumChange = cfValJudgeNumChange;
        this.hascfValJudgeNumChange = true;
    }

    /**
     * Sets the value of field 'cfValJudgeText'.
     * 
     * @param cfValJudgeText the value of field 'cfValJudgeText'.
     */
    public void setCfValJudgeText(final java.lang.String cfValJudgeText) {
        this.cfValJudgeText = cfValJudgeText;
    }

    /**
     * Sets the value of field 'cfValJudgeTextChange'.
     * 
     * @param cfValJudgeTextChange the value of field
     * 'cfValJudgeTextChange'.
     */
    public void setCfValJudgeTextChange(final java.lang.String cfValJudgeTextChange) {
        this.cfValJudgeTextChange = cfValJudgeTextChange;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfMeas__Type
     */
    public static gr.ekt.cerif.schema.CfMeas__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfMeas__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfMeas__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfMeas__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfMeas__TypeCfIndic_Meas cfIndic_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfMeas_Class cfMeas_Class;

    private gr.ekt.cerif.schema.CfMeas__TypeCfPers_Meas cfPers_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfOrgUnit_Meas cfOrgUnit_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfProj_Meas cfProj_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfResPubl_Meas cfResPubl_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfResPat_Meas cfResPat_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfResProd_Meas cfResProd_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfFacil_Meas cfFacil_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfSrv_Meas cfSrv_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfEquip_Meas cfEquip_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfEvent_Meas cfEvent_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfMedium_Meas cfMedium_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfMeas_Meas cfMeas_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfFund_Meas cfFund_Meas;

    private gr.ekt.cerif.schema.CfMeas__TypeCfGeoBBox_Meas cfGeoBBox_Meas;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfMeas__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfEquip_Meas'.
     * 
     * @return the value of field 'CfEquip_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfEquip_Meas getCfEquip_Meas() {
        return this.cfEquip_Meas;
    }

    /**
     * Returns the value of field 'cfEvent_Meas'.
     * 
     * @return the value of field 'CfEvent_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfEvent_Meas getCfEvent_Meas() {
        return this.cfEvent_Meas;
    }

    /**
     * Returns the value of field 'cfFacil_Meas'.
     * 
     * @return the value of field 'CfFacil_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfFacil_Meas getCfFacil_Meas() {
        return this.cfFacil_Meas;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfFund_Meas'.
     * 
     * @return the value of field 'CfFund_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfFund_Meas getCfFund_Meas() {
        return this.cfFund_Meas;
    }

    /**
     * Returns the value of field 'cfGeoBBox_Meas'.
     * 
     * @return the value of field 'CfGeoBBox_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfGeoBBox_Meas getCfGeoBBox_Meas() {
        return this.cfGeoBBox_Meas;
    }

    /**
     * Returns the value of field 'cfIndic_Meas'.
     * 
     * @return the value of field 'CfIndic_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfIndic_Meas getCfIndic_Meas() {
        return this.cfIndic_Meas;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfMeas_Class'.
     * 
     * @return the value of field 'CfMeas_Class'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfMeas_Class getCfMeas_Class() {
        return this.cfMeas_Class;
    }

    /**
     * Returns the value of field 'cfMeas_Meas'.
     * 
     * @return the value of field 'CfMeas_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfMeas_Meas getCfMeas_Meas() {
        return this.cfMeas_Meas;
    }

    /**
     * Returns the value of field 'cfMedium_Meas'.
     * 
     * @return the value of field 'CfMedium_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfMedium_Meas getCfMedium_Meas() {
        return this.cfMedium_Meas;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Meas'.
     * 
     * @return the value of field 'CfOrgUnit_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfOrgUnit_Meas getCfOrgUnit_Meas() {
        return this.cfOrgUnit_Meas;
    }

    /**
     * Returns the value of field 'cfPers_Meas'.
     * 
     * @return the value of field 'CfPers_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfPers_Meas getCfPers_Meas() {
        return this.cfPers_Meas;
    }

    /**
     * Returns the value of field 'cfProj_Meas'.
     * 
     * @return the value of field 'CfProj_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfProj_Meas getCfProj_Meas() {
        return this.cfProj_Meas;
    }

    /**
     * Returns the value of field 'cfResPat_Meas'.
     * 
     * @return the value of field 'CfResPat_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfResPat_Meas getCfResPat_Meas() {
        return this.cfResPat_Meas;
    }

    /**
     * Returns the value of field 'cfResProd_Meas'.
     * 
     * @return the value of field 'CfResProd_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfResProd_Meas getCfResProd_Meas() {
        return this.cfResProd_Meas;
    }

    /**
     * Returns the value of field 'cfResPubl_Meas'.
     * 
     * @return the value of field 'CfResPubl_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfResPubl_Meas getCfResPubl_Meas() {
        return this.cfResPubl_Meas;
    }

    /**
     * Returns the value of field 'cfSrv_Meas'.
     * 
     * @return the value of field 'CfSrv_Meas'.
     */
    public gr.ekt.cerif.schema.CfMeas__TypeCfSrv_Meas getCfSrv_Meas() {
        return this.cfSrv_Meas;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfEquip_Meas'.
     * 
     * @param cfEquip_Meas the value of field 'cfEquip_Meas'.
     */
    public void setCfEquip_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfEquip_Meas cfEquip_Meas) {
        this.cfEquip_Meas = cfEquip_Meas;
    }

    /**
     * Sets the value of field 'cfEvent_Meas'.
     * 
     * @param cfEvent_Meas the value of field 'cfEvent_Meas'.
     */
    public void setCfEvent_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfEvent_Meas cfEvent_Meas) {
        this.cfEvent_Meas = cfEvent_Meas;
    }

    /**
     * Sets the value of field 'cfFacil_Meas'.
     * 
     * @param cfFacil_Meas the value of field 'cfFacil_Meas'.
     */
    public void setCfFacil_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfFacil_Meas cfFacil_Meas) {
        this.cfFacil_Meas = cfFacil_Meas;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfFund_Meas'.
     * 
     * @param cfFund_Meas the value of field 'cfFund_Meas'.
     */
    public void setCfFund_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfFund_Meas cfFund_Meas) {
        this.cfFund_Meas = cfFund_Meas;
    }

    /**
     * Sets the value of field 'cfGeoBBox_Meas'.
     * 
     * @param cfGeoBBox_Meas the value of field 'cfGeoBBox_Meas'.
     */
    public void setCfGeoBBox_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfGeoBBox_Meas cfGeoBBox_Meas) {
        this.cfGeoBBox_Meas = cfGeoBBox_Meas;
    }

    /**
     * Sets the value of field 'cfIndic_Meas'.
     * 
     * @param cfIndic_Meas the value of field 'cfIndic_Meas'.
     */
    public void setCfIndic_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfIndic_Meas cfIndic_Meas) {
        this.cfIndic_Meas = cfIndic_Meas;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfMeas_Class'.
     * 
     * @param cfMeas_Class the value of field 'cfMeas_Class'.
     */
    public void setCfMeas_Class(final gr.ekt.cerif.schema.CfMeas__TypeCfMeas_Class cfMeas_Class) {
        this.cfMeas_Class = cfMeas_Class;
    }

    /**
     * Sets the value of field 'cfMeas_Meas'.
     * 
     * @param cfMeas_Meas the value of field 'cfMeas_Meas'.
     */
    public void setCfMeas_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfMeas_Meas cfMeas_Meas) {
        this.cfMeas_Meas = cfMeas_Meas;
    }

    /**
     * Sets the value of field 'cfMedium_Meas'.
     * 
     * @param cfMedium_Meas the value of field 'cfMedium_Meas'.
     */
    public void setCfMedium_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfMedium_Meas cfMedium_Meas) {
        this.cfMedium_Meas = cfMedium_Meas;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Meas'.
     * 
     * @param cfOrgUnit_Meas the value of field 'cfOrgUnit_Meas'.
     */
    public void setCfOrgUnit_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfOrgUnit_Meas cfOrgUnit_Meas) {
        this.cfOrgUnit_Meas = cfOrgUnit_Meas;
    }

    /**
     * Sets the value of field 'cfPers_Meas'.
     * 
     * @param cfPers_Meas the value of field 'cfPers_Meas'.
     */
    public void setCfPers_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfPers_Meas cfPers_Meas) {
        this.cfPers_Meas = cfPers_Meas;
    }

    /**
     * Sets the value of field 'cfProj_Meas'.
     * 
     * @param cfProj_Meas the value of field 'cfProj_Meas'.
     */
    public void setCfProj_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfProj_Meas cfProj_Meas) {
        this.cfProj_Meas = cfProj_Meas;
    }

    /**
     * Sets the value of field 'cfResPat_Meas'.
     * 
     * @param cfResPat_Meas the value of field 'cfResPat_Meas'.
     */
    public void setCfResPat_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfResPat_Meas cfResPat_Meas) {
        this.cfResPat_Meas = cfResPat_Meas;
    }

    /**
     * Sets the value of field 'cfResProd_Meas'.
     * 
     * @param cfResProd_Meas the value of field 'cfResProd_Meas'.
     */
    public void setCfResProd_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfResProd_Meas cfResProd_Meas) {
        this.cfResProd_Meas = cfResProd_Meas;
    }

    /**
     * Sets the value of field 'cfResPubl_Meas'.
     * 
     * @param cfResPubl_Meas the value of field 'cfResPubl_Meas'.
     */
    public void setCfResPubl_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfResPubl_Meas cfResPubl_Meas) {
        this.cfResPubl_Meas = cfResPubl_Meas;
    }

    /**
     * Sets the value of field 'cfSrv_Meas'.
     * 
     * @param cfSrv_Meas the value of field 'cfSrv_Meas'.
     */
    public void setCfSrv_Meas(final gr.ekt.cerif.schema.CfMeas__TypeCfSrv_Meas cfSrv_Meas) {
        this.cfSrv_Meas = cfSrv_Meas;
    }

}

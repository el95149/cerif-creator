/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfMedium_MediumChoice implements java.io.Serializable {

    private java.lang.String cfMediumId2;

    private java.lang.String cfMediumId1;

    public CfMedium_MediumChoice() {
        super();
    }

    /**
     * Returns the value of field 'cfMediumId1'.
     * 
     * @return the value of field 'CfMediumId1'.
     */
    public java.lang.String getCfMediumId1() {
        return this.cfMediumId1;
    }

    /**
     * Returns the value of field 'cfMediumId2'.
     * 
     * @return the value of field 'CfMediumId2'.
     */
    public java.lang.String getCfMediumId2() {
        return this.cfMediumId2;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfMediumId1'.
     * 
     * @param cfMediumId1 the value of field 'cfMediumId1'.
     */
    public void setCfMediumId1(final java.lang.String cfMediumId1) {
        this.cfMediumId1 = cfMediumId1;
    }

    /**
     * Sets the value of field 'cfMediumId2'.
     * 
     * @param cfMediumId2 the value of field 'cfMediumId2'.
     */
    public void setCfMediumId2(final java.lang.String cfMediumId2) {
        this.cfMediumId2 = cfMediumId2;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfMedium_MediumChoice
     */
    public static gr.ekt.cerif.schema.CfMedium_MediumChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfMedium_MediumChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfMedium_MediumChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

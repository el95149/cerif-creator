/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfMedium__Type implements java.io.Serializable {

    private java.lang.String cfMediumId;

    private java.lang.String cfMediumCreationDate;

    private float cfSize;

    /**
     * Keeps track of whether primitive field cfSize has been set
     * already.
     */
    private boolean hascfSize;

    private java.lang.String cfMimeType;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfMedium__TypeChoice cfMedium__TypeChoice;

    public CfMedium__Type() {
        super();
    }

    /**
     */
    public void deleteCfSize() {
        this.hascfSize= false;
    }

    /**
     * Returns the value of field 'cfMediumCreationDate'.
     * 
     * @return the value of field 'CfMediumCreationDate'.
     */
    public java.lang.String getCfMediumCreationDate() {
        return this.cfMediumCreationDate;
    }

    /**
     * Returns the value of field 'cfMediumId'.
     * 
     * @return the value of field 'CfMediumId'.
     */
    public java.lang.String getCfMediumId() {
        return this.cfMediumId;
    }

    /**
     * Returns the value of field 'cfMedium__TypeChoice'.
     * 
     * @return the value of field 'CfMedium__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeChoice getCfMedium__TypeChoice() {
        return this.cfMedium__TypeChoice;
    }

    /**
     * Returns the value of field 'cfMimeType'.
     * 
     * @return the value of field 'CfMimeType'.
     */
    public java.lang.String getCfMimeType() {
        return this.cfMimeType;
    }

    /**
     * Returns the value of field 'cfSize'.
     * 
     * @return the value of field 'CfSize'.
     */
    public float getCfSize() {
        return this.cfSize;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method hasCfSize.
     * 
     * @return true if at least one CfSize has been added
     */
    public boolean hasCfSize() {
        return this.hascfSize;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfMediumCreationDate'.
     * 
     * @param cfMediumCreationDate the value of field
     * 'cfMediumCreationDate'.
     */
    public void setCfMediumCreationDate(final java.lang.String cfMediumCreationDate) {
        this.cfMediumCreationDate = cfMediumCreationDate;
    }

    /**
     * Sets the value of field 'cfMediumId'.
     * 
     * @param cfMediumId the value of field 'cfMediumId'.
     */
    public void setCfMediumId(final java.lang.String cfMediumId) {
        this.cfMediumId = cfMediumId;
    }

    /**
     * Sets the value of field 'cfMedium__TypeChoice'.
     * 
     * @param cfMedium__TypeChoice the value of field
     * 'cfMedium__TypeChoice'.
     */
    public void setCfMedium__TypeChoice(final gr.ekt.cerif.schema.CfMedium__TypeChoice cfMedium__TypeChoice) {
        this.cfMedium__TypeChoice = cfMedium__TypeChoice;
    }

    /**
     * Sets the value of field 'cfMimeType'.
     * 
     * @param cfMimeType the value of field 'cfMimeType'.
     */
    public void setCfMimeType(final java.lang.String cfMimeType) {
        this.cfMimeType = cfMimeType;
    }

    /**
     * Sets the value of field 'cfSize'.
     * 
     * @param cfSize the value of field 'cfSize'.
     */
    public void setCfSize(final float cfSize) {
        this.cfSize = cfSize;
        this.hascfSize = true;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfMedium__Type
     */
    public static gr.ekt.cerif.schema.CfMedium__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfMedium__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfMedium__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfMedium__TypeChoice implements java.io.Serializable {

    private java.util.Vector<gr.ekt.cerif.schema.CfMedium__TypeChoiceItem> _items;

    public CfMedium__TypeChoice() {
        super();
        this._items = new java.util.Vector<gr.ekt.cerif.schema.CfMedium__TypeChoiceItem>();
    }

    /**
     * 
     * 
     * @param vCfMedium__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfMedium__TypeChoiceItem(final gr.ekt.cerif.schema.CfMedium__TypeChoiceItem vCfMedium__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.addElement(vCfMedium__TypeChoiceItem);
    }

    /**
     * 
     * 
     * @param index
     * @param vCfMedium__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfMedium__TypeChoiceItem(final int index,final gr.ekt.cerif.schema.CfMedium__TypeChoiceItem vCfMedium__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.add(index, vCfMedium__TypeChoiceItem);
    }

    /**
     * Method enumerateCfMedium__TypeChoiceItem.
     * 
     * @return an Enumeration over all
     * gr.ekt.cerif.schema.CfMedium__TypeChoiceItem elements
     */
    public java.util.Enumeration<? extends gr.ekt.cerif.schema.CfMedium__TypeChoiceItem> enumerateCfMedium__TypeChoiceItem() {
        return this._items.elements();
    }

    /**
     * Method getCfMedium__TypeChoiceItem.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * gr.ekt.cerif.schema.CfMedium__TypeChoiceItem at the given
     * index
     */
    public gr.ekt.cerif.schema.CfMedium__TypeChoiceItem getCfMedium__TypeChoiceItem(final int index) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("getCfMedium__TypeChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        return _items.get(index);
    }

    /**
     * Method getCfMedium__TypeChoiceItem.Returns the contents of
     * the collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public gr.ekt.cerif.schema.CfMedium__TypeChoiceItem[] getCfMedium__TypeChoiceItem() {
        gr.ekt.cerif.schema.CfMedium__TypeChoiceItem[] array = new gr.ekt.cerif.schema.CfMedium__TypeChoiceItem[0];
        return this._items.toArray(array);
    }

    /**
     * Method getCfMedium__TypeChoiceItemCount.
     * 
     * @return the size of this collection
     */
    public int getCfMedium__TypeChoiceItemCount() {
        return this._items.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllCfMedium__TypeChoiceItem() {
        this._items.clear();
    }

    /**
     * Method removeCfMedium__TypeChoiceItem.
     * 
     * @param vCfMedium__TypeChoiceItem
     * @return true if the object was removed from the collection.
     */
    public boolean removeCfMedium__TypeChoiceItem(final gr.ekt.cerif.schema.CfMedium__TypeChoiceItem vCfMedium__TypeChoiceItem) {
        boolean removed = _items.remove(vCfMedium__TypeChoiceItem);
        return removed;
    }

    /**
     * Method removeCfMedium__TypeChoiceItemAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public gr.ekt.cerif.schema.CfMedium__TypeChoiceItem removeCfMedium__TypeChoiceItemAt(final int index) {
        java.lang.Object obj = this._items.remove(index);
        return (gr.ekt.cerif.schema.CfMedium__TypeChoiceItem) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vCfMedium__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setCfMedium__TypeChoiceItem(final int index,final gr.ekt.cerif.schema.CfMedium__TypeChoiceItem vCfMedium__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("setCfMedium__TypeChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        this._items.set(index, vCfMedium__TypeChoiceItem);
    }

    /**
     * 
     * 
     * @param vCfMedium__TypeChoiceItemArray
     */
    public void setCfMedium__TypeChoiceItem(final gr.ekt.cerif.schema.CfMedium__TypeChoiceItem[] vCfMedium__TypeChoiceItemArray) {
        //-- copy array
        _items.clear();

        for (int i = 0; i < vCfMedium__TypeChoiceItemArray.length; i++) {
                this._items.add(vCfMedium__TypeChoiceItemArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfMedium__TypeChoice
     */
    public static gr.ekt.cerif.schema.CfMedium__TypeChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfMedium__TypeChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfMedium__TypeChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

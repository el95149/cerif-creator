/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfMedium__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfTitle cfTitle;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfMedium__TypeCfResPubl_Medium cfResPubl_Medium;

    private gr.ekt.cerif.schema.CfMedium__TypeCfResPat_Medium cfResPat_Medium;

    private gr.ekt.cerif.schema.CfMedium__TypeCfResProd_Medium cfResProd_Medium;

    private gr.ekt.cerif.schema.CfMedium__TypeCfFacil_Medium cfFacil_Medium;

    private gr.ekt.cerif.schema.CfMedium__TypeCfEquip_Medium cfEquip_Medium;

    private gr.ekt.cerif.schema.CfMedium__TypeCfSrv_Medium cfSrv_Medium;

    private gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Medium cfMedium_Medium;

    private gr.ekt.cerif.schema.CfMedium__TypeCfProj_Medium cfProj_Medium;

    private gr.ekt.cerif.schema.CfMedium__TypeCfPers_Medium cfPers_Medium;

    private gr.ekt.cerif.schema.CfMedium__TypeCfOrgUnit_Medium cfOrgUnit_Medium;

    private gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Class cfMedium_Class;

    private gr.ekt.cerif.schema.CfMedium__TypeCfEvent_Medium cfEvent_Medium;

    private gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Fund cfMedium_Fund;

    private gr.ekt.cerif.schema.CfMedium__TypeCfCite_Medium cfCite_Medium;

    private gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Indic cfMedium_Indic;

    private gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Meas cfMedium_Meas;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfMedium__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfCite_Medium'.
     * 
     * @return the value of field 'CfCite_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfCite_Medium getCfCite_Medium() {
        return this.cfCite_Medium;
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfEquip_Medium'.
     * 
     * @return the value of field 'CfEquip_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfEquip_Medium getCfEquip_Medium() {
        return this.cfEquip_Medium;
    }

    /**
     * Returns the value of field 'cfEvent_Medium'.
     * 
     * @return the value of field 'CfEvent_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfEvent_Medium getCfEvent_Medium() {
        return this.cfEvent_Medium;
    }

    /**
     * Returns the value of field 'cfFacil_Medium'.
     * 
     * @return the value of field 'CfFacil_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfFacil_Medium getCfFacil_Medium() {
        return this.cfFacil_Medium;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfMedium_Class'.
     * 
     * @return the value of field 'CfMedium_Class'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Class getCfMedium_Class() {
        return this.cfMedium_Class;
    }

    /**
     * Returns the value of field 'cfMedium_Fund'.
     * 
     * @return the value of field 'CfMedium_Fund'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Fund getCfMedium_Fund() {
        return this.cfMedium_Fund;
    }

    /**
     * Returns the value of field 'cfMedium_Indic'.
     * 
     * @return the value of field 'CfMedium_Indic'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Indic getCfMedium_Indic() {
        return this.cfMedium_Indic;
    }

    /**
     * Returns the value of field 'cfMedium_Meas'.
     * 
     * @return the value of field 'CfMedium_Meas'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Meas getCfMedium_Meas() {
        return this.cfMedium_Meas;
    }

    /**
     * Returns the value of field 'cfMedium_Medium'.
     * 
     * @return the value of field 'CfMedium_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Medium getCfMedium_Medium() {
        return this.cfMedium_Medium;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Medium'.
     * 
     * @return the value of field 'CfOrgUnit_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfOrgUnit_Medium getCfOrgUnit_Medium() {
        return this.cfOrgUnit_Medium;
    }

    /**
     * Returns the value of field 'cfPers_Medium'.
     * 
     * @return the value of field 'CfPers_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfPers_Medium getCfPers_Medium() {
        return this.cfPers_Medium;
    }

    /**
     * Returns the value of field 'cfProj_Medium'.
     * 
     * @return the value of field 'CfProj_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfProj_Medium getCfProj_Medium() {
        return this.cfProj_Medium;
    }

    /**
     * Returns the value of field 'cfResPat_Medium'.
     * 
     * @return the value of field 'CfResPat_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfResPat_Medium getCfResPat_Medium() {
        return this.cfResPat_Medium;
    }

    /**
     * Returns the value of field 'cfResProd_Medium'.
     * 
     * @return the value of field 'CfResProd_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfResProd_Medium getCfResProd_Medium() {
        return this.cfResProd_Medium;
    }

    /**
     * Returns the value of field 'cfResPubl_Medium'.
     * 
     * @return the value of field 'CfResPubl_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfResPubl_Medium getCfResPubl_Medium() {
        return this.cfResPubl_Medium;
    }

    /**
     * Returns the value of field 'cfSrv_Medium'.
     * 
     * @return the value of field 'CfSrv_Medium'.
     */
    public gr.ekt.cerif.schema.CfMedium__TypeCfSrv_Medium getCfSrv_Medium() {
        return this.cfSrv_Medium;
    }

    /**
     * Returns the value of field 'cfTitle'.
     * 
     * @return the value of field 'CfTitle'.
     */
    public gr.ekt.cerif.schema.CfTitle getCfTitle() {
        return this.cfTitle;
    }

    /**
     * Sets the value of field 'cfCite_Medium'.
     * 
     * @param cfCite_Medium the value of field 'cfCite_Medium'.
     */
    public void setCfCite_Medium(final gr.ekt.cerif.schema.CfMedium__TypeCfCite_Medium cfCite_Medium) {
        this.cfCite_Medium = cfCite_Medium;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfEquip_Medium'.
     * 
     * @param cfEquip_Medium the value of field 'cfEquip_Medium'.
     */
    public void setCfEquip_Medium(final gr.ekt.cerif.schema.CfMedium__TypeCfEquip_Medium cfEquip_Medium) {
        this.cfEquip_Medium = cfEquip_Medium;
    }

    /**
     * Sets the value of field 'cfEvent_Medium'.
     * 
     * @param cfEvent_Medium the value of field 'cfEvent_Medium'.
     */
    public void setCfEvent_Medium(final gr.ekt.cerif.schema.CfMedium__TypeCfEvent_Medium cfEvent_Medium) {
        this.cfEvent_Medium = cfEvent_Medium;
    }

    /**
     * Sets the value of field 'cfFacil_Medium'.
     * 
     * @param cfFacil_Medium the value of field 'cfFacil_Medium'.
     */
    public void setCfFacil_Medium(final gr.ekt.cerif.schema.CfMedium__TypeCfFacil_Medium cfFacil_Medium) {
        this.cfFacil_Medium = cfFacil_Medium;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfMedium_Class'.
     * 
     * @param cfMedium_Class the value of field 'cfMedium_Class'.
     */
    public void setCfMedium_Class(final gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Class cfMedium_Class) {
        this.cfMedium_Class = cfMedium_Class;
    }

    /**
     * Sets the value of field 'cfMedium_Fund'.
     * 
     * @param cfMedium_Fund the value of field 'cfMedium_Fund'.
     */
    public void setCfMedium_Fund(final gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Fund cfMedium_Fund) {
        this.cfMedium_Fund = cfMedium_Fund;
    }

    /**
     * Sets the value of field 'cfMedium_Indic'.
     * 
     * @param cfMedium_Indic the value of field 'cfMedium_Indic'.
     */
    public void setCfMedium_Indic(final gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Indic cfMedium_Indic) {
        this.cfMedium_Indic = cfMedium_Indic;
    }

    /**
     * Sets the value of field 'cfMedium_Meas'.
     * 
     * @param cfMedium_Meas the value of field 'cfMedium_Meas'.
     */
    public void setCfMedium_Meas(final gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Meas cfMedium_Meas) {
        this.cfMedium_Meas = cfMedium_Meas;
    }

    /**
     * Sets the value of field 'cfMedium_Medium'.
     * 
     * @param cfMedium_Medium the value of field 'cfMedium_Medium'.
     */
    public void setCfMedium_Medium(final gr.ekt.cerif.schema.CfMedium__TypeCfMedium_Medium cfMedium_Medium) {
        this.cfMedium_Medium = cfMedium_Medium;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Medium'.
     * 
     * @param cfOrgUnit_Medium the value of field 'cfOrgUnit_Medium'
     */
    public void setCfOrgUnit_Medium(final gr.ekt.cerif.schema.CfMedium__TypeCfOrgUnit_Medium cfOrgUnit_Medium) {
        this.cfOrgUnit_Medium = cfOrgUnit_Medium;
    }

    /**
     * Sets the value of field 'cfPers_Medium'.
     * 
     * @param cfPers_Medium the value of field 'cfPers_Medium'.
     */
    public void setCfPers_Medium(final gr.ekt.cerif.schema.CfMedium__TypeCfPers_Medium cfPers_Medium) {
        this.cfPers_Medium = cfPers_Medium;
    }

    /**
     * Sets the value of field 'cfProj_Medium'.
     * 
     * @param cfProj_Medium the value of field 'cfProj_Medium'.
     */
    public void setCfProj_Medium(final gr.ekt.cerif.schema.CfMedium__TypeCfProj_Medium cfProj_Medium) {
        this.cfProj_Medium = cfProj_Medium;
    }

    /**
     * Sets the value of field 'cfResPat_Medium'.
     * 
     * @param cfResPat_Medium the value of field 'cfResPat_Medium'.
     */
    public void setCfResPat_Medium(final gr.ekt.cerif.schema.CfMedium__TypeCfResPat_Medium cfResPat_Medium) {
        this.cfResPat_Medium = cfResPat_Medium;
    }

    /**
     * Sets the value of field 'cfResProd_Medium'.
     * 
     * @param cfResProd_Medium the value of field 'cfResProd_Medium'
     */
    public void setCfResProd_Medium(final gr.ekt.cerif.schema.CfMedium__TypeCfResProd_Medium cfResProd_Medium) {
        this.cfResProd_Medium = cfResProd_Medium;
    }

    /**
     * Sets the value of field 'cfResPubl_Medium'.
     * 
     * @param cfResPubl_Medium the value of field 'cfResPubl_Medium'
     */
    public void setCfResPubl_Medium(final gr.ekt.cerif.schema.CfMedium__TypeCfResPubl_Medium cfResPubl_Medium) {
        this.cfResPubl_Medium = cfResPubl_Medium;
    }

    /**
     * Sets the value of field 'cfSrv_Medium'.
     * 
     * @param cfSrv_Medium the value of field 'cfSrv_Medium'.
     */
    public void setCfSrv_Medium(final gr.ekt.cerif.schema.CfMedium__TypeCfSrv_Medium cfSrv_Medium) {
        this.cfSrv_Medium = cfSrv_Medium;
    }

    /**
     * Sets the value of field 'cfTitle'.
     * 
     * @param cfTitle the value of field 'cfTitle'.
     */
    public void setCfTitle(final gr.ekt.cerif.schema.CfTitle cfTitle) {
        this.cfTitle = cfTitle;
    }

}

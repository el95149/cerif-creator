/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfMetrics__TypeCfResPubl_Metrics implements java.io.Serializable {

    private java.lang.String cfResPublId;

    private gr.ekt.cerif.schema.CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group;

    private int cfYear;

    /**
     * Keeps track of whether primitive field cfYear has been set
     * already.
     */
    private boolean hascfYear;

    private float cfCount;

    /**
     * Keeps track of whether primitive field cfCount has been set
     * already.
     */
    private boolean hascfCount;

    public CfMetrics__TypeCfResPubl_Metrics() {
        super();
    }

    /**
     */
    public void deleteCfCount() {
        this.hascfCount= false;
    }

    /**
     */
    public void deleteCfYear() {
        this.hascfYear= false;
    }

    /**
     * Returns the value of field 'cfCoreClassWithFraction__Group'.
     * 
     * @return the value of field 'CfCoreClassWithFraction__Group'.
     */
    public gr.ekt.cerif.schema.CfCoreClassWithFraction__Group getCfCoreClassWithFraction__Group() {
        return this.cfCoreClassWithFraction__Group;
    }

    /**
     * Returns the value of field 'cfCount'.
     * 
     * @return the value of field 'CfCount'.
     */
    public float getCfCount() {
        return this.cfCount;
    }

    /**
     * Returns the value of field 'cfResPublId'.
     * 
     * @return the value of field 'CfResPublId'.
     */
    public java.lang.String getCfResPublId() {
        return this.cfResPublId;
    }

    /**
     * Returns the value of field 'cfYear'.
     * 
     * @return the value of field 'CfYear'.
     */
    public int getCfYear() {
        return this.cfYear;
    }

    /**
     * Method hasCfCount.
     * 
     * @return true if at least one CfCount has been added
     */
    public boolean hasCfCount() {
        return this.hascfCount;
    }

    /**
     * Method hasCfYear.
     * 
     * @return true if at least one CfYear has been added
     */
    public boolean hasCfYear() {
        return this.hascfYear;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCoreClassWithFraction__Group'.
     * 
     * @param cfCoreClassWithFraction__Group the value of field
     * 'cfCoreClassWithFraction__Group'.
     */
    public void setCfCoreClassWithFraction__Group(final gr.ekt.cerif.schema.CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group) {
        this.cfCoreClassWithFraction__Group = cfCoreClassWithFraction__Group;
    }

    /**
     * Sets the value of field 'cfCount'.
     * 
     * @param cfCount the value of field 'cfCount'.
     */
    public void setCfCount(final float cfCount) {
        this.cfCount = cfCount;
        this.hascfCount = true;
    }

    /**
     * Sets the value of field 'cfResPublId'.
     * 
     * @param cfResPublId the value of field 'cfResPublId'.
     */
    public void setCfResPublId(final java.lang.String cfResPublId) {
        this.cfResPublId = cfResPublId;
    }

    /**
     * Sets the value of field 'cfYear'.
     * 
     * @param cfYear the value of field 'cfYear'.
     */
    public void setCfYear(final int cfYear) {
        this.cfYear = cfYear;
        this.hascfYear = true;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfMetrics__TypeCfResPubl_Metrics
     */
    public static gr.ekt.cerif.schema.CfMetrics__TypeCfResPubl_Metrics unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfMetrics__TypeCfResPubl_Metrics) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfMetrics__TypeCfResPubl_Metrics.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfMetrics__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfMetrics__TypeCfMetrics_Class cfMetrics_Class;

    private gr.ekt.cerif.schema.CfMetrics__TypeCfResPubl_Metrics cfResPubl_Metrics;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfMetrics__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfMetrics_Class'.
     * 
     * @return the value of field 'CfMetrics_Class'.
     */
    public gr.ekt.cerif.schema.CfMetrics__TypeCfMetrics_Class getCfMetrics_Class() {
        return this.cfMetrics_Class;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfResPubl_Metrics'.
     * 
     * @return the value of field 'CfResPubl_Metrics'.
     */
    public gr.ekt.cerif.schema.CfMetrics__TypeCfResPubl_Metrics getCfResPubl_Metrics() {
        return this.cfResPubl_Metrics;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfMetrics_Class'.
     * 
     * @param cfMetrics_Class the value of field 'cfMetrics_Class'.
     */
    public void setCfMetrics_Class(final gr.ekt.cerif.schema.CfMetrics__TypeCfMetrics_Class cfMetrics_Class) {
        this.cfMetrics_Class = cfMetrics_Class;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfResPubl_Metrics'.
     * 
     * @param cfResPubl_Metrics the value of field
     * 'cfResPubl_Metrics'.
     */
    public void setCfResPubl_Metrics(final gr.ekt.cerif.schema.CfMetrics__TypeCfResPubl_Metrics cfResPubl_Metrics) {
        this.cfResPubl_Metrics = cfResPubl_Metrics;
    }

}

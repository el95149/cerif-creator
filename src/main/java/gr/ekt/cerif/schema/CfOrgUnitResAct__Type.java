/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfOrgUnitResAct__Type implements java.io.Serializable {

    private java.lang.String cfOrgUnitId;

    private gr.ekt.cerif.schema.CfResAct cfResAct;

    public CfOrgUnitResAct__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfOrgUnitId'.
     * 
     * @return the value of field 'CfOrgUnitId'.
     */
    public java.lang.String getCfOrgUnitId() {
        return this.cfOrgUnitId;
    }

    /**
     * Returns the value of field 'cfResAct'.
     * 
     * @return the value of field 'CfResAct'.
     */
    public gr.ekt.cerif.schema.CfResAct getCfResAct() {
        return this.cfResAct;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfOrgUnitId'.
     * 
     * @param cfOrgUnitId the value of field 'cfOrgUnitId'.
     */
    public void setCfOrgUnitId(final java.lang.String cfOrgUnitId) {
        this.cfOrgUnitId = cfOrgUnitId;
    }

    /**
     * Sets the value of field 'cfResAct'.
     * 
     * @param cfResAct the value of field 'cfResAct'.
     */
    public void setCfResAct(final gr.ekt.cerif.schema.CfResAct cfResAct) {
        this.cfResAct = cfResAct;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfOrgUnitResAct__Type
     */
    public static gr.ekt.cerif.schema.CfOrgUnitResAct__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfOrgUnitResAct__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfOrgUnitResAct__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

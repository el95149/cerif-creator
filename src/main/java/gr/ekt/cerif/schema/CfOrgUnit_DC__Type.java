/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfOrgUnit_DC__Type implements java.io.Serializable {

    private java.lang.String cfOrgUnitId;

    private java.lang.String cfDCId;

    private java.lang.String cfDCScheme;

    private gr.ekt.cerif.schema.CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group;

    public CfOrgUnit_DC__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfCoreClassWithFraction__Group'.
     * 
     * @return the value of field 'CfCoreClassWithFraction__Group'.
     */
    public gr.ekt.cerif.schema.CfCoreClassWithFraction__Group getCfCoreClassWithFraction__Group() {
        return this.cfCoreClassWithFraction__Group;
    }

    /**
     * Returns the value of field 'cfDCId'.
     * 
     * @return the value of field 'CfDCId'.
     */
    public java.lang.String getCfDCId() {
        return this.cfDCId;
    }

    /**
     * Returns the value of field 'cfDCScheme'.
     * 
     * @return the value of field 'CfDCScheme'.
     */
    public java.lang.String getCfDCScheme() {
        return this.cfDCScheme;
    }

    /**
     * Returns the value of field 'cfOrgUnitId'.
     * 
     * @return the value of field 'CfOrgUnitId'.
     */
    public java.lang.String getCfOrgUnitId() {
        return this.cfOrgUnitId;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCoreClassWithFraction__Group'.
     * 
     * @param cfCoreClassWithFraction__Group the value of field
     * 'cfCoreClassWithFraction__Group'.
     */
    public void setCfCoreClassWithFraction__Group(final gr.ekt.cerif.schema.CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group) {
        this.cfCoreClassWithFraction__Group = cfCoreClassWithFraction__Group;
    }

    /**
     * Sets the value of field 'cfDCId'.
     * 
     * @param cfDCId the value of field 'cfDCId'.
     */
    public void setCfDCId(final java.lang.String cfDCId) {
        this.cfDCId = cfDCId;
    }

    /**
     * Sets the value of field 'cfDCScheme'.
     * 
     * @param cfDCScheme the value of field 'cfDCScheme'.
     */
    public void setCfDCScheme(final java.lang.String cfDCScheme) {
        this.cfDCScheme = cfDCScheme;
    }

    /**
     * Sets the value of field 'cfOrgUnitId'.
     * 
     * @param cfOrgUnitId the value of field 'cfOrgUnitId'.
     */
    public void setCfOrgUnitId(final java.lang.String cfOrgUnitId) {
        this.cfOrgUnitId = cfOrgUnitId;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfOrgUnit_DC__Typ
     */
    public static gr.ekt.cerif.schema.CfOrgUnit_DC__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfOrgUnit_DC__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfOrgUnit_DC__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

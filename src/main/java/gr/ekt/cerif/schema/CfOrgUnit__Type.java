/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfOrgUnit__Type implements java.io.Serializable {

    private java.lang.String cfOrgUnitId;

    private java.lang.String cfAcro;

    private int cfHeadcount;

    /**
     * Keeps track of whether primitive field cfHeadcount has been
     * set already.
     */
    private boolean hascfHeadcount;

    private gr.ekt.cerif.schema.CfTurn cfTurn;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeChoice cfOrgUnit__TypeChoice;

    public CfOrgUnit__Type() {
        super();
    }

    /**
     */
    public void deleteCfHeadcount() {
        this.hascfHeadcount= false;
    }

    /**
     * Returns the value of field 'cfAcro'.
     * 
     * @return the value of field 'CfAcro'.
     */
    public java.lang.String getCfAcro() {
        return this.cfAcro;
    }

    /**
     * Returns the value of field 'cfHeadcount'.
     * 
     * @return the value of field 'CfHeadcount'.
     */
    public int getCfHeadcount() {
        return this.cfHeadcount;
    }

    /**
     * Returns the value of field 'cfOrgUnitId'.
     * 
     * @return the value of field 'CfOrgUnitId'.
     */
    public java.lang.String getCfOrgUnitId() {
        return this.cfOrgUnitId;
    }

    /**
     * Returns the value of field 'cfOrgUnit__TypeChoice'.
     * 
     * @return the value of field 'CfOrgUnit__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeChoice getCfOrgUnit__TypeChoice() {
        return this.cfOrgUnit__TypeChoice;
    }

    /**
     * Returns the value of field 'cfTurn'.
     * 
     * @return the value of field 'CfTurn'.
     */
    public gr.ekt.cerif.schema.CfTurn getCfTurn() {
        return this.cfTurn;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method hasCfHeadcount.
     * 
     * @return true if at least one CfHeadcount has been added
     */
    public boolean hasCfHeadcount() {
        return this.hascfHeadcount;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfAcro'.
     * 
     * @param cfAcro the value of field 'cfAcro'.
     */
    public void setCfAcro(final java.lang.String cfAcro) {
        this.cfAcro = cfAcro;
    }

    /**
     * Sets the value of field 'cfHeadcount'.
     * 
     * @param cfHeadcount the value of field 'cfHeadcount'.
     */
    public void setCfHeadcount(final int cfHeadcount) {
        this.cfHeadcount = cfHeadcount;
        this.hascfHeadcount = true;
    }

    /**
     * Sets the value of field 'cfOrgUnitId'.
     * 
     * @param cfOrgUnitId the value of field 'cfOrgUnitId'.
     */
    public void setCfOrgUnitId(final java.lang.String cfOrgUnitId) {
        this.cfOrgUnitId = cfOrgUnitId;
    }

    /**
     * Sets the value of field 'cfOrgUnit__TypeChoice'.
     * 
     * @param cfOrgUnit__TypeChoice the value of field
     * 'cfOrgUnit__TypeChoice'.
     */
    public void setCfOrgUnit__TypeChoice(final gr.ekt.cerif.schema.CfOrgUnit__TypeChoice cfOrgUnit__TypeChoice) {
        this.cfOrgUnit__TypeChoice = cfOrgUnit__TypeChoice;
    }

    /**
     * Sets the value of field 'cfTurn'.
     * 
     * @param cfTurn the value of field 'cfTurn'.
     */
    public void setCfTurn(final gr.ekt.cerif.schema.CfTurn cfTurn) {
        this.cfTurn = cfTurn;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfOrgUnit__Type
     */
    public static gr.ekt.cerif.schema.CfOrgUnit__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfOrgUnit__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfOrgUnit__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfOrgUnit__TypeCfOrgUnit_Facil implements java.io.Serializable {

    private java.lang.String cfFacilId;

    private gr.ekt.cerif.schema.CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group;

    private java.lang.String cfAvailability;

    private java.lang.String cfConditions;

    private gr.ekt.cerif.schema.CfPrice cfPrice;

    public CfOrgUnit__TypeCfOrgUnit_Facil() {
        super();
    }

    /**
     * Returns the value of field 'cfAvailability'.
     * 
     * @return the value of field 'CfAvailability'.
     */
    public java.lang.String getCfAvailability() {
        return this.cfAvailability;
    }

    /**
     * Returns the value of field 'cfConditions'.
     * 
     * @return the value of field 'CfConditions'.
     */
    public java.lang.String getCfConditions() {
        return this.cfConditions;
    }

    /**
     * Returns the value of field 'cfCoreClassWithFraction__Group'.
     * 
     * @return the value of field 'CfCoreClassWithFraction__Group'.
     */
    public gr.ekt.cerif.schema.CfCoreClassWithFraction__Group getCfCoreClassWithFraction__Group() {
        return this.cfCoreClassWithFraction__Group;
    }

    /**
     * Returns the value of field 'cfFacilId'.
     * 
     * @return the value of field 'CfFacilId'.
     */
    public java.lang.String getCfFacilId() {
        return this.cfFacilId;
    }

    /**
     * Returns the value of field 'cfPrice'.
     * 
     * @return the value of field 'CfPrice'.
     */
    public gr.ekt.cerif.schema.CfPrice getCfPrice() {
        return this.cfPrice;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfAvailability'.
     * 
     * @param cfAvailability the value of field 'cfAvailability'.
     */
    public void setCfAvailability(final java.lang.String cfAvailability) {
        this.cfAvailability = cfAvailability;
    }

    /**
     * Sets the value of field 'cfConditions'.
     * 
     * @param cfConditions the value of field 'cfConditions'.
     */
    public void setCfConditions(final java.lang.String cfConditions) {
        this.cfConditions = cfConditions;
    }

    /**
     * Sets the value of field 'cfCoreClassWithFraction__Group'.
     * 
     * @param cfCoreClassWithFraction__Group the value of field
     * 'cfCoreClassWithFraction__Group'.
     */
    public void setCfCoreClassWithFraction__Group(final gr.ekt.cerif.schema.CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group) {
        this.cfCoreClassWithFraction__Group = cfCoreClassWithFraction__Group;
    }

    /**
     * Sets the value of field 'cfFacilId'.
     * 
     * @param cfFacilId the value of field 'cfFacilId'.
     */
    public void setCfFacilId(final java.lang.String cfFacilId) {
        this.cfFacilId = cfFacilId;
    }

    /**
     * Sets the value of field 'cfPrice'.
     * 
     * @param cfPrice the value of field 'cfPrice'.
     */
    public void setCfPrice(final gr.ekt.cerif.schema.CfPrice cfPrice) {
        this.cfPrice = cfPrice;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Facil
     */
    public static gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Facil unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Facil) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Facil.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

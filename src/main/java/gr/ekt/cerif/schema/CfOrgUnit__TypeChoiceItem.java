/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfOrgUnit__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfResAct cfResAct;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Class cfOrgUnit_Class;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Equip cfOrgUnit_Equip;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_EAddr cfOrgUnit_EAddr;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Event cfOrgUnit_Event;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_ExpSkills cfOrgUnit_ExpSkills;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Facil cfOrgUnit_Facil;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Fund cfOrgUnit_Fund;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_OrgUnit cfOrgUnit_OrgUnit;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Prize cfOrgUnit_Prize;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_ResPat cfOrgUnit_ResPat;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_ResProd cfOrgUnit_ResProd;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_ResPubl cfOrgUnit_ResPubl;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Srv cfOrgUnit_Srv;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfPers_OrgUnit cfPers_OrgUnit;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfProj_OrgUnit cfProj_OrgUnit;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_PAddr cfOrgUnit_PAddr;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_DC cfOrgUnit_DC;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Medium cfOrgUnit_Medium;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Meas cfOrgUnit_Meas;

    private gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Indic cfOrgUnit_Indic;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfOrgUnit__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Class'.
     * 
     * @return the value of field 'CfOrgUnit_Class'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Class getCfOrgUnit_Class() {
        return this.cfOrgUnit_Class;
    }

    /**
     * Returns the value of field 'cfOrgUnit_DC'.
     * 
     * @return the value of field 'CfOrgUnit_DC'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_DC getCfOrgUnit_DC() {
        return this.cfOrgUnit_DC;
    }

    /**
     * Returns the value of field 'cfOrgUnit_EAddr'.
     * 
     * @return the value of field 'CfOrgUnit_EAddr'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_EAddr getCfOrgUnit_EAddr() {
        return this.cfOrgUnit_EAddr;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Equip'.
     * 
     * @return the value of field 'CfOrgUnit_Equip'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Equip getCfOrgUnit_Equip() {
        return this.cfOrgUnit_Equip;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Event'.
     * 
     * @return the value of field 'CfOrgUnit_Event'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Event getCfOrgUnit_Event() {
        return this.cfOrgUnit_Event;
    }

    /**
     * Returns the value of field 'cfOrgUnit_ExpSkills'.
     * 
     * @return the value of field 'CfOrgUnit_ExpSkills'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_ExpSkills getCfOrgUnit_ExpSkills() {
        return this.cfOrgUnit_ExpSkills;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Facil'.
     * 
     * @return the value of field 'CfOrgUnit_Facil'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Facil getCfOrgUnit_Facil() {
        return this.cfOrgUnit_Facil;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Fund'.
     * 
     * @return the value of field 'CfOrgUnit_Fund'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Fund getCfOrgUnit_Fund() {
        return this.cfOrgUnit_Fund;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Indic'.
     * 
     * @return the value of field 'CfOrgUnit_Indic'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Indic getCfOrgUnit_Indic() {
        return this.cfOrgUnit_Indic;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Meas'.
     * 
     * @return the value of field 'CfOrgUnit_Meas'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Meas getCfOrgUnit_Meas() {
        return this.cfOrgUnit_Meas;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Medium'.
     * 
     * @return the value of field 'CfOrgUnit_Medium'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Medium getCfOrgUnit_Medium() {
        return this.cfOrgUnit_Medium;
    }

    /**
     * Returns the value of field 'cfOrgUnit_OrgUnit'.
     * 
     * @return the value of field 'CfOrgUnit_OrgUnit'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_OrgUnit getCfOrgUnit_OrgUnit() {
        return this.cfOrgUnit_OrgUnit;
    }

    /**
     * Returns the value of field 'cfOrgUnit_PAddr'.
     * 
     * @return the value of field 'CfOrgUnit_PAddr'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_PAddr getCfOrgUnit_PAddr() {
        return this.cfOrgUnit_PAddr;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Prize'.
     * 
     * @return the value of field 'CfOrgUnit_Prize'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Prize getCfOrgUnit_Prize() {
        return this.cfOrgUnit_Prize;
    }

    /**
     * Returns the value of field 'cfOrgUnit_ResPat'.
     * 
     * @return the value of field 'CfOrgUnit_ResPat'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_ResPat getCfOrgUnit_ResPat() {
        return this.cfOrgUnit_ResPat;
    }

    /**
     * Returns the value of field 'cfOrgUnit_ResProd'.
     * 
     * @return the value of field 'CfOrgUnit_ResProd'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_ResProd getCfOrgUnit_ResProd() {
        return this.cfOrgUnit_ResProd;
    }

    /**
     * Returns the value of field 'cfOrgUnit_ResPubl'.
     * 
     * @return the value of field 'CfOrgUnit_ResPubl'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_ResPubl getCfOrgUnit_ResPubl() {
        return this.cfOrgUnit_ResPubl;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Srv'.
     * 
     * @return the value of field 'CfOrgUnit_Srv'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Srv getCfOrgUnit_Srv() {
        return this.cfOrgUnit_Srv;
    }

    /**
     * Returns the value of field 'cfPers_OrgUnit'.
     * 
     * @return the value of field 'CfPers_OrgUnit'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfPers_OrgUnit getCfPers_OrgUnit() {
        return this.cfPers_OrgUnit;
    }

    /**
     * Returns the value of field 'cfProj_OrgUnit'.
     * 
     * @return the value of field 'CfProj_OrgUnit'.
     */
    public gr.ekt.cerif.schema.CfOrgUnit__TypeCfProj_OrgUnit getCfProj_OrgUnit() {
        return this.cfProj_OrgUnit;
    }

    /**
     * Returns the value of field 'cfResAct'.
     * 
     * @return the value of field 'CfResAct'.
     */
    public gr.ekt.cerif.schema.CfResAct getCfResAct() {
        return this.cfResAct;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Class'.
     * 
     * @param cfOrgUnit_Class the value of field 'cfOrgUnit_Class'.
     */
    public void setCfOrgUnit_Class(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Class cfOrgUnit_Class) {
        this.cfOrgUnit_Class = cfOrgUnit_Class;
    }

    /**
     * Sets the value of field 'cfOrgUnit_DC'.
     * 
     * @param cfOrgUnit_DC the value of field 'cfOrgUnit_DC'.
     */
    public void setCfOrgUnit_DC(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_DC cfOrgUnit_DC) {
        this.cfOrgUnit_DC = cfOrgUnit_DC;
    }

    /**
     * Sets the value of field 'cfOrgUnit_EAddr'.
     * 
     * @param cfOrgUnit_EAddr the value of field 'cfOrgUnit_EAddr'.
     */
    public void setCfOrgUnit_EAddr(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_EAddr cfOrgUnit_EAddr) {
        this.cfOrgUnit_EAddr = cfOrgUnit_EAddr;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Equip'.
     * 
     * @param cfOrgUnit_Equip the value of field 'cfOrgUnit_Equip'.
     */
    public void setCfOrgUnit_Equip(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Equip cfOrgUnit_Equip) {
        this.cfOrgUnit_Equip = cfOrgUnit_Equip;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Event'.
     * 
     * @param cfOrgUnit_Event the value of field 'cfOrgUnit_Event'.
     */
    public void setCfOrgUnit_Event(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Event cfOrgUnit_Event) {
        this.cfOrgUnit_Event = cfOrgUnit_Event;
    }

    /**
     * Sets the value of field 'cfOrgUnit_ExpSkills'.
     * 
     * @param cfOrgUnit_ExpSkills the value of field
     * 'cfOrgUnit_ExpSkills'.
     */
    public void setCfOrgUnit_ExpSkills(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_ExpSkills cfOrgUnit_ExpSkills) {
        this.cfOrgUnit_ExpSkills = cfOrgUnit_ExpSkills;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Facil'.
     * 
     * @param cfOrgUnit_Facil the value of field 'cfOrgUnit_Facil'.
     */
    public void setCfOrgUnit_Facil(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Facil cfOrgUnit_Facil) {
        this.cfOrgUnit_Facil = cfOrgUnit_Facil;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Fund'.
     * 
     * @param cfOrgUnit_Fund the value of field 'cfOrgUnit_Fund'.
     */
    public void setCfOrgUnit_Fund(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Fund cfOrgUnit_Fund) {
        this.cfOrgUnit_Fund = cfOrgUnit_Fund;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Indic'.
     * 
     * @param cfOrgUnit_Indic the value of field 'cfOrgUnit_Indic'.
     */
    public void setCfOrgUnit_Indic(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Indic cfOrgUnit_Indic) {
        this.cfOrgUnit_Indic = cfOrgUnit_Indic;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Meas'.
     * 
     * @param cfOrgUnit_Meas the value of field 'cfOrgUnit_Meas'.
     */
    public void setCfOrgUnit_Meas(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Meas cfOrgUnit_Meas) {
        this.cfOrgUnit_Meas = cfOrgUnit_Meas;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Medium'.
     * 
     * @param cfOrgUnit_Medium the value of field 'cfOrgUnit_Medium'
     */
    public void setCfOrgUnit_Medium(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Medium cfOrgUnit_Medium) {
        this.cfOrgUnit_Medium = cfOrgUnit_Medium;
    }

    /**
     * Sets the value of field 'cfOrgUnit_OrgUnit'.
     * 
     * @param cfOrgUnit_OrgUnit the value of field
     * 'cfOrgUnit_OrgUnit'.
     */
    public void setCfOrgUnit_OrgUnit(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_OrgUnit cfOrgUnit_OrgUnit) {
        this.cfOrgUnit_OrgUnit = cfOrgUnit_OrgUnit;
    }

    /**
     * Sets the value of field 'cfOrgUnit_PAddr'.
     * 
     * @param cfOrgUnit_PAddr the value of field 'cfOrgUnit_PAddr'.
     */
    public void setCfOrgUnit_PAddr(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_PAddr cfOrgUnit_PAddr) {
        this.cfOrgUnit_PAddr = cfOrgUnit_PAddr;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Prize'.
     * 
     * @param cfOrgUnit_Prize the value of field 'cfOrgUnit_Prize'.
     */
    public void setCfOrgUnit_Prize(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Prize cfOrgUnit_Prize) {
        this.cfOrgUnit_Prize = cfOrgUnit_Prize;
    }

    /**
     * Sets the value of field 'cfOrgUnit_ResPat'.
     * 
     * @param cfOrgUnit_ResPat the value of field 'cfOrgUnit_ResPat'
     */
    public void setCfOrgUnit_ResPat(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_ResPat cfOrgUnit_ResPat) {
        this.cfOrgUnit_ResPat = cfOrgUnit_ResPat;
    }

    /**
     * Sets the value of field 'cfOrgUnit_ResProd'.
     * 
     * @param cfOrgUnit_ResProd the value of field
     * 'cfOrgUnit_ResProd'.
     */
    public void setCfOrgUnit_ResProd(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_ResProd cfOrgUnit_ResProd) {
        this.cfOrgUnit_ResProd = cfOrgUnit_ResProd;
    }

    /**
     * Sets the value of field 'cfOrgUnit_ResPubl'.
     * 
     * @param cfOrgUnit_ResPubl the value of field
     * 'cfOrgUnit_ResPubl'.
     */
    public void setCfOrgUnit_ResPubl(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_ResPubl cfOrgUnit_ResPubl) {
        this.cfOrgUnit_ResPubl = cfOrgUnit_ResPubl;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Srv'.
     * 
     * @param cfOrgUnit_Srv the value of field 'cfOrgUnit_Srv'.
     */
    public void setCfOrgUnit_Srv(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfOrgUnit_Srv cfOrgUnit_Srv) {
        this.cfOrgUnit_Srv = cfOrgUnit_Srv;
    }

    /**
     * Sets the value of field 'cfPers_OrgUnit'.
     * 
     * @param cfPers_OrgUnit the value of field 'cfPers_OrgUnit'.
     */
    public void setCfPers_OrgUnit(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfPers_OrgUnit cfPers_OrgUnit) {
        this.cfPers_OrgUnit = cfPers_OrgUnit;
    }

    /**
     * Sets the value of field 'cfProj_OrgUnit'.
     * 
     * @param cfProj_OrgUnit the value of field 'cfProj_OrgUnit'.
     */
    public void setCfProj_OrgUnit(final gr.ekt.cerif.schema.CfOrgUnit__TypeCfProj_OrgUnit cfProj_OrgUnit) {
        this.cfProj_OrgUnit = cfProj_OrgUnit;
    }

    /**
     * Sets the value of field 'cfResAct'.
     * 
     * @param cfResAct the value of field 'cfResAct'.
     */
    public void setCfResAct(final gr.ekt.cerif.schema.CfResAct cfResAct) {
        this.cfResAct = cfResAct;
    }

}

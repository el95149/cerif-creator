/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfPAddr__Type implements java.io.Serializable {

    private java.lang.String cfPAddrId;

    private java.lang.String cfCountryCode;

    private java.lang.String cfAddrline1;

    private java.lang.String cfAddrline2;

    private java.lang.String cfAddrline3;

    private java.lang.String cfAddrline4;

    private java.lang.String cfAddrline5;

    private java.lang.String cfPostCode;

    private java.lang.String cfCityTown;

    private java.lang.String cfStateOfCountry;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfPAddr__TypeChoice cfPAddr__TypeChoice;

    public CfPAddr__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfAddrline1'.
     * 
     * @return the value of field 'CfAddrline1'.
     */
    public java.lang.String getCfAddrline1() {
        return this.cfAddrline1;
    }

    /**
     * Returns the value of field 'cfAddrline2'.
     * 
     * @return the value of field 'CfAddrline2'.
     */
    public java.lang.String getCfAddrline2() {
        return this.cfAddrline2;
    }

    /**
     * Returns the value of field 'cfAddrline3'.
     * 
     * @return the value of field 'CfAddrline3'.
     */
    public java.lang.String getCfAddrline3() {
        return this.cfAddrline3;
    }

    /**
     * Returns the value of field 'cfAddrline4'.
     * 
     * @return the value of field 'CfAddrline4'.
     */
    public java.lang.String getCfAddrline4() {
        return this.cfAddrline4;
    }

    /**
     * Returns the value of field 'cfAddrline5'.
     * 
     * @return the value of field 'CfAddrline5'.
     */
    public java.lang.String getCfAddrline5() {
        return this.cfAddrline5;
    }

    /**
     * Returns the value of field 'cfCityTown'.
     * 
     * @return the value of field 'CfCityTown'.
     */
    public java.lang.String getCfCityTown() {
        return this.cfCityTown;
    }

    /**
     * Returns the value of field 'cfCountryCode'.
     * 
     * @return the value of field 'CfCountryCode'.
     */
    public java.lang.String getCfCountryCode() {
        return this.cfCountryCode;
    }

    /**
     * Returns the value of field 'cfPAddrId'.
     * 
     * @return the value of field 'CfPAddrId'.
     */
    public java.lang.String getCfPAddrId() {
        return this.cfPAddrId;
    }

    /**
     * Returns the value of field 'cfPAddr__TypeChoice'.
     * 
     * @return the value of field 'CfPAddr__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfPAddr__TypeChoice getCfPAddr__TypeChoice() {
        return this.cfPAddr__TypeChoice;
    }

    /**
     * Returns the value of field 'cfPostCode'.
     * 
     * @return the value of field 'CfPostCode'.
     */
    public java.lang.String getCfPostCode() {
        return this.cfPostCode;
    }

    /**
     * Returns the value of field 'cfStateOfCountry'.
     * 
     * @return the value of field 'CfStateOfCountry'.
     */
    public java.lang.String getCfStateOfCountry() {
        return this.cfStateOfCountry;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfAddrline1'.
     * 
     * @param cfAddrline1 the value of field 'cfAddrline1'.
     */
    public void setCfAddrline1(final java.lang.String cfAddrline1) {
        this.cfAddrline1 = cfAddrline1;
    }

    /**
     * Sets the value of field 'cfAddrline2'.
     * 
     * @param cfAddrline2 the value of field 'cfAddrline2'.
     */
    public void setCfAddrline2(final java.lang.String cfAddrline2) {
        this.cfAddrline2 = cfAddrline2;
    }

    /**
     * Sets the value of field 'cfAddrline3'.
     * 
     * @param cfAddrline3 the value of field 'cfAddrline3'.
     */
    public void setCfAddrline3(final java.lang.String cfAddrline3) {
        this.cfAddrline3 = cfAddrline3;
    }

    /**
     * Sets the value of field 'cfAddrline4'.
     * 
     * @param cfAddrline4 the value of field 'cfAddrline4'.
     */
    public void setCfAddrline4(final java.lang.String cfAddrline4) {
        this.cfAddrline4 = cfAddrline4;
    }

    /**
     * Sets the value of field 'cfAddrline5'.
     * 
     * @param cfAddrline5 the value of field 'cfAddrline5'.
     */
    public void setCfAddrline5(final java.lang.String cfAddrline5) {
        this.cfAddrline5 = cfAddrline5;
    }

    /**
     * Sets the value of field 'cfCityTown'.
     * 
     * @param cfCityTown the value of field 'cfCityTown'.
     */
    public void setCfCityTown(final java.lang.String cfCityTown) {
        this.cfCityTown = cfCityTown;
    }

    /**
     * Sets the value of field 'cfCountryCode'.
     * 
     * @param cfCountryCode the value of field 'cfCountryCode'.
     */
    public void setCfCountryCode(final java.lang.String cfCountryCode) {
        this.cfCountryCode = cfCountryCode;
    }

    /**
     * Sets the value of field 'cfPAddrId'.
     * 
     * @param cfPAddrId the value of field 'cfPAddrId'.
     */
    public void setCfPAddrId(final java.lang.String cfPAddrId) {
        this.cfPAddrId = cfPAddrId;
    }

    /**
     * Sets the value of field 'cfPAddr__TypeChoice'.
     * 
     * @param cfPAddr__TypeChoice the value of field
     * 'cfPAddr__TypeChoice'.
     */
    public void setCfPAddr__TypeChoice(final gr.ekt.cerif.schema.CfPAddr__TypeChoice cfPAddr__TypeChoice) {
        this.cfPAddr__TypeChoice = cfPAddr__TypeChoice;
    }

    /**
     * Sets the value of field 'cfPostCode'.
     * 
     * @param cfPostCode the value of field 'cfPostCode'.
     */
    public void setCfPostCode(final java.lang.String cfPostCode) {
        this.cfPostCode = cfPostCode;
    }

    /**
     * Sets the value of field 'cfStateOfCountry'.
     * 
     * @param cfStateOfCountry the value of field 'cfStateOfCountry'
     */
    public void setCfStateOfCountry(final java.lang.String cfStateOfCountry) {
        this.cfStateOfCountry = cfStateOfCountry;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfPAddr__Type
     */
    public static gr.ekt.cerif.schema.CfPAddr__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfPAddr__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfPAddr__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

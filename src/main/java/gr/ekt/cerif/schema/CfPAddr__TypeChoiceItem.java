/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfPAddr__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfPAddr__TypeCfOrgUnit_PAddr cfOrgUnit_PAddr;

    private gr.ekt.cerif.schema.CfPAddr__TypeCfPers_PAddr cfPers_PAddr;

    private gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_Class cfPAddr_Class;

    private gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_GeoBBox cfPAddr_GeoBBox;

    private gr.ekt.cerif.schema.CfPAddr__TypeCfEquip_PAddr cfEquip_PAddr;

    private gr.ekt.cerif.schema.CfPAddr__TypeCfFacil_PAddr cfFacil_PAddr;

    private gr.ekt.cerif.schema.CfPAddr__TypeCfSrv_PAddr cfSrv_PAddr;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfPAddr__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfEquip_PAddr'.
     * 
     * @return the value of field 'CfEquip_PAddr'.
     */
    public gr.ekt.cerif.schema.CfPAddr__TypeCfEquip_PAddr getCfEquip_PAddr() {
        return this.cfEquip_PAddr;
    }

    /**
     * Returns the value of field 'cfFacil_PAddr'.
     * 
     * @return the value of field 'CfFacil_PAddr'.
     */
    public gr.ekt.cerif.schema.CfPAddr__TypeCfFacil_PAddr getCfFacil_PAddr() {
        return this.cfFacil_PAddr;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfOrgUnit_PAddr'.
     * 
     * @return the value of field 'CfOrgUnit_PAddr'.
     */
    public gr.ekt.cerif.schema.CfPAddr__TypeCfOrgUnit_PAddr getCfOrgUnit_PAddr() {
        return this.cfOrgUnit_PAddr;
    }

    /**
     * Returns the value of field 'cfPAddr_Class'.
     * 
     * @return the value of field 'CfPAddr_Class'.
     */
    public gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_Class getCfPAddr_Class() {
        return this.cfPAddr_Class;
    }

    /**
     * Returns the value of field 'cfPAddr_GeoBBox'.
     * 
     * @return the value of field 'CfPAddr_GeoBBox'.
     */
    public gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_GeoBBox getCfPAddr_GeoBBox() {
        return this.cfPAddr_GeoBBox;
    }

    /**
     * Returns the value of field 'cfPers_PAddr'.
     * 
     * @return the value of field 'CfPers_PAddr'.
     */
    public gr.ekt.cerif.schema.CfPAddr__TypeCfPers_PAddr getCfPers_PAddr() {
        return this.cfPers_PAddr;
    }

    /**
     * Returns the value of field 'cfSrv_PAddr'.
     * 
     * @return the value of field 'CfSrv_PAddr'.
     */
    public gr.ekt.cerif.schema.CfPAddr__TypeCfSrv_PAddr getCfSrv_PAddr() {
        return this.cfSrv_PAddr;
    }

    /**
     * Sets the value of field 'cfEquip_PAddr'.
     * 
     * @param cfEquip_PAddr the value of field 'cfEquip_PAddr'.
     */
    public void setCfEquip_PAddr(final gr.ekt.cerif.schema.CfPAddr__TypeCfEquip_PAddr cfEquip_PAddr) {
        this.cfEquip_PAddr = cfEquip_PAddr;
    }

    /**
     * Sets the value of field 'cfFacil_PAddr'.
     * 
     * @param cfFacil_PAddr the value of field 'cfFacil_PAddr'.
     */
    public void setCfFacil_PAddr(final gr.ekt.cerif.schema.CfPAddr__TypeCfFacil_PAddr cfFacil_PAddr) {
        this.cfFacil_PAddr = cfFacil_PAddr;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfOrgUnit_PAddr'.
     * 
     * @param cfOrgUnit_PAddr the value of field 'cfOrgUnit_PAddr'.
     */
    public void setCfOrgUnit_PAddr(final gr.ekt.cerif.schema.CfPAddr__TypeCfOrgUnit_PAddr cfOrgUnit_PAddr) {
        this.cfOrgUnit_PAddr = cfOrgUnit_PAddr;
    }

    /**
     * Sets the value of field 'cfPAddr_Class'.
     * 
     * @param cfPAddr_Class the value of field 'cfPAddr_Class'.
     */
    public void setCfPAddr_Class(final gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_Class cfPAddr_Class) {
        this.cfPAddr_Class = cfPAddr_Class;
    }

    /**
     * Sets the value of field 'cfPAddr_GeoBBox'.
     * 
     * @param cfPAddr_GeoBBox the value of field 'cfPAddr_GeoBBox'.
     */
    public void setCfPAddr_GeoBBox(final gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_GeoBBox cfPAddr_GeoBBox) {
        this.cfPAddr_GeoBBox = cfPAddr_GeoBBox;
    }

    /**
     * Sets the value of field 'cfPers_PAddr'.
     * 
     * @param cfPers_PAddr the value of field 'cfPers_PAddr'.
     */
    public void setCfPers_PAddr(final gr.ekt.cerif.schema.CfPAddr__TypeCfPers_PAddr cfPers_PAddr) {
        this.cfPers_PAddr = cfPers_PAddr;
    }

    /**
     * Sets the value of field 'cfSrv_PAddr'.
     * 
     * @param cfSrv_PAddr the value of field 'cfSrv_PAddr'.
     */
    public void setCfSrv_PAddr(final gr.ekt.cerif.schema.CfPAddr__TypeCfSrv_PAddr cfSrv_PAddr) {
        this.cfSrv_PAddr = cfSrv_PAddr;
    }

}

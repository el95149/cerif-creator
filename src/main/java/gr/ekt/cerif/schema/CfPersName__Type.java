/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfPersName__Type implements java.io.Serializable {

    private java.lang.String cfPersNameId;

    private java.lang.String cfFamilyNames;

    private java.lang.String cfFirstNames;

    private java.lang.String cfOtherNames;

    private gr.ekt.cerif.schema.CfPersName__TypeChoice cfPersName__TypeChoice;

    public CfPersName__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfFamilyNames'.
     * 
     * @return the value of field 'CfFamilyNames'.
     */
    public java.lang.String getCfFamilyNames() {
        return this.cfFamilyNames;
    }

    /**
     * Returns the value of field 'cfFirstNames'.
     * 
     * @return the value of field 'CfFirstNames'.
     */
    public java.lang.String getCfFirstNames() {
        return this.cfFirstNames;
    }

    /**
     * Returns the value of field 'cfOtherNames'.
     * 
     * @return the value of field 'CfOtherNames'.
     */
    public java.lang.String getCfOtherNames() {
        return this.cfOtherNames;
    }

    /**
     * Returns the value of field 'cfPersNameId'.
     * 
     * @return the value of field 'CfPersNameId'.
     */
    public java.lang.String getCfPersNameId() {
        return this.cfPersNameId;
    }

    /**
     * Returns the value of field 'cfPersName__TypeChoice'.
     * 
     * @return the value of field 'CfPersName__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfPersName__TypeChoice getCfPersName__TypeChoice() {
        return this.cfPersName__TypeChoice;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfFamilyNames'.
     * 
     * @param cfFamilyNames the value of field 'cfFamilyNames'.
     */
    public void setCfFamilyNames(final java.lang.String cfFamilyNames) {
        this.cfFamilyNames = cfFamilyNames;
    }

    /**
     * Sets the value of field 'cfFirstNames'.
     * 
     * @param cfFirstNames the value of field 'cfFirstNames'.
     */
    public void setCfFirstNames(final java.lang.String cfFirstNames) {
        this.cfFirstNames = cfFirstNames;
    }

    /**
     * Sets the value of field 'cfOtherNames'.
     * 
     * @param cfOtherNames the value of field 'cfOtherNames'.
     */
    public void setCfOtherNames(final java.lang.String cfOtherNames) {
        this.cfOtherNames = cfOtherNames;
    }

    /**
     * Sets the value of field 'cfPersNameId'.
     * 
     * @param cfPersNameId the value of field 'cfPersNameId'.
     */
    public void setCfPersNameId(final java.lang.String cfPersNameId) {
        this.cfPersNameId = cfPersNameId;
    }

    /**
     * Sets the value of field 'cfPersName__TypeChoice'.
     * 
     * @param cfPersName__TypeChoice the value of field
     * 'cfPersName__TypeChoice'.
     */
    public void setCfPersName__TypeChoice(final gr.ekt.cerif.schema.CfPersName__TypeChoice cfPersName__TypeChoice) {
        this.cfPersName__TypeChoice = cfPersName__TypeChoice;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfPersName__Type
     */
    public static gr.ekt.cerif.schema.CfPersName__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfPersName__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfPersName__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfPersName__TypeChoice implements java.io.Serializable {

    private java.util.Vector<gr.ekt.cerif.schema.CfPersName__TypeChoiceItem> _items;

    public CfPersName__TypeChoice() {
        super();
        this._items = new java.util.Vector<gr.ekt.cerif.schema.CfPersName__TypeChoiceItem>();
    }

    /**
     * 
     * 
     * @param vCfPersName__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfPersName__TypeChoiceItem(final gr.ekt.cerif.schema.CfPersName__TypeChoiceItem vCfPersName__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.addElement(vCfPersName__TypeChoiceItem);
    }

    /**
     * 
     * 
     * @param index
     * @param vCfPersName__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfPersName__TypeChoiceItem(final int index,final gr.ekt.cerif.schema.CfPersName__TypeChoiceItem vCfPersName__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.add(index, vCfPersName__TypeChoiceItem);
    }

    /**
     * Method enumerateCfPersName__TypeChoiceItem.
     * 
     * @return an Enumeration over all
     * gr.ekt.cerif.schema.CfPersName__TypeChoiceItem elements
     */
    public java.util.Enumeration<? extends gr.ekt.cerif.schema.CfPersName__TypeChoiceItem> enumerateCfPersName__TypeChoiceItem() {
        return this._items.elements();
    }

    /**
     * Method getCfPersName__TypeChoiceItem.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * gr.ekt.cerif.schema.CfPersName__TypeChoiceItem at the given
     * index
     */
    public gr.ekt.cerif.schema.CfPersName__TypeChoiceItem getCfPersName__TypeChoiceItem(final int index) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("getCfPersName__TypeChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        return _items.get(index);
    }

    /**
     * Method getCfPersName__TypeChoiceItem.Returns the contents of
     * the collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public gr.ekt.cerif.schema.CfPersName__TypeChoiceItem[] getCfPersName__TypeChoiceItem() {
        gr.ekt.cerif.schema.CfPersName__TypeChoiceItem[] array = new gr.ekt.cerif.schema.CfPersName__TypeChoiceItem[0];
        return this._items.toArray(array);
    }

    /**
     * Method getCfPersName__TypeChoiceItemCount.
     * 
     * @return the size of this collection
     */
    public int getCfPersName__TypeChoiceItemCount() {
        return this._items.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllCfPersName__TypeChoiceItem() {
        this._items.clear();
    }

    /**
     * Method removeCfPersName__TypeChoiceItem.
     * 
     * @param vCfPersName__TypeChoiceItem
     * @return true if the object was removed from the collection.
     */
    public boolean removeCfPersName__TypeChoiceItem(final gr.ekt.cerif.schema.CfPersName__TypeChoiceItem vCfPersName__TypeChoiceItem) {
        boolean removed = _items.remove(vCfPersName__TypeChoiceItem);
        return removed;
    }

    /**
     * Method removeCfPersName__TypeChoiceItemAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public gr.ekt.cerif.schema.CfPersName__TypeChoiceItem removeCfPersName__TypeChoiceItemAt(final int index) {
        java.lang.Object obj = this._items.remove(index);
        return (gr.ekt.cerif.schema.CfPersName__TypeChoiceItem) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vCfPersName__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setCfPersName__TypeChoiceItem(final int index,final gr.ekt.cerif.schema.CfPersName__TypeChoiceItem vCfPersName__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("setCfPersName__TypeChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        this._items.set(index, vCfPersName__TypeChoiceItem);
    }

    /**
     * 
     * 
     * @param vCfPersName__TypeChoiceItemArray
     */
    public void setCfPersName__TypeChoiceItem(final gr.ekt.cerif.schema.CfPersName__TypeChoiceItem[] vCfPersName__TypeChoiceItemArray) {
        //-- copy array
        _items.clear();

        for (int i = 0; i < vCfPersName__TypeChoiceItemArray.length; i++) {
                this._items.add(vCfPersName__TypeChoiceItemArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfPersName__TypeChoice
     */
    public static gr.ekt.cerif.schema.CfPersName__TypeChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfPersName__TypeChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfPersName__TypeChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

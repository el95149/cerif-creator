/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfPersName__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfPersName__TypeCfPersName_Pers cfPersName_Pers;

    public CfPersName__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfPersName_Pers'.
     * 
     * @return the value of field 'CfPersName_Pers'.
     */
    public gr.ekt.cerif.schema.CfPersName__TypeCfPersName_Pers getCfPersName_Pers() {
        return this.cfPersName_Pers;
    }

    /**
     * Sets the value of field 'cfPersName_Pers'.
     * 
     * @param cfPersName_Pers the value of field 'cfPersName_Pers'.
     */
    public void setCfPersName_Pers(final gr.ekt.cerif.schema.CfPersName__TypeCfPersName_Pers cfPersName_Pers) {
        this.cfPersName_Pers = cfPersName_Pers;
    }

}

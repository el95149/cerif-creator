/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfPers_PersChoice implements java.io.Serializable {

    private java.lang.String cfPersId2;

    private java.lang.String cfPersId1;

    public CfPers_PersChoice() {
        super();
    }

    /**
     * Returns the value of field 'cfPersId1'.
     * 
     * @return the value of field 'CfPersId1'.
     */
    public java.lang.String getCfPersId1() {
        return this.cfPersId1;
    }

    /**
     * Returns the value of field 'cfPersId2'.
     * 
     * @return the value of field 'CfPersId2'.
     */
    public java.lang.String getCfPersId2() {
        return this.cfPersId2;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfPersId1'.
     * 
     * @param cfPersId1 the value of field 'cfPersId1'.
     */
    public void setCfPersId1(final java.lang.String cfPersId1) {
        this.cfPersId1 = cfPersId1;
    }

    /**
     * Sets the value of field 'cfPersId2'.
     * 
     * @param cfPersId2 the value of field 'cfPersId2'.
     */
    public void setCfPersId2(final java.lang.String cfPersId2) {
        this.cfPersId2 = cfPersId2;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfPers_PersChoice
     */
    public static gr.ekt.cerif.schema.CfPers_PersChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfPers_PersChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfPers_PersChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

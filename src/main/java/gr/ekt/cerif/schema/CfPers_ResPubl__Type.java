/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfPers_ResPubl__Type implements java.io.Serializable {

    private java.lang.String cfPersId;

    private java.lang.String cfResPublId;

    private gr.ekt.cerif.schema.CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group;

    private java.lang.String cfCopyright;

    private int cfOrder;

    /**
     * Keeps track of whether primitive field cfOrder has been set
     * already.
     */
    private boolean hascfOrder;

    public CfPers_ResPubl__Type() {
        super();
    }

    /**
     */
    public void deleteCfOrder() {
        this.hascfOrder= false;
    }

    /**
     * Returns the value of field 'cfCopyright'.
     * 
     * @return the value of field 'CfCopyright'.
     */
    public java.lang.String getCfCopyright() {
        return this.cfCopyright;
    }

    /**
     * Returns the value of field 'cfCoreClassWithFraction__Group'.
     * 
     * @return the value of field 'CfCoreClassWithFraction__Group'.
     */
    public gr.ekt.cerif.schema.CfCoreClassWithFraction__Group getCfCoreClassWithFraction__Group() {
        return this.cfCoreClassWithFraction__Group;
    }

    /**
     * Returns the value of field 'cfOrder'.
     * 
     * @return the value of field 'CfOrder'.
     */
    public int getCfOrder() {
        return this.cfOrder;
    }

    /**
     * Returns the value of field 'cfPersId'.
     * 
     * @return the value of field 'CfPersId'.
     */
    public java.lang.String getCfPersId() {
        return this.cfPersId;
    }

    /**
     * Returns the value of field 'cfResPublId'.
     * 
     * @return the value of field 'CfResPublId'.
     */
    public java.lang.String getCfResPublId() {
        return this.cfResPublId;
    }

    /**
     * Method hasCfOrder.
     * 
     * @return true if at least one CfOrder has been added
     */
    public boolean hasCfOrder() {
        return this.hascfOrder;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCopyright'.
     * 
     * @param cfCopyright the value of field 'cfCopyright'.
     */
    public void setCfCopyright(final java.lang.String cfCopyright) {
        this.cfCopyright = cfCopyright;
    }

    /**
     * Sets the value of field 'cfCoreClassWithFraction__Group'.
     * 
     * @param cfCoreClassWithFraction__Group the value of field
     * 'cfCoreClassWithFraction__Group'.
     */
    public void setCfCoreClassWithFraction__Group(final gr.ekt.cerif.schema.CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group) {
        this.cfCoreClassWithFraction__Group = cfCoreClassWithFraction__Group;
    }

    /**
     * Sets the value of field 'cfOrder'.
     * 
     * @param cfOrder the value of field 'cfOrder'.
     */
    public void setCfOrder(final int cfOrder) {
        this.cfOrder = cfOrder;
        this.hascfOrder = true;
    }

    /**
     * Sets the value of field 'cfPersId'.
     * 
     * @param cfPersId the value of field 'cfPersId'.
     */
    public void setCfPersId(final java.lang.String cfPersId) {
        this.cfPersId = cfPersId;
    }

    /**
     * Sets the value of field 'cfResPublId'.
     * 
     * @param cfResPublId the value of field 'cfResPublId'.
     */
    public void setCfResPublId(final java.lang.String cfResPublId) {
        this.cfResPublId = cfResPublId;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfPers_ResPubl__Type
     */
    public static gr.ekt.cerif.schema.CfPers_ResPubl__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfPers_ResPubl__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfPers_ResPubl__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfPers__Type implements java.io.Serializable {

    private java.lang.String cfPersId;

    private org.exolab.castor.types.Date cfBirthdate;

    private gr.ekt.cerif.schema.types.CfGender__Type cfGender;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfPers__TypeChoice cfPers__TypeChoice;

    public CfPers__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfBirthdate'.
     * 
     * @return the value of field 'CfBirthdate'.
     */
    public org.exolab.castor.types.Date getCfBirthdate() {
        return this.cfBirthdate;
    }

    /**
     * Returns the value of field 'cfGender'.
     * 
     * @return the value of field 'CfGender'.
     */
    public gr.ekt.cerif.schema.types.CfGender__Type getCfGender() {
        return this.cfGender;
    }

    /**
     * Returns the value of field 'cfPersId'.
     * 
     * @return the value of field 'CfPersId'.
     */
    public java.lang.String getCfPersId() {
        return this.cfPersId;
    }

    /**
     * Returns the value of field 'cfPers__TypeChoice'.
     * 
     * @return the value of field 'CfPers__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeChoice getCfPers__TypeChoice() {
        return this.cfPers__TypeChoice;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfBirthdate'.
     * 
     * @param cfBirthdate the value of field 'cfBirthdate'.
     */
    public void setCfBirthdate(final org.exolab.castor.types.Date cfBirthdate) {
        this.cfBirthdate = cfBirthdate;
    }

    /**
     * Sets the value of field 'cfGender'.
     * 
     * @param cfGender the value of field 'cfGender'.
     */
    public void setCfGender(final gr.ekt.cerif.schema.types.CfGender__Type cfGender) {
        this.cfGender = cfGender;
    }

    /**
     * Sets the value of field 'cfPersId'.
     * 
     * @param cfPersId the value of field 'cfPersId'.
     */
    public void setCfPersId(final java.lang.String cfPersId) {
        this.cfPersId = cfPersId;
    }

    /**
     * Sets the value of field 'cfPers__TypeChoice'.
     * 
     * @param cfPers__TypeChoice the value of field
     * 'cfPers__TypeChoice'.
     */
    public void setCfPers__TypeChoice(final gr.ekt.cerif.schema.CfPers__TypeChoice cfPers__TypeChoice) {
        this.cfPers__TypeChoice = cfPers__TypeChoice;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfPers__Type
     */
    public static gr.ekt.cerif.schema.CfPers__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfPers__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfPers__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

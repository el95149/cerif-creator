/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfPers__TypeChoice implements java.io.Serializable {

    private java.util.Vector<gr.ekt.cerif.schema.CfPers__TypeChoiceItem> _items;

    public CfPers__TypeChoice() {
        super();
        this._items = new java.util.Vector<gr.ekt.cerif.schema.CfPers__TypeChoiceItem>();
    }

    /**
     * 
     * 
     * @param vCfPers__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfPers__TypeChoiceItem(final gr.ekt.cerif.schema.CfPers__TypeChoiceItem vCfPers__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.addElement(vCfPers__TypeChoiceItem);
    }

    /**
     * 
     * 
     * @param index
     * @param vCfPers__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addCfPers__TypeChoiceItem(final int index,final gr.ekt.cerif.schema.CfPers__TypeChoiceItem vCfPers__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        this._items.add(index, vCfPers__TypeChoiceItem);
    }

    /**
     * Method enumerateCfPers__TypeChoiceItem.
     * 
     * @return an Enumeration over all
     * gr.ekt.cerif.schema.CfPers__TypeChoiceItem elements
     */
    public java.util.Enumeration<? extends gr.ekt.cerif.schema.CfPers__TypeChoiceItem> enumerateCfPers__TypeChoiceItem() {
        return this._items.elements();
    }

    /**
     * Method getCfPers__TypeChoiceItem.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * gr.ekt.cerif.schema.CfPers__TypeChoiceItem at the given index
     */
    public gr.ekt.cerif.schema.CfPers__TypeChoiceItem getCfPers__TypeChoiceItem(final int index) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("getCfPers__TypeChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        return _items.get(index);
    }

    /**
     * Method getCfPers__TypeChoiceItem.Returns the contents of the
     * collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public gr.ekt.cerif.schema.CfPers__TypeChoiceItem[] getCfPers__TypeChoiceItem() {
        gr.ekt.cerif.schema.CfPers__TypeChoiceItem[] array = new gr.ekt.cerif.schema.CfPers__TypeChoiceItem[0];
        return this._items.toArray(array);
    }

    /**
     * Method getCfPers__TypeChoiceItemCount.
     * 
     * @return the size of this collection
     */
    public int getCfPers__TypeChoiceItemCount() {
        return this._items.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllCfPers__TypeChoiceItem() {
        this._items.clear();
    }

    /**
     * Method removeCfPers__TypeChoiceItem.
     * 
     * @param vCfPers__TypeChoiceItem
     * @return true if the object was removed from the collection.
     */
    public boolean removeCfPers__TypeChoiceItem(final gr.ekt.cerif.schema.CfPers__TypeChoiceItem vCfPers__TypeChoiceItem) {
        boolean removed = _items.remove(vCfPers__TypeChoiceItem);
        return removed;
    }

    /**
     * Method removeCfPers__TypeChoiceItemAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public gr.ekt.cerif.schema.CfPers__TypeChoiceItem removeCfPers__TypeChoiceItemAt(final int index) {
        java.lang.Object obj = this._items.remove(index);
        return (gr.ekt.cerif.schema.CfPers__TypeChoiceItem) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vCfPers__TypeChoiceItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setCfPers__TypeChoiceItem(final int index,final gr.ekt.cerif.schema.CfPers__TypeChoiceItem vCfPers__TypeChoiceItem) throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("setCfPers__TypeChoiceItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }

        this._items.set(index, vCfPers__TypeChoiceItem);
    }

    /**
     * 
     * 
     * @param vCfPers__TypeChoiceItemArray
     */
    public void setCfPers__TypeChoiceItem(final gr.ekt.cerif.schema.CfPers__TypeChoiceItem[] vCfPers__TypeChoiceItemArray) {
        //-- copy array
        _items.clear();

        for (int i = 0; i < vCfPers__TypeChoiceItemArray.length; i++) {
                this._items.add(vCfPers__TypeChoiceItemArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfPers__TypeChoic
     */
    public static gr.ekt.cerif.schema.CfPers__TypeChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfPers__TypeChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfPers__TypeChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

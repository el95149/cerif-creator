/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfPers__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfResInt cfResInt;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Pers cfPers_Pers;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_EAddr cfPers_EAddr;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Class cfPers_Class;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_CV cfPers_CV;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Equip cfPers_Equip;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Event cfPers_Event;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_ExpSkills cfPers_ExpSkills;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Facil cfPers_Facil;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Fund cfPers_Fund;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Lang cfPers_Lang;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Country cfPers_Country;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_OrgUnit cfPers_OrgUnit;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Prize cfPers_Prize;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_ResPat cfPers_ResPat;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_ResProd cfPers_ResProd;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_ResPubl cfPers_ResPubl;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Srv cfPers_Srv;

    private gr.ekt.cerif.schema.CfPers__TypeCfProj_Pers cfProj_Pers;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_PAddr cfPers_PAddr;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_DC cfPers_DC;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Qual cfPers_Qual;

    private gr.ekt.cerif.schema.CfPers__TypeCfPersName_Pers cfPersName_Pers;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Medium cfPers_Medium;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Meas cfPers_Meas;

    private gr.ekt.cerif.schema.CfPers__TypeCfPers_Indic cfPers_Indic;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfPers__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfPersName_Pers'.
     * 
     * @return the value of field 'CfPersName_Pers'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPersName_Pers getCfPersName_Pers() {
        return this.cfPersName_Pers;
    }

    /**
     * Returns the value of field 'cfPers_CV'.
     * 
     * @return the value of field 'CfPers_CV'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_CV getCfPers_CV() {
        return this.cfPers_CV;
    }

    /**
     * Returns the value of field 'cfPers_Class'.
     * 
     * @return the value of field 'CfPers_Class'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Class getCfPers_Class() {
        return this.cfPers_Class;
    }

    /**
     * Returns the value of field 'cfPers_Country'.
     * 
     * @return the value of field 'CfPers_Country'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Country getCfPers_Country() {
        return this.cfPers_Country;
    }

    /**
     * Returns the value of field 'cfPers_DC'.
     * 
     * @return the value of field 'CfPers_DC'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_DC getCfPers_DC() {
        return this.cfPers_DC;
    }

    /**
     * Returns the value of field 'cfPers_EAddr'.
     * 
     * @return the value of field 'CfPers_EAddr'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_EAddr getCfPers_EAddr() {
        return this.cfPers_EAddr;
    }

    /**
     * Returns the value of field 'cfPers_Equip'.
     * 
     * @return the value of field 'CfPers_Equip'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Equip getCfPers_Equip() {
        return this.cfPers_Equip;
    }

    /**
     * Returns the value of field 'cfPers_Event'.
     * 
     * @return the value of field 'CfPers_Event'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Event getCfPers_Event() {
        return this.cfPers_Event;
    }

    /**
     * Returns the value of field 'cfPers_ExpSkills'.
     * 
     * @return the value of field 'CfPers_ExpSkills'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_ExpSkills getCfPers_ExpSkills() {
        return this.cfPers_ExpSkills;
    }

    /**
     * Returns the value of field 'cfPers_Facil'.
     * 
     * @return the value of field 'CfPers_Facil'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Facil getCfPers_Facil() {
        return this.cfPers_Facil;
    }

    /**
     * Returns the value of field 'cfPers_Fund'.
     * 
     * @return the value of field 'CfPers_Fund'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Fund getCfPers_Fund() {
        return this.cfPers_Fund;
    }

    /**
     * Returns the value of field 'cfPers_Indic'.
     * 
     * @return the value of field 'CfPers_Indic'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Indic getCfPers_Indic() {
        return this.cfPers_Indic;
    }

    /**
     * Returns the value of field 'cfPers_Lang'.
     * 
     * @return the value of field 'CfPers_Lang'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Lang getCfPers_Lang() {
        return this.cfPers_Lang;
    }

    /**
     * Returns the value of field 'cfPers_Meas'.
     * 
     * @return the value of field 'CfPers_Meas'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Meas getCfPers_Meas() {
        return this.cfPers_Meas;
    }

    /**
     * Returns the value of field 'cfPers_Medium'.
     * 
     * @return the value of field 'CfPers_Medium'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Medium getCfPers_Medium() {
        return this.cfPers_Medium;
    }

    /**
     * Returns the value of field 'cfPers_OrgUnit'.
     * 
     * @return the value of field 'CfPers_OrgUnit'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_OrgUnit getCfPers_OrgUnit() {
        return this.cfPers_OrgUnit;
    }

    /**
     * Returns the value of field 'cfPers_PAddr'.
     * 
     * @return the value of field 'CfPers_PAddr'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_PAddr getCfPers_PAddr() {
        return this.cfPers_PAddr;
    }

    /**
     * Returns the value of field 'cfPers_Pers'.
     * 
     * @return the value of field 'CfPers_Pers'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Pers getCfPers_Pers() {
        return this.cfPers_Pers;
    }

    /**
     * Returns the value of field 'cfPers_Prize'.
     * 
     * @return the value of field 'CfPers_Prize'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Prize getCfPers_Prize() {
        return this.cfPers_Prize;
    }

    /**
     * Returns the value of field 'cfPers_Qual'.
     * 
     * @return the value of field 'CfPers_Qual'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Qual getCfPers_Qual() {
        return this.cfPers_Qual;
    }

    /**
     * Returns the value of field 'cfPers_ResPat'.
     * 
     * @return the value of field 'CfPers_ResPat'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_ResPat getCfPers_ResPat() {
        return this.cfPers_ResPat;
    }

    /**
     * Returns the value of field 'cfPers_ResProd'.
     * 
     * @return the value of field 'CfPers_ResProd'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_ResProd getCfPers_ResProd() {
        return this.cfPers_ResProd;
    }

    /**
     * Returns the value of field 'cfPers_ResPubl'.
     * 
     * @return the value of field 'CfPers_ResPubl'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_ResPubl getCfPers_ResPubl() {
        return this.cfPers_ResPubl;
    }

    /**
     * Returns the value of field 'cfPers_Srv'.
     * 
     * @return the value of field 'CfPers_Srv'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfPers_Srv getCfPers_Srv() {
        return this.cfPers_Srv;
    }

    /**
     * Returns the value of field 'cfProj_Pers'.
     * 
     * @return the value of field 'CfProj_Pers'.
     */
    public gr.ekt.cerif.schema.CfPers__TypeCfProj_Pers getCfProj_Pers() {
        return this.cfProj_Pers;
    }

    /**
     * Returns the value of field 'cfResInt'.
     * 
     * @return the value of field 'CfResInt'.
     */
    public gr.ekt.cerif.schema.CfResInt getCfResInt() {
        return this.cfResInt;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfPersName_Pers'.
     * 
     * @param cfPersName_Pers the value of field 'cfPersName_Pers'.
     */
    public void setCfPersName_Pers(final gr.ekt.cerif.schema.CfPers__TypeCfPersName_Pers cfPersName_Pers) {
        this.cfPersName_Pers = cfPersName_Pers;
    }

    /**
     * Sets the value of field 'cfPers_CV'.
     * 
     * @param cfPers_CV the value of field 'cfPers_CV'.
     */
    public void setCfPers_CV(final gr.ekt.cerif.schema.CfPers__TypeCfPers_CV cfPers_CV) {
        this.cfPers_CV = cfPers_CV;
    }

    /**
     * Sets the value of field 'cfPers_Class'.
     * 
     * @param cfPers_Class the value of field 'cfPers_Class'.
     */
    public void setCfPers_Class(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Class cfPers_Class) {
        this.cfPers_Class = cfPers_Class;
    }

    /**
     * Sets the value of field 'cfPers_Country'.
     * 
     * @param cfPers_Country the value of field 'cfPers_Country'.
     */
    public void setCfPers_Country(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Country cfPers_Country) {
        this.cfPers_Country = cfPers_Country;
    }

    /**
     * Sets the value of field 'cfPers_DC'.
     * 
     * @param cfPers_DC the value of field 'cfPers_DC'.
     */
    public void setCfPers_DC(final gr.ekt.cerif.schema.CfPers__TypeCfPers_DC cfPers_DC) {
        this.cfPers_DC = cfPers_DC;
    }

    /**
     * Sets the value of field 'cfPers_EAddr'.
     * 
     * @param cfPers_EAddr the value of field 'cfPers_EAddr'.
     */
    public void setCfPers_EAddr(final gr.ekt.cerif.schema.CfPers__TypeCfPers_EAddr cfPers_EAddr) {
        this.cfPers_EAddr = cfPers_EAddr;
    }

    /**
     * Sets the value of field 'cfPers_Equip'.
     * 
     * @param cfPers_Equip the value of field 'cfPers_Equip'.
     */
    public void setCfPers_Equip(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Equip cfPers_Equip) {
        this.cfPers_Equip = cfPers_Equip;
    }

    /**
     * Sets the value of field 'cfPers_Event'.
     * 
     * @param cfPers_Event the value of field 'cfPers_Event'.
     */
    public void setCfPers_Event(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Event cfPers_Event) {
        this.cfPers_Event = cfPers_Event;
    }

    /**
     * Sets the value of field 'cfPers_ExpSkills'.
     * 
     * @param cfPers_ExpSkills the value of field 'cfPers_ExpSkills'
     */
    public void setCfPers_ExpSkills(final gr.ekt.cerif.schema.CfPers__TypeCfPers_ExpSkills cfPers_ExpSkills) {
        this.cfPers_ExpSkills = cfPers_ExpSkills;
    }

    /**
     * Sets the value of field 'cfPers_Facil'.
     * 
     * @param cfPers_Facil the value of field 'cfPers_Facil'.
     */
    public void setCfPers_Facil(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Facil cfPers_Facil) {
        this.cfPers_Facil = cfPers_Facil;
    }

    /**
     * Sets the value of field 'cfPers_Fund'.
     * 
     * @param cfPers_Fund the value of field 'cfPers_Fund'.
     */
    public void setCfPers_Fund(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Fund cfPers_Fund) {
        this.cfPers_Fund = cfPers_Fund;
    }

    /**
     * Sets the value of field 'cfPers_Indic'.
     * 
     * @param cfPers_Indic the value of field 'cfPers_Indic'.
     */
    public void setCfPers_Indic(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Indic cfPers_Indic) {
        this.cfPers_Indic = cfPers_Indic;
    }

    /**
     * Sets the value of field 'cfPers_Lang'.
     * 
     * @param cfPers_Lang the value of field 'cfPers_Lang'.
     */
    public void setCfPers_Lang(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Lang cfPers_Lang) {
        this.cfPers_Lang = cfPers_Lang;
    }

    /**
     * Sets the value of field 'cfPers_Meas'.
     * 
     * @param cfPers_Meas the value of field 'cfPers_Meas'.
     */
    public void setCfPers_Meas(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Meas cfPers_Meas) {
        this.cfPers_Meas = cfPers_Meas;
    }

    /**
     * Sets the value of field 'cfPers_Medium'.
     * 
     * @param cfPers_Medium the value of field 'cfPers_Medium'.
     */
    public void setCfPers_Medium(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Medium cfPers_Medium) {
        this.cfPers_Medium = cfPers_Medium;
    }

    /**
     * Sets the value of field 'cfPers_OrgUnit'.
     * 
     * @param cfPers_OrgUnit the value of field 'cfPers_OrgUnit'.
     */
    public void setCfPers_OrgUnit(final gr.ekt.cerif.schema.CfPers__TypeCfPers_OrgUnit cfPers_OrgUnit) {
        this.cfPers_OrgUnit = cfPers_OrgUnit;
    }

    /**
     * Sets the value of field 'cfPers_PAddr'.
     * 
     * @param cfPers_PAddr the value of field 'cfPers_PAddr'.
     */
    public void setCfPers_PAddr(final gr.ekt.cerif.schema.CfPers__TypeCfPers_PAddr cfPers_PAddr) {
        this.cfPers_PAddr = cfPers_PAddr;
    }

    /**
     * Sets the value of field 'cfPers_Pers'.
     * 
     * @param cfPers_Pers the value of field 'cfPers_Pers'.
     */
    public void setCfPers_Pers(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Pers cfPers_Pers) {
        this.cfPers_Pers = cfPers_Pers;
    }

    /**
     * Sets the value of field 'cfPers_Prize'.
     * 
     * @param cfPers_Prize the value of field 'cfPers_Prize'.
     */
    public void setCfPers_Prize(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Prize cfPers_Prize) {
        this.cfPers_Prize = cfPers_Prize;
    }

    /**
     * Sets the value of field 'cfPers_Qual'.
     * 
     * @param cfPers_Qual the value of field 'cfPers_Qual'.
     */
    public void setCfPers_Qual(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Qual cfPers_Qual) {
        this.cfPers_Qual = cfPers_Qual;
    }

    /**
     * Sets the value of field 'cfPers_ResPat'.
     * 
     * @param cfPers_ResPat the value of field 'cfPers_ResPat'.
     */
    public void setCfPers_ResPat(final gr.ekt.cerif.schema.CfPers__TypeCfPers_ResPat cfPers_ResPat) {
        this.cfPers_ResPat = cfPers_ResPat;
    }

    /**
     * Sets the value of field 'cfPers_ResProd'.
     * 
     * @param cfPers_ResProd the value of field 'cfPers_ResProd'.
     */
    public void setCfPers_ResProd(final gr.ekt.cerif.schema.CfPers__TypeCfPers_ResProd cfPers_ResProd) {
        this.cfPers_ResProd = cfPers_ResProd;
    }

    /**
     * Sets the value of field 'cfPers_ResPubl'.
     * 
     * @param cfPers_ResPubl the value of field 'cfPers_ResPubl'.
     */
    public void setCfPers_ResPubl(final gr.ekt.cerif.schema.CfPers__TypeCfPers_ResPubl cfPers_ResPubl) {
        this.cfPers_ResPubl = cfPers_ResPubl;
    }

    /**
     * Sets the value of field 'cfPers_Srv'.
     * 
     * @param cfPers_Srv the value of field 'cfPers_Srv'.
     */
    public void setCfPers_Srv(final gr.ekt.cerif.schema.CfPers__TypeCfPers_Srv cfPers_Srv) {
        this.cfPers_Srv = cfPers_Srv;
    }

    /**
     * Sets the value of field 'cfProj_Pers'.
     * 
     * @param cfProj_Pers the value of field 'cfProj_Pers'.
     */
    public void setCfProj_Pers(final gr.ekt.cerif.schema.CfPers__TypeCfProj_Pers cfProj_Pers) {
        this.cfProj_Pers = cfProj_Pers;
    }

    /**
     * Sets the value of field 'cfResInt'.
     * 
     * @param cfResInt the value of field 'cfResInt'.
     */
    public void setCfResInt(final gr.ekt.cerif.schema.CfResInt cfResInt) {
        this.cfResInt = cfResInt;
    }

}

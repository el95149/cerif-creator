/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfPrize__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfPrize__TypeCfOrgUnit_Prize cfOrgUnit_Prize;

    private gr.ekt.cerif.schema.CfPrize__TypeCfPers_Prize cfPers_Prize;

    private gr.ekt.cerif.schema.CfPrize__TypeCfProj_Prize cfProj_Prize;

    private gr.ekt.cerif.schema.CfPrize__TypeCfPrize_Class cfPrize_Class;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfPrize__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Prize'.
     * 
     * @return the value of field 'CfOrgUnit_Prize'.
     */
    public gr.ekt.cerif.schema.CfPrize__TypeCfOrgUnit_Prize getCfOrgUnit_Prize() {
        return this.cfOrgUnit_Prize;
    }

    /**
     * Returns the value of field 'cfPers_Prize'.
     * 
     * @return the value of field 'CfPers_Prize'.
     */
    public gr.ekt.cerif.schema.CfPrize__TypeCfPers_Prize getCfPers_Prize() {
        return this.cfPers_Prize;
    }

    /**
     * Returns the value of field 'cfPrize_Class'.
     * 
     * @return the value of field 'CfPrize_Class'.
     */
    public gr.ekt.cerif.schema.CfPrize__TypeCfPrize_Class getCfPrize_Class() {
        return this.cfPrize_Class;
    }

    /**
     * Returns the value of field 'cfProj_Prize'.
     * 
     * @return the value of field 'CfProj_Prize'.
     */
    public gr.ekt.cerif.schema.CfPrize__TypeCfProj_Prize getCfProj_Prize() {
        return this.cfProj_Prize;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Prize'.
     * 
     * @param cfOrgUnit_Prize the value of field 'cfOrgUnit_Prize'.
     */
    public void setCfOrgUnit_Prize(final gr.ekt.cerif.schema.CfPrize__TypeCfOrgUnit_Prize cfOrgUnit_Prize) {
        this.cfOrgUnit_Prize = cfOrgUnit_Prize;
    }

    /**
     * Sets the value of field 'cfPers_Prize'.
     * 
     * @param cfPers_Prize the value of field 'cfPers_Prize'.
     */
    public void setCfPers_Prize(final gr.ekt.cerif.schema.CfPrize__TypeCfPers_Prize cfPers_Prize) {
        this.cfPers_Prize = cfPers_Prize;
    }

    /**
     * Sets the value of field 'cfPrize_Class'.
     * 
     * @param cfPrize_Class the value of field 'cfPrize_Class'.
     */
    public void setCfPrize_Class(final gr.ekt.cerif.schema.CfPrize__TypeCfPrize_Class cfPrize_Class) {
        this.cfPrize_Class = cfPrize_Class;
    }

    /**
     * Sets the value of field 'cfProj_Prize'.
     * 
     * @param cfProj_Prize the value of field 'cfProj_Prize'.
     */
    public void setCfProj_Prize(final gr.ekt.cerif.schema.CfPrize__TypeCfProj_Prize cfProj_Prize) {
        this.cfProj_Prize = cfProj_Prize;
    }

}

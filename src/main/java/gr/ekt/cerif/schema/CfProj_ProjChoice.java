/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfProj_ProjChoice implements java.io.Serializable {

    private java.lang.String cfProjId2;

    private java.lang.String cfProjId1;

    public CfProj_ProjChoice() {
        super();
    }

    /**
     * Returns the value of field 'cfProjId1'.
     * 
     * @return the value of field 'CfProjId1'.
     */
    public java.lang.String getCfProjId1() {
        return this.cfProjId1;
    }

    /**
     * Returns the value of field 'cfProjId2'.
     * 
     * @return the value of field 'CfProjId2'.
     */
    public java.lang.String getCfProjId2() {
        return this.cfProjId2;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfProjId1'.
     * 
     * @param cfProjId1 the value of field 'cfProjId1'.
     */
    public void setCfProjId1(final java.lang.String cfProjId1) {
        this.cfProjId1 = cfProjId1;
    }

    /**
     * Sets the value of field 'cfProjId2'.
     * 
     * @param cfProjId2 the value of field 'cfProjId2'.
     */
    public void setCfProjId2(final java.lang.String cfProjId2) {
        this.cfProjId2 = cfProjId2;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfProj_ProjChoice
     */
    public static gr.ekt.cerif.schema.CfProj_ProjChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfProj_ProjChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfProj_ProjChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

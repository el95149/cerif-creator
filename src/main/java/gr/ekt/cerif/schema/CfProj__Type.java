/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfProj__Type implements java.io.Serializable {

    private java.lang.String cfProjId;

    private org.exolab.castor.types.Date cfStartDate;

    private org.exolab.castor.types.Date cfEndDate;

    private java.lang.String cfAcro;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfProj__TypeChoice cfProj__TypeChoice;

    public CfProj__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfAcro'.
     * 
     * @return the value of field 'CfAcro'.
     */
    public java.lang.String getCfAcro() {
        return this.cfAcro;
    }

    /**
     * Returns the value of field 'cfEndDate'.
     * 
     * @return the value of field 'CfEndDate'.
     */
    public org.exolab.castor.types.Date getCfEndDate() {
        return this.cfEndDate;
    }

    /**
     * Returns the value of field 'cfProjId'.
     * 
     * @return the value of field 'CfProjId'.
     */
    public java.lang.String getCfProjId() {
        return this.cfProjId;
    }

    /**
     * Returns the value of field 'cfProj__TypeChoice'.
     * 
     * @return the value of field 'CfProj__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeChoice getCfProj__TypeChoice() {
        return this.cfProj__TypeChoice;
    }

    /**
     * Returns the value of field 'cfStartDate'.
     * 
     * @return the value of field 'CfStartDate'.
     */
    public org.exolab.castor.types.Date getCfStartDate() {
        return this.cfStartDate;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfAcro'.
     * 
     * @param cfAcro the value of field 'cfAcro'.
     */
    public void setCfAcro(final java.lang.String cfAcro) {
        this.cfAcro = cfAcro;
    }

    /**
     * Sets the value of field 'cfEndDate'.
     * 
     * @param cfEndDate the value of field 'cfEndDate'.
     */
    public void setCfEndDate(final org.exolab.castor.types.Date cfEndDate) {
        this.cfEndDate = cfEndDate;
    }

    /**
     * Sets the value of field 'cfProjId'.
     * 
     * @param cfProjId the value of field 'cfProjId'.
     */
    public void setCfProjId(final java.lang.String cfProjId) {
        this.cfProjId = cfProjId;
    }

    /**
     * Sets the value of field 'cfProj__TypeChoice'.
     * 
     * @param cfProj__TypeChoice the value of field
     * 'cfProj__TypeChoice'.
     */
    public void setCfProj__TypeChoice(final gr.ekt.cerif.schema.CfProj__TypeChoice cfProj__TypeChoice) {
        this.cfProj__TypeChoice = cfProj__TypeChoice;
    }

    /**
     * Sets the value of field 'cfStartDate'.
     * 
     * @param cfStartDate the value of field 'cfStartDate'.
     */
    public void setCfStartDate(final org.exolab.castor.types.Date cfStartDate) {
        this.cfStartDate = cfStartDate;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfProj__Type
     */
    public static gr.ekt.cerif.schema.CfProj__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfProj__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfProj__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

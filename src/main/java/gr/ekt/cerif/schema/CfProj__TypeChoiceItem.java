/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfProj__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfTitle cfTitle;

    private gr.ekt.cerif.schema.CfAbstr cfAbstr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_Class cfProj_Class;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_Equip cfProj_Equip;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_Event cfProj_Event;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_Facil cfProj_Facil;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_Fund cfProj_Fund;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_OrgUnit cfProj_OrgUnit;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_Pers cfProj_Pers;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_Prize cfProj_Prize;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_ResPat cfProj_ResPat;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_Proj cfProj_Proj;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_ResProd cfProj_ResProd;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_ResPubl cfProj_ResPubl;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_DC cfProj_DC;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_Srv cfProj_Srv;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_Medium cfProj_Medium;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_Meas cfProj_Meas;

    private gr.ekt.cerif.schema.CfProj__TypeCfProj_Indic cfProj_Indic;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfProj__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfAbstr'.
     * 
     * @return the value of field 'CfAbstr'.
     */
    public gr.ekt.cerif.schema.CfAbstr getCfAbstr() {
        return this.cfAbstr;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfProj_Class'.
     * 
     * @return the value of field 'CfProj_Class'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_Class getCfProj_Class() {
        return this.cfProj_Class;
    }

    /**
     * Returns the value of field 'cfProj_DC'.
     * 
     * @return the value of field 'CfProj_DC'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_DC getCfProj_DC() {
        return this.cfProj_DC;
    }

    /**
     * Returns the value of field 'cfProj_Equip'.
     * 
     * @return the value of field 'CfProj_Equip'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_Equip getCfProj_Equip() {
        return this.cfProj_Equip;
    }

    /**
     * Returns the value of field 'cfProj_Event'.
     * 
     * @return the value of field 'CfProj_Event'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_Event getCfProj_Event() {
        return this.cfProj_Event;
    }

    /**
     * Returns the value of field 'cfProj_Facil'.
     * 
     * @return the value of field 'CfProj_Facil'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_Facil getCfProj_Facil() {
        return this.cfProj_Facil;
    }

    /**
     * Returns the value of field 'cfProj_Fund'.
     * 
     * @return the value of field 'CfProj_Fund'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_Fund getCfProj_Fund() {
        return this.cfProj_Fund;
    }

    /**
     * Returns the value of field 'cfProj_Indic'.
     * 
     * @return the value of field 'CfProj_Indic'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_Indic getCfProj_Indic() {
        return this.cfProj_Indic;
    }

    /**
     * Returns the value of field 'cfProj_Meas'.
     * 
     * @return the value of field 'CfProj_Meas'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_Meas getCfProj_Meas() {
        return this.cfProj_Meas;
    }

    /**
     * Returns the value of field 'cfProj_Medium'.
     * 
     * @return the value of field 'CfProj_Medium'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_Medium getCfProj_Medium() {
        return this.cfProj_Medium;
    }

    /**
     * Returns the value of field 'cfProj_OrgUnit'.
     * 
     * @return the value of field 'CfProj_OrgUnit'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_OrgUnit getCfProj_OrgUnit() {
        return this.cfProj_OrgUnit;
    }

    /**
     * Returns the value of field 'cfProj_Pers'.
     * 
     * @return the value of field 'CfProj_Pers'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_Pers getCfProj_Pers() {
        return this.cfProj_Pers;
    }

    /**
     * Returns the value of field 'cfProj_Prize'.
     * 
     * @return the value of field 'CfProj_Prize'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_Prize getCfProj_Prize() {
        return this.cfProj_Prize;
    }

    /**
     * Returns the value of field 'cfProj_Proj'.
     * 
     * @return the value of field 'CfProj_Proj'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_Proj getCfProj_Proj() {
        return this.cfProj_Proj;
    }

    /**
     * Returns the value of field 'cfProj_ResPat'.
     * 
     * @return the value of field 'CfProj_ResPat'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_ResPat getCfProj_ResPat() {
        return this.cfProj_ResPat;
    }

    /**
     * Returns the value of field 'cfProj_ResProd'.
     * 
     * @return the value of field 'CfProj_ResProd'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_ResProd getCfProj_ResProd() {
        return this.cfProj_ResProd;
    }

    /**
     * Returns the value of field 'cfProj_ResPubl'.
     * 
     * @return the value of field 'CfProj_ResPubl'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_ResPubl getCfProj_ResPubl() {
        return this.cfProj_ResPubl;
    }

    /**
     * Returns the value of field 'cfProj_Srv'.
     * 
     * @return the value of field 'CfProj_Srv'.
     */
    public gr.ekt.cerif.schema.CfProj__TypeCfProj_Srv getCfProj_Srv() {
        return this.cfProj_Srv;
    }

    /**
     * Returns the value of field 'cfTitle'.
     * 
     * @return the value of field 'CfTitle'.
     */
    public gr.ekt.cerif.schema.CfTitle getCfTitle() {
        return this.cfTitle;
    }

    /**
     * Sets the value of field 'cfAbstr'.
     * 
     * @param cfAbstr the value of field 'cfAbstr'.
     */
    public void setCfAbstr(final gr.ekt.cerif.schema.CfAbstr cfAbstr) {
        this.cfAbstr = cfAbstr;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfProj_Class'.
     * 
     * @param cfProj_Class the value of field 'cfProj_Class'.
     */
    public void setCfProj_Class(final gr.ekt.cerif.schema.CfProj__TypeCfProj_Class cfProj_Class) {
        this.cfProj_Class = cfProj_Class;
    }

    /**
     * Sets the value of field 'cfProj_DC'.
     * 
     * @param cfProj_DC the value of field 'cfProj_DC'.
     */
    public void setCfProj_DC(final gr.ekt.cerif.schema.CfProj__TypeCfProj_DC cfProj_DC) {
        this.cfProj_DC = cfProj_DC;
    }

    /**
     * Sets the value of field 'cfProj_Equip'.
     * 
     * @param cfProj_Equip the value of field 'cfProj_Equip'.
     */
    public void setCfProj_Equip(final gr.ekt.cerif.schema.CfProj__TypeCfProj_Equip cfProj_Equip) {
        this.cfProj_Equip = cfProj_Equip;
    }

    /**
     * Sets the value of field 'cfProj_Event'.
     * 
     * @param cfProj_Event the value of field 'cfProj_Event'.
     */
    public void setCfProj_Event(final gr.ekt.cerif.schema.CfProj__TypeCfProj_Event cfProj_Event) {
        this.cfProj_Event = cfProj_Event;
    }

    /**
     * Sets the value of field 'cfProj_Facil'.
     * 
     * @param cfProj_Facil the value of field 'cfProj_Facil'.
     */
    public void setCfProj_Facil(final gr.ekt.cerif.schema.CfProj__TypeCfProj_Facil cfProj_Facil) {
        this.cfProj_Facil = cfProj_Facil;
    }

    /**
     * Sets the value of field 'cfProj_Fund'.
     * 
     * @param cfProj_Fund the value of field 'cfProj_Fund'.
     */
    public void setCfProj_Fund(final gr.ekt.cerif.schema.CfProj__TypeCfProj_Fund cfProj_Fund) {
        this.cfProj_Fund = cfProj_Fund;
    }

    /**
     * Sets the value of field 'cfProj_Indic'.
     * 
     * @param cfProj_Indic the value of field 'cfProj_Indic'.
     */
    public void setCfProj_Indic(final gr.ekt.cerif.schema.CfProj__TypeCfProj_Indic cfProj_Indic) {
        this.cfProj_Indic = cfProj_Indic;
    }

    /**
     * Sets the value of field 'cfProj_Meas'.
     * 
     * @param cfProj_Meas the value of field 'cfProj_Meas'.
     */
    public void setCfProj_Meas(final gr.ekt.cerif.schema.CfProj__TypeCfProj_Meas cfProj_Meas) {
        this.cfProj_Meas = cfProj_Meas;
    }

    /**
     * Sets the value of field 'cfProj_Medium'.
     * 
     * @param cfProj_Medium the value of field 'cfProj_Medium'.
     */
    public void setCfProj_Medium(final gr.ekt.cerif.schema.CfProj__TypeCfProj_Medium cfProj_Medium) {
        this.cfProj_Medium = cfProj_Medium;
    }

    /**
     * Sets the value of field 'cfProj_OrgUnit'.
     * 
     * @param cfProj_OrgUnit the value of field 'cfProj_OrgUnit'.
     */
    public void setCfProj_OrgUnit(final gr.ekt.cerif.schema.CfProj__TypeCfProj_OrgUnit cfProj_OrgUnit) {
        this.cfProj_OrgUnit = cfProj_OrgUnit;
    }

    /**
     * Sets the value of field 'cfProj_Pers'.
     * 
     * @param cfProj_Pers the value of field 'cfProj_Pers'.
     */
    public void setCfProj_Pers(final gr.ekt.cerif.schema.CfProj__TypeCfProj_Pers cfProj_Pers) {
        this.cfProj_Pers = cfProj_Pers;
    }

    /**
     * Sets the value of field 'cfProj_Prize'.
     * 
     * @param cfProj_Prize the value of field 'cfProj_Prize'.
     */
    public void setCfProj_Prize(final gr.ekt.cerif.schema.CfProj__TypeCfProj_Prize cfProj_Prize) {
        this.cfProj_Prize = cfProj_Prize;
    }

    /**
     * Sets the value of field 'cfProj_Proj'.
     * 
     * @param cfProj_Proj the value of field 'cfProj_Proj'.
     */
    public void setCfProj_Proj(final gr.ekt.cerif.schema.CfProj__TypeCfProj_Proj cfProj_Proj) {
        this.cfProj_Proj = cfProj_Proj;
    }

    /**
     * Sets the value of field 'cfProj_ResPat'.
     * 
     * @param cfProj_ResPat the value of field 'cfProj_ResPat'.
     */
    public void setCfProj_ResPat(final gr.ekt.cerif.schema.CfProj__TypeCfProj_ResPat cfProj_ResPat) {
        this.cfProj_ResPat = cfProj_ResPat;
    }

    /**
     * Sets the value of field 'cfProj_ResProd'.
     * 
     * @param cfProj_ResProd the value of field 'cfProj_ResProd'.
     */
    public void setCfProj_ResProd(final gr.ekt.cerif.schema.CfProj__TypeCfProj_ResProd cfProj_ResProd) {
        this.cfProj_ResProd = cfProj_ResProd;
    }

    /**
     * Sets the value of field 'cfProj_ResPubl'.
     * 
     * @param cfProj_ResPubl the value of field 'cfProj_ResPubl'.
     */
    public void setCfProj_ResPubl(final gr.ekt.cerif.schema.CfProj__TypeCfProj_ResPubl cfProj_ResPubl) {
        this.cfProj_ResPubl = cfProj_ResPubl;
    }

    /**
     * Sets the value of field 'cfProj_Srv'.
     * 
     * @param cfProj_Srv the value of field 'cfProj_Srv'.
     */
    public void setCfProj_Srv(final gr.ekt.cerif.schema.CfProj__TypeCfProj_Srv cfProj_Srv) {
        this.cfProj_Srv = cfProj_Srv;
    }

    /**
     * Sets the value of field 'cfTitle'.
     * 
     * @param cfTitle the value of field 'cfTitle'.
     */
    public void setCfTitle(final gr.ekt.cerif.schema.CfTitle cfTitle) {
        this.cfTitle = cfTitle;
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfQual__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfTitle cfTitle;

    private gr.ekt.cerif.schema.CfQual__TypeCfPers_Qual cfPers_Qual;

    private gr.ekt.cerif.schema.CfQual__TypeCfQual_Class cfQual_Class;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfQual__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfPers_Qual'.
     * 
     * @return the value of field 'CfPers_Qual'.
     */
    public gr.ekt.cerif.schema.CfQual__TypeCfPers_Qual getCfPers_Qual() {
        return this.cfPers_Qual;
    }

    /**
     * Returns the value of field 'cfQual_Class'.
     * 
     * @return the value of field 'CfQual_Class'.
     */
    public gr.ekt.cerif.schema.CfQual__TypeCfQual_Class getCfQual_Class() {
        return this.cfQual_Class;
    }

    /**
     * Returns the value of field 'cfTitle'.
     * 
     * @return the value of field 'CfTitle'.
     */
    public gr.ekt.cerif.schema.CfTitle getCfTitle() {
        return this.cfTitle;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfPers_Qual'.
     * 
     * @param cfPers_Qual the value of field 'cfPers_Qual'.
     */
    public void setCfPers_Qual(final gr.ekt.cerif.schema.CfQual__TypeCfPers_Qual cfPers_Qual) {
        this.cfPers_Qual = cfPers_Qual;
    }

    /**
     * Sets the value of field 'cfQual_Class'.
     * 
     * @param cfQual_Class the value of field 'cfQual_Class'.
     */
    public void setCfQual_Class(final gr.ekt.cerif.schema.CfQual__TypeCfQual_Class cfQual_Class) {
        this.cfQual_Class = cfQual_Class;
    }

    /**
     * Sets the value of field 'cfTitle'.
     * 
     * @param cfTitle the value of field 'cfTitle'.
     */
    public void setCfTitle(final gr.ekt.cerif.schema.CfTitle cfTitle) {
        this.cfTitle = cfTitle;
    }

}

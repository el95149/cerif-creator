/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfResPat_ResPatChoice implements java.io.Serializable {

    private java.lang.String cfResPatId2;

    private java.lang.String cfResPatId1;

    public CfResPat_ResPatChoice() {
        super();
    }

    /**
     * Returns the value of field 'cfResPatId1'.
     * 
     * @return the value of field 'CfResPatId1'.
     */
    public java.lang.String getCfResPatId1() {
        return this.cfResPatId1;
    }

    /**
     * Returns the value of field 'cfResPatId2'.
     * 
     * @return the value of field 'CfResPatId2'.
     */
    public java.lang.String getCfResPatId2() {
        return this.cfResPatId2;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfResPatId1'.
     * 
     * @param cfResPatId1 the value of field 'cfResPatId1'.
     */
    public void setCfResPatId1(final java.lang.String cfResPatId1) {
        this.cfResPatId1 = cfResPatId1;
    }

    /**
     * Sets the value of field 'cfResPatId2'.
     * 
     * @param cfResPatId2 the value of field 'cfResPatId2'.
     */
    public void setCfResPatId2(final java.lang.String cfResPatId2) {
        this.cfResPatId2 = cfResPatId2;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfResPat_ResPatChoice
     */
    public static gr.ekt.cerif.schema.CfResPat_ResPatChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfResPat_ResPatChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfResPat_ResPatChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfResPat__Type implements java.io.Serializable {

    private java.lang.String cfResPatId;

    private java.lang.String cfCountryCode;

    private org.exolab.castor.types.Date cfRegistrDate;

    private org.exolab.castor.types.Date cfApprovDate;

    private java.lang.String cfPatentNum;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfResPat__TypeChoice cfResPat__TypeChoice;

    public CfResPat__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfApprovDate'.
     * 
     * @return the value of field 'CfApprovDate'.
     */
    public org.exolab.castor.types.Date getCfApprovDate() {
        return this.cfApprovDate;
    }

    /**
     * Returns the value of field 'cfCountryCode'.
     * 
     * @return the value of field 'CfCountryCode'.
     */
    public java.lang.String getCfCountryCode() {
        return this.cfCountryCode;
    }

    /**
     * Returns the value of field 'cfPatentNum'.
     * 
     * @return the value of field 'CfPatentNum'.
     */
    public java.lang.String getCfPatentNum() {
        return this.cfPatentNum;
    }

    /**
     * Returns the value of field 'cfRegistrDate'.
     * 
     * @return the value of field 'CfRegistrDate'.
     */
    public org.exolab.castor.types.Date getCfRegistrDate() {
        return this.cfRegistrDate;
    }

    /**
     * Returns the value of field 'cfResPatId'.
     * 
     * @return the value of field 'CfResPatId'.
     */
    public java.lang.String getCfResPatId() {
        return this.cfResPatId;
    }

    /**
     * Returns the value of field 'cfResPat__TypeChoice'.
     * 
     * @return the value of field 'CfResPat__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeChoice getCfResPat__TypeChoice() {
        return this.cfResPat__TypeChoice;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfApprovDate'.
     * 
     * @param cfApprovDate the value of field 'cfApprovDate'.
     */
    public void setCfApprovDate(final org.exolab.castor.types.Date cfApprovDate) {
        this.cfApprovDate = cfApprovDate;
    }

    /**
     * Sets the value of field 'cfCountryCode'.
     * 
     * @param cfCountryCode the value of field 'cfCountryCode'.
     */
    public void setCfCountryCode(final java.lang.String cfCountryCode) {
        this.cfCountryCode = cfCountryCode;
    }

    /**
     * Sets the value of field 'cfPatentNum'.
     * 
     * @param cfPatentNum the value of field 'cfPatentNum'.
     */
    public void setCfPatentNum(final java.lang.String cfPatentNum) {
        this.cfPatentNum = cfPatentNum;
    }

    /**
     * Sets the value of field 'cfRegistrDate'.
     * 
     * @param cfRegistrDate the value of field 'cfRegistrDate'.
     */
    public void setCfRegistrDate(final org.exolab.castor.types.Date cfRegistrDate) {
        this.cfRegistrDate = cfRegistrDate;
    }

    /**
     * Sets the value of field 'cfResPatId'.
     * 
     * @param cfResPatId the value of field 'cfResPatId'.
     */
    public void setCfResPatId(final java.lang.String cfResPatId) {
        this.cfResPatId = cfResPatId;
    }

    /**
     * Sets the value of field 'cfResPat__TypeChoice'.
     * 
     * @param cfResPat__TypeChoice the value of field
     * 'cfResPat__TypeChoice'.
     */
    public void setCfResPat__TypeChoice(final gr.ekt.cerif.schema.CfResPat__TypeChoice cfResPat__TypeChoice) {
        this.cfResPat__TypeChoice = cfResPat__TypeChoice;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfResPat__Type
     */
    public static gr.ekt.cerif.schema.CfResPat__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfResPat__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfResPat__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfResPat__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfTitle cfTitle;

    private gr.ekt.cerif.schema.CfAbstr cfAbstr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfVersInfo cfVersInfo;

    private gr.ekt.cerif.schema.CfResPat__TypeCfOrgUnit_ResPat cfOrgUnit_ResPat;

    private gr.ekt.cerif.schema.CfResPat__TypeCfPers_ResPat cfPers_ResPat;

    private gr.ekt.cerif.schema.CfResPat__TypeCfProj_ResPat cfProj_ResPat;

    private gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Class cfResPat_Class;

    private gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Fund cfResPat_Fund;

    private gr.ekt.cerif.schema.CfResPat__TypeCfResPubl_ResPat cfResPubl_ResPat;

    private gr.ekt.cerif.schema.CfResPat__TypeCfResPat_ResPat cfResPat_ResPat;

    private gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Medium cfResPat_Medium;

    private gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Facil cfResPat_Facil;

    private gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Srv cfResPat_Srv;

    private gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Equip cfResPat_Equip;

    private gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Meas cfResPat_Meas;

    private gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Indic cfResPat_Indic;

    private gr.ekt.cerif.schema.CfResPat__TypeCfResProd_ResPat cfResProd_ResPat;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfResPat__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfAbstr'.
     * 
     * @return the value of field 'CfAbstr'.
     */
    public gr.ekt.cerif.schema.CfAbstr getCfAbstr() {
        return this.cfAbstr;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfOrgUnit_ResPat'.
     * 
     * @return the value of field 'CfOrgUnit_ResPat'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfOrgUnit_ResPat getCfOrgUnit_ResPat() {
        return this.cfOrgUnit_ResPat;
    }

    /**
     * Returns the value of field 'cfPers_ResPat'.
     * 
     * @return the value of field 'CfPers_ResPat'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfPers_ResPat getCfPers_ResPat() {
        return this.cfPers_ResPat;
    }

    /**
     * Returns the value of field 'cfProj_ResPat'.
     * 
     * @return the value of field 'CfProj_ResPat'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfProj_ResPat getCfProj_ResPat() {
        return this.cfProj_ResPat;
    }

    /**
     * Returns the value of field 'cfResPat_Class'.
     * 
     * @return the value of field 'CfResPat_Class'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Class getCfResPat_Class() {
        return this.cfResPat_Class;
    }

    /**
     * Returns the value of field 'cfResPat_Equip'.
     * 
     * @return the value of field 'CfResPat_Equip'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Equip getCfResPat_Equip() {
        return this.cfResPat_Equip;
    }

    /**
     * Returns the value of field 'cfResPat_Facil'.
     * 
     * @return the value of field 'CfResPat_Facil'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Facil getCfResPat_Facil() {
        return this.cfResPat_Facil;
    }

    /**
     * Returns the value of field 'cfResPat_Fund'.
     * 
     * @return the value of field 'CfResPat_Fund'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Fund getCfResPat_Fund() {
        return this.cfResPat_Fund;
    }

    /**
     * Returns the value of field 'cfResPat_Indic'.
     * 
     * @return the value of field 'CfResPat_Indic'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Indic getCfResPat_Indic() {
        return this.cfResPat_Indic;
    }

    /**
     * Returns the value of field 'cfResPat_Meas'.
     * 
     * @return the value of field 'CfResPat_Meas'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Meas getCfResPat_Meas() {
        return this.cfResPat_Meas;
    }

    /**
     * Returns the value of field 'cfResPat_Medium'.
     * 
     * @return the value of field 'CfResPat_Medium'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Medium getCfResPat_Medium() {
        return this.cfResPat_Medium;
    }

    /**
     * Returns the value of field 'cfResPat_ResPat'.
     * 
     * @return the value of field 'CfResPat_ResPat'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfResPat_ResPat getCfResPat_ResPat() {
        return this.cfResPat_ResPat;
    }

    /**
     * Returns the value of field 'cfResPat_Srv'.
     * 
     * @return the value of field 'CfResPat_Srv'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Srv getCfResPat_Srv() {
        return this.cfResPat_Srv;
    }

    /**
     * Returns the value of field 'cfResProd_ResPat'.
     * 
     * @return the value of field 'CfResProd_ResPat'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfResProd_ResPat getCfResProd_ResPat() {
        return this.cfResProd_ResPat;
    }

    /**
     * Returns the value of field 'cfResPubl_ResPat'.
     * 
     * @return the value of field 'CfResPubl_ResPat'.
     */
    public gr.ekt.cerif.schema.CfResPat__TypeCfResPubl_ResPat getCfResPubl_ResPat() {
        return this.cfResPubl_ResPat;
    }

    /**
     * Returns the value of field 'cfTitle'.
     * 
     * @return the value of field 'CfTitle'.
     */
    public gr.ekt.cerif.schema.CfTitle getCfTitle() {
        return this.cfTitle;
    }

    /**
     * Returns the value of field 'cfVersInfo'.
     * 
     * @return the value of field 'CfVersInfo'.
     */
    public gr.ekt.cerif.schema.CfVersInfo getCfVersInfo() {
        return this.cfVersInfo;
    }

    /**
     * Sets the value of field 'cfAbstr'.
     * 
     * @param cfAbstr the value of field 'cfAbstr'.
     */
    public void setCfAbstr(final gr.ekt.cerif.schema.CfAbstr cfAbstr) {
        this.cfAbstr = cfAbstr;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfOrgUnit_ResPat'.
     * 
     * @param cfOrgUnit_ResPat the value of field 'cfOrgUnit_ResPat'
     */
    public void setCfOrgUnit_ResPat(final gr.ekt.cerif.schema.CfResPat__TypeCfOrgUnit_ResPat cfOrgUnit_ResPat) {
        this.cfOrgUnit_ResPat = cfOrgUnit_ResPat;
    }

    /**
     * Sets the value of field 'cfPers_ResPat'.
     * 
     * @param cfPers_ResPat the value of field 'cfPers_ResPat'.
     */
    public void setCfPers_ResPat(final gr.ekt.cerif.schema.CfResPat__TypeCfPers_ResPat cfPers_ResPat) {
        this.cfPers_ResPat = cfPers_ResPat;
    }

    /**
     * Sets the value of field 'cfProj_ResPat'.
     * 
     * @param cfProj_ResPat the value of field 'cfProj_ResPat'.
     */
    public void setCfProj_ResPat(final gr.ekt.cerif.schema.CfResPat__TypeCfProj_ResPat cfProj_ResPat) {
        this.cfProj_ResPat = cfProj_ResPat;
    }

    /**
     * Sets the value of field 'cfResPat_Class'.
     * 
     * @param cfResPat_Class the value of field 'cfResPat_Class'.
     */
    public void setCfResPat_Class(final gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Class cfResPat_Class) {
        this.cfResPat_Class = cfResPat_Class;
    }

    /**
     * Sets the value of field 'cfResPat_Equip'.
     * 
     * @param cfResPat_Equip the value of field 'cfResPat_Equip'.
     */
    public void setCfResPat_Equip(final gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Equip cfResPat_Equip) {
        this.cfResPat_Equip = cfResPat_Equip;
    }

    /**
     * Sets the value of field 'cfResPat_Facil'.
     * 
     * @param cfResPat_Facil the value of field 'cfResPat_Facil'.
     */
    public void setCfResPat_Facil(final gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Facil cfResPat_Facil) {
        this.cfResPat_Facil = cfResPat_Facil;
    }

    /**
     * Sets the value of field 'cfResPat_Fund'.
     * 
     * @param cfResPat_Fund the value of field 'cfResPat_Fund'.
     */
    public void setCfResPat_Fund(final gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Fund cfResPat_Fund) {
        this.cfResPat_Fund = cfResPat_Fund;
    }

    /**
     * Sets the value of field 'cfResPat_Indic'.
     * 
     * @param cfResPat_Indic the value of field 'cfResPat_Indic'.
     */
    public void setCfResPat_Indic(final gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Indic cfResPat_Indic) {
        this.cfResPat_Indic = cfResPat_Indic;
    }

    /**
     * Sets the value of field 'cfResPat_Meas'.
     * 
     * @param cfResPat_Meas the value of field 'cfResPat_Meas'.
     */
    public void setCfResPat_Meas(final gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Meas cfResPat_Meas) {
        this.cfResPat_Meas = cfResPat_Meas;
    }

    /**
     * Sets the value of field 'cfResPat_Medium'.
     * 
     * @param cfResPat_Medium the value of field 'cfResPat_Medium'.
     */
    public void setCfResPat_Medium(final gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Medium cfResPat_Medium) {
        this.cfResPat_Medium = cfResPat_Medium;
    }

    /**
     * Sets the value of field 'cfResPat_ResPat'.
     * 
     * @param cfResPat_ResPat the value of field 'cfResPat_ResPat'.
     */
    public void setCfResPat_ResPat(final gr.ekt.cerif.schema.CfResPat__TypeCfResPat_ResPat cfResPat_ResPat) {
        this.cfResPat_ResPat = cfResPat_ResPat;
    }

    /**
     * Sets the value of field 'cfResPat_Srv'.
     * 
     * @param cfResPat_Srv the value of field 'cfResPat_Srv'.
     */
    public void setCfResPat_Srv(final gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Srv cfResPat_Srv) {
        this.cfResPat_Srv = cfResPat_Srv;
    }

    /**
     * Sets the value of field 'cfResProd_ResPat'.
     * 
     * @param cfResProd_ResPat the value of field 'cfResProd_ResPat'
     */
    public void setCfResProd_ResPat(final gr.ekt.cerif.schema.CfResPat__TypeCfResProd_ResPat cfResProd_ResPat) {
        this.cfResProd_ResPat = cfResProd_ResPat;
    }

    /**
     * Sets the value of field 'cfResPubl_ResPat'.
     * 
     * @param cfResPubl_ResPat the value of field 'cfResPubl_ResPat'
     */
    public void setCfResPubl_ResPat(final gr.ekt.cerif.schema.CfResPat__TypeCfResPubl_ResPat cfResPubl_ResPat) {
        this.cfResPubl_ResPat = cfResPubl_ResPat;
    }

    /**
     * Sets the value of field 'cfTitle'.
     * 
     * @param cfTitle the value of field 'cfTitle'.
     */
    public void setCfTitle(final gr.ekt.cerif.schema.CfTitle cfTitle) {
        this.cfTitle = cfTitle;
    }

    /**
     * Sets the value of field 'cfVersInfo'.
     * 
     * @param cfVersInfo the value of field 'cfVersInfo'.
     */
    public void setCfVersInfo(final gr.ekt.cerif.schema.CfVersInfo cfVersInfo) {
        this.cfVersInfo = cfVersInfo;
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfResProd__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfVersInfo cfVersInfo;

    private gr.ekt.cerif.schema.CfAltName cfAltName;

    private gr.ekt.cerif.schema.CfResProd__TypeCfOrgUnit_ResProd cfOrgUnit_ResProd;

    private gr.ekt.cerif.schema.CfResProd__TypeCfPers_ResProd cfPers_ResProd;

    private gr.ekt.cerif.schema.CfResProd__TypeCfProj_ResProd cfProj_ResProd;

    private gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Class cfResProd_Class;

    private gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Fund cfResProd_Fund;

    private gr.ekt.cerif.schema.CfResProd__TypeCfResPubl_ResProd cfResPubl_ResProd;

    private gr.ekt.cerif.schema.CfResProd__TypeCfResProd_ResProd cfResProd_ResProd;

    private gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Medium cfResProd_Medium;

    private gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Facil cfResProd_Facil;

    private gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Srv cfResProd_Srv;

    private gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Equip cfResProd_Equip;

    private gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Meas cfResProd_Meas;

    private gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Indic cfResProd_Indic;

    private gr.ekt.cerif.schema.CfResProd__TypeCfResProd_ResPat cfResProd_ResPat;

    private gr.ekt.cerif.schema.CfResProd__TypeCfResProd_GeoBBox cfResProd_GeoBBox;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfResProd__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfAltName'.
     * 
     * @return the value of field 'CfAltName'.
     */
    public gr.ekt.cerif.schema.CfAltName getCfAltName() {
        return this.cfAltName;
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfOrgUnit_ResProd'.
     * 
     * @return the value of field 'CfOrgUnit_ResProd'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfOrgUnit_ResProd getCfOrgUnit_ResProd() {
        return this.cfOrgUnit_ResProd;
    }

    /**
     * Returns the value of field 'cfPers_ResProd'.
     * 
     * @return the value of field 'CfPers_ResProd'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfPers_ResProd getCfPers_ResProd() {
        return this.cfPers_ResProd;
    }

    /**
     * Returns the value of field 'cfProj_ResProd'.
     * 
     * @return the value of field 'CfProj_ResProd'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfProj_ResProd getCfProj_ResProd() {
        return this.cfProj_ResProd;
    }

    /**
     * Returns the value of field 'cfResProd_Class'.
     * 
     * @return the value of field 'CfResProd_Class'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Class getCfResProd_Class() {
        return this.cfResProd_Class;
    }

    /**
     * Returns the value of field 'cfResProd_Equip'.
     * 
     * @return the value of field 'CfResProd_Equip'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Equip getCfResProd_Equip() {
        return this.cfResProd_Equip;
    }

    /**
     * Returns the value of field 'cfResProd_Facil'.
     * 
     * @return the value of field 'CfResProd_Facil'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Facil getCfResProd_Facil() {
        return this.cfResProd_Facil;
    }

    /**
     * Returns the value of field 'cfResProd_Fund'.
     * 
     * @return the value of field 'CfResProd_Fund'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Fund getCfResProd_Fund() {
        return this.cfResProd_Fund;
    }

    /**
     * Returns the value of field 'cfResProd_GeoBBox'.
     * 
     * @return the value of field 'CfResProd_GeoBBox'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfResProd_GeoBBox getCfResProd_GeoBBox() {
        return this.cfResProd_GeoBBox;
    }

    /**
     * Returns the value of field 'cfResProd_Indic'.
     * 
     * @return the value of field 'CfResProd_Indic'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Indic getCfResProd_Indic() {
        return this.cfResProd_Indic;
    }

    /**
     * Returns the value of field 'cfResProd_Meas'.
     * 
     * @return the value of field 'CfResProd_Meas'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Meas getCfResProd_Meas() {
        return this.cfResProd_Meas;
    }

    /**
     * Returns the value of field 'cfResProd_Medium'.
     * 
     * @return the value of field 'CfResProd_Medium'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Medium getCfResProd_Medium() {
        return this.cfResProd_Medium;
    }

    /**
     * Returns the value of field 'cfResProd_ResPat'.
     * 
     * @return the value of field 'CfResProd_ResPat'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfResProd_ResPat getCfResProd_ResPat() {
        return this.cfResProd_ResPat;
    }

    /**
     * Returns the value of field 'cfResProd_ResProd'.
     * 
     * @return the value of field 'CfResProd_ResProd'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfResProd_ResProd getCfResProd_ResProd() {
        return this.cfResProd_ResProd;
    }

    /**
     * Returns the value of field 'cfResProd_Srv'.
     * 
     * @return the value of field 'CfResProd_Srv'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Srv getCfResProd_Srv() {
        return this.cfResProd_Srv;
    }

    /**
     * Returns the value of field 'cfResPubl_ResProd'.
     * 
     * @return the value of field 'CfResPubl_ResProd'.
     */
    public gr.ekt.cerif.schema.CfResProd__TypeCfResPubl_ResProd getCfResPubl_ResProd() {
        return this.cfResPubl_ResProd;
    }

    /**
     * Returns the value of field 'cfVersInfo'.
     * 
     * @return the value of field 'CfVersInfo'.
     */
    public gr.ekt.cerif.schema.CfVersInfo getCfVersInfo() {
        return this.cfVersInfo;
    }

    /**
     * Sets the value of field 'cfAltName'.
     * 
     * @param cfAltName the value of field 'cfAltName'.
     */
    public void setCfAltName(final gr.ekt.cerif.schema.CfAltName cfAltName) {
        this.cfAltName = cfAltName;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfOrgUnit_ResProd'.
     * 
     * @param cfOrgUnit_ResProd the value of field
     * 'cfOrgUnit_ResProd'.
     */
    public void setCfOrgUnit_ResProd(final gr.ekt.cerif.schema.CfResProd__TypeCfOrgUnit_ResProd cfOrgUnit_ResProd) {
        this.cfOrgUnit_ResProd = cfOrgUnit_ResProd;
    }

    /**
     * Sets the value of field 'cfPers_ResProd'.
     * 
     * @param cfPers_ResProd the value of field 'cfPers_ResProd'.
     */
    public void setCfPers_ResProd(final gr.ekt.cerif.schema.CfResProd__TypeCfPers_ResProd cfPers_ResProd) {
        this.cfPers_ResProd = cfPers_ResProd;
    }

    /**
     * Sets the value of field 'cfProj_ResProd'.
     * 
     * @param cfProj_ResProd the value of field 'cfProj_ResProd'.
     */
    public void setCfProj_ResProd(final gr.ekt.cerif.schema.CfResProd__TypeCfProj_ResProd cfProj_ResProd) {
        this.cfProj_ResProd = cfProj_ResProd;
    }

    /**
     * Sets the value of field 'cfResProd_Class'.
     * 
     * @param cfResProd_Class the value of field 'cfResProd_Class'.
     */
    public void setCfResProd_Class(final gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Class cfResProd_Class) {
        this.cfResProd_Class = cfResProd_Class;
    }

    /**
     * Sets the value of field 'cfResProd_Equip'.
     * 
     * @param cfResProd_Equip the value of field 'cfResProd_Equip'.
     */
    public void setCfResProd_Equip(final gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Equip cfResProd_Equip) {
        this.cfResProd_Equip = cfResProd_Equip;
    }

    /**
     * Sets the value of field 'cfResProd_Facil'.
     * 
     * @param cfResProd_Facil the value of field 'cfResProd_Facil'.
     */
    public void setCfResProd_Facil(final gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Facil cfResProd_Facil) {
        this.cfResProd_Facil = cfResProd_Facil;
    }

    /**
     * Sets the value of field 'cfResProd_Fund'.
     * 
     * @param cfResProd_Fund the value of field 'cfResProd_Fund'.
     */
    public void setCfResProd_Fund(final gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Fund cfResProd_Fund) {
        this.cfResProd_Fund = cfResProd_Fund;
    }

    /**
     * Sets the value of field 'cfResProd_GeoBBox'.
     * 
     * @param cfResProd_GeoBBox the value of field
     * 'cfResProd_GeoBBox'.
     */
    public void setCfResProd_GeoBBox(final gr.ekt.cerif.schema.CfResProd__TypeCfResProd_GeoBBox cfResProd_GeoBBox) {
        this.cfResProd_GeoBBox = cfResProd_GeoBBox;
    }

    /**
     * Sets the value of field 'cfResProd_Indic'.
     * 
     * @param cfResProd_Indic the value of field 'cfResProd_Indic'.
     */
    public void setCfResProd_Indic(final gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Indic cfResProd_Indic) {
        this.cfResProd_Indic = cfResProd_Indic;
    }

    /**
     * Sets the value of field 'cfResProd_Meas'.
     * 
     * @param cfResProd_Meas the value of field 'cfResProd_Meas'.
     */
    public void setCfResProd_Meas(final gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Meas cfResProd_Meas) {
        this.cfResProd_Meas = cfResProd_Meas;
    }

    /**
     * Sets the value of field 'cfResProd_Medium'.
     * 
     * @param cfResProd_Medium the value of field 'cfResProd_Medium'
     */
    public void setCfResProd_Medium(final gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Medium cfResProd_Medium) {
        this.cfResProd_Medium = cfResProd_Medium;
    }

    /**
     * Sets the value of field 'cfResProd_ResPat'.
     * 
     * @param cfResProd_ResPat the value of field 'cfResProd_ResPat'
     */
    public void setCfResProd_ResPat(final gr.ekt.cerif.schema.CfResProd__TypeCfResProd_ResPat cfResProd_ResPat) {
        this.cfResProd_ResPat = cfResProd_ResPat;
    }

    /**
     * Sets the value of field 'cfResProd_ResProd'.
     * 
     * @param cfResProd_ResProd the value of field
     * 'cfResProd_ResProd'.
     */
    public void setCfResProd_ResProd(final gr.ekt.cerif.schema.CfResProd__TypeCfResProd_ResProd cfResProd_ResProd) {
        this.cfResProd_ResProd = cfResProd_ResProd;
    }

    /**
     * Sets the value of field 'cfResProd_Srv'.
     * 
     * @param cfResProd_Srv the value of field 'cfResProd_Srv'.
     */
    public void setCfResProd_Srv(final gr.ekt.cerif.schema.CfResProd__TypeCfResProd_Srv cfResProd_Srv) {
        this.cfResProd_Srv = cfResProd_Srv;
    }

    /**
     * Sets the value of field 'cfResPubl_ResProd'.
     * 
     * @param cfResPubl_ResProd the value of field
     * 'cfResPubl_ResProd'.
     */
    public void setCfResPubl_ResProd(final gr.ekt.cerif.schema.CfResProd__TypeCfResPubl_ResProd cfResPubl_ResProd) {
        this.cfResPubl_ResProd = cfResPubl_ResProd;
    }

    /**
     * Sets the value of field 'cfVersInfo'.
     * 
     * @param cfVersInfo the value of field 'cfVersInfo'.
     */
    public void setCfVersInfo(final gr.ekt.cerif.schema.CfVersInfo cfVersInfo) {
        this.cfVersInfo = cfVersInfo;
    }

}

/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfResPublNameAbbrev__Type implements java.io.Serializable {

    private java.lang.String cfResPublId;

    private gr.ekt.cerif.schema.CfNameAbbrev cfNameAbbrev;

    public CfResPublNameAbbrev__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfNameAbbrev'.
     * 
     * @return the value of field 'CfNameAbbrev'.
     */
    public gr.ekt.cerif.schema.CfNameAbbrev getCfNameAbbrev() {
        return this.cfNameAbbrev;
    }

    /**
     * Returns the value of field 'cfResPublId'.
     * 
     * @return the value of field 'CfResPublId'.
     */
    public java.lang.String getCfResPublId() {
        return this.cfResPublId;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfNameAbbrev'.
     * 
     * @param cfNameAbbrev the value of field 'cfNameAbbrev'.
     */
    public void setCfNameAbbrev(final gr.ekt.cerif.schema.CfNameAbbrev cfNameAbbrev) {
        this.cfNameAbbrev = cfNameAbbrev;
    }

    /**
     * Sets the value of field 'cfResPublId'.
     * 
     * @param cfResPublId the value of field 'cfResPublId'.
     */
    public void setCfResPublId(final java.lang.String cfResPublId) {
        this.cfResPublId = cfResPublId;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfResPublNameAbbrev__Type
     */
    public static gr.ekt.cerif.schema.CfResPublNameAbbrev__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfResPublNameAbbrev__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfResPublNameAbbrev__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

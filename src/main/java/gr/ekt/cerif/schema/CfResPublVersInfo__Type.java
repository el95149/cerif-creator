/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfResPublVersInfo__Type implements java.io.Serializable {

    private java.lang.String cfResPublId;

    private gr.ekt.cerif.schema.CfVersInfo cfVersInfo;

    public CfResPublVersInfo__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfResPublId'.
     * 
     * @return the value of field 'CfResPublId'.
     */
    public java.lang.String getCfResPublId() {
        return this.cfResPublId;
    }

    /**
     * Returns the value of field 'cfVersInfo'.
     * 
     * @return the value of field 'CfVersInfo'.
     */
    public gr.ekt.cerif.schema.CfVersInfo getCfVersInfo() {
        return this.cfVersInfo;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfResPublId'.
     * 
     * @param cfResPublId the value of field 'cfResPublId'.
     */
    public void setCfResPublId(final java.lang.String cfResPublId) {
        this.cfResPublId = cfResPublId;
    }

    /**
     * Sets the value of field 'cfVersInfo'.
     * 
     * @param cfVersInfo the value of field 'cfVersInfo'.
     */
    public void setCfVersInfo(final gr.ekt.cerif.schema.CfVersInfo cfVersInfo) {
        this.cfVersInfo = cfVersInfo;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfResPublVersInfo__Type
     */
    public static gr.ekt.cerif.schema.CfResPublVersInfo__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfResPublVersInfo__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfResPublVersInfo__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

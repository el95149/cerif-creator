/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfResPubl__Type implements java.io.Serializable {

    private java.lang.String cfResPublId;

    private org.exolab.castor.types.Date cfResPublDate;

    private java.lang.String cfNum;

    private java.lang.String cfVol;

    private java.lang.String cfEdition;

    private java.lang.String cfSeries;

    private java.lang.String cfIssue;

    private java.lang.String cfStartPage;

    private java.lang.String cfEndPage;

    private java.lang.String cfTotalPages;

    private java.lang.String cfISBN;

    private java.lang.String cfISSN;

    private java.lang.String cfURI;

    private gr.ekt.cerif.schema.CfResPubl__TypeChoice cfResPubl__TypeChoice;

    public CfResPubl__Type() {
        super();
    }

    /**
     * Returns the value of field 'cfEdition'.
     * 
     * @return the value of field 'CfEdition'.
     */
    public java.lang.String getCfEdition() {
        return this.cfEdition;
    }

    /**
     * Returns the value of field 'cfEndPage'.
     * 
     * @return the value of field 'CfEndPage'.
     */
    public java.lang.String getCfEndPage() {
        return this.cfEndPage;
    }

    /**
     * Returns the value of field 'cfISBN'.
     * 
     * @return the value of field 'CfISBN'.
     */
    public java.lang.String getCfISBN() {
        return this.cfISBN;
    }

    /**
     * Returns the value of field 'cfISSN'.
     * 
     * @return the value of field 'CfISSN'.
     */
    public java.lang.String getCfISSN() {
        return this.cfISSN;
    }

    /**
     * Returns the value of field 'cfIssue'.
     * 
     * @return the value of field 'CfIssue'.
     */
    public java.lang.String getCfIssue() {
        return this.cfIssue;
    }

    /**
     * Returns the value of field 'cfNum'.
     * 
     * @return the value of field 'CfNum'.
     */
    public java.lang.String getCfNum() {
        return this.cfNum;
    }

    /**
     * Returns the value of field 'cfResPublDate'.
     * 
     * @return the value of field 'CfResPublDate'.
     */
    public org.exolab.castor.types.Date getCfResPublDate() {
        return this.cfResPublDate;
    }

    /**
     * Returns the value of field 'cfResPublId'.
     * 
     * @return the value of field 'CfResPublId'.
     */
    public java.lang.String getCfResPublId() {
        return this.cfResPublId;
    }

    /**
     * Returns the value of field 'cfResPubl__TypeChoice'.
     * 
     * @return the value of field 'CfResPubl__TypeChoice'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeChoice getCfResPubl__TypeChoice() {
        return this.cfResPubl__TypeChoice;
    }

    /**
     * Returns the value of field 'cfSeries'.
     * 
     * @return the value of field 'CfSeries'.
     */
    public java.lang.String getCfSeries() {
        return this.cfSeries;
    }

    /**
     * Returns the value of field 'cfStartPage'.
     * 
     * @return the value of field 'CfStartPage'.
     */
    public java.lang.String getCfStartPage() {
        return this.cfStartPage;
    }

    /**
     * Returns the value of field 'cfTotalPages'.
     * 
     * @return the value of field 'CfTotalPages'.
     */
    public java.lang.String getCfTotalPages() {
        return this.cfTotalPages;
    }

    /**
     * Returns the value of field 'cfURI'.
     * 
     * @return the value of field 'CfURI'.
     */
    public java.lang.String getCfURI() {
        return this.cfURI;
    }

    /**
     * Returns the value of field 'cfVol'.
     * 
     * @return the value of field 'CfVol'.
     */
    public java.lang.String getCfVol() {
        return this.cfVol;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfEdition'.
     * 
     * @param cfEdition the value of field 'cfEdition'.
     */
    public void setCfEdition(final java.lang.String cfEdition) {
        this.cfEdition = cfEdition;
    }

    /**
     * Sets the value of field 'cfEndPage'.
     * 
     * @param cfEndPage the value of field 'cfEndPage'.
     */
    public void setCfEndPage(final java.lang.String cfEndPage) {
        this.cfEndPage = cfEndPage;
    }

    /**
     * Sets the value of field 'cfISBN'.
     * 
     * @param cfISBN the value of field 'cfISBN'.
     */
    public void setCfISBN(final java.lang.String cfISBN) {
        this.cfISBN = cfISBN;
    }

    /**
     * Sets the value of field 'cfISSN'.
     * 
     * @param cfISSN the value of field 'cfISSN'.
     */
    public void setCfISSN(final java.lang.String cfISSN) {
        this.cfISSN = cfISSN;
    }

    /**
     * Sets the value of field 'cfIssue'.
     * 
     * @param cfIssue the value of field 'cfIssue'.
     */
    public void setCfIssue(final java.lang.String cfIssue) {
        this.cfIssue = cfIssue;
    }

    /**
     * Sets the value of field 'cfNum'.
     * 
     * @param cfNum the value of field 'cfNum'.
     */
    public void setCfNum(final java.lang.String cfNum) {
        this.cfNum = cfNum;
    }

    /**
     * Sets the value of field 'cfResPublDate'.
     * 
     * @param cfResPublDate the value of field 'cfResPublDate'.
     */
    public void setCfResPublDate(final org.exolab.castor.types.Date cfResPublDate) {
        this.cfResPublDate = cfResPublDate;
    }

    /**
     * Sets the value of field 'cfResPublId'.
     * 
     * @param cfResPublId the value of field 'cfResPublId'.
     */
    public void setCfResPublId(final java.lang.String cfResPublId) {
        this.cfResPublId = cfResPublId;
    }

    /**
     * Sets the value of field 'cfResPubl__TypeChoice'.
     * 
     * @param cfResPubl__TypeChoice the value of field
     * 'cfResPubl__TypeChoice'.
     */
    public void setCfResPubl__TypeChoice(final gr.ekt.cerif.schema.CfResPubl__TypeChoice cfResPubl__TypeChoice) {
        this.cfResPubl__TypeChoice = cfResPubl__TypeChoice;
    }

    /**
     * Sets the value of field 'cfSeries'.
     * 
     * @param cfSeries the value of field 'cfSeries'.
     */
    public void setCfSeries(final java.lang.String cfSeries) {
        this.cfSeries = cfSeries;
    }

    /**
     * Sets the value of field 'cfStartPage'.
     * 
     * @param cfStartPage the value of field 'cfStartPage'.
     */
    public void setCfStartPage(final java.lang.String cfStartPage) {
        this.cfStartPage = cfStartPage;
    }

    /**
     * Sets the value of field 'cfTotalPages'.
     * 
     * @param cfTotalPages the value of field 'cfTotalPages'.
     */
    public void setCfTotalPages(final java.lang.String cfTotalPages) {
        this.cfTotalPages = cfTotalPages;
    }

    /**
     * Sets the value of field 'cfURI'.
     * 
     * @param cfURI the value of field 'cfURI'.
     */
    public void setCfURI(final java.lang.String cfURI) {
        this.cfURI = cfURI;
    }

    /**
     * Sets the value of field 'cfVol'.
     * 
     * @param cfVol the value of field 'cfVol'.
     */
    public void setCfVol(final java.lang.String cfVol) {
        this.cfVol = cfVol;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfResPubl__Type
     */
    public static gr.ekt.cerif.schema.CfResPubl__Type unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfResPubl__Type) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfResPubl__Type.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

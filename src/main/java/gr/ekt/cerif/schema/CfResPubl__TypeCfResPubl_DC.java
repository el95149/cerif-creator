/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfResPubl__TypeCfResPubl_DC implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfResPubl_DCSequence cfResPubl_DCSequence;

    private gr.ekt.cerif.schema.CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group;

    public CfResPubl__TypeCfResPubl_DC() {
        super();
    }

    /**
     * Returns the value of field 'cfCoreClassWithFraction__Group'.
     * 
     * @return the value of field 'CfCoreClassWithFraction__Group'.
     */
    public gr.ekt.cerif.schema.CfCoreClassWithFraction__Group getCfCoreClassWithFraction__Group() {
        return this.cfCoreClassWithFraction__Group;
    }

    /**
     * Returns the value of field 'cfResPubl_DCSequence'.
     * 
     * @return the value of field 'CfResPubl_DCSequence'.
     */
    public gr.ekt.cerif.schema.CfResPubl_DCSequence getCfResPubl_DCSequence() {
        return this.cfResPubl_DCSequence;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfCoreClassWithFraction__Group'.
     * 
     * @param cfCoreClassWithFraction__Group the value of field
     * 'cfCoreClassWithFraction__Group'.
     */
    public void setCfCoreClassWithFraction__Group(final gr.ekt.cerif.schema.CfCoreClassWithFraction__Group cfCoreClassWithFraction__Group) {
        this.cfCoreClassWithFraction__Group = cfCoreClassWithFraction__Group;
    }

    /**
     * Sets the value of field 'cfResPubl_DCSequence'.
     * 
     * @param cfResPubl_DCSequence the value of field
     * 'cfResPubl_DCSequence'.
     */
    public void setCfResPubl_DCSequence(final gr.ekt.cerif.schema.CfResPubl_DCSequence cfResPubl_DCSequence) {
        this.cfResPubl_DCSequence = cfResPubl_DCSequence;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_DC
     */
    public static gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_DC unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_DC) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_DC.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

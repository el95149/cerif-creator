/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfResPubl__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfTitle cfTitle;

    private gr.ekt.cerif.schema.CfAbstr cfAbstr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfBiblNote cfBiblNote;

    private gr.ekt.cerif.schema.CfNameAbbrev cfNameAbbrev;

    private gr.ekt.cerif.schema.CfSubtitle cfSubtitle;

    private gr.ekt.cerif.schema.CfVersInfo cfVersInfo;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Event cfResPubl_Event;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfOrgUnit_ResPubl cfOrgUnit_ResPubl;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfPers_ResPubl cfPers_ResPubl;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfProj_ResPubl cfProj_ResPubl;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_ResPubl cfResPubl_ResPubl;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Class cfResPubl_Class;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Fund cfResPubl_Fund;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_DC cfResPubl_DC;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Facil cfResPubl_Facil;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Equip cfResPubl_Equip;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_ResProd cfResPubl_ResProd;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_ResPat cfResPubl_ResPat;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Cite cfResPubl_Cite;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Metrics cfResPubl_Metrics;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Medium cfResPubl_Medium;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Srv cfResPubl_Srv;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Meas cfResPubl_Meas;

    private gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Indic cfResPubl_Indic;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfResPubl__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfAbstr'.
     * 
     * @return the value of field 'CfAbstr'.
     */
    public gr.ekt.cerif.schema.CfAbstr getCfAbstr() {
        return this.cfAbstr;
    }

    /**
     * Returns the value of field 'cfBiblNote'.
     * 
     * @return the value of field 'CfBiblNote'.
     */
    public gr.ekt.cerif.schema.CfBiblNote getCfBiblNote() {
        return this.cfBiblNote;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfNameAbbrev'.
     * 
     * @return the value of field 'CfNameAbbrev'.
     */
    public gr.ekt.cerif.schema.CfNameAbbrev getCfNameAbbrev() {
        return this.cfNameAbbrev;
    }

    /**
     * Returns the value of field 'cfOrgUnit_ResPubl'.
     * 
     * @return the value of field 'CfOrgUnit_ResPubl'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfOrgUnit_ResPubl getCfOrgUnit_ResPubl() {
        return this.cfOrgUnit_ResPubl;
    }

    /**
     * Returns the value of field 'cfPers_ResPubl'.
     * 
     * @return the value of field 'CfPers_ResPubl'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfPers_ResPubl getCfPers_ResPubl() {
        return this.cfPers_ResPubl;
    }

    /**
     * Returns the value of field 'cfProj_ResPubl'.
     * 
     * @return the value of field 'CfProj_ResPubl'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfProj_ResPubl getCfProj_ResPubl() {
        return this.cfProj_ResPubl;
    }

    /**
     * Returns the value of field 'cfResPubl_Cite'.
     * 
     * @return the value of field 'CfResPubl_Cite'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Cite getCfResPubl_Cite() {
        return this.cfResPubl_Cite;
    }

    /**
     * Returns the value of field 'cfResPubl_Class'.
     * 
     * @return the value of field 'CfResPubl_Class'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Class getCfResPubl_Class() {
        return this.cfResPubl_Class;
    }

    /**
     * Returns the value of field 'cfResPubl_DC'.
     * 
     * @return the value of field 'CfResPubl_DC'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_DC getCfResPubl_DC() {
        return this.cfResPubl_DC;
    }

    /**
     * Returns the value of field 'cfResPubl_Equip'.
     * 
     * @return the value of field 'CfResPubl_Equip'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Equip getCfResPubl_Equip() {
        return this.cfResPubl_Equip;
    }

    /**
     * Returns the value of field 'cfResPubl_Event'.
     * 
     * @return the value of field 'CfResPubl_Event'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Event getCfResPubl_Event() {
        return this.cfResPubl_Event;
    }

    /**
     * Returns the value of field 'cfResPubl_Facil'.
     * 
     * @return the value of field 'CfResPubl_Facil'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Facil getCfResPubl_Facil() {
        return this.cfResPubl_Facil;
    }

    /**
     * Returns the value of field 'cfResPubl_Fund'.
     * 
     * @return the value of field 'CfResPubl_Fund'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Fund getCfResPubl_Fund() {
        return this.cfResPubl_Fund;
    }

    /**
     * Returns the value of field 'cfResPubl_Indic'.
     * 
     * @return the value of field 'CfResPubl_Indic'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Indic getCfResPubl_Indic() {
        return this.cfResPubl_Indic;
    }

    /**
     * Returns the value of field 'cfResPubl_Meas'.
     * 
     * @return the value of field 'CfResPubl_Meas'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Meas getCfResPubl_Meas() {
        return this.cfResPubl_Meas;
    }

    /**
     * Returns the value of field 'cfResPubl_Medium'.
     * 
     * @return the value of field 'CfResPubl_Medium'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Medium getCfResPubl_Medium() {
        return this.cfResPubl_Medium;
    }

    /**
     * Returns the value of field 'cfResPubl_Metrics'.
     * 
     * @return the value of field 'CfResPubl_Metrics'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Metrics getCfResPubl_Metrics() {
        return this.cfResPubl_Metrics;
    }

    /**
     * Returns the value of field 'cfResPubl_ResPat'.
     * 
     * @return the value of field 'CfResPubl_ResPat'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_ResPat getCfResPubl_ResPat() {
        return this.cfResPubl_ResPat;
    }

    /**
     * Returns the value of field 'cfResPubl_ResProd'.
     * 
     * @return the value of field 'CfResPubl_ResProd'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_ResProd getCfResPubl_ResProd() {
        return this.cfResPubl_ResProd;
    }

    /**
     * Returns the value of field 'cfResPubl_ResPubl'.
     * 
     * @return the value of field 'CfResPubl_ResPubl'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_ResPubl getCfResPubl_ResPubl() {
        return this.cfResPubl_ResPubl;
    }

    /**
     * Returns the value of field 'cfResPubl_Srv'.
     * 
     * @return the value of field 'CfResPubl_Srv'.
     */
    public gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Srv getCfResPubl_Srv() {
        return this.cfResPubl_Srv;
    }

    /**
     * Returns the value of field 'cfSubtitle'.
     * 
     * @return the value of field 'CfSubtitle'.
     */
    public gr.ekt.cerif.schema.CfSubtitle getCfSubtitle() {
        return this.cfSubtitle;
    }

    /**
     * Returns the value of field 'cfTitle'.
     * 
     * @return the value of field 'CfTitle'.
     */
    public gr.ekt.cerif.schema.CfTitle getCfTitle() {
        return this.cfTitle;
    }

    /**
     * Returns the value of field 'cfVersInfo'.
     * 
     * @return the value of field 'CfVersInfo'.
     */
    public gr.ekt.cerif.schema.CfVersInfo getCfVersInfo() {
        return this.cfVersInfo;
    }

    /**
     * Sets the value of field 'cfAbstr'.
     * 
     * @param cfAbstr the value of field 'cfAbstr'.
     */
    public void setCfAbstr(final gr.ekt.cerif.schema.CfAbstr cfAbstr) {
        this.cfAbstr = cfAbstr;
    }

    /**
     * Sets the value of field 'cfBiblNote'.
     * 
     * @param cfBiblNote the value of field 'cfBiblNote'.
     */
    public void setCfBiblNote(final gr.ekt.cerif.schema.CfBiblNote cfBiblNote) {
        this.cfBiblNote = cfBiblNote;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfNameAbbrev'.
     * 
     * @param cfNameAbbrev the value of field 'cfNameAbbrev'.
     */
    public void setCfNameAbbrev(final gr.ekt.cerif.schema.CfNameAbbrev cfNameAbbrev) {
        this.cfNameAbbrev = cfNameAbbrev;
    }

    /**
     * Sets the value of field 'cfOrgUnit_ResPubl'.
     * 
     * @param cfOrgUnit_ResPubl the value of field
     * 'cfOrgUnit_ResPubl'.
     */
    public void setCfOrgUnit_ResPubl(final gr.ekt.cerif.schema.CfResPubl__TypeCfOrgUnit_ResPubl cfOrgUnit_ResPubl) {
        this.cfOrgUnit_ResPubl = cfOrgUnit_ResPubl;
    }

    /**
     * Sets the value of field 'cfPers_ResPubl'.
     * 
     * @param cfPers_ResPubl the value of field 'cfPers_ResPubl'.
     */
    public void setCfPers_ResPubl(final gr.ekt.cerif.schema.CfResPubl__TypeCfPers_ResPubl cfPers_ResPubl) {
        this.cfPers_ResPubl = cfPers_ResPubl;
    }

    /**
     * Sets the value of field 'cfProj_ResPubl'.
     * 
     * @param cfProj_ResPubl the value of field 'cfProj_ResPubl'.
     */
    public void setCfProj_ResPubl(final gr.ekt.cerif.schema.CfResPubl__TypeCfProj_ResPubl cfProj_ResPubl) {
        this.cfProj_ResPubl = cfProj_ResPubl;
    }

    /**
     * Sets the value of field 'cfResPubl_Cite'.
     * 
     * @param cfResPubl_Cite the value of field 'cfResPubl_Cite'.
     */
    public void setCfResPubl_Cite(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Cite cfResPubl_Cite) {
        this.cfResPubl_Cite = cfResPubl_Cite;
    }

    /**
     * Sets the value of field 'cfResPubl_Class'.
     * 
     * @param cfResPubl_Class the value of field 'cfResPubl_Class'.
     */
    public void setCfResPubl_Class(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Class cfResPubl_Class) {
        this.cfResPubl_Class = cfResPubl_Class;
    }

    /**
     * Sets the value of field 'cfResPubl_DC'.
     * 
     * @param cfResPubl_DC the value of field 'cfResPubl_DC'.
     */
    public void setCfResPubl_DC(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_DC cfResPubl_DC) {
        this.cfResPubl_DC = cfResPubl_DC;
    }

    /**
     * Sets the value of field 'cfResPubl_Equip'.
     * 
     * @param cfResPubl_Equip the value of field 'cfResPubl_Equip'.
     */
    public void setCfResPubl_Equip(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Equip cfResPubl_Equip) {
        this.cfResPubl_Equip = cfResPubl_Equip;
    }

    /**
     * Sets the value of field 'cfResPubl_Event'.
     * 
     * @param cfResPubl_Event the value of field 'cfResPubl_Event'.
     */
    public void setCfResPubl_Event(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Event cfResPubl_Event) {
        this.cfResPubl_Event = cfResPubl_Event;
    }

    /**
     * Sets the value of field 'cfResPubl_Facil'.
     * 
     * @param cfResPubl_Facil the value of field 'cfResPubl_Facil'.
     */
    public void setCfResPubl_Facil(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Facil cfResPubl_Facil) {
        this.cfResPubl_Facil = cfResPubl_Facil;
    }

    /**
     * Sets the value of field 'cfResPubl_Fund'.
     * 
     * @param cfResPubl_Fund the value of field 'cfResPubl_Fund'.
     */
    public void setCfResPubl_Fund(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Fund cfResPubl_Fund) {
        this.cfResPubl_Fund = cfResPubl_Fund;
    }

    /**
     * Sets the value of field 'cfResPubl_Indic'.
     * 
     * @param cfResPubl_Indic the value of field 'cfResPubl_Indic'.
     */
    public void setCfResPubl_Indic(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Indic cfResPubl_Indic) {
        this.cfResPubl_Indic = cfResPubl_Indic;
    }

    /**
     * Sets the value of field 'cfResPubl_Meas'.
     * 
     * @param cfResPubl_Meas the value of field 'cfResPubl_Meas'.
     */
    public void setCfResPubl_Meas(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Meas cfResPubl_Meas) {
        this.cfResPubl_Meas = cfResPubl_Meas;
    }

    /**
     * Sets the value of field 'cfResPubl_Medium'.
     * 
     * @param cfResPubl_Medium the value of field 'cfResPubl_Medium'
     */
    public void setCfResPubl_Medium(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Medium cfResPubl_Medium) {
        this.cfResPubl_Medium = cfResPubl_Medium;
    }

    /**
     * Sets the value of field 'cfResPubl_Metrics'.
     * 
     * @param cfResPubl_Metrics the value of field
     * 'cfResPubl_Metrics'.
     */
    public void setCfResPubl_Metrics(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Metrics cfResPubl_Metrics) {
        this.cfResPubl_Metrics = cfResPubl_Metrics;
    }

    /**
     * Sets the value of field 'cfResPubl_ResPat'.
     * 
     * @param cfResPubl_ResPat the value of field 'cfResPubl_ResPat'
     */
    public void setCfResPubl_ResPat(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_ResPat cfResPubl_ResPat) {
        this.cfResPubl_ResPat = cfResPubl_ResPat;
    }

    /**
     * Sets the value of field 'cfResPubl_ResProd'.
     * 
     * @param cfResPubl_ResProd the value of field
     * 'cfResPubl_ResProd'.
     */
    public void setCfResPubl_ResProd(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_ResProd cfResPubl_ResProd) {
        this.cfResPubl_ResProd = cfResPubl_ResProd;
    }

    /**
     * Sets the value of field 'cfResPubl_ResPubl'.
     * 
     * @param cfResPubl_ResPubl the value of field
     * 'cfResPubl_ResPubl'.
     */
    public void setCfResPubl_ResPubl(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_ResPubl cfResPubl_ResPubl) {
        this.cfResPubl_ResPubl = cfResPubl_ResPubl;
    }

    /**
     * Sets the value of field 'cfResPubl_Srv'.
     * 
     * @param cfResPubl_Srv the value of field 'cfResPubl_Srv'.
     */
    public void setCfResPubl_Srv(final gr.ekt.cerif.schema.CfResPubl__TypeCfResPubl_Srv cfResPubl_Srv) {
        this.cfResPubl_Srv = cfResPubl_Srv;
    }

    /**
     * Sets the value of field 'cfSubtitle'.
     * 
     * @param cfSubtitle the value of field 'cfSubtitle'.
     */
    public void setCfSubtitle(final gr.ekt.cerif.schema.CfSubtitle cfSubtitle) {
        this.cfSubtitle = cfSubtitle;
    }

    /**
     * Sets the value of field 'cfTitle'.
     * 
     * @param cfTitle the value of field 'cfTitle'.
     */
    public void setCfTitle(final gr.ekt.cerif.schema.CfTitle cfTitle) {
        this.cfTitle = cfTitle;
    }

    /**
     * Sets the value of field 'cfVersInfo'.
     * 
     * @param cfVersInfo the value of field 'cfVersInfo'.
     */
    public void setCfVersInfo(final gr.ekt.cerif.schema.CfVersInfo cfVersInfo) {
        this.cfVersInfo = cfVersInfo;
    }

}

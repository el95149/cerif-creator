/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfSrv_SrvChoice implements java.io.Serializable {

    private java.lang.String cfSrvId2;

    private java.lang.String cfSrvId1;

    public CfSrv_SrvChoice() {
        super();
    }

    /**
     * Returns the value of field 'cfSrvId1'.
     * 
     * @return the value of field 'CfSrvId1'.
     */
    public java.lang.String getCfSrvId1() {
        return this.cfSrvId1;
    }

    /**
     * Returns the value of field 'cfSrvId2'.
     * 
     * @return the value of field 'CfSrvId2'.
     */
    public java.lang.String getCfSrvId2() {
        return this.cfSrvId2;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(final java.io.Writer out) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(final org.xml.sax.ContentHandler handler) throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'cfSrvId1'.
     * 
     * @param cfSrvId1 the value of field 'cfSrvId1'.
     */
    public void setCfSrvId1(final java.lang.String cfSrvId1) {
        this.cfSrvId1 = cfSrvId1;
    }

    /**
     * Sets the value of field 'cfSrvId2'.
     * 
     * @param cfSrvId2 the value of field 'cfSrvId2'.
     */
    public void setCfSrvId2(final java.lang.String cfSrvId2) {
        this.cfSrvId2 = cfSrvId2;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled gr.ekt.cerif.schema.CfSrv_SrvChoice
     */
    public static gr.ekt.cerif.schema.CfSrv_SrvChoice unmarshal(final java.io.Reader reader) throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (gr.ekt.cerif.schema.CfSrv_SrvChoice) org.exolab.castor.xml.Unmarshaller.unmarshal(gr.ekt.cerif.schema.CfSrv_SrvChoice.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate() throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}

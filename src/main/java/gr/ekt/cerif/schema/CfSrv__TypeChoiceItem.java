/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
@SuppressWarnings("serial")
public class CfSrv__TypeChoiceItem implements java.io.Serializable {

    private gr.ekt.cerif.schema.CfName cfName;

    private gr.ekt.cerif.schema.CfDescr cfDescr;

    private gr.ekt.cerif.schema.CfKeyw cfKeyw;

    private gr.ekt.cerif.schema.CfSrv__TypeCfOrgUnit_Srv cfOrgUnit_Srv;

    private gr.ekt.cerif.schema.CfSrv__TypeCfPers_Srv cfPers_Srv;

    private gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Class cfSrv_Class;

    private gr.ekt.cerif.schema.CfSrv__TypeCfProj_Srv cfProj_Srv;

    private gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Fund cfSrv_Fund;

    private gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Medium cfSrv_Medium;

    private gr.ekt.cerif.schema.CfSrv__TypeCfSrv_PAddr cfSrv_PAddr;

    private gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Srv cfSrv_Srv;

    private gr.ekt.cerif.schema.CfSrv__TypeCfFacil_Srv cfFacil_Srv;

    private gr.ekt.cerif.schema.CfSrv__TypeCfEquip_Srv cfEquip_Srv;

    private gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Event cfSrv_Event;

    private gr.ekt.cerif.schema.CfSrv__TypeCfResPubl_Srv cfResPubl_Srv;

    private gr.ekt.cerif.schema.CfSrv__TypeCfResPat_Srv cfResPat_Srv;

    private gr.ekt.cerif.schema.CfSrv__TypeCfResProd_Srv cfResProd_Srv;

    private gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Meas cfSrv_Meas;

    private gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Indic cfSrv_Indic;

    private gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId;

    public CfSrv__TypeChoiceItem() {
        super();
    }

    /**
     * Returns the value of field 'cfDescr'.
     * 
     * @return the value of field 'CfDescr'.
     */
    public gr.ekt.cerif.schema.CfDescr getCfDescr() {
        return this.cfDescr;
    }

    /**
     * Returns the value of field 'cfEquip_Srv'.
     * 
     * @return the value of field 'CfEquip_Srv'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfEquip_Srv getCfEquip_Srv() {
        return this.cfEquip_Srv;
    }

    /**
     * Returns the value of field 'cfFacil_Srv'.
     * 
     * @return the value of field 'CfFacil_Srv'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfFacil_Srv getCfFacil_Srv() {
        return this.cfFacil_Srv;
    }

    /**
     * Returns the value of field 'cfFedId'.
     * 
     * @return the value of field 'CfFedId'.
     */
    public gr.ekt.cerif.schema.CfClass__TypeCfFedId getCfFedId() {
        return this.cfFedId;
    }

    /**
     * Returns the value of field 'cfKeyw'.
     * 
     * @return the value of field 'CfKeyw'.
     */
    public gr.ekt.cerif.schema.CfKeyw getCfKeyw() {
        return this.cfKeyw;
    }

    /**
     * Returns the value of field 'cfName'.
     * 
     * @return the value of field 'CfName'.
     */
    public gr.ekt.cerif.schema.CfName getCfName() {
        return this.cfName;
    }

    /**
     * Returns the value of field 'cfOrgUnit_Srv'.
     * 
     * @return the value of field 'CfOrgUnit_Srv'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfOrgUnit_Srv getCfOrgUnit_Srv() {
        return this.cfOrgUnit_Srv;
    }

    /**
     * Returns the value of field 'cfPers_Srv'.
     * 
     * @return the value of field 'CfPers_Srv'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfPers_Srv getCfPers_Srv() {
        return this.cfPers_Srv;
    }

    /**
     * Returns the value of field 'cfProj_Srv'.
     * 
     * @return the value of field 'CfProj_Srv'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfProj_Srv getCfProj_Srv() {
        return this.cfProj_Srv;
    }

    /**
     * Returns the value of field 'cfResPat_Srv'.
     * 
     * @return the value of field 'CfResPat_Srv'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfResPat_Srv getCfResPat_Srv() {
        return this.cfResPat_Srv;
    }

    /**
     * Returns the value of field 'cfResProd_Srv'.
     * 
     * @return the value of field 'CfResProd_Srv'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfResProd_Srv getCfResProd_Srv() {
        return this.cfResProd_Srv;
    }

    /**
     * Returns the value of field 'cfResPubl_Srv'.
     * 
     * @return the value of field 'CfResPubl_Srv'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfResPubl_Srv getCfResPubl_Srv() {
        return this.cfResPubl_Srv;
    }

    /**
     * Returns the value of field 'cfSrv_Class'.
     * 
     * @return the value of field 'CfSrv_Class'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Class getCfSrv_Class() {
        return this.cfSrv_Class;
    }

    /**
     * Returns the value of field 'cfSrv_Event'.
     * 
     * @return the value of field 'CfSrv_Event'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Event getCfSrv_Event() {
        return this.cfSrv_Event;
    }

    /**
     * Returns the value of field 'cfSrv_Fund'.
     * 
     * @return the value of field 'CfSrv_Fund'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Fund getCfSrv_Fund() {
        return this.cfSrv_Fund;
    }

    /**
     * Returns the value of field 'cfSrv_Indic'.
     * 
     * @return the value of field 'CfSrv_Indic'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Indic getCfSrv_Indic() {
        return this.cfSrv_Indic;
    }

    /**
     * Returns the value of field 'cfSrv_Meas'.
     * 
     * @return the value of field 'CfSrv_Meas'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Meas getCfSrv_Meas() {
        return this.cfSrv_Meas;
    }

    /**
     * Returns the value of field 'cfSrv_Medium'.
     * 
     * @return the value of field 'CfSrv_Medium'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Medium getCfSrv_Medium() {
        return this.cfSrv_Medium;
    }

    /**
     * Returns the value of field 'cfSrv_PAddr'.
     * 
     * @return the value of field 'CfSrv_PAddr'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfSrv_PAddr getCfSrv_PAddr() {
        return this.cfSrv_PAddr;
    }

    /**
     * Returns the value of field 'cfSrv_Srv'.
     * 
     * @return the value of field 'CfSrv_Srv'.
     */
    public gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Srv getCfSrv_Srv() {
        return this.cfSrv_Srv;
    }

    /**
     * Sets the value of field 'cfDescr'.
     * 
     * @param cfDescr the value of field 'cfDescr'.
     */
    public void setCfDescr(final gr.ekt.cerif.schema.CfDescr cfDescr) {
        this.cfDescr = cfDescr;
    }

    /**
     * Sets the value of field 'cfEquip_Srv'.
     * 
     * @param cfEquip_Srv the value of field 'cfEquip_Srv'.
     */
    public void setCfEquip_Srv(final gr.ekt.cerif.schema.CfSrv__TypeCfEquip_Srv cfEquip_Srv) {
        this.cfEquip_Srv = cfEquip_Srv;
    }

    /**
     * Sets the value of field 'cfFacil_Srv'.
     * 
     * @param cfFacil_Srv the value of field 'cfFacil_Srv'.
     */
    public void setCfFacil_Srv(final gr.ekt.cerif.schema.CfSrv__TypeCfFacil_Srv cfFacil_Srv) {
        this.cfFacil_Srv = cfFacil_Srv;
    }

    /**
     * Sets the value of field 'cfFedId'.
     * 
     * @param cfFedId the value of field 'cfFedId'.
     */
    public void setCfFedId(final gr.ekt.cerif.schema.CfClass__TypeCfFedId cfFedId) {
        this.cfFedId = cfFedId;
    }

    /**
     * Sets the value of field 'cfKeyw'.
     * 
     * @param cfKeyw the value of field 'cfKeyw'.
     */
    public void setCfKeyw(final gr.ekt.cerif.schema.CfKeyw cfKeyw) {
        this.cfKeyw = cfKeyw;
    }

    /**
     * Sets the value of field 'cfName'.
     * 
     * @param cfName the value of field 'cfName'.
     */
    public void setCfName(final gr.ekt.cerif.schema.CfName cfName) {
        this.cfName = cfName;
    }

    /**
     * Sets the value of field 'cfOrgUnit_Srv'.
     * 
     * @param cfOrgUnit_Srv the value of field 'cfOrgUnit_Srv'.
     */
    public void setCfOrgUnit_Srv(final gr.ekt.cerif.schema.CfSrv__TypeCfOrgUnit_Srv cfOrgUnit_Srv) {
        this.cfOrgUnit_Srv = cfOrgUnit_Srv;
    }

    /**
     * Sets the value of field 'cfPers_Srv'.
     * 
     * @param cfPers_Srv the value of field 'cfPers_Srv'.
     */
    public void setCfPers_Srv(final gr.ekt.cerif.schema.CfSrv__TypeCfPers_Srv cfPers_Srv) {
        this.cfPers_Srv = cfPers_Srv;
    }

    /**
     * Sets the value of field 'cfProj_Srv'.
     * 
     * @param cfProj_Srv the value of field 'cfProj_Srv'.
     */
    public void setCfProj_Srv(final gr.ekt.cerif.schema.CfSrv__TypeCfProj_Srv cfProj_Srv) {
        this.cfProj_Srv = cfProj_Srv;
    }

    /**
     * Sets the value of field 'cfResPat_Srv'.
     * 
     * @param cfResPat_Srv the value of field 'cfResPat_Srv'.
     */
    public void setCfResPat_Srv(final gr.ekt.cerif.schema.CfSrv__TypeCfResPat_Srv cfResPat_Srv) {
        this.cfResPat_Srv = cfResPat_Srv;
    }

    /**
     * Sets the value of field 'cfResProd_Srv'.
     * 
     * @param cfResProd_Srv the value of field 'cfResProd_Srv'.
     */
    public void setCfResProd_Srv(final gr.ekt.cerif.schema.CfSrv__TypeCfResProd_Srv cfResProd_Srv) {
        this.cfResProd_Srv = cfResProd_Srv;
    }

    /**
     * Sets the value of field 'cfResPubl_Srv'.
     * 
     * @param cfResPubl_Srv the value of field 'cfResPubl_Srv'.
     */
    public void setCfResPubl_Srv(final gr.ekt.cerif.schema.CfSrv__TypeCfResPubl_Srv cfResPubl_Srv) {
        this.cfResPubl_Srv = cfResPubl_Srv;
    }

    /**
     * Sets the value of field 'cfSrv_Class'.
     * 
     * @param cfSrv_Class the value of field 'cfSrv_Class'.
     */
    public void setCfSrv_Class(final gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Class cfSrv_Class) {
        this.cfSrv_Class = cfSrv_Class;
    }

    /**
     * Sets the value of field 'cfSrv_Event'.
     * 
     * @param cfSrv_Event the value of field 'cfSrv_Event'.
     */
    public void setCfSrv_Event(final gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Event cfSrv_Event) {
        this.cfSrv_Event = cfSrv_Event;
    }

    /**
     * Sets the value of field 'cfSrv_Fund'.
     * 
     * @param cfSrv_Fund the value of field 'cfSrv_Fund'.
     */
    public void setCfSrv_Fund(final gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Fund cfSrv_Fund) {
        this.cfSrv_Fund = cfSrv_Fund;
    }

    /**
     * Sets the value of field 'cfSrv_Indic'.
     * 
     * @param cfSrv_Indic the value of field 'cfSrv_Indic'.
     */
    public void setCfSrv_Indic(final gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Indic cfSrv_Indic) {
        this.cfSrv_Indic = cfSrv_Indic;
    }

    /**
     * Sets the value of field 'cfSrv_Meas'.
     * 
     * @param cfSrv_Meas the value of field 'cfSrv_Meas'.
     */
    public void setCfSrv_Meas(final gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Meas cfSrv_Meas) {
        this.cfSrv_Meas = cfSrv_Meas;
    }

    /**
     * Sets the value of field 'cfSrv_Medium'.
     * 
     * @param cfSrv_Medium the value of field 'cfSrv_Medium'.
     */
    public void setCfSrv_Medium(final gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Medium cfSrv_Medium) {
        this.cfSrv_Medium = cfSrv_Medium;
    }

    /**
     * Sets the value of field 'cfSrv_PAddr'.
     * 
     * @param cfSrv_PAddr the value of field 'cfSrv_PAddr'.
     */
    public void setCfSrv_PAddr(final gr.ekt.cerif.schema.CfSrv__TypeCfSrv_PAddr cfSrv_PAddr) {
        this.cfSrv_PAddr = cfSrv_PAddr;
    }

    /**
     * Sets the value of field 'cfSrv_Srv'.
     * 
     * @param cfSrv_Srv the value of field 'cfSrv_Srv'.
     */
    public void setCfSrv_Srv(final gr.ekt.cerif.schema.CfSrv__TypeCfSrv_Srv cfSrv_Srv) {
        this.cfSrv_Srv = cfSrv_Srv;
    }

}

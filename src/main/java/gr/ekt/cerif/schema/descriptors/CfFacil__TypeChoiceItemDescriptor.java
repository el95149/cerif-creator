/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema.descriptors;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import gr.ekt.cerif.schema.CfFacil__TypeChoiceItem;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
public class CfFacil__TypeChoiceItemDescriptor extends org.exolab.castor.xml.util.XMLClassDescriptorImpl {

    /**
     * Field _elementDefinition.
     */
    private boolean _elementDefinition;

    /**
     * Field _nsPrefix.
     */
    private java.lang.String _nsPrefix;

    /**
     * Field _nsURI.
     */
    private java.lang.String _nsURI;

    /**
     * Field _xmlName.
     */
    private java.lang.String _xmlName;

    /**
     * Field _identity.
     */
    private org.exolab.castor.xml.XMLFieldDescriptor _identity;

    public CfFacil__TypeChoiceItemDescriptor() {
        super();
        _nsURI = "urn:xmlns:org:eurocris:cerif-1.6-2";
        _elementDefinition = false;

        //-- set grouping compositor
        setCompositorAsChoice();
        org.exolab.castor.xml.util.XMLFieldDescriptorImpl  desc           = null;
        org.exolab.castor.mapping.FieldHandler             handler        = null;
        org.exolab.castor.xml.FieldValidator               fieldValidator = null;
        //-- initialize attribute descriptors

        //-- initialize element descriptors

        //-- cfName
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfName.class, "cfName", "cfName", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfName();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfName( (gr.ekt.cerif.schema.CfName) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfName();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfName");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfName
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfDescr
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfDescr.class, "cfDescr", "cfDescr", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfDescr();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfDescr( (gr.ekt.cerif.schema.CfDescr) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfDescr();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfDescr");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfDescr
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfKeyw
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfKeyw.class, "cfKeyw", "cfKeyw", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfKeyw();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfKeyw( (gr.ekt.cerif.schema.CfKeyw) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfKeyw();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfKeyw");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfKeyw
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFacil_Class
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Class.class, "cfFacil_Class", "cfFacil_Class", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfFacil_Class();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfFacil_Class( (gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Class) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Class();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Class");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFacil_Class
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFacil_Fund
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Fund.class, "cfFacil_Fund", "cfFacil_Fund", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfFacil_Fund();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfFacil_Fund( (gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Fund) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Fund();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Fund");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFacil_Fund
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfOrgUnit_Facil
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfOrgUnit_Facil.class, "cfOrgUnit_Facil", "cfOrgUnit_Facil", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfOrgUnit_Facil();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfOrgUnit_Facil( (gr.ekt.cerif.schema.CfFacil__TypeCfOrgUnit_Facil) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfOrgUnit_Facil();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfOrgUnit_Facil");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfOrgUnit_Facil
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfPers_Facil
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfPers_Facil.class, "cfPers_Facil", "cfPers_Facil", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfPers_Facil();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfPers_Facil( (gr.ekt.cerif.schema.CfFacil__TypeCfPers_Facil) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfPers_Facil();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfPers_Facil");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfPers_Facil
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfProj_Facil
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfProj_Facil.class, "cfProj_Facil", "cfProj_Facil", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfProj_Facil();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfProj_Facil( (gr.ekt.cerif.schema.CfFacil__TypeCfProj_Facil) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfProj_Facil();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfProj_Facil");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfProj_Facil
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResPubl_Facil
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfResPubl_Facil.class, "cfResPubl_Facil", "cfResPubl_Facil", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfResPubl_Facil();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfResPubl_Facil( (gr.ekt.cerif.schema.CfFacil__TypeCfResPubl_Facil) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfResPubl_Facil();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfResPubl_Facil");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResPubl_Facil
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFacil_Medium
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Medium.class, "cfFacil_Medium", "cfFacil_Medium", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfFacil_Medium();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfFacil_Medium( (gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Medium) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Medium();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Medium");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFacil_Medium
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFacil_PAddr
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfFacil_PAddr.class, "cfFacil_PAddr", "cfFacil_PAddr", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfFacil_PAddr();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfFacil_PAddr( (gr.ekt.cerif.schema.CfFacil__TypeCfFacil_PAddr) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfFacil_PAddr();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfFacil_PAddr");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFacil_PAddr
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFacil_Facil
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Facil.class, "cfFacil_Facil", "cfFacil_Facil", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfFacil_Facil();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfFacil_Facil( (gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Facil) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Facil();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Facil");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFacil_Facil
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFacil_Equip
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Equip.class, "cfFacil_Equip", "cfFacil_Equip", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfFacil_Equip();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfFacil_Equip( (gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Equip) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Equip();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Equip");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFacil_Equip
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFacil_Srv
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Srv.class, "cfFacil_Srv", "cfFacil_Srv", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfFacil_Srv();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfFacil_Srv( (gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Srv) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Srv();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Srv");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFacil_Srv
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFacil_Event
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Event.class, "cfFacil_Event", "cfFacil_Event", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfFacil_Event();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfFacil_Event( (gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Event) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Event();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Event");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFacil_Event
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResPat_Facil
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfResPat_Facil.class, "cfResPat_Facil", "cfResPat_Facil", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfResPat_Facil();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfResPat_Facil( (gr.ekt.cerif.schema.CfFacil__TypeCfResPat_Facil) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfResPat_Facil();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfResPat_Facil");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResPat_Facil
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResProd_Facil
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfResProd_Facil.class, "cfResProd_Facil", "cfResProd_Facil", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfResProd_Facil();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfResProd_Facil( (gr.ekt.cerif.schema.CfFacil__TypeCfResProd_Facil) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfResProd_Facil();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfResProd_Facil");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResProd_Facil
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFacil_Meas
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Meas.class, "cfFacil_Meas", "cfFacil_Meas", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfFacil_Meas();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfFacil_Meas( (gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Meas) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Meas();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Meas");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFacil_Meas
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFacil_Indic
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Indic.class, "cfFacil_Indic", "cfFacil_Indic", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfFacil_Indic();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfFacil_Indic( (gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Indic) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Indic();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfFacil__TypeCfFacil_Indic");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFacil_Indic
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFedId
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfClass__TypeCfFedId.class, "cfFedId", "cfFedId", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                return target.getCfFedId();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfFacil__TypeChoiceItem target = (CfFacil__TypeChoiceItem) object;
                    target.setCfFedId( (gr.ekt.cerif.schema.CfClass__TypeCfFedId) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfClass__TypeCfFedId();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfClass__TypeCfFedId");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFedId
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
    }

    /**
     * Method getAccessMode.
     * 
     * @return the access mode specified for this class.
     */
    @Override()
    public org.exolab.castor.mapping.AccessMode getAccessMode() {
        return null;
    }

    /**
     * Method getIdentity.
     * 
     * @return the identity field, null if this class has no
     * identity.
     */
    @Override()
    public org.exolab.castor.mapping.FieldDescriptor getIdentity() {
        return _identity;
    }

    /**
     * Method getJavaClass.
     * 
     * @return the Java class represented by this descriptor.
     */
    @Override()
    public java.lang.Class getJavaClass() {
        return gr.ekt.cerif.schema.CfFacil__TypeChoiceItem.class;
    }

    /**
     * Method getNameSpacePrefix.
     * 
     * @return the namespace prefix to use when marshaling as XML.
     */
    @Override()
    public java.lang.String getNameSpacePrefix() {
        return _nsPrefix;
    }

    /**
     * Method getNameSpaceURI.
     * 
     * @return the namespace URI used when marshaling and
     * unmarshaling as XML.
     */
    @Override()
    public java.lang.String getNameSpaceURI() {
        return _nsURI;
    }

    /**
     * Method getValidator.
     * 
     * @return a specific validator for the class described by this
     * ClassDescriptor.
     */
    @Override()
    public org.exolab.castor.xml.TypeValidator getValidator() {
        return this;
    }

    /**
     * Method getXMLName.
     * 
     * @return the XML Name for the Class being described.
     */
    @Override()
    public java.lang.String getXMLName() {
        return _xmlName;
    }

    /**
     * Method isElementDefinition.
     * 
     * @return true if XML schema definition of this Class is that
     * of a global
     * element or element with anonymous type definition.
     */
    public boolean isElementDefinition() {
        return _elementDefinition;
    }

}

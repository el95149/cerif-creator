/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema.descriptors;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import gr.ekt.cerif.schema.CfPAddr__TypeChoiceItem;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
public class CfPAddr__TypeChoiceItemDescriptor extends org.exolab.castor.xml.util.XMLClassDescriptorImpl {

    /**
     * Field _elementDefinition.
     */
    private boolean _elementDefinition;

    /**
     * Field _nsPrefix.
     */
    private java.lang.String _nsPrefix;

    /**
     * Field _nsURI.
     */
    private java.lang.String _nsURI;

    /**
     * Field _xmlName.
     */
    private java.lang.String _xmlName;

    /**
     * Field _identity.
     */
    private org.exolab.castor.xml.XMLFieldDescriptor _identity;

    public CfPAddr__TypeChoiceItemDescriptor() {
        super();
        _nsURI = "urn:xmlns:org:eurocris:cerif-1.6-2";
        _elementDefinition = false;

        //-- set grouping compositor
        setCompositorAsChoice();
        org.exolab.castor.xml.util.XMLFieldDescriptorImpl  desc           = null;
        org.exolab.castor.mapping.FieldHandler             handler        = null;
        org.exolab.castor.xml.FieldValidator               fieldValidator = null;
        //-- initialize attribute descriptors

        //-- initialize element descriptors

        //-- cfOrgUnit_PAddr
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfPAddr__TypeCfOrgUnit_PAddr.class, "cfOrgUnit_PAddr", "cfOrgUnit_PAddr", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                return target.getCfOrgUnit_PAddr();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                    target.setCfOrgUnit_PAddr( (gr.ekt.cerif.schema.CfPAddr__TypeCfOrgUnit_PAddr) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfPAddr__TypeCfOrgUnit_PAddr();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfPAddr__TypeCfOrgUnit_PAddr");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfOrgUnit_PAddr
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfPers_PAddr
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfPAddr__TypeCfPers_PAddr.class, "cfPers_PAddr", "cfPers_PAddr", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                return target.getCfPers_PAddr();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                    target.setCfPers_PAddr( (gr.ekt.cerif.schema.CfPAddr__TypeCfPers_PAddr) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfPAddr__TypeCfPers_PAddr();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfPAddr__TypeCfPers_PAddr");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfPers_PAddr
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfPAddr_Class
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_Class.class, "cfPAddr_Class", "cfPAddr_Class", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                return target.getCfPAddr_Class();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                    target.setCfPAddr_Class( (gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_Class) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_Class();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_Class");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfPAddr_Class
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfPAddr_GeoBBox
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_GeoBBox.class, "cfPAddr_GeoBBox", "cfPAddr_GeoBBox", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                return target.getCfPAddr_GeoBBox();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                    target.setCfPAddr_GeoBBox( (gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_GeoBBox) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_GeoBBox();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfPAddr__TypeCfPAddr_GeoBBox");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfPAddr_GeoBBox
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfEquip_PAddr
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfPAddr__TypeCfEquip_PAddr.class, "cfEquip_PAddr", "cfEquip_PAddr", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                return target.getCfEquip_PAddr();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                    target.setCfEquip_PAddr( (gr.ekt.cerif.schema.CfPAddr__TypeCfEquip_PAddr) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfPAddr__TypeCfEquip_PAddr();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfPAddr__TypeCfEquip_PAddr");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfEquip_PAddr
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFacil_PAddr
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfPAddr__TypeCfFacil_PAddr.class, "cfFacil_PAddr", "cfFacil_PAddr", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                return target.getCfFacil_PAddr();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                    target.setCfFacil_PAddr( (gr.ekt.cerif.schema.CfPAddr__TypeCfFacil_PAddr) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfPAddr__TypeCfFacil_PAddr();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfPAddr__TypeCfFacil_PAddr");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFacil_PAddr
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfSrv_PAddr
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfPAddr__TypeCfSrv_PAddr.class, "cfSrv_PAddr", "cfSrv_PAddr", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                return target.getCfSrv_PAddr();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                    target.setCfSrv_PAddr( (gr.ekt.cerif.schema.CfPAddr__TypeCfSrv_PAddr) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfPAddr__TypeCfSrv_PAddr();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfPAddr__TypeCfSrv_PAddr");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfSrv_PAddr
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFedId
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfClass__TypeCfFedId.class, "cfFedId", "cfFedId", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                return target.getCfFedId();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfPAddr__TypeChoiceItem target = (CfPAddr__TypeChoiceItem) object;
                    target.setCfFedId( (gr.ekt.cerif.schema.CfClass__TypeCfFedId) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfClass__TypeCfFedId();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfClass__TypeCfFedId");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFedId
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
    }

    /**
     * Method getAccessMode.
     * 
     * @return the access mode specified for this class.
     */
    @Override()
    public org.exolab.castor.mapping.AccessMode getAccessMode() {
        return null;
    }

    /**
     * Method getIdentity.
     * 
     * @return the identity field, null if this class has no
     * identity.
     */
    @Override()
    public org.exolab.castor.mapping.FieldDescriptor getIdentity() {
        return _identity;
    }

    /**
     * Method getJavaClass.
     * 
     * @return the Java class represented by this descriptor.
     */
    @Override()
    public java.lang.Class getJavaClass() {
        return gr.ekt.cerif.schema.CfPAddr__TypeChoiceItem.class;
    }

    /**
     * Method getNameSpacePrefix.
     * 
     * @return the namespace prefix to use when marshaling as XML.
     */
    @Override()
    public java.lang.String getNameSpacePrefix() {
        return _nsPrefix;
    }

    /**
     * Method getNameSpaceURI.
     * 
     * @return the namespace URI used when marshaling and
     * unmarshaling as XML.
     */
    @Override()
    public java.lang.String getNameSpaceURI() {
        return _nsURI;
    }

    /**
     * Method getValidator.
     * 
     * @return a specific validator for the class described by this
     * ClassDescriptor.
     */
    @Override()
    public org.exolab.castor.xml.TypeValidator getValidator() {
        return this;
    }

    /**
     * Method getXMLName.
     * 
     * @return the XML Name for the Class being described.
     */
    @Override()
    public java.lang.String getXMLName() {
        return _xmlName;
    }

    /**
     * Method isElementDefinition.
     * 
     * @return true if XML schema definition of this Class is that
     * of a global
     * element or element with anonymous type definition.
     */
    public boolean isElementDefinition() {
        return _elementDefinition;
    }

}

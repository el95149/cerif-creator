/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema.descriptors;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import gr.ekt.cerif.schema.CfResPat__TypeChoiceItem;

/**
 * 
 * 
 * @version $Revision$ $Date$
 */
public class CfResPat__TypeChoiceItemDescriptor extends org.exolab.castor.xml.util.XMLClassDescriptorImpl {

    /**
     * Field _elementDefinition.
     */
    private boolean _elementDefinition;

    /**
     * Field _nsPrefix.
     */
    private java.lang.String _nsPrefix;

    /**
     * Field _nsURI.
     */
    private java.lang.String _nsURI;

    /**
     * Field _xmlName.
     */
    private java.lang.String _xmlName;

    /**
     * Field _identity.
     */
    private org.exolab.castor.xml.XMLFieldDescriptor _identity;

    public CfResPat__TypeChoiceItemDescriptor() {
        super();
        _nsURI = "urn:xmlns:org:eurocris:cerif-1.6-2";
        _elementDefinition = false;

        //-- set grouping compositor
        setCompositorAsChoice();
        org.exolab.castor.xml.util.XMLFieldDescriptorImpl  desc           = null;
        org.exolab.castor.mapping.FieldHandler             handler        = null;
        org.exolab.castor.xml.FieldValidator               fieldValidator = null;
        //-- initialize attribute descriptors

        //-- initialize element descriptors

        //-- cfTitle
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfTitle.class, "cfTitle", "cfTitle", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfTitle();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfTitle( (gr.ekt.cerif.schema.CfTitle) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfTitle();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfTitle");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfTitle
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfAbstr
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfAbstr.class, "cfAbstr", "cfAbstr", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfAbstr();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfAbstr( (gr.ekt.cerif.schema.CfAbstr) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfAbstr();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfAbstr");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfAbstr
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfKeyw
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfKeyw.class, "cfKeyw", "cfKeyw", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfKeyw();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfKeyw( (gr.ekt.cerif.schema.CfKeyw) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfKeyw();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfKeyw");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfKeyw
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfVersInfo
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfVersInfo.class, "cfVersInfo", "cfVersInfo", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfVersInfo();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfVersInfo( (gr.ekt.cerif.schema.CfVersInfo) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfVersInfo();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfVersInfo");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfVersInfo
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfOrgUnit_ResPat
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfOrgUnit_ResPat.class, "cfOrgUnit_ResPat", "cfOrgUnit_ResPat", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfOrgUnit_ResPat();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfOrgUnit_ResPat( (gr.ekt.cerif.schema.CfResPat__TypeCfOrgUnit_ResPat) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfOrgUnit_ResPat();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfOrgUnit_ResPat");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfOrgUnit_ResPat
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfPers_ResPat
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfPers_ResPat.class, "cfPers_ResPat", "cfPers_ResPat", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfPers_ResPat();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfPers_ResPat( (gr.ekt.cerif.schema.CfResPat__TypeCfPers_ResPat) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfPers_ResPat();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfPers_ResPat");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfPers_ResPat
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfProj_ResPat
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfProj_ResPat.class, "cfProj_ResPat", "cfProj_ResPat", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfProj_ResPat();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfProj_ResPat( (gr.ekt.cerif.schema.CfResPat__TypeCfProj_ResPat) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfProj_ResPat();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfProj_ResPat");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfProj_ResPat
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResPat_Class
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Class.class, "cfResPat_Class", "cfResPat_Class", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfResPat_Class();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfResPat_Class( (gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Class) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Class();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Class");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResPat_Class
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResPat_Fund
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Fund.class, "cfResPat_Fund", "cfResPat_Fund", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfResPat_Fund();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfResPat_Fund( (gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Fund) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Fund();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Fund");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResPat_Fund
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResPubl_ResPat
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfResPubl_ResPat.class, "cfResPubl_ResPat", "cfResPubl_ResPat", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfResPubl_ResPat();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfResPubl_ResPat( (gr.ekt.cerif.schema.CfResPat__TypeCfResPubl_ResPat) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfResPubl_ResPat();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfResPubl_ResPat");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResPubl_ResPat
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResPat_ResPat
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfResPat_ResPat.class, "cfResPat_ResPat", "cfResPat_ResPat", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfResPat_ResPat();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfResPat_ResPat( (gr.ekt.cerif.schema.CfResPat__TypeCfResPat_ResPat) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfResPat_ResPat();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfResPat_ResPat");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResPat_ResPat
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResPat_Medium
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Medium.class, "cfResPat_Medium", "cfResPat_Medium", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfResPat_Medium();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfResPat_Medium( (gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Medium) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Medium();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Medium");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResPat_Medium
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResPat_Facil
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Facil.class, "cfResPat_Facil", "cfResPat_Facil", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfResPat_Facil();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfResPat_Facil( (gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Facil) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Facil();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Facil");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResPat_Facil
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResPat_Srv
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Srv.class, "cfResPat_Srv", "cfResPat_Srv", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfResPat_Srv();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfResPat_Srv( (gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Srv) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Srv();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Srv");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResPat_Srv
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResPat_Equip
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Equip.class, "cfResPat_Equip", "cfResPat_Equip", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfResPat_Equip();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfResPat_Equip( (gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Equip) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Equip();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Equip");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResPat_Equip
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResPat_Meas
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Meas.class, "cfResPat_Meas", "cfResPat_Meas", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfResPat_Meas();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfResPat_Meas( (gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Meas) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Meas();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Meas");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResPat_Meas
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResPat_Indic
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Indic.class, "cfResPat_Indic", "cfResPat_Indic", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfResPat_Indic();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfResPat_Indic( (gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Indic) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Indic();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfResPat_Indic");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResPat_Indic
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfResProd_ResPat
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfResPat__TypeCfResProd_ResPat.class, "cfResProd_ResPat", "cfResProd_ResPat", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfResProd_ResPat();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfResProd_ResPat( (gr.ekt.cerif.schema.CfResPat__TypeCfResProd_ResPat) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfResPat__TypeCfResProd_ResPat();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfResPat__TypeCfResProd_ResPat");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfResProd_ResPat
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
        //-- cfFedId
        desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(gr.ekt.cerif.schema.CfClass__TypeCfFedId.class, "cfFedId", "cfFedId", org.exolab.castor.xml.NodeType.Element);
        handler = new org.exolab.castor.xml.XMLFieldHandler() {
            @Override
            public java.lang.Object getValue( java.lang.Object object ) 
                throws IllegalStateException
            {
                CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                return target.getCfFedId();
            }
            @Override
            public void setValue( java.lang.Object object, java.lang.Object value) 
                throws IllegalStateException, IllegalArgumentException
            {
                try {
                    CfResPat__TypeChoiceItem target = (CfResPat__TypeChoiceItem) object;
                    target.setCfFedId( (gr.ekt.cerif.schema.CfClass__TypeCfFedId) value);
                } catch (java.lang.Exception ex) {
                    throw new IllegalStateException(ex.toString());
                }
            }
            @Override
            @SuppressWarnings("unused")
            public java.lang.Object newInstance(java.lang.Object parent) {
                return new gr.ekt.cerif.schema.CfClass__TypeCfFedId();
            }
        };
        desc.setSchemaType("gr.ekt.cerif.schema.CfClass__TypeCfFedId");
        desc.setHandler(handler);
        desc.setNameSpaceURI("urn:xmlns:org:eurocris:cerif-1.6-2");
        desc.setMultivalued(false);
        addFieldDescriptor(desc);
        addSequenceElement(desc);

        //-- validation code for: cfFedId
        fieldValidator = new org.exolab.castor.xml.FieldValidator();
        { //-- local scope
        }
        desc.setValidator(fieldValidator);
    }

    /**
     * Method getAccessMode.
     * 
     * @return the access mode specified for this class.
     */
    @Override()
    public org.exolab.castor.mapping.AccessMode getAccessMode() {
        return null;
    }

    /**
     * Method getIdentity.
     * 
     * @return the identity field, null if this class has no
     * identity.
     */
    @Override()
    public org.exolab.castor.mapping.FieldDescriptor getIdentity() {
        return _identity;
    }

    /**
     * Method getJavaClass.
     * 
     * @return the Java class represented by this descriptor.
     */
    @Override()
    public java.lang.Class getJavaClass() {
        return gr.ekt.cerif.schema.CfResPat__TypeChoiceItem.class;
    }

    /**
     * Method getNameSpacePrefix.
     * 
     * @return the namespace prefix to use when marshaling as XML.
     */
    @Override()
    public java.lang.String getNameSpacePrefix() {
        return _nsPrefix;
    }

    /**
     * Method getNameSpaceURI.
     * 
     * @return the namespace URI used when marshaling and
     * unmarshaling as XML.
     */
    @Override()
    public java.lang.String getNameSpaceURI() {
        return _nsURI;
    }

    /**
     * Method getValidator.
     * 
     * @return a specific validator for the class described by this
     * ClassDescriptor.
     */
    @Override()
    public org.exolab.castor.xml.TypeValidator getValidator() {
        return this;
    }

    /**
     * Method getXMLName.
     * 
     * @return the XML Name for the Class being described.
     */
    @Override()
    public java.lang.String getXMLName() {
        return _xmlName;
    }

    /**
     * Method isElementDefinition.
     * 
     * @return true if XML schema definition of this Class is that
     * of a global
     * element or element with anonymous type definition.
     */
    public boolean isElementDefinition() {
        return _elementDefinition;
    }

}

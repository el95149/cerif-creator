/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.3</a>, using an XML
 * Schema.
 * $Id$
 */

package gr.ekt.cerif.schema.types;

/**
 * Enumeration CfTrans__Type.
 * 
 * @version $Revision$ $Date$
 */
public enum CfTrans__Type {


      //------------------/
     //- Enum Constants -/
    //------------------/

    /**
     * Constant O
     */
    O("o"),
    /**
     * Constant H
     */
    H("h"),
    /**
     * Constant M
     */
    M("m");
    /**
     * Field value.
     */
    private final java.lang.String value;

    /**
     * Field enumConstants.
     */
    private static final java.util.Map<java.lang.String, CfTrans__Type> enumConstants = new java.util.HashMap<java.lang.String, CfTrans__Type>();


    static {
        for (CfTrans__Type c: CfTrans__Type.values()) {
            CfTrans__Type.enumConstants.put(c.value, c);
        }

    }

    private CfTrans__Type(final java.lang.String value) {
        this.value = value;
    }

    /**
     * Method fromValue.
     * 
     * @param value
     * @return the constant for this value
     */
    public static gr.ekt.cerif.schema.types.CfTrans__Type fromValue(final java.lang.String value) {
        CfTrans__Type c = CfTrans__Type.enumConstants.get(value);
        if (c != null) {
            return c;
        }
        throw new IllegalArgumentException(value);
    }

    /**
     * 
     * 
     * @param value
     */
    public void setValue(final java.lang.String value) {
    }

    /**
     * Method toString.
     * 
     * @return the value of this constant
     */
    public java.lang.String toString() {
        return this.value;
    }

    /**
     * Method value.
     * 
     * @return the value of this constant
     */
    public java.lang.String value() {
        return this.value;
    }

}

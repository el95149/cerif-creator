/**
 * 
 */
package gr.ekt.generator;

import java.io.IOException;

import org.exolab.castor.builder.SourceGenerator;

/**
 * @author aanagnostopoulos
 *
 */
public class SchemaGenerator {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		SourceGenerator generator = new SourceGenerator();
//		generator.setBinding("src/main/resources/binding.properties");
//		generator.getDefault().setProperty("org.exolab.castor.builder.equalsmethod", "true");
//		generator.getDefault().setProperty("org.exolab.castor.builder.javaclassmapping", "element");
		generator.getDefault().setProperty("org.exolab.castor.builder.automaticConflictResolution", "true");
		generator.setClassNameConflictResolver("xpath");
//		generator.setClassDescFieldNames(classDescFieldNames);
		generator.getDefault().setProperty("org.exolab.castor.builder.extraCollectionMethods", "false");
		generator.setDestDir("src/main/java");
		generator.setSuppressNonFatalWarnings(true);
		generator.setDescriptorCreation(true);
		generator.setGenerateImportedSchemas(false);
//		generator.setNameConflictStrategy(nameConflictStrategy);
		generator.generateSource("/home/aanagnostopoulos/workspace_luna/cerif-creator/src/main/resources/CERIF_1.6_2.xsd", "gr.ekt.cerif.schema");

	}

}

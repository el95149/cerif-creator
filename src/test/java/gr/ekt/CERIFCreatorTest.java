package gr.ekt;

import gr.ekt.cerif.creator.service.ICERIFExporter;
import gr.ekt.cerif.creator.service.ICERIFPopulator;
import gr.ekt.cerif.creator.service.ICSVConverter;
import gr.ekt.cerif.creator.service.IGenericQueryService;
import gr.ekt.cerif.creator.service.RowResult;
import gr.ekt.cerif.schema.CERIF;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.exolab.castor.types.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class CERIFCreatorTest {

	private static Log log = LogFactory.getLog(CERIFCreatorTest.class);

	@Autowired
	private IGenericQueryService genericQueryService;

	@Autowired
	private ICSVConverter icsvConverter;

	@Autowired
	private ICERIFPopulator populator;

	@Autowired
	private ICERIFExporter exporter;

	public CERIFCreatorTest() {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				"/META-INF/applicationContext.xml");
		AutowireCapableBeanFactory autowireCapableBeanFactory = applicationContext
				.getAutowireCapableBeanFactory();
		autowireCapableBeanFactory.autowireBean(this);
	}

	public void test() throws Exception {

		CERIF cerif = populator
				.populateWithCSVFile(new File(
						"/home/aanagnostopoulos/git/cerif-creator/src/main/resources/cfProj.csv"));

		cerif = populator
				.populateWithCSVFile(new File(
						"/home/aanagnostopoulos/git/cerif-creator/src/main/resources/cfPers.csv"),
						cerif);
		
		List<RowResult> results = new ArrayList<RowResult>();
		cerif = populator
				.populateNestedLinkEntities(new File(
						"/home/aanagnostopoulos/git/cerif-creator/src/main/resources/cfProj_Pers_test.csv"),
						cerif, results);

		cerif.setDate(new Date(new java.util.Date()));
		cerif.setSourceDatabase("TEST");
		log.info(exporter.exportToXMLString(cerif, false));

	}
//         478 Finance-ID                7916dbe8-9264-4810-92df-7c21ab733338


	public void testFedId() {
		log.info(genericQueryService.federatedIdentifierExistsForEntityType("id7", "cfOrgUnit", "7916dbe8-9264-4810-92df-7c21ab733338"));
	}
	
	public void testClassUUIDs() {
		log.debug(genericQueryService.findClassUUIDsByTerm("project", "6e0d9af0-1cd6-11e1-8bc2-0800200c9a66"));
	}

	public static void main(String[] args) throws Exception {

		CERIFCreatorTest creator = new CERIFCreatorTest();
//		 creator.test();
//		creator.testFedId();
		creator.testClassUUIDs();

	}

}
